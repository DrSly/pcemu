#include "main_test.h"

namespace
{

// decode.h

TEST(decode_DIV)
{

	mode32 = 1;

	// --- DIV: F6 /6 ---

	// DIV r/m8 => F6 /6 []
	// mod = 00; + r/m = 1; [ECX]
	// f6 31                   div    BYTE PTR [ecx]
	uint8_t *code = new uint8_t [2] {0xF6, 0x31};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(DIV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// DIV r/m8 => F6 /6 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// f6 35 ff 00 00 01       div    BYTE PTR ds:0x10000ff
	code = new uint8_t [6] {0xF6, 0x35, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(DIV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// DIV r/m8 => F6 /6 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// f6 75 ff                div    BYTE PTR [ebp-0x1]
	code = new uint8_t [3] {0xF6, 0x75, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(DIV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// DIV r/m8 => F6 /6 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// f6 b2 ff 00 00 01       div    BYTE PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xF6, 0xB2, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(DIV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// DIV r/m8 => F6 /6 []
	// mod = 11; + r/m = 0; register (AL)
	// f6 f0                   div    al
	code = new uint8_t [2] {0xF6, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(DIV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	delete code;

	// --- DIV: F7 /6 ---

	// DIV r/m32 => F7 /6 []
	// mod = 00; + r/m = 1; [ECX]
	// f7 31                   div    DWORD PTR [ecx]
	code = new uint8_t [2] {0xF7, 0x31};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(DIV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// DIV r/m32 => F7 /6 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// f7 35 ff 00 00 01       div    DWORD PTR ds:0x10000ff
	code = new uint8_t [6] {0xF7, 0x35, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(DIV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// DIV r/m32 => F7 /6 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// f7 75 ff                div    DWORD PTR [ebp-0x1]
	code = new uint8_t [3] {0xF7, 0x75, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(DIV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// DIV r/m32 => F7 /6 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// f7 b2 ff 00 00 01       div    DWORD PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xF7, 0xB2, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(DIV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// DIV r/m32 => F7 /6 []
	// mod = 11; + r/m = 0; register (EAX)
	// f7 f0                   div    eax
	code = new uint8_t [2] {0xF7, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(DIV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// --- IDIV: F6 /7 ---

	// IDIV r/m8 => F6 /7 []
	// mod = 00; + r/m = 1; [ECX]
	// f6 39                   idiv   BYTE PTR [ecx]
	code = new uint8_t [2] {0xF6, 0x39};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(IDIV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// IDIV r/m8 => F6 /7 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// f6 3d ff 00 00 01       idiv   BYTE PTR ds:0x10000ff
	code = new uint8_t [6] {0xF6, 0x3D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(IDIV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// IDIV r/m8 => F6 /7 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// f6 7d ff                idiv   BYTE PTR [ebp-0x1]
	code = new uint8_t [3] {0xF6, 0x7D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(IDIV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// IDIV r/m8 => F6 /7 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// f6 ba ff 00 00 01       idiv   BYTE PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xF6, 0xBA, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(IDIV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// IDIV r/m8 => F6 /7 []
	// mod = 11; + r/m = 0; register (AL)
	// f6 f8                   idiv   al
	code = new uint8_t [2] {0xF6, 0xF8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(IDIV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	delete code;

	// --- IDIV: F7 /7 ---

	// IDIV r/m32 => F7 /7 []
	// mod = 00; + r/m = 1; [ECX]
	// f7 39                   idiv   DWORD PTR [ecx]
	code = new uint8_t [2] {0xF7, 0x39};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(IDIV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// IDIV r/m32 => F7 /7 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// f7 3d ff 00 00 01       idiv   DWORD PTR ds:0x10000ff
	code = new uint8_t [6] {0xF7, 0x3D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(IDIV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// IDIV r/m32 => F7 /7 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// f7 7d ff                idiv   DWORD PTR [ebp-0x1]
	code = new uint8_t [3] {0xF7, 0x7D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(IDIV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// IDIV r/m32 => F7 /7 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// f7 ba ff 00 00 01       idiv   DWORD PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xF7, 0xBA, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(IDIV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// IDIV r/m32 => F7 /7 []
	// mod = 11; + r/m = 0; register (AEX)
	// f7 f8                   idiv   eax
	code = new uint8_t [2] {0xF7, 0xF8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(IDIV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

}

TEST(decode_MUL)
{

	mode32 = 1;

	// --- MUL: F6 /4 ---

	// MUL r/m8 => F6 /4 []
	// mod = 00; + r/m = 1; [ECX]
	// f6 21                   mul    BYTE PTR [ecx]
	uint8_t *code = new uint8_t [2] {0xF6, 0x21};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MUL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// MUL r/m8 => F6 /4 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// f6 25 ff 00 00 01       mul    BYTE PTR ds:0x10000ff
	code = new uint8_t [6] {0xF6, 0x25, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MUL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// MUL r/m8 => F6 /4 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// f6 65 ff                mul    BYTE PTR [ebp-0x1]
	code = new uint8_t [3] {0xF6, 0x65, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MUL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// MUL r/m8 => F6 /4 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// f6 a2 ff 00 00 01       mul    BYTE PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xF6, 0xA2, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MUL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// MUL r/m8 => F6 /4 []
	// mod = 11; + r/m = 0; register (AL)
	// f6 e0                   mul    al
	code = new uint8_t [2] {0xF6, 0xE0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MUL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	delete code;

	// --- MUL: F7 /4 ---

	// MUL r/m32 => F7 /4 []
	// mod = 00; + r/m = 1; [ECX]
	// f7 21                   mul    DWORD PTR [ecx]
	code = new uint8_t [2] {0xF7, 0x21};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// MUL r/m32 => F7 /4 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// f7 25 ff 00 00 01       mul    DWORD PTR ds:0x10000ff
	code = new uint8_t [6] {0xF7, 0x25, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// MUL r/m32 => F7 /4 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// f7 65 ff                mul    DWORD PTR [ebp-0x1]
	code = new uint8_t [3] {0xF7, 0x65, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// MUL r/m32 => F7 /4 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// f7 a2 ff 00 00 01       mul    DWORD PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xF7, 0xA2, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// MUL r/m32 => F7 /4 []
	// mod = 11; + r/m = 0; register (EAX)
	// f7 e0                   mul    eax
	code = new uint8_t [2] {0xF7, 0xE0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// --- IMUL: F6 /5 ---

	// IMUL r/m8 => F6 /5 []
	// mod = 00; + r/m = 1; [ECX]
	// f6 29                   imul   BYTE PTR [ecx]
	code = new uint8_t [2] {0xF6, 0x29};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// IMUL r/m8 => F6 /5 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// f6 2d ff 00 00 01       imul   BYTE PTR ds:0x10000ff
	code = new uint8_t [6] {0xF6, 0x2D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// IMUL r/m8 => F6 /5 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// f6 6d ff                imul   BYTE PTR [ebp-0x1]
	code = new uint8_t [3] {0xF6, 0x6D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// IMUL r/m8 => F6 /5 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// f6 aa ff 00 00 01       imul   BYTE PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xF6, 0xAA, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// IMUL r/m8 => F6 /5 []
	// mod = 11; + r/m = 0; register (AL)
	// f6 e8                   imul   al
	code = new uint8_t [2] {0xF6, 0xE8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	delete code;

	// --- IMUL: F7 /5 ---

	// IMUL r/m32 => F7 /5 []
	// mod = 00; + r/m = 1; [ECX]
	// f7 29                   imul   DWORD PTR [ecx]
	code = new uint8_t [2] {0xF7, 0x29};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// IMUL r/m32 => F7 /5 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// f7 2d ff 00 00 01       imul   DWORD PTR ds:0x10000ff
	code = new uint8_t [6] {0xF7, 0x2D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// IMUL r/m32 => F7 /5 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// f7 6d ff                imul   DWORD PTR [ebp-0x1]
	code = new uint8_t [3] {0xF7, 0x6D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// IMUL r/m32 => F7 /5 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// f7 aa ff 00 00 01       imul   DWORD PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xF7, 0xAA, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// IMUL r/m32 => F7 /5 []
	// mod = 11; + r/m = 0; register (EAX)
	// f7 e8                   imul   eax
	code = new uint8_t [2] {0xF7, 0xE8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// --- r/m opcodes (69, 6B) ---

	// IMUL reg32,r/m32,imm32 => 69 [mod/reg/rm] [imm32]
	// IMUL reg32,r/m32,imm8 => 6B [mod/reg/rm] [imm8]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = DB
	// mod = 11; register addressing
	// reg = 3 (EBX); r/m = 3 (EBX)

	// 69 db 0f 01 00 01       imul   ebx,ebx,0x100010f
	code = new uint8_t [6] {0x69, 0xDB, 0x0F, 0x01, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x100010f, opc.source.imm_word_data);
	delete code;

	// mod/reg/rm = C9
	// mod = 11; register addressing
	// reg = 1 (ECX); r/m = 1 (ECX)

	// 6b c9 8f                imul   ecx,ecx,0xffffff8f
	code = new uint8_t [3] {0x6B, 0xC9, 0x8F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffff8f, opc.source.imm_word_data);
	delete code;

	// mod/reg/rm = 0D
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 1 (CL)

	// 69 0d 01 00 00 00 0f 01 00 01    imul   ecx,DWORD PTR ds:0x1,0x100010f
	code = new uint8_t [10] {0x69, 0x0D, 0x01, 0x00, 0x00, 0x00, 0x0F, 0x01, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x100010f, opc.source.imm_word_data);
	delete code;

	// mod/reg/rm = 1D
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 3 (EBX)

	// 6b 1d 01 00 00 00 8f    imul   ebx,DWORD PTR ds:0x1,0xffffff8f
	code = new uint8_t [7] {0x6B, 0x1D, 0x01, 0x00, 0x00, 0x00, 0x8F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffff8f, opc.source.imm_word_data);
	delete code;

	// mod/reg/rm = 66
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 6 (ESI)

	// 69 66 01 0f 01 00 01    imul   esp,DWORD PTR [esi+0x1],0x100010f
	code = new uint8_t [7] {0x69, 0x66, 0x01, 0x0F, 0x01, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x100010f, opc.source.imm_word_data);
	delete code;

	// mod/reg/rm = 77
	// mod = 01; indirect register with one byte offset
	// reg = 6 (ESI); r/m = 7 (EDI)

	// 6b 77 01 8f             imul   esi,DWORD PTR [edi+0x1],0xffffff8f
	code = new uint8_t [4] {0x6B, 0x77, 0x01, 0x8F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffff8f, opc.source.imm_word_data);
	delete code;

	// mod/reg/rm = 9A
	// mod = 10; indirect register with four byte offset
	// reg = 3 (BL); r/m = 2 (EDX)

	// 69 9a 00 00 00 f0 0f 01 00 01    imul   ebx,DWORD PTR [edx-0x10000000],0x100010f
	code = new uint8_t [10] {0x69, 0x9A, 0x00, 0x00, 0x00, 0xF0, 0x0F, 0x01, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0xf0000000, opc.source.offset_data);
	CHECK_EQUAL(-268435456, opc.source.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x100010f, opc.source.imm_word_data);
	delete code;

	// mod/reg/rm = AE
	// mod = 10; indirect register with four byte offset
	// reg = 5 (EBP); r/m = 6 (ESI)

	// 6b ae 00 00 00 f0 8f    imul   ebp,DWORD PTR [esi-0x10000000],0xffffff8f
	code = new uint8_t [7] {0x6B, 0xAE, 0x00, 0x00, 0x00, 0xF0, 0x8F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0xf0000000, opc.source.offset_data);
	CHECK_EQUAL(-268435456, opc.source.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffff8f, opc.source.imm_word_data);
	delete code;

	// mod/reg/rm = F3
	// mod = 11; register addressing
	// reg = 6 (ESI); r/m = 3 (EBX)

	// 69 f3 0f 01 00 01       imul   esi,ebx,0x100010f
	code = new uint8_t [6] {0x69, 0xF3, 0x0F, 0x01, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x100010f, opc.source.imm_word_data);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing
	// reg = 0 (EAX); r/m = 1 (ECX)

	// 6b c1 8f                imul   eax,ecx,0xffffff8f
	code = new uint8_t [3] {0x6B, 0xC1, 0x8F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffff8f, opc.source.imm_word_data);
	delete code;

	// --- two byte opcode (0FAF) ---

	// IMUL reg32,r/m32 => 0FAF [mod/reg/rm] []
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = DA
	// mod = 11; register addressing
	// reg = 3 (EBX); r/m = 2 (EDX)

	// 0f af da                imul   ebx,edx
	code = new uint8_t [3] {0x0F, 0xAF, 0xDA};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 0D
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 1 (ECX)

	// 0f af 0d 01 00 00 00    imul   ecx,DWORD PTR ds:0x1
	code = new uint8_t [7] {0x0F, 0xAF, 0x0D, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 77
	// mod = 01; indirect register with one byte offset
	// reg = 6 (ESI); r/m = 7 (EDI)

	// 0f af 77 01             imul   esi,DWORD PTR [edi+0x1]
	code = new uint8_t [4] {0x0F, 0xAF, 0x77, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = AE
	// mod = 10; indirect register with four byte offset
	// reg = 5 (EBP); r/m = 6 (ESI)

	// 0f af ae 00 00 00 f0    imul   ebp,DWORD PTR [esi-0x10000000]
	code = new uint8_t [7] {0x0F, 0xAF, 0xAE, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0xf0000000, opc.source.offset_data);
	CHECK_EQUAL(-268435456, opc.source.offset_data);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing
	// reg = 0 (EAX); r/m = 1 (ECX)

	// 0f af c1                imul   eax,ecx
	code = new uint8_t [3] {0x0F, 0xAF, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(IMUL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

}

}
