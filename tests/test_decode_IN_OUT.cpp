#include "main_test.h"

namespace
{

// decode.h

TEST(decode_IN_OUT)
{

	mode32 = 1;

	// e4 10                   in     al,0x10
	uint8_t *code = new uint8_t [2] {0xE4, 0x10};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(IN, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x10, opc.source.imm_word_data);
	delete code;

	// e5 20                   in     eax,0x20
	code = new uint8_t [2] {0xE5, 0x20};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(IN, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x20, opc.source.imm_word_data);
	delete code;

	// e6 30                   out    0x30,al
	code = new uint8_t [2] {0xE6, 0x30};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(OUT, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x30, opc.dest.imm_word_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// e7 40                   out    0x40,eax
	code = new uint8_t [2] {0xE7, 0x40};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(OUT, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x40, opc.dest.imm_word_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// ec                      in     al,dx
	code = new uint8_t [1] {0xEC};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(IN, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// ed                      in     eax,dx
	code = new uint8_t [1] {0xED};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(IN, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// ee                      out    dx,al
	code = new uint8_t [1] {0xEE};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(OUT, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// ef                      out    dx,eax
	code = new uint8_t [1] {0xEF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(OUT, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// 6c                      ins    BYTE PTR es:[edi],dx
	code = new uint8_t [1] {0x6C};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_es, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// f3 6c                   rep ins BYTE PTR es:[edi],dx
	code = new uint8_t [2] {0xF3, 0x6C};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(REP_INS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_es, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// 6d                      ins    DWORD PTR es:[edi],dx
	code = new uint8_t [1] {0x6D};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_es, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// 67 6d                   ins    DWORD PTR es:[di],dx
	code = new uint8_t [2] {0x67, 0x6D};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_es, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// 6e                      outs   dx,BYTE PTR ds:[esi]
	code = new uint8_t [1] {0x6E};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(OUTS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// f3 6e                   rep outs dx,BYTE PTR ds:[esi]
	code = new uint8_t [2] {0xF3, 0x6E};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(REP_OUTS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// 26 6e                   outs   dx,BYTE PTR es:[esi]
	code = new uint8_t [2] {0x26, 0x6E};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(OUTS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_es, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// 6f                      outs   dx,DWORD PTR ds:[esi]
	code = new uint8_t [1] {0x6F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(OUTS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// 66 6f                   outs   dx,WORD PTR ds:[esi]
	code = new uint8_t [2] {0x66, 0x6F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(OUTS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// 2e 6f                   outs   dx,DWORD PTR cs:[esi]
	code = new uint8_t [2] {0x2e, 0x6F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(OUTS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_cs, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

}

TEST(decode_IN_OUT_16bit)
{

	mode32 = 0;

	// e4 10                   in     al,0x10
	uint8_t *code = new uint8_t [2] {0xE4, 0x10};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(IN, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x10, opc.source.imm_word_data);
	delete code;

	// e5 20                   in     ax,0x20
	code = new uint8_t [2] {0xE5, 0x20};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(IN, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x20, opc.source.imm_word_data);
	delete code;

	// e6 30                   out    0x30,al
	code = new uint8_t [2] {0xE6, 0x30};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(OUT, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x30, opc.dest.imm_word_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// e7 40                   out    0x40,ax
	code = new uint8_t [2] {0xE7, 0x40};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(OUT, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x40, opc.dest.imm_word_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// ec                      in     al,dx
	code = new uint8_t [1] {0xEC};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(IN, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// ed                      in     ax,dx
	code = new uint8_t [1] {0xED};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(IN, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// ee                      out    dx,al
	code = new uint8_t [1] {0xEE};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(OUT, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// ef                      out    dx,ax
	code = new uint8_t [1] {0xEF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(OUT, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// 6c                      ins    BYTE PTR es:[di],dx
	code = new uint8_t [1] {0x6C};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_es, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// f3 6c                   rep ins BYTE PTR es:[di],dx
	code = new uint8_t [2] {0xF3, 0x6C};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(REP_INS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_es, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// 6d                      ins    WORD PTR es:[di],dx
	code = new uint8_t [1] {0x6D};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_es, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// 67 6d                   ins    WORD PTR es:[edi],dx
	code = new uint8_t [2] {0x67, 0x6D};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_es, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// 6e                      outs   dx,BYTE PTR ds:[si]
	code = new uint8_t [1] {0x6E};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(OUTS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// f3 6e                   rep outs dx,BYTE PTR ds:[si]
	code = new uint8_t [2] {0xF3, 0x6E};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(REP_OUTS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// 26 6e                   outs   dx,BYTE PTR es:[si]
	code = new uint8_t [2] {0x26, 0x6E};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(OUTS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_es, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// 6f                      outs   dx,WORD PTR ds:[si]
	code = new uint8_t [1] {0x6F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(OUTS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// 66 6f                   outs   dx,DWORD PTR ds:[si]
	code = new uint8_t [2] {0x66, 0x6F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(OUTS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// 2e 6f                   outs   dx,WORD PTR cs:[si]
	code = new uint8_t [2] {0x2e, 0x6F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(OUTS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_cs, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

}

}
