#include "main_test.h"

namespace
{

// decode.h

TEST(decode_JCC)
{

	mode32 = 1;

	// --- JO: 70, 0F80 ---

	// JO imm   => 70 [imm8]
	// 70 01                   jo     0x3 [+1] (new address is calculated from the start of the following instruction)
	uint8_t *code = new uint8_t [2] {0x70, 0x01};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JO, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JO imm   => 70 [imm8]
	// 70 80                   jo     0xffffff82 [-128]
	code = new uint8_t [2] {0x70, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JO, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JO imm   => 0F 80 [imm32]
	// 0f 80 01 00 00 00                   jo    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x80, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JO, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JO imm   => 0F 80 [imm32]
	// 0f 80 f8 ff ff ff                   jo    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x80, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JO, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JNO: 71, 0F81 ---

	// JNO imm   => 71 [imm8]
	// 71 01                   jno    0x3 [+1]
	code = new uint8_t [2] {0x71, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNO, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNO imm   => 71 [imm8]
	// 71 80                   jno    0xffffff82 [-128]
	code = new uint8_t [2] {0x71, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNO, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JNO imm   => 0F 81 [imm32]
	// 0f 81 01 00 00 00                   jno    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x81, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JNO, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNO imm   => 0F 81 [imm32]
	// 0f 81 f8 ff ff ff                   jno    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x81, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JNO, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JC/JB: 72, 0F82 ---

	// JC imm   => 72 [imm8]
	// 72 01                   jb     0x3 [+1]
	code = new uint8_t [2] {0x72, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JC imm   => 72 [imm8]
	// 72 80                   jb     0xffffff82 [-128]
	code = new uint8_t [2] {0x72, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JC imm   => 0F 82 [imm32]
	// 0f 82 01 00 00 00                   jb    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x82, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JC imm   => 0F 82 [imm32]
	// 0f 82 f8 ff ff ff                   jb    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x82, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JNC/JAE: 73, 0F83 ---

	// JNC imm   => 73 [imm8]
	// 73 01                   jae    0x3 [+1]
	code = new uint8_t [2] {0x73, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNC imm   => 73 [imm8]
	// 73 80                   jae    0xffffff82 [-128]
	code = new uint8_t [2] {0x73, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JNC imm   => 0F 83 [imm32]
	// 0f 83 01 00 00 00                   jnc    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x83, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JNC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNC imm   => 0F 83 [imm32]
	// 0f 83 f8 ff ff ff                   jnc    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x83, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JNC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JE/JZ: 74, 0F84 ---

	// JZ imm   => 74 [imm8]
	// 74 01                   je     0x3 [+1] (new address is calculated from the start of the following instruction)
	code = new uint8_t [2] {0x74, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JZ, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JZ imm   => 74 [imm8]
	// 74 80                   je     0xffffff82 [-128]
	code = new uint8_t [2] {0x74, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JZ, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JZ imm   => 0F 84 [imm32]
	// 0f 84 01 00 00 00                   je    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x84, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JZ, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JZ imm   => 0F 84 [imm32]
	// 0f 84 f8 ff ff ff                   je    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x84, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JZ, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JNE/JNZ: 75, 0F85 ---

	// JNZ imm   => 75 [imm8]
	// 75 01                   jne    0x3 [+1]
	code = new uint8_t [2] {0x75, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNZ, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNZ imm   => 75 [imm8]
	// 75 80                   jne    0xffffff82 [-128]
	code = new uint8_t [2] {0x75, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNZ, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JNZ imm   => 0F 85 [imm32]
	// 0f 85 01 00 00 00                   jne    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x85, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JNZ, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNZ imm   => 0F 85 [imm32]
	// 0f 85 f8 ff ff ff                   jne    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x85, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JNZ, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JBE/JNA: 76, 0F86 ---

	// JNA imm   => 76 [imm8]
	// 76 01                   jbe    0x3 [+1]
	code = new uint8_t [2] {0x76, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNA, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNA imm   => 76 [imm8]
	// 76 80                   jbe    0xffffff82 [-128]
	code = new uint8_t [2] {0x76, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNA, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JNA imm   => 0F 86 [imm32]
	// 0f 86 01 00 00 00                   jbe    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x86, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JNA, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNA imm   => 0F 86 [imm32]
	// 0f 86 f8 ff ff ff                   jbe    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x86, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JNA, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JA/JNBE: 77, 0F87 ---

	// JA imm   => 77 [imm8]
	// 77 01                   ja     0x3 [+1]
	code = new uint8_t [2] {0x77, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JA, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JA imm   => 77 [imm8]
	// 77 80                   ja     0xffffff82 [-128]
	code = new uint8_t [2] {0x77, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JA, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JA imm   => 0F 87 [imm32]
	// 0f 87 01 00 00 00                   ja    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x87, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JA, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JA imm   => 0F 87 [imm32]
	// 0f 87 f8 ff ff ff                   ja    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x87, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JA, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JS: 78, 0F88 ---

	// JS imm   => 78 [imm8]
	// 78 01                   js     0x3 [+1]
	code = new uint8_t [2] {0x78, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JS imm   => 78 [imm8]
	// 78 80                   js     0xffffff82 [-128]
	code = new uint8_t [2] {0x78, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JS imm   => 0F 88 [imm32]
	// 0f 88 01 00 00 00                   js    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x88, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JS imm   => 0F 88 [imm32]
	// 0f 88 f8 ff ff ff                   js    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x88, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JNS: 79, 0F89 ---

	// JNS imm   => 79 [imm8]
	// 79 01                   jns    0x3 [+1]
	code = new uint8_t [2] {0x79, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNS imm   => 79 [imm8]
	// 79 80                   jns    0xffffff82 [-128]
	code = new uint8_t [2] {0x79, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JNS imm   => 0F 89 [imm32]
	// 0f 89 01 00 00 00                   jns    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x89, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JNS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNS imm   => 0F 89 [imm32]
	// 0f 89 f8 ff ff ff                   jns    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x89, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JNS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JP/JPE: 7A, 0F8A ---

	// JP imm   => 7A [imm8]
	// 7a 01                   jp     0x3 [+1]
	code = new uint8_t [2] {0x7A, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JP imm   => 7A [imm8]
	// 7a 80                   jp     0xffffff82 [-128]
	code = new uint8_t [2] {0x7A, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JP imm   => 0F 8A [imm32]
	// 0f 8a 01 00 00 00                   jp    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x8A, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JP imm   => 0F 8A [imm32]
	// 0f 8a f8 ff ff ff                   jp    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x8A, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JNP/JPO: 7B, 0F8B ---

	// JNP imm   => 7B [imm8]
	// 7b 01                   jp     0x3 [+1]
	code = new uint8_t [2] {0x7B, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNP imm   => 7B [imm8]
	// 7b 80                   jp     0xffffff82 [-128]
	code = new uint8_t [2] {0x7B, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JNP imm   => 0F 8B [imm32]
	// 0f 8b 01 00 00 00                   jnp    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x8B, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JNP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNP imm   => 0F 8B [imm32]
	// 0f 8b f8 ff ff ff                   jnp    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x8B, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JNP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JL/JNGE: 7C, 0F8C ---

	// JL imm   => 7C [imm8]
	// 7c 01                   jl     0x3 [+1]
	code = new uint8_t [2] {0x7C, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JL imm   => 7C [imm8]
	// 7c 80                   jl     0xffffff82 [-128]
	code = new uint8_t [2] {0x7C, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JL imm   => 0F 8C [imm32]
	// 0f 8c 01 00 00 00                   jl    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x8C, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JL imm   => 0F 8C [imm32]
	// 0f 8c f8 ff ff ff                   jl    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x8C, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JGE/JNL: 7D, 0F8D ---

	// JGE imm   => 7D [imm8]
	// 7d 01                   jge    0x3 [+1]
	code = new uint8_t [2] {0x7D, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JGE, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JGE imm   => 7D [imm8]
	// 7d 80                   jge    0xffffff82 [-128]
	code = new uint8_t [2] {0x7D, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JGE, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JGE imm   => 0F 8D [imm32]
	// 0f 8d 01 00 00 00                   jge    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x8D, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JGE, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JGE imm   => 0F 8D [imm32]
	// 0f 8d f8 ff ff ff                   jge    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x8D, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JGE, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JLE/JNG: 7E, 0F8E ---

	// JLE imm   => 7E [imm8]
	// 7e 01                   jle    0x3 [+1]
	code = new uint8_t [2] {0x7E, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JLE, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JLE imm   => 7E [imm8]
	// 7e 80                   jle    0xffffff82 [-128]
	code = new uint8_t [2] {0x7E, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JLE, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JLE imm   => 0F 8E [imm32]
	// 0f 8e 01 00 00 00                   jle    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x8E, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JLE, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JLE imm   => 0F 8E [imm32]
	// 0f 8e f8 ff ff ff                   jle    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x8E, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JLE, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JG/JNLE: 7F, 0F8F ---

	// JG imm   => 7F [imm8]
	// 7f 01                   jg     0x3 [+1]
	code = new uint8_t [2] {0x7F, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JG, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JG imm   => 7F [imm8]
	// 7f 80                   jg     0xffffff82 [-128]
	code = new uint8_t [2] {0x7F, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JG, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JG imm   => 0F 8F [imm32]
	// 0f 8f 01 00 00 00                   jg    0x7 [+1]
	code = new uint8_t [6] {0x0F, 0x8F, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JG, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JG imm   => 0F 8F [imm32]
	// 0f 8f f8 ff ff ff                   jg    0xfffffffe [-8]
	code = new uint8_t [6] {0x0F, 0x8F, 0xF8, 0xFF, 0xFF, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JG, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-8, opc.dest.imm_word_data);
	delete code;

	// --- JCXZ/JECXZ: E3 ---

	// JCXZ imm   => E3 [imm8]
	// e3 01                   jecxz  0x3 [+1]
	code = new uint8_t [2] {0xE3, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JCXZ, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JCXZ imm   => E3 [imm8]
	// e3 80                   jecxz  0xffffff82 [-128]
	code = new uint8_t [2] {0xE3, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JCXZ, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

}

TEST(decode_JCC_16bit)
{

	mode32 = 0;

	// JO imm   => 70 [imm8]
	// 70 01                   jo     0x3 [+1] (new address is calculated from the start of the following instruction)
	uint8_t *code = new uint8_t [2] {0x70, 0x01};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JO, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JO imm   => 70 [imm8]
	// 70 80                   jo     0xff82 [-128]
	code = new uint8_t [2] {0x70, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JO, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JO imm   => 0F 80 [imm16]
	// 0f 80 01 00                   jo    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x80, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JO, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JO imm   => 0F 80 [imm16]
	// 0f 80 fa ff                   jo    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x80, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JO, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JNO: 71, 0F81 ---

	// JNO imm   => 71 [imm8]
	// 71 01                   jno    0x3 [+1]
	code = new uint8_t [2] {0x71, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNO, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNO imm   => 71 [imm8]
	// 71 80                   jno    0xff82 [-128]
	code = new uint8_t [2] {0x71, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNO, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JNO imm   => 0F 81 [imm16]
	// 0f 81 01 00                   jno    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x81, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JNO, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNO imm   => 0F 81 [imm16]
	// 0f 81 fa ff                   jno    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x81, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JNO, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JC/JB: 72, 0F82 ---

	// JC imm   => 72 [imm8]
	// 72 01                   jb     0x3 [+1]
	code = new uint8_t [2] {0x72, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JC imm   => 72 [imm8]
	// 72 80                   jb     0xff82 [-128]
	code = new uint8_t [2] {0x72, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JC imm   => 0F 82 [imm16]
	// 0f 82 01 00                   jb    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x82, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JC imm   => 0F 82 [imm16]
	// 0f 82 fa ff                   jb    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x82, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JNC/JAE: 73, 0F83 ---

	// JNC imm   => 73 [imm8]
	// 73 01                   jae    0x3 [+1]
	code = new uint8_t [2] {0x73, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNC imm   => 73 [imm8]
	// 73 80                   jae    0xff82 [-128]
	code = new uint8_t [2] {0x73, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JNC imm   => 0F 83 [imm16]
	// 0f 83 01 00                   jnc    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x83, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JNC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNC imm   => 0F 83 [imm16]
	// 0f 83 fa ff                   jnc    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x83, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JNC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JE/JZ: 74, 0F84 ---

	// JZ imm   => 74 [imm8]
	// 74 01                   je     0x3 [+1] (new address is calculated from the start of the following instruction)
	code = new uint8_t [2] {0x74, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JZ, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JZ imm   => 74 [imm8]
	// 74 80                   je     0xff82 [-128]
	code = new uint8_t [2] {0x74, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JZ, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JZ imm   => 0F 84 [imm16]
	// 0f 84 01 00                   je    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x84, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JZ, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JZ imm   => 0F 84 [imm16]
	// 0f 84 fa ff                   je    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x84, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JZ, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JNE/JNZ: 75, 0F85 ---

	// JNZ imm   => 75 [imm8]
	// 75 01                   jne    0x3 [+1]
	code = new uint8_t [2] {0x75, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNZ, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNZ imm   => 75 [imm8]
	// 75 80                   jne    0xff82 [-128]
	code = new uint8_t [2] {0x75, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNZ, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JNZ imm   => 0F 85 [imm16]
	// 0f 85 01 00                   jne    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x85, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JNZ, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNZ imm   => 0F 85 [imm16]
	// 0f 85 fa ff                   jne    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x85, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JNZ, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JBE/JNA: 76, 0F86 ---

	// JNA imm   => 76 [imm8]
	// 76 01                   jbe    0x3 [+1]
	code = new uint8_t [2] {0x76, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNA, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNA imm   => 76 [imm8]
	// 76 80                   jbe    0xff82 [-128]
	code = new uint8_t [2] {0x76, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNA, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JNA imm   => 0F 86 [imm16]
	// 0f 86 01 00                   jbe    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x86, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JNA, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNA imm   => 0F 86 [imm16]
	// 0f 86 fa ff                   jbe    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x86, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JNA, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JA/JNBE: 77, 0F87 ---

	// JA imm   => 77 [imm8]
	// 77 01                   ja     0x3 [+1]
	code = new uint8_t [2] {0x77, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JA, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JA imm   => 77 [imm8]
	// 77 80                   ja     0xff82 [-128]
	code = new uint8_t [2] {0x77, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JA, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JA imm   => 0F 87 [imm16]
	// 0f 87 01 00                   ja    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x87, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JA, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JA imm   => 0F 87 [imm16]
	// 0f 87 fa ff                   ja    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x87, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JA, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JS: 78, 0F88 ---

	// JS imm   => 78 [imm8]
	// 78 01                   js     0x3 [+1]
	code = new uint8_t [2] {0x78, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JS imm   => 78 [imm8]
	// 78 80                   js     0xff82 [-128]
	code = new uint8_t [2] {0x78, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JS imm   => 0F 88 [imm16]
	// 0f 88 01 00                   js    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x88, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JS imm   => 0F 88 [imm16]
	// 0f 88 fa ff                   js    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x88, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JNS: 79, 0F89 ---

	// JNS imm   => 79 [imm8]
	// 79 01                   jns    0x3 [+1]
	code = new uint8_t [2] {0x79, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNS imm   => 79 [imm8]
	// 79 80                   jns    0xff82 [-128]
	code = new uint8_t [2] {0x79, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNS, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JNS imm   => 0F 89 [imm16]
	// 0f 89 01 00                   jns    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x89, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JNS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNS imm   => 0F 89 [imm16]
	// 0f 89 fa ff                   jns    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x89, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JNS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JP/JPE: 7A, 0F8A ---

	// JP imm   => 7A [imm8]
	// 7a 01                   jp     0x3 [+1]
	code = new uint8_t [2] {0x7A, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JP imm   => 7A [imm8]
	// 7a 80                   jp     0xff82 [-128]
	code = new uint8_t [2] {0x7A, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JP imm   => 0F 8A [imm16]
	// 0f 8a 01 00                   jp    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x8A, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JP imm   => 0F 8A [imm16]
	// 0f 8a fa ff                   jp    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x8A, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JNP/JPO: 7B, 0F8B ---

	// JNP imm   => 7B [imm8]
	// 7b 01                   jp     0x3 [+1]
	code = new uint8_t [2] {0x7B, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNP imm   => 7B [imm8]
	// 7b 80                   jp     0xff82 [-128]
	code = new uint8_t [2] {0x7B, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JNP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JNP imm   => 0F 8B [imm16]
	// 0f 8b 01 00                   jnp    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x8B, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JNP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JNP imm   => 0F 8B [imm16]
	// 0f 8b fa ff                   jnp    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x8B, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JNP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JL/JNGE: 7C, 0F8C ---

	// JL imm   => 7C [imm8]
	// 7c 01                   jl     0x3 [+1]
	code = new uint8_t [2] {0x7C, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JL imm   => 7C [imm8]
	// 7c 80                   jl     0xff82 [-128]
	code = new uint8_t [2] {0x7C, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JL imm   => 0F 8C [imm16]
	// 0f 8c 01 00                   jl    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x8C, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JL imm   => 0F 8C [imm16]
	// 0f 8c fa ff                   jl    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x8C, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JGE/JNL: 7D, 0F8D ---

	// JGE imm   => 7D [imm8]
	// 7d 01                   jge    0x3 [+1]
	code = new uint8_t [2] {0x7D, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JGE, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JGE imm   => 7D [imm8]
	// 7d 80                   jge    0xff82 [-128]
	code = new uint8_t [2] {0x7D, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JGE, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JGE imm   => 0F 8D [imm16]
	// 0f 8d 01 00                   jge    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x8D, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JGE, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JGE imm   => 0F 8D [imm16]
	// 0f 8d fa ff                   jge    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x8D, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JGE, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JLE/JNG: 7E, 0F8E ---

	// JLE imm   => 7E [imm8]
	// 7e 01                   jle    0x3 [+1]
	code = new uint8_t [2] {0x7E, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JLE, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JLE imm   => 7E [imm8]
	// 7e 80                   jle    0xff82 [-128]
	code = new uint8_t [2] {0x7E, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JLE, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JLE imm   => 0F 8E [imm16]
	// 0f 8e 01 00                   jle    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x8E, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JLE, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JLE imm   => 0F 8E [imm16]
	// 0f 8e fa ff                   jle    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x8E, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JLE, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JG/JNLE: 7F, 0F8F ---

	// JG imm   => 7F [imm8]
	// 7f 01                   jg     0x3 [+1]
	code = new uint8_t [2] {0x7F, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JG, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JG imm   => 7F [imm8]
	// 7f 80                   jg     0xff82 [-128]
	code = new uint8_t [2] {0x7F, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JG, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JG imm   => 0F 8F [imm16]
	// 0f 8f 01 00                   jg    0x5 [+1]
	code = new uint8_t [4] {0x0F, 0x8F, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JG, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JG imm   => 0F 8F [imm16]
	// 0f 8f fa ff                   jg    0xfffe [-6]
	code = new uint8_t [4] {0x0F, 0x8F, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JG, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-6, opc.dest.imm_word_data);
	delete code;

	// --- JCXZ/JECXZ: E3 ---

	// JCXZ imm   => E3 [imm8]
	// e3 01                   jecxz  0x3 [+1]
	code = new uint8_t [2] {0xE3, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JCXZ, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JCXZ imm   => E3 [imm8]
	// e3 80                   jecxz  0xff82 [-128]
	code = new uint8_t [2] {0xE3, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JCXZ, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

}

}
