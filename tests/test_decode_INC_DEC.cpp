#include "main_test.h"

namespace
{

// decode.h

TEST(decode_INC_DEC)
{

	mode32 = 1;

	// --- INC: 40, 41, 42, 43, 44, 45, 46, 47 ---

	// 40                      inc    eax
	uint8_t *code = new uint8_t [1] {0x40};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// 41                      inc    ecx
	code = new uint8_t [1] {0x41};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// 42                      inc    edx
	code = new uint8_t [1] {0x42};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// 43                      inc    ebx
	code = new uint8_t [1] {0x43};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// 44                      inc    esp
	code = new uint8_t [1] {0x44};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// 45                      inc    ebp
	code = new uint8_t [1] {0x45};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// 46                      inc    esi
	code = new uint8_t [1] {0x46};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// 47                      inc    edi
	code = new uint8_t [1] {0x47};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// --- INC: FE /0 ---

	// INC r/m8 => FE /0 []
	// mod = 00; + r/m = 1; [ECX]
	// fe 01                   inc    BYTE PTR [ecx]
	code = new uint8_t [2] {0xFE, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// INC r/m8 => FE /0 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// fe 05 ff 00 00 01       inc    BYTE PTR ds:0x10000ff
	code = new uint8_t [6] {0xFE, 0x05, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// INC r/m8 => FE /0 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// fe 45 ff                inc    BYTE PTR [ebp-0x1]
	code = new uint8_t [3] {0xFE, 0x45, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// INC r/m8 => FE /0 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// fe 82 ff 00 00 01       inc    BYTE PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xFE, 0x82, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// INC r/m8 => FE /0 []
	// mod = 11; + r/m = 0; register (AL)
	// fe c0                   inc    al
	code = new uint8_t [2] {0xFE, 0xC0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	delete code;

	// --- INC: FF /0 ---

	// INC r/m32 => FF /0 []
	// mod = 00; + r/m = 1; [ECX]
	// ff 01                   inc    DWORD PTR [ecx]
	code = new uint8_t [2] {0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// INC r/m32 => FF /0 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// ff 05 ff 00 00 01       inc    DWORD PTR ds:0x10000ff
	code = new uint8_t [6] {0xFF, 0x05, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// INC r/m32 => FF /0 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// ff 45 ff                inc    DWORD PTR [ebp-0x1]
	code = new uint8_t [3] {0xFF, 0x45, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// INC r/m32 => FF /0 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// ff 82 ff 00 00 01       inc    DWORD PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xFF, 0x82, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// INC r/m32 => FF /0 []
	// mod = 11; + r/m = 0; register (EAX)
	// ff c0                   inc    eax
	code = new uint8_t [2] {0xFF, 0xC0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// --- DEC: 48, 49, 4A, 4B, 4C, 4D, 4E, 4F ---

	// 48                      dec    eax
	code = new uint8_t [1] {0x48};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// 49                      dec    ecx
	code = new uint8_t [1] {0x49};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// 4a                      dec    edx
	code = new uint8_t [1] {0x4A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// 4b                      dec    ebx
	code = new uint8_t [1] {0x4B};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// 4c                      dec    esp
	code = new uint8_t [1] {0x4C};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// 4d                      dec    ebp
	code = new uint8_t [1] {0x4D};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// 4e                      dec    esi
	code = new uint8_t [1] {0x4E};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// 4f                      dec    edi
	code = new uint8_t [1] {0x4F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// --- DEC: FE /1 ---

	// DEC r/m8 => FE /1 []
	// mod = 00; + r/m = 1; [ECX]
	// fe 09                   dec    BYTE PTR [ecx]
	code = new uint8_t [2] {0xFE, 0x09};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// DEC r/m8 => FE /1 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// fe 0d ff 00 00 01       dec    BYTE PTR ds:0x10000ff
	code = new uint8_t [6] {0xFE, 0x0D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// DEC r/m8 => FE /1 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// fe 4d ff                dec    BYTE PTR [ebp-0x1]
	code = new uint8_t [3] {0xFE, 0x4D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// DEC r/m8 => FE /1 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// fe 8a ff 00 00 01       dec    BYTE PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xFE, 0x8A, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// DEC r/m8 => FE /1 []
	// mod = 11; + r/m = 0; register (AL)
	// fe c8                   dec    al
	code = new uint8_t [2] {0xFE, 0xC8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	delete code;

	// --- DEC: FF /1 ---

	// DEC r/m32 => FF /1 []
	// mod = 00; + r/m = 1; [ECX]
	// ff 09                   dec    DWORD PTR [ecx]
	code = new uint8_t [2] {0xFF, 0x09};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// DEC r/m32 => FF /1 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// ff 0d ff 00 00 01       dec    DWORD PTR ds:0x10000ff
	code = new uint8_t [6] {0xFF, 0x0D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// DEC r/m32 => FF /1 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// ff 4d ff                dec    DWORD PTR [ebp-0x1]
	code = new uint8_t [3] {0xFF, 0x4D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// DEC r/m32 => FF /1 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// ff 8a ff 00 00 01       dec    DWORD PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xFF, 0x8A, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// DEC r/m32 => FF /1 []
	// mod = 11; + r/m = 0; register (AEX)
	// ff c8                   dec    eax
	code = new uint8_t [2] {0xFF, 0xC8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

}

TEST(decode_INC_DEC_16bit)
{

	mode32 = 0;

	// --- INC: 40, 41, 42, 43, 44, 45, 46, 47 ---

	// 40                      inc    ax
	uint8_t *code = new uint8_t [1] {0x40};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// 41                      inc    cx
	code = new uint8_t [1] {0x41};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// 42                      inc    dx
	code = new uint8_t [1] {0x42};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// 43                      inc    bx
	code = new uint8_t [1] {0x43};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// 44                      inc    sp
	code = new uint8_t [1] {0x44};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// 45                      inc    bp
	code = new uint8_t [1] {0x45};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// 46                      inc    si
	code = new uint8_t [1] {0x46};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// 47                      inc    di
	code = new uint8_t [1] {0x47};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// --- INC: FE /0 ---

	// INC r/m8 => FE /0 []
	// mod = 00; + r/m = 1; [BX+DI]
	// fe 01                   inc    BYTE PTR [bx+di]
	code = new uint8_t [2] {0xFE, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// INC r/m8 => FE /0 []
	// mod = 00; + r/m = 6; 16 bit displacement only
	// fe 06 ff 01       inc    BYTE PTR ds:0x1ff
	code = new uint8_t [4] {0xFE, 0x06, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	delete code;

	// INC r/m8 => FE /0 []
	// mod = 01; + r/m = 5; [DI+off8]
	// fe 45 ff                inc    BYTE PTR [di-0x1]
	code = new uint8_t [3] {0xFE, 0x45, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// INC r/m8 => FE /0 []
	// mod = 10; + r/m = 2; [BP+SI+off16]
	// fe 82 ff 01       inc    BYTE PTR [bp+si+0x1ff]
	code = new uint8_t [4] {0xFE, 0x82, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	delete code;

	// INC r/m8 => FE /0 []
	// mod = 11; + r/m = 0; register (AL)
	// fe c0                   inc    al
	code = new uint8_t [2] {0xFE, 0xC0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	delete code;

	// --- INC: FF /0 ---

	// INC r/m16 => FF /0 []
	// mod = 00; + r/m = 1; [BX+DI]
	// ff 01                   inc    WORD PTR [bx+di]
	code = new uint8_t [2] {0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// INC r/m16 => FF /0 []
	// mod = 00; + r/m = 6; 16 bit displacement only
	// ff 06 ff 01       inc    WORD PTR ds:0x1ff
	code = new uint8_t [4] {0xFF, 0x06, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	delete code;

	// INC r/m16 => FF /0 []
	// mod = 01; + r/m = 5; [DI+off8]
	// ff 45 ff                inc    WORD PTR [di-0x1]
	code = new uint8_t [3] {0xFF, 0x45, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// INC r/m16 => FF /0 []
	// mod = 10; + r/m = 2; [BP+SI+off16]
	// ff 82 ff 01       inc    WORD PTR [bp+si+0x1ff]
	code = new uint8_t [4] {0xFF, 0x82, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	delete code;

	// INC r/m16 => FF /0 []
	// mod = 11; + r/m = 0; register (AX)
	// ff c0                   inc    ax
	code = new uint8_t [2] {0xFF, 0xC0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// --- DEC: 48, 49, 4A, 4B, 4C, 4D, 4E, 4F ---

	// 48                      dec    ax
	code = new uint8_t [1] {0x48};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// 49                      dec    cx
	code = new uint8_t [1] {0x49};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// 4a                      dec    dx
	code = new uint8_t [1] {0x4A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// 4b                      dec    bx
	code = new uint8_t [1] {0x4B};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// 4c                      dec    sp
	code = new uint8_t [1] {0x4C};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// 4d                      dec    bp
	code = new uint8_t [1] {0x4D};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// 4e                      dec    si
	code = new uint8_t [1] {0x4E};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// 4f                      dec    di
	code = new uint8_t [1] {0x4F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// --- DEC: FE /1 ---

	// DEC r/m8 => FE /1 []
	// mod = 00; + r/m = 1; [BX+DI]
	// fe 09                   dec    BYTE PTR [bx+di]
	code = new uint8_t [2] {0xFE, 0x09};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// DEC r/m8 => FE /1 []
	// mod = 00; + r/m = 6; 16 bit displacement only
	// fe 0e ff 01       dec    BYTE PTR ds:0x1ff
	code = new uint8_t [4] {0xFE, 0x0E, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	delete code;

	// DEC r/m8 => FE /1 []
	// mod = 01; + r/m = 5; [DI+off8]
	// fe 4d ff                dec    BYTE PTR [di-0x1]
	code = new uint8_t [3] {0xFE, 0x4D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// DEC r/m8 => FE /1 []
	// mod = 10; + r/m = 2; [BP+SI+off16]
	// fe 8a ff 01       dec    BYTE PTR [bp+si+0x1ff]
	code = new uint8_t [4] {0xFE, 0x8A, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	delete code;

	// DEC r/m8 => FE /1 []
	// mod = 11; + r/m = 0; register (AL)
	// fe c8                   dec    al
	code = new uint8_t [2] {0xFE, 0xC8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	delete code;

	// --- DEC: FF /1 ---

	// DEC r/m16 => FF /1 []
	// mod = 00; + r/m = 1; [BX+DI]
	// ff 09                   dec    WORD PTR [bx+di]
	code = new uint8_t [2] {0xFF, 0x09};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// DEC r/m16 => FF /1 []
	// mod = 00; + r/m = 6; 16 bit displacement only
	// ff 0e ff 01       dec    WORD PTR ds:0x1ff
	code = new uint8_t [4] {0xFF, 0x0E, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	delete code;

	// DEC r/m16 => FF /1 []
	// mod = 01; + r/m = 5; [DI+off8]
	// ff 4d ff                dec    WORD PTR [di-0x1]
	code = new uint8_t [3] {0xFF, 0x4D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// DEC r/m16 => FF /1 []
	// mod = 10; + r/m = 2; [BP+SI+off16]
	// ff 8a ff 01       dec    WORD PTR [bp+si+0x1ff]
	code = new uint8_t [4] {0xFF, 0x8A, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	delete code;

	// DEC r/m16 => FF /1 []
	// mod = 11; + r/m = 0; register (AX)
	// ff c8                   dec    ax
	code = new uint8_t [2] {0xFF, 0xC8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(DEC, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

}

}
