#include "main_test.h"

namespace
{

// decode.h

TEST(decode_OTHER)
{

	mode32 = 1;

	// SAHF, LAHF

	// 9e                      sahf
	uint8_t *code = new uint8_t [1] {0x9E};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(SAHF, opc.op);
	CHECK_EQUAL(NIL, opc.datasize);
	delete code;

	// 9f                      lahf
	code = new uint8_t [1] {0x9F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(LAHF, opc.op);
	CHECK_EQUAL(NIL, opc.datasize);
	delete code;

	// CBW, CWDE

	// 66 98                   cbw
	code = new uint8_t [2] {0x66, 0x98};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CBW, opc.op);
	CHECK_EQUAL(NIL, opc.datasize);
	delete code;

	// 98                      cwde
	code = new uint8_t [1] {0x98};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(CWDE, opc.op);
	CHECK_EQUAL(NIL, opc.datasize);
	delete code;

	// CWD, CDQ

	// 66 99                   cwd
	code = new uint8_t [2] {0x66, 0x99};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CWD, opc.op);
	CHECK_EQUAL(NIL, opc.datasize);
	delete code;

	// 99                      cdq
	code = new uint8_t [1] {0x99};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(CDQ, opc.op);
	CHECK_EQUAL(NIL, opc.datasize);
	delete code;

	// --- LOOP/LOOPE/LOOPNE: E2 E1 E0 ---

	// LOOPNE imm   => E0 [imm8]
	// e0 01                   loopne  0x3 [+1]
	code = new uint8_t [2] {0xE0, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LOOPNE, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// LOOPNE imm   => E0 [imm8]
	// e0 80                   loopne  0xff82 [-128]
	code = new uint8_t [2] {0xE0, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LOOPNE, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// LOOPE imm   => E1 [imm8]
	// e1 01                   loope  0x3 [+1]
	code = new uint8_t [2] {0xE1, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LOOPE, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// LOOPE imm   => E1 [imm8]
	// e1 80                   loope  0xff82 [-128]
	code = new uint8_t [2] {0xE1, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LOOPE, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// LOOP imm   => E2 [imm8]
	// e2 01                   loop  0x3 [+1]
	code = new uint8_t [2] {0xE2, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LOOP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// LOOP imm   => E2 [imm8]
	// e2 80                   loop  0xff82 [-128]
	code = new uint8_t [2] {0xE2, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LOOP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

}

}
