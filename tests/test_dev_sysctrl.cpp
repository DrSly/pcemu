#include "main_test.h"

namespace
{

// dev_sysctrl.h

TEST(dev_sysctrl)
{

	// initialise cpu
	init_cpu();

	// initialise debug output
	CHECK_EQUAL(0, init_sysctrl());

	// check ports are available
	CHECK(write_to_port[0x92]);
	CHECK(read_from_port[0x92]);

	CHECK_EQUAL(0x00, read_from_port[0x92](0x92, BYTE));
	write_to_port[0x92](0x92, 0x02, BYTE);
	CHECK_EQUAL(0x02, read_from_port[0x92](0x92, BYTE));

	// shutdown
	shutdown_sysctrl();

}

}
