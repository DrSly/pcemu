#include "main_test.h"

namespace
{

// dev_debug.h

TEST(dev_debug)
{

	// initialise cpu
	init_cpu();

	// initialise debug output
	CHECK_EQUAL(0, init_debug());

	// check ports are available
	CHECK(write_to_port[0x402]);

	// output sent to port 402 will be written to debug.log
	write_to_port[0x402](0x402, 'T', BYTE);
	write_to_port[0x402](0x402, 'E', BYTE);
	write_to_port[0x402](0x402, 'S', BYTE);
	write_to_port[0x402](0x402, 'T', BYTE);
	// note that due to the file being opened in text mode
	// outputting 0a actualy results in 0d 0a written to the file!
	write_to_port[0x402](0x402, 0x0a, BYTE);

	// shutdown
	shutdown_debug();

}

}
