#ifndef _MAINTEST_H_
#define _MAINTEST_H_

#include "UnitTest++.h"
#include <string.h>		// for memset()
#include "x86_types.h"
#include "memory.h"
#include "decode.h"
#include "cpu.h"
#include "dev_debug.h"
#include "dev_sysctrl.h"
#include "dev_8259.h"
#include "dev_ps2_keyboard.h"
#include "dev_ps2_mouse.h"
#include "dev_8042.h"
#include "dev_8237A.h"
#include "dev_8253.h"
#include "dev_16550.h"
#include "dev_parallel.h"

namespace
{

	// 'C' globals
	// 32 bit registers
	extern "C" union register32 reg_eax, reg_ebx, reg_ecx, reg_edx, reg_esp, reg_esi, reg_ebp, reg_edi;
	extern "C" int32_t reg_cs, reg_ds, reg_es, reg_ss, reg_fs, reg_gs;
	extern "C" int32_t reg_ip;
	extern "C" int32_t flags;
	extern "C" int mode32;
	extern "C" void * mem_ptr[MEM_SEG_COUNT];
	// io ports
	extern "C" int (*read_from_port[0x10000])(int port, enum datasize size);
	extern "C" void (*write_to_port[0x10000])(int port, int data, enum datasize size);

	extern "C" char *opcode_names[];
	extern "C" char *byte_register_names[];
	extern "C" char *word_register_names[];
	extern "C" char *dword_register_names[];
	extern "C" char *indirect_register_16_names[];

	// PIC interrupt request lines
	extern "C" int irq_request[16];

	// PIT output signal
	extern "C" int pit_output[3];
	// PIT gate input
	extern "C" int gate_input[3];

}

#endif
