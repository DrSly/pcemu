#include "main_test.h"

namespace
{

// decode.h

TEST(decode_MOV)
{

	mode32 = 1;

	// --- a random example ---

	uint8_t *code = new uint8_t [5] {0xB8, 0x3C, 0x0, 0x0, 0x0};	// b8 3c 00 00 00 = MOV EAX, 3c
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x003c, opc.source.imm_word_data);
	delete code;

	// --- various opcodes (A0, A1, A2, A3, B0, B1, B2, B3, B4, B5, B6, B7, B8, B9, BA, BB, BC, BD, BE, BF) ---

	// mov al, rmb => A0 [mem16/mem32]
	// unfortunately initialising this way requires -std=c++11
	code = new uint8_t [5] {0xA0, 0x2, 0x3, 0x0, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x1000302, opc.source.disponly_data);
	delete code;

	// mov eax, rmw => A1 [mem16/mem32]
	code = new uint8_t [5] {0xA1, 0x2, 0x3, 0x0, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x1000302, opc.source.disponly_data);
	delete code;

	// mov rmb, al => A2 [mem16/mem32]
	code = new uint8_t [5] {0xA2, 0x2, 0x3, 0x0, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1000302, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mov rmw, eax => A3 [mem16/mem32]
	code = new uint8_t [5] {0xA3, 0x2, 0x3, 0x0, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1000302, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mov al, imm8 => B0 imm8
	code = new uint8_t [2] {0xB0, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov ah, imm8 => B4 imm8
	code = new uint8_t [2] {0xB4, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov eax, imm32 => B8 imm32
	code = new uint8_t [5] {0xB8, 0x40, 0x30, 0x32, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x1323040, opc.source.imm_word_data);
	delete code;

	// mov cl, imm8 => B1 imm8
	code = new uint8_t [2] {0xB1, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov ch, imm8 => B5 imm8
	code = new uint8_t [2] {0xB5, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov ecx, imm32 => B9 imm32
	code = new uint8_t [5] {0xB9, 0x40, 0x30, 0x32, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x1323040, opc.source.imm_word_data);
	delete code;

	// mov dl, imm8 => B2 imm8
	code = new uint8_t [2] {0xB2, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov dh, imm8 => B6 imm8
	code = new uint8_t [2] {0xB6, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov edx, imm32 => BA imm32
	code = new uint8_t [5] {0xBA, 0x40, 0x30, 0x32, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x1323040, opc.source.imm_word_data);
	delete code;

	// mov bl, imm8 => B3 imm8
	code = new uint8_t [2] {0xB3, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov bh, imm8 => B7 imm8
	code = new uint8_t [2] {0xB7, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov ebx, imm32 => BB imm32
	code = new uint8_t [5] {0xBB, 0x40, 0x30, 0x32, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x1323040, opc.source.imm_word_data);
	delete code;

	// mov esp, imm32 => BC imm32
	code = new uint8_t [5] {0xBC, 0x02, 0xF1, 0xF0, 0x7};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x7f0f102, opc.source.imm_word_data);
	delete code;

	// mov ebp, imm32 => BD imm32
	code = new uint8_t [5] {0xBD, 0x02, 0xF1, 0xF0, 0x7};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x7f0f102, opc.source.imm_word_data);
	delete code;

	// mov esi, imm32 => BE imm32
	code = new uint8_t [5] {0xBE, 0x02, 0xF1, 0xF0, 0x7};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x7f0f102, opc.source.imm_word_data);
	delete code;

	// mov edi, imm32 => BF imm32
	code = new uint8_t [5] {0xBF, 0x02, 0xF1, 0xF0, 0x7};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x7f0f102, opc.source.imm_word_data);
	delete code;

	// --- r/m opcodes (88, 89, 8A, 8B) ---

	// mov m/reg8, m/reg8 => 88 [mod/reg/rm] [imm8/imm32]
	// mov m/reg8, m/reg8 => 8A [mod/reg/rm] [imm8/imm32]
	// mov m/reg32, m/reg32 => 89 [mod/reg/rm] [imm8/imm32]
	// mov m/reg32, m/reg32 => 8B [mod/reg/rm] [imm8/imm32]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = 05
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 0 (AL)

	// 88 05 01 00 00 00       mov    BYTE PTR ds:0x1,al
	code = new uint8_t [6] {0x88, 0x05, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 0D
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 1 (CL)

	// 8a 0d 01 00 00 00       mov    cl,BYTE PTR ds:0x1
	code = new uint8_t [6] {0x8A, 0x0D, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 15
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 2 (EDX)

	// 89 15 01 00 00 00       mov    DWORD PTR ds:0x1,edx
	code = new uint8_t [6] {0x89, 0x15, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 1D
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 3 (EBX)

	// 8b 1d 01 00 00 00       mov    ebx,DWORD PTR ds:0x1
	code = new uint8_t [6] {0x8B, 0x1D, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 65
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 5 (EBP)

	// 88 65 01                mov    BYTE PTR [ebp+0x1],ah
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [3] {0x88, 0x65, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 66
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 6 (ESI)

	// 8a 66 01                mov    ah,BYTE PTR [esi+0x1]
	code = new uint8_t [3] {0x8A, 0x66, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 6F
	// mod = 01; indirect register with one byte offset
	// reg = 5 (EBP); r/m = 7 (EDI)

	// 89 6f 01                mov    DWORD PTR [edi+0x1],ebp
	code = new uint8_t [3] {0x89, 0x6F, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 77
	// mod = 01; indirect register with one byte offset
	// reg = 6 (ESI); r/m = 7 (EDI)

	// 8b 77 01                mov    esi,DWORD PTR [edi+0x1]
	code = new uint8_t [3] {0x8B, 0x77, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 91
	// mod = 10; indirect register with four byte offset
	// reg = 2 (DL); r/m = 1 (ECX)

	// 88 91 00 00 00 f0       mov    BYTE PTR [ecx-0x10000000],dl
	// todo: note how the offset is signed, check it works!!
	code = new uint8_t [6] {0x88, 0x91, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0xf0000000, opc.dest.offset_data);
	CHECK_EQUAL(-268435456, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 9A
	// mod = 10; indirect register with four byte offset
	// reg = 3 (BL); r/m = 2 (EDX)

	// 8a 9a 00 00 00 f0       mov    bl,BYTE PTR [edx-0x10000000]
	code = new uint8_t [6] {0x8A, 0x9A, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0xf0000000, opc.source.offset_data);
	CHECK_EQUAL(-268435456, opc.source.offset_data);
	delete code;

	// mod/reg/rm = A3
	// mod = 10; indirect register with four byte offset
	// reg = 4 (ESP); r/m = 3 (EBX)

	// 89 a3 00 00 00 f0       mov    DWORD PTR [ebx-0x10000000],esp
	code = new uint8_t [6] {0x89, 0xA3, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0xf0000000, opc.dest.offset_data);
	CHECK_EQUAL(-268435456, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = AE
	// mod = 10; indirect register with four byte offset
	// reg = 5 (EBP); r/m = 6 (ESI)

	// 8b ae 00 00 00 f0       mov    ebp,DWORD PTR [esi-0x10000000]
	code = new uint8_t [6] {0x8B, 0xAE, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0xf0000000, opc.source.offset_data);
	CHECK_EQUAL(-268435456, opc.source.offset_data);
	delete code;

	// mod/reg/rm = EC
	// mod = 11; register addressing
	// reg = 5 (CH); r/m = 4 (AH)

	// 88 ec                   mov    ah,ch
	code = new uint8_t [2] {0x88, 0xEC};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = F3
	// mod = 11; register addressing
	// reg = 6 (DH); r/m = 3 (BL)

	// 8a f3                   mov    dh,bl
	code = new uint8_t [2] {0x8A, 0xF3};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = FA
	// mod = 11; register addressing
	// reg = 7 (EDI); r/m = 2 (EDX)

	// 89 fa                   mov    edx,edi
	code = new uint8_t [2] {0x89, 0xFA};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing
	// reg = 0 (EAX); r/m = 1 (ECX)

	// 8b c1                   mov    eax,ecx
	code = new uint8_t [2] {0x8B, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// --- r/m opcodes (C6 /0, C7 /0) ---

	// mov m/reg8, imm8 => C6 /0 [] [imm8]
	// mod = 00; + r/m = 0; [EAX]
	// c6 00 ff                mov    BYTE PTR [eax],0xff
	code = new uint8_t [3] {0xC6, 0x00, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mov m/reg8, imm8 => C6 /0 [] [imm8]
	// mod = 00; + r/m = 5; 32 bit displacement only
	// c6 05 ff 00 00 01 0a    mov    BYTE PTR ds:0x10000ff,0xa
	code = new uint8_t [7] {0xC6, 0x05, 0xFF, 0x00, 0x00, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mov m/reg8, imm8 => C6 /0 [] [imm8]
	// mod = 01; + r/m = 1; [ECX+off8]
	// c6 41 ff 02             mov    BYTE PTR [ecx-0x1],0x2
	code = new uint8_t [4] {0xC6, 0x41, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mov m/reg8, imm8 => C6 /0 [] [imm8]
	// mod = 10; + r/m = 2; [EDX+off32]
	// c6 82 ff 00 00 01 0a    mov    BYTE PTR [edx+0x10000ff],0xa
	code = new uint8_t [7] {0xC6, 0x82, 0xFF, 0x00, 0x00, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mov m/reg8, imm8 => C6 /0 [] [imm8]
	// mod = 11; + r/m = 2; register (DL)
	// c6 c2 ff                mov    dl,0xff
	code = new uint8_t [3] {0xC6, 0xC2, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mov m/reg32, imm32 => C7 /0 [] [imm32]
	// mod = 00; + r/m = 2; [EDX]
	// c7 02 ff 00 00 01       mov    DWORD PTR [edx],0x10000ff
	code = new uint8_t [6] {0xC7, 0x02, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x10000ff, opc.source.imm_word_data);
	delete code;

	// mov m/reg32, imm32 => C7 /0 [] [imm32]
	// mod = 00; + r/m = 5; 32 bit displacement only
	// c7 05 ff 00 00 01 0a 00 00 0f    mov    DWORD PTR ds:0x10000ff,0xf00000a
	code = new uint8_t [10] {0xC7, 0x05, 0xFF, 0x00, 0x00, 0x01, 0x0A, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf00000a, opc.source.imm_word_data);
	delete code;

	// mov m/reg32, imm32 => C7 /0 [] [imm32]
	// mod = 01; + r/m = 3; [EBX+off8]
	// c7 43 ff 02 00 00 0f    mov    DWORD PTR [ebx-0x1],0xf000002
	code = new uint8_t [7] {0xC7, 0x43, 0xFF, 0x02, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf000002, opc.source.imm_word_data);
	delete code;

	// mov m/reg32, imm32 => C7 /0 [] [imm32]
	// mod = 10; + r/m = 5; [EBP+off32]
	// c7 85 ff 00 00 01 0a 00 00 0f    mov    DWORD PTR [ebp+0x10000ff],0xf00000a
	code = new uint8_t [10] {0xC7, 0x85, 0xFF, 0x00, 0x00, 0x01, 0x0A, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf00000a, opc.source.imm_word_data);
	delete code;

	// mov m/reg32, imm32 => C7 /0 [] [imm32]
	// mod = 11; + r/m = 6; register (ESI)
	// c7 c6 ff 00 00 01       mov    esi,0x10000ff
	code = new uint8_t [6] {0xC7, 0xC6, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x10000ff, opc.source.imm_word_data);
	delete code;

	// --- segment register opcodes (8C, 8E) ---

	// mov m/reg32, m/reg32 => 8C [mod/reg/rm] []
	// mod = 00; + r/m = 2; [EDX]
	// 8c 02                   mov    WORD PTR [edx],es
	code = new uint8_t [2] {0x8C, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(seg_es, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mov m/reg32, m/reg32 => 8E [mod/reg/rm] []
	// 8e 02                   mov    es,WORD PTR [edx]
	code = new uint8_t [2] {0x8E, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(seg_es, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mov m/reg32, m/reg32 => 8C [mod/reg/rm] []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// 8c 05 ff 00 00 01       mov    WORD PTR ds:0x10000ff,es
	code = new uint8_t [6] {0x8C, 0x05, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(seg_es, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mov m/reg32, m/reg32 => 8E [mod/reg/rm] []
	// 8e 05 ff 00 00 01       mov    es,WORD PTR ds:0x10000ff
	code = new uint8_t [6] {0x8E, 0x05, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(seg_es, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x10000ff, opc.source.disponly_data);
	delete code;

	// mov m/reg32, m/reg32 => 8C [mod/reg/rm] []
	// mod = 01; + r/m = 3; [EBX+off8]
	// 8c 43 ff                mov    WORD PTR [ebx-0x1],es
	code = new uint8_t [3] {0x8C, 0x43, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(seg_es, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mov m/reg32, m/reg32 => 8E [mod/reg/rm] []
	// 8e 43 ff                mov    es,WORD PTR [ebx-0x1]
	code = new uint8_t [3] {0x8E, 0x43, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(seg_es, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-1, opc.source.offset_data);
	delete code;

	// mov m/reg32, m/reg32 => 8C [mod/reg/rm] []
	// mod = 10; + r/m = 5; [EBP+off32]
	// 8c 85 ff 00 00 01       mov    WORD PTR [ebp+0x10000ff],es
	code = new uint8_t [6] {0x8C, 0x85, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(seg_es, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mov m/reg32, m/reg32 => 8E [mod/reg/rm] []
	// 8e 85 ff 00 00 01       mov    es,WORD PTR [ebp+0x10000ff]
	code = new uint8_t [6] {0x8E, 0x85, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(seg_es, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x10000ff, opc.source.offset_data);
	delete code;

	// mov m/reg32, m/reg32 => 8C [mod/reg/rm] []
	// mod = 11; + r/m = 0; register (EAX)
	// 8c c0                   mov    eax,es
	code = new uint8_t [2] {0x8C, 0xC0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// register is actually a 32 bit register, however the top two bytes of the result are undefined
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(seg_es, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mov m/reg32, m/reg32 => 8E [mod/reg/rm] []
	// 8e c1                   mov    es,ecx
	code = new uint8_t [2] {0x8E, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(seg_es, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// register is actually a 32 bit register, however the top two bytes are ignored
	delete code;

	// mov m/reg32, m/reg32 => 8E [mod/reg/rm] []
	// 8e c6                   mov    es,esi	<=	*** funky ***
	code = new uint8_t [2] {0x8E, 0xC6};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(seg_es, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

}

TEST(decode_MOV_16bit)
{

	mode32 = 0;

	// --- various opcodes (A0, A1, A2, A3, B0, B1, B2, B3, B4, B5, B6, B7, B8, B9, BA, BB, BC, BD, BE, BF) ---

	// mov al, rmb => A0 [mem16/mem32]
	uint8_t *code = new uint8_t [3] {0xA0, 0x0, 0x1};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x100, opc.source.disponly_data);
	delete code;

	// mov ax, rmw => A1 [mem16/mem32]
	code = new uint8_t [3] {0xA1, 0x0, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x100, opc.source.disponly_data);
	delete code;

	// mov rmb, al => A2 [mem16/mem32]
	code = new uint8_t [3] {0xA2, 0x0, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x100, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mov rmw, ax => A3 [mem16/mem32]
	code = new uint8_t [3] {0xA3, 0x0, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x100, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mov al, imm8 => B0 imm8
	code = new uint8_t [2] {0xB0, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov ah, imm8 => B4 imm8
	code = new uint8_t [2] {0xB4, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov ax, imm16 => B8 imm16
	code = new uint8_t [3] {0xB8, 0x32, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x132, opc.source.imm_word_data);
	delete code;

	// mov cl, imm8 => B1 imm8
	code = new uint8_t [2] {0xB1, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov ch, imm8 => B5 imm8
	code = new uint8_t [2] {0xB5, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov cx, imm16 => B9 imm16
	code = new uint8_t [3] {0xB9, 0x32, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x132, opc.source.imm_word_data);
	delete code;

	// mov dl, imm8 => B2 imm8
	code = new uint8_t [2] {0xB2, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov dh, imm8 => B6 imm8
	code = new uint8_t [2] {0xB6, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov dx, imm16 => BA imm16
	code = new uint8_t [3] {0xBA, 0x32, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x132, opc.source.imm_word_data);
	delete code;

	// mov bl, imm8 => B3 imm8
	code = new uint8_t [2] {0xB3, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov bh, imm8 => B7 imm8
	code = new uint8_t [2] {0xB7, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mov bx, imm16 => BB imm16
	code = new uint8_t [3] {0xBB, 0x32, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x132, opc.source.imm_word_data);
	delete code;

	// mov sp, imm16 => BC imm16
	code = new uint8_t [3] {0xBC, 0xF0, 0x7};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x7f0, opc.source.imm_word_data);
	delete code;

	// mov bp, imm16 => BD imm16
	code = new uint8_t [3] {0xBD, 0xF0, 0x7};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x7f0, opc.source.imm_word_data);
	delete code;

	// mov si, imm16 => BE imm16
	code = new uint8_t [3] {0xBE, 0xF0, 0x7};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x7f0, opc.source.imm_word_data);
	delete code;

	// mov di, imm16 => BF imm16
	code = new uint8_t [3] {0xBF, 0xF0, 0x7};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x7f0, opc.source.imm_word_data);
	delete code;

	// --- r/m opcodes (88, 89, 8A, 8B) ---

	// mov m/reg8, m/reg8 => 88 [mod/reg/rm] [imm8/imm16]
	// mov m/reg8, m/reg8 => 8A [mod/reg/rm] [imm8/imm16]
	// mov m/reg16, m/reg16 => 89 [mod/reg/rm] [imm8/imm16]
	// mov m/reg16, m/reg16 => 8B [mod/reg/rm] [imm8/imm16]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = 06
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 0 (AL)

	// 88 06 01 00       mov    BYTE PTR ds:0x1,al
	code = new uint8_t [4] {0x88, 0x06, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 0E
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 1 (CL)

	// 8a 0e 01 00       mov    cl,BYTE PTR ds:0x1
	code = new uint8_t [4] {0x8A, 0x0E, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 16
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 2 (DX)

	// 89 16 01 00       mov    WORD PTR ds:0x1,dx
	code = new uint8_t [4] {0x89, 0x16, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 1E
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 3 (BX)

	// 8b 1e 01 00       mov    bx,WORD PTR ds:0x1
	code = new uint8_t [4] {0x8B, 0x1E, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 65
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 5 (DI)

	// 88 65 01                mov    BYTE PTR [di+0x1],ah
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [3] {0x88, 0x65, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 66
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 6 (BP)

	// 8a 66 01                mov    ah,BYTE PTR [bp+0x1]
	code = new uint8_t [3] {0x8A, 0x66, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 6F
	// mod = 01; indirect register with one byte offset
	// reg = 5 (BP); r/m = 7 (BX)

	// 89 6f 01                mov    WORD PTR [bx+0x1],bp
	code = new uint8_t [3] {0x89, 0x6F, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 77
	// mod = 01; indirect register with one byte offset
	// reg = 6 (SI); r/m = 7 (BX)

	// 8b 77 01                mov    si,WORD PTR [bx+0x1]
	code = new uint8_t [3] {0x8B, 0x77, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 91
	// mod = 10; indirect register with four byte offset
	// reg = 2 (DL); r/m = 1 (BX+DI)

	// 88 91 00 f0       mov    BYTE PTR [bx+di-0x1000],dl
	// todo: note how the offset is signed, check it works!!
	code = new uint8_t [4] {0x88, 0x91, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-4096, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 9A
	// mod = 10; indirect register with four byte offset
	// reg = 3 (BL); r/m = 2 (BP+SI)

	// 8a 9a 00 f0       mov    bl,BYTE PTR [bp+si-0x1000]
	code = new uint8_t [4] {0x8A, 0x9A, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-4096, opc.source.offset_data);
	delete code;

	// mod/reg/rm = A3
	// mod = 10; indirect register with four byte offset
	// reg = 4 (SP); r/m = 3 (BP+DI)

	// 89 a3 00 f0       mov    WORD PTR [bp+di-0x1000],sp
	code = new uint8_t [4] {0x89, 0xA3, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-4096, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = AE
	// mod = 10; indirect register with four byte offset
	// reg = 5 (BP); r/m = 6 (BP)

	// 8b ae 00 f0       mov    bp,WORD PTR [bp-0x1000]
	code = new uint8_t [4] {0x8B, 0xAE, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-4096, opc.source.offset_data);
	delete code;

	// mod/reg/rm = EC
	// mod = 11; register addressing
	// reg = 5 (CH); r/m = 4 (AH)

	// 88 ec                   mov    ah,ch
	code = new uint8_t [2] {0x88, 0xEC};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = F3
	// mod = 11; register addressing
	// reg = 6 (DH); r/m = 3 (BL)

	// 8a f3                   mov    dh,bl
	code = new uint8_t [2] {0x8A, 0xF3};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = FA
	// mod = 11; register addressing
	// reg = 7 (DI); r/m = 2 (DX)

	// 89 fa                   mov    dx,di
	code = new uint8_t [2] {0x89, 0xFA};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing
	// reg = 0 (AX); r/m = 1 (CX)

	// 8b c1                   mov    ax,cx
	code = new uint8_t [2] {0x8B, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// --- r/m opcodes (C6 /0, C7 /0) ---

	// mov m/reg8, imm8 => C6 /0 [] [imm8]
	// mod = 00; + r/m = 0; [BX+SI]
	// c6 00 ff                mov    BYTE PTR [bx+si],0xff
	code = new uint8_t [3] {0xC6, 0x00, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mov m/reg8, imm8 => C6 /0 [] [imm8]
	// mod = 00; + r/m = 6; 16 bit displacement only
	// c6 06 ff 01 0a    mov    BYTE PTR ds:0x1ff,0xa
	code = new uint8_t [5] {0xC6, 0x06, 0xFF, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mov m/reg8, imm8 => C6 /0 [] [imm8]
	// mod = 01; + r/m = 1; [BX+DI+off8]
	// c6 41 ff 02             mov    BYTE PTR [bx+di-0x1],0x2
	code = new uint8_t [4] {0xC6, 0x41, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mov m/reg8, imm8 => C6 /0 [] [imm8]
	// mod = 10; + r/m = 2; [BP+SI+off16]
	// c6 82 ff 01 0a    mov    BYTE PTR [bp+si+0x1ff],0xa
	code = new uint8_t [5] {0xC6, 0x82, 0xFF, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mov m/reg8, imm8 => C6 /0 [] [imm8]
	// mod = 11; + r/m = 2; register (DL)
	// c6 c2 ff                mov    dl,0xff
	code = new uint8_t [3] {0xC6, 0xC2, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mov m/reg16, imm16 => C7 /0 [] [imm16]
	// mod = 00; + r/m = 2; [BP+SI]
	// c7 02 ff 01       mov    WORD PTR [bp+si],0x1ff
	code = new uint8_t [4] {0xC7, 0x02, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x1ff, opc.source.imm_word_data);
	delete code;

	// mov m/reg16, imm16 => C7 /0 [] [imm16]
	// mod = 00; + r/m = 6; 16 bit displacement only
	// c7 06 ff 01 0a 0f    mov    WORD PTR ds:0x1ff,0xf0a
	code = new uint8_t [6] {0xC7, 0x06, 0xFF, 0x01, 0x0A, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf0a, opc.source.imm_word_data);
	delete code;

	// mov m/reg16, imm16 => C7 /0 [] [imm16]
	// mod = 01; + r/m = 3; [BP+DI+off8]
	// c7 43 ff 02 0f    mov    WORD PTR [bp+di-0x1],0xf02
	code = new uint8_t [5] {0xC7, 0x43, 0xFF, 0x02, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf02, opc.source.imm_word_data);
	delete code;

	// mov m/reg16, imm16 => C7 /0 [] [imm16]
	// mod = 10; + r/m = 5; [DI+off16]
	// c7 85 ff 01 0a 0f    mov    WORD PTR [di+0x1ff],0xf0a
	code = new uint8_t [6] {0xC7, 0x85, 0xFF, 0x01, 0x0A, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf0a, opc.source.imm_word_data);
	delete code;

	// mov m/reg16, imm16 => C7 /0 [] [imm16]
	// mod = 11; + r/m = 6; register (SI)
	// c7 c6 ff 01       mov    si,0x1ff
	code = new uint8_t [4] {0xC7, 0xC6, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x1ff, opc.source.imm_word_data);
	delete code;

	// --- segment register opcodes (8C, 8E) ---

	// mov m/reg16, m/reg16 => 8C [mod/reg/rm] []
	// mod = 00; + r/m = 2; [BP+SI]
	// 8c 02                   mov    WORD PTR [bp+si],es
	code = new uint8_t [2] {0x8C, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(seg_es, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mov m/reg16, m/reg16 => 8E [mod/reg/rm] []
	// 8e 02                   mov    es,WORD PTR [bp+si]
	code = new uint8_t [2] {0x8E, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(seg_es, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mov m/reg16, m/reg16 => 8C [mod/reg/rm] []
	// mod = 00; + r/m = 6; 16 bit displacement only
	// 8c 06 ff 01       mov    WORD PTR ds:0x1ff,es
	code = new uint8_t [4] {0x8C, 0x06, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(seg_es, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mov m/reg16, m/reg16 => 8E [mod/reg/rm] []
	// 8e 06 ff 01       mov    es,WORD PTR ds:0x1ff
	code = new uint8_t [6] {0x8E, 0x06, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(seg_es, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x1ff, opc.source.disponly_data);
	delete code;

	// mov m/reg16, m/reg16 => 8C [mod/reg/rm] []
	// mod = 01; + r/m = 3; [BP+DI+off8]
	// 8c 43 ff                mov    WORD PTR [bp+di-0x1],es
	code = new uint8_t [3] {0x8C, 0x43, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(seg_es, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mov m/reg16, m/reg16 => 8E [mod/reg/rm] []
	// 8e 43 ff                mov    es,WORD PTR [bp+di-0x1]
	code = new uint8_t [3] {0x8E, 0x43, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(seg_es, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_di, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-1, opc.source.offset_data);
	delete code;

	// mov m/reg16, m/reg16 => 8C [mod/reg/rm] []
	// mod = 10; + r/m = 5; [DI+off16]
	// 8c 85 ff 01       mov    WORD PTR [di+0x1ff],es
	code = new uint8_t [4] {0x8C, 0x85, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(seg_es, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mov m/reg16, m/reg16 => 8E [mod/reg/rm] []
	// 8e 85 ff 01       mov    es,WORD PTR [di+0x1ff]
	code = new uint8_t [4] {0x8E, 0x85, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(seg_es, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_di, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x1ff, opc.source.offset_data);
	delete code;

	// mov m/reg16, m/reg16 => 8C [mod/reg/rm] []
	// mod = 11; + r/m = 0; register (AX)
	// 8c c0                   mov    ax,es
	code = new uint8_t [2] {0x8C, 0xC0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// register is actually a 32 bit register, however the top two bytes of the result are undefined
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(seg_es, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mov m/reg16, m/reg16 => 8E [mod/reg/rm] []
	// 8e c1                   mov    es,cx
	code = new uint8_t [2] {0x8E, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(seg_es, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// register is actually a 32 bit register, however the top two bytes are ignored
	delete code;

	// mov m/reg16, m/reg16 => 8E [mod/reg/rm] []
	// 8e c6                   mov    es,si
	code = new uint8_t [2] {0x8E, 0xC6};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(MOV, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(seg_es, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

}

}
