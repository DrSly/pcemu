#include "main_test.h"

namespace
{

// debug.h

TEST(debug)
{

	CHECK_EQUAL(0, strcmp("ADD", opcode_names[ADD]));

	CHECK_EQUAL("AND", opcode_names[AND]);
	CHECK_EQUAL("JMP", opcode_names[JMP]);
	CHECK_EQUAL("CMP", opcode_names[CMP]);
	CHECK_EQUAL("MOV", opcode_names[MOV]);
	CHECK_EQUAL("PUSH", opcode_names[PUSH]);
	CHECK_EQUAL("POP", opcode_names[POP]);
	CHECK_EQUAL("LEA", opcode_names[LEA]);
	CHECK_EQUAL("CALL", opcode_names[CALL]);
	CHECK_EQUAL("INT", opcode_names[INT]);
	CHECK_EQUAL("NOT_IMPL", opcode_names[NOT_IMPL]);

	// byte_register_names
	CHECK_EQUAL("AL", byte_register_names[al_ax_eax]);
	CHECK_EQUAL("CL", byte_register_names[cl_cx_ecx]);
	CHECK_EQUAL("DL", byte_register_names[dl_dx_edx]);
	CHECK_EQUAL("BL", byte_register_names[bl_bx_ebx]);
	CHECK_EQUAL("AH", byte_register_names[ah_sp_esp]);
	CHECK_EQUAL("CH", byte_register_names[ch_bp_ebp]);
	CHECK_EQUAL("DH", byte_register_names[dh_si_esi]);
	CHECK_EQUAL("BH", byte_register_names[bh_di_edi]);
	CHECK_EQUAL("ES", byte_register_names[seg_es]);
	CHECK_EQUAL("CS", byte_register_names[seg_cs]);
	CHECK_EQUAL("SS", byte_register_names[seg_ss]);
	CHECK_EQUAL("DS", byte_register_names[seg_ds]);
	CHECK_EQUAL("FS", byte_register_names[seg_fs]);
	CHECK_EQUAL("GS", byte_register_names[seg_gs]);
	CHECK_EQUAL("flags", byte_register_names[rm_flags]);
	CHECK_EQUAL("ip", byte_register_names[ip]);
	CHECK_EQUAL("BX+SI", byte_register_names[ind_bx_si]);
	CHECK_EQUAL("BX+DI", byte_register_names[ind_bx_di]);
	CHECK_EQUAL("BP+SI", byte_register_names[ind_bp_si]);
	CHECK_EQUAL("BP+DI", byte_register_names[ind_bp_di]);
	CHECK_EQUAL("SI", byte_register_names[ind_si]);
	CHECK_EQUAL("DI", byte_register_names[ind_di]);
	CHECK_EQUAL("BP", byte_register_names[ind_bp]);
	CHECK_EQUAL("BX", byte_register_names[ind_bx]);

	// word_register_names
	CHECK_EQUAL("AX", word_register_names[al_ax_eax]);
	CHECK_EQUAL("CX", word_register_names[cl_cx_ecx]);
	CHECK_EQUAL("DX", word_register_names[dl_dx_edx]);
	CHECK_EQUAL("BX", word_register_names[bl_bx_ebx]);
	CHECK_EQUAL("SP", word_register_names[ah_sp_esp]);
	CHECK_EQUAL("BP", word_register_names[ch_bp_ebp]);
	CHECK_EQUAL("SI", word_register_names[dh_si_esi]);
	CHECK_EQUAL("DI", word_register_names[bh_di_edi]);
	CHECK_EQUAL("ES", word_register_names[seg_es]);
	CHECK_EQUAL("CS", word_register_names[seg_cs]);
	CHECK_EQUAL("SS", word_register_names[seg_ss]);
	CHECK_EQUAL("DS", word_register_names[seg_ds]);
	CHECK_EQUAL("FS", word_register_names[seg_fs]);
	CHECK_EQUAL("GS", word_register_names[seg_gs]);
	CHECK_EQUAL("flags", word_register_names[rm_flags]);
	CHECK_EQUAL("ip", word_register_names[ip]);
	CHECK_EQUAL("BX+SI", word_register_names[ind_bx_si]);
	CHECK_EQUAL("BX+DI", word_register_names[ind_bx_di]);
	CHECK_EQUAL("BP+SI", word_register_names[ind_bp_si]);
	CHECK_EQUAL("BP+DI", word_register_names[ind_bp_di]);
	CHECK_EQUAL("SI", word_register_names[ind_si]);
	CHECK_EQUAL("DI", word_register_names[ind_di]);
	CHECK_EQUAL("BP", word_register_names[ind_bp]);
	CHECK_EQUAL("BX", word_register_names[ind_bx]);

	// dword_register_names
	CHECK_EQUAL("EAX", dword_register_names[al_ax_eax]);
	CHECK_EQUAL("ECX", dword_register_names[cl_cx_ecx]);
	CHECK_EQUAL("EDX", dword_register_names[dl_dx_edx]);
	CHECK_EQUAL("EBX", dword_register_names[bl_bx_ebx]);
	CHECK_EQUAL("ESP", dword_register_names[ah_sp_esp]);
	CHECK_EQUAL("EBP", dword_register_names[ch_bp_ebp]);
	CHECK_EQUAL("ESI", dword_register_names[dh_si_esi]);
	CHECK_EQUAL("EDI", dword_register_names[bh_di_edi]);
	CHECK_EQUAL("ES", dword_register_names[seg_es]);
	CHECK_EQUAL("CS", dword_register_names[seg_cs]);
	CHECK_EQUAL("SS", dword_register_names[seg_ss]);
	CHECK_EQUAL("DS", dword_register_names[seg_ds]);
	CHECK_EQUAL("FS", dword_register_names[seg_fs]);
	CHECK_EQUAL("GS", dword_register_names[seg_gs]);
	CHECK_EQUAL("flags", dword_register_names[rm_flags]);
	CHECK_EQUAL("ip", dword_register_names[ip]);
	CHECK_EQUAL("BX+SI", dword_register_names[ind_bx_si]);
	CHECK_EQUAL("BX+DI", dword_register_names[ind_bx_di]);
	CHECK_EQUAL("BP+SI", dword_register_names[ind_bp_si]);
	CHECK_EQUAL("BP+DI", dword_register_names[ind_bp_di]);
	CHECK_EQUAL("SI", dword_register_names[ind_si]);
	CHECK_EQUAL("DI", dword_register_names[ind_di]);
	CHECK_EQUAL("BP", dword_register_names[ind_bp]);
	CHECK_EQUAL("BX", dword_register_names[ind_bx]);

}

}
