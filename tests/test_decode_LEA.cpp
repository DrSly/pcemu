#include "main_test.h"

namespace
{

// decode.h

TEST(decode_LEA)
{

	mode32 = 1;

	// LEA reg32,mem => 8D [mod/reg/rm] []

	// mod/reg/rm = 01
	// mod = 00; + r/m = 1; [ECX]
	// reg = 0 (EAX)

	// 8d 01                   lea    eax,[ecx]
	uint8_t *code = new uint8_t [2] {0x8D, 0x01};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LEA, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 08
	// mod = 00; + r/m = 0; [EAX]
	// reg = 1 (ECX)

	// 8d 08                   lea    ecx,[eax]
	code = new uint8_t [2] {0x8D, 0x08};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LEA, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 05
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 0 (EAX)

	// 8d 05 ff 00 00 01       lea    eax,ds:0x10000ff
	code = new uint8_t [6] {0x8D, 0x05, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(LEA, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x10000ff, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 42
	// mod = 01; indirect register with one byte offset
	// reg = 0 (EAX); r/m = 2 (EDX)

	// 8d 42 ff                lea    eax,[edx-0x1]
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [3] {0x8D, 0x42, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LEA, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-1, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 82
	// mod = 10; indirect register with four byte offset
	// reg = 0 (EAX); r/m = 2 (EDX)

	// 8d 82 ff 00 00 01       lea    eax,[edx+0x10000ff]
	code = new uint8_t [6] {0x8D, 0x82, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(LEA, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x10000ff, opc.source.offset_data);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing not allowed
	// reg = 0 (EAX); r/m = 1 (ECX)

	// 8d c1                   (bad)
	code = new uint8_t [2] {0x8D, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);
	CHECK_EQUAL(0, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_register);
	CHECK_EQUAL(0, opc.dest.use_indregister);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.use_offset);
	CHECK_EQUAL(0, opc.dest.use_disponly);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_indregister);
	CHECK_EQUAL(0, opc.source.use_imm_word);
	CHECK_EQUAL(0, opc.source.use_offset);
	CHECK_EQUAL(0, opc.source.use_disponly);
	delete code;

}

TEST(decode_LEA_16bit)
{

	mode32 = 0;

	// LEA reg16,mem => 8D [mod/reg/rm] []

	// mod/reg/rm = 01
	// mod = 00; + r/m = 1; [BX+DI]
	// reg = 0 (AX)

	// 8d 01                   lea    ax,[bx+di]
	uint8_t *code = new uint8_t [2] {0x8D, 0x01};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LEA, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 08
	// mod = 00; + r/m = 0; [BX+SI]
	// reg = 1 (CX)

	// 8d 08                   lea    cx,[bx+si]
	code = new uint8_t [2] {0x8D, 0x08};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LEA, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 06
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 0 (AX)

	// 8d 06 ff 01       lea    ax,ds:0x1ff
	code = new uint8_t [4] {0x8D, 0x06, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(LEA, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x1ff, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 42
	// mod = 01; indirect register with one byte offset
	// reg = 0 (AX); r/m = 2 (BP+SI)

	// 8d 42 ff                lea    ax,[bp+si-0x1]
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [3] {0x8D, 0x42, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LEA, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-1, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 82
	// mod = 10; indirect register with four byte offset
	// reg = 0 (AX); r/m = 2 (BP+SI)

	// 8d 82 ff 01       lea    ax,[bp+si+0x1ff]
	code = new uint8_t [4] {0x8D, 0x82, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(LEA, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x1ff, opc.source.offset_data);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing not allowed
	// reg = 0 (AX); r/m = 1 (CX)

	// 8d c1                   (bad)
	code = new uint8_t [2] {0x8D, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);
	CHECK_EQUAL(0, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_register);
	CHECK_EQUAL(0, opc.dest.use_indregister);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.use_offset);
	CHECK_EQUAL(0, opc.dest.use_disponly);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_indregister);
	CHECK_EQUAL(0, opc.source.use_imm_word);
	CHECK_EQUAL(0, opc.source.use_offset);
	CHECK_EQUAL(0, opc.source.use_disponly);
	delete code;

}

}
