#include "main_test.h"

namespace
{

// decode.h

TEST(decode_RCL_RCR)
{

	mode32 = 1;

	// --- RCL: D0 /2 ---

	// RCL r/m8,1 => D0 /2 []
	// mod = 00; + r/m = 1; [ECX]
	// d0 11                   rcl    BYTE PTR [ecx],1
	uint8_t *code = new uint8_t [2] {0xD0, 0x11};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// d0 15 ff 00 00 01       rcl    BYTE PTR ds:0x10000ff,1
	code = new uint8_t [6] {0xD0, 0x15, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 5; [EBP+off8]
	// d0 55 ff                rcl    BYTE PTR [ebp-0x1],1
	code = new uint8_t [3] {0xD0, 0x55, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// d0 92 ff 00 00 01       rcl    BYTE PTR [edx+0x10000ff],1
	code = new uint8_t [6] {0xD0, 0x92, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 0; register (AL)
	// d0 d0                   rcl    al,1
	code = new uint8_t [2] {0xD0, 0xD0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// --- RCL: D1 /2 ---

	// RCL r/m32,1 => D1 /2 []
	// mod = 00; + r/m = 1; [ECX]
	// d1 11                   rcl    DWORD PTR [ecx],1
	code = new uint8_t [2] {0xD1, 0x11};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// d1 15 ff 00 00 01       rcl    DWORD PTR ds:0x10000ff,1
	code = new uint8_t [6] {0xD1, 0x15, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 5; [EBP+off8]
	// d1 55 ff                rcl    DWORD PTR [ebp-0x1],1
	code = new uint8_t [3] {0xD1, 0x55, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// d1 92 ff 00 00 01       rcl    DWORD PTR [edx+0x10000ff],1
	code = new uint8_t [6] {0xD1, 0x92, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 0; register (EAX)
	// d1 d0                   rcl    eax,1
	code = new uint8_t [2] {0xD1, 0xD0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// --- RCL: D2 /2 ---

	// RCL r/m8,CL => D2 /2 []
	// mod = 00; + r/m = 1; [ECX]
	// d2 11                   rcl    BYTE PTR [ecx],cl
	code = new uint8_t [2] {0xD2, 0x11};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// d2 15 ff 00 00 01       rcl    BYTE PTR ds:0x10000ff,cl
	code = new uint8_t [6] {0xD2, 0x15, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 01; + r/m = 5; [EBP+off8]
	// d2 55 ff                rcl    BYTE PTR [ebp-0x1],cl
	code = new uint8_t [3] {0xD2, 0x55, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// d2 92 ff 00 00 01       rcl    BYTE PTR [edx+0x10000ff],cl
	code = new uint8_t [6] {0xD2, 0x92, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 11; + r/m = 0; register (AL)
	// d2 d0                   rcl    al,cl
	code = new uint8_t [2] {0xD2, 0xD0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// --- RCL: D3 /2 ---

	// RCL r/m32,CL => D3 /2 []
	// mod = 00; + r/m = 1; [ECX]
	// d3 11                   rcl    DWORD PTR [ecx],cl
	code = new uint8_t [2] {0xD3, 0x11};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// d3 15 ff 00 00 01       rcl    DWORD PTR ds:0x10000ff,cl
	code = new uint8_t [6] {0xD3, 0x15, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 01; + r/m = 5; [EBP+off8]
	// d3 55 ff                rcl    DWORD PTR [ebp-0x1],cl
	code = new uint8_t [3] {0xD3, 0x55, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// d3 92 ff 00 00 01       rcl    DWORD PTR [edx+0x10000ff],cl
	code = new uint8_t [6] {0xD3, 0x92, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 11; + r/m = 0; register (EAX)
	// d3 d0                   rcl    eax,cl
	code = new uint8_t [2] {0xD3, 0xD0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// --- RCL: C0 /2 ---

	// RCL r/m8,imm8   => C0 /2 [] [imm8]
	// mod = 00; + r/m = 0; [EAX]
	// c0 10 ff                rcl    BYTE PTR [eax],0xff
	code = new uint8_t [3] {0xC0, 0x10, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 1; [ECX+off8]
	// c0 51 ff 02             rcl    BYTE PTR [ecx-0x1],0x2
	code = new uint8_t [4] {0xC0, 0x51, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// c0 92 ff 00 00 01 ff    rcl    BYTE PTR [edx+0x10000ff],0xff
	code = new uint8_t [7] {0xC0, 0x92, 0xFF, 0x00, 0x00, 0x01, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 2; register (DL)
	// c0 d2 ff                rcl    dl,0xff
	code = new uint8_t [3] {0xC0, 0xD2, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// --- RCL: C1 /2 ---

	// RCL r/m32,imm8   => C1 /2 [] [imm8]
	// mod = 00; + r/m = 0; [EAX]
	// c1 10 ff                rcl    DWORD PTR [eax],0xff
	code = new uint8_t [3] {0xC1, 0x10, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 1; [ECX+off8]
	// c1 51 ff 02             rcl    DWORD PTR [ecx-0x1],0x2
	code = new uint8_t [4] {0xC1, 0x51, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// c1 92 ff 00 00 01 ff    rcl    DWORD PTR [edx+0x10000ff],0xff
	code = new uint8_t [7] {0xC1, 0x92, 0xFF, 0x00, 0x00, 0x01, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 2; register (EDX)
	// c1 d2 ff                rcl    edx,0xff
	code = new uint8_t [3] {0xC1, 0xD2, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// --- RCR: D0 /3 ---

	// RCR r/m8,1 => D0 /3 []
	// mod = 00; + r/m = 1; [ECX]
	// d0 19                   rcr    BYTE PTR [ecx],1
	code = new uint8_t [2] {0xD0, 0x19};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// d0 1d ff 00 00 01       rcr    BYTE PTR ds:0x10000ff,1
	code = new uint8_t [6] {0xD0, 0x1D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 5; [EBP+off8]
	// d0 5d ff                rcr    BYTE PTR [ebp-0x1],1
	code = new uint8_t [3] {0xD0, 0x5D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// d0 9a ff 00 00 01       rcr    BYTE PTR [edx+0x10000ff],1
	code = new uint8_t [6] {0xD0, 0x9A, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 0; register (AL)
	// d0 d8                   rcr    al,1
	code = new uint8_t [2] {0xD0, 0xD8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// --- RCR: D1 /3 ---

	// RCR r/m32,1 => D1 /3 []
	// mod = 00; + r/m = 1; [ECX]
	// d1 19                   rcr    DWORD PTR [ecx],1
	code = new uint8_t [2] {0xD1, 0x19};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// d1 1d ff 00 00 01       rcr    DWORD PTR ds:0x10000ff,1
	code = new uint8_t [6] {0xD1, 0x1D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 5; [EBP+off8]
	// d1 5d ff                rcr    DWORD PTR [ebp-0x1],1
	code = new uint8_t [3] {0xD1, 0x5D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// d1 9a ff 00 00 01       rcr    DWORD PTR [edx+0x10000ff],1
	code = new uint8_t [6] {0xD1, 0x9A, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 0; register (EAX)
	// d1 d8                   rcr    eax,1
	code = new uint8_t [2] {0xD1, 0xD8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// --- RCR: D2 /3 ---

	// RCR r/m8,CL => D2 /3 []
	// mod = 00; + r/m = 1; [ECX]
	// d2 19                   rcr    BYTE PTR [ecx],cl
	code = new uint8_t [2] {0xD2, 0x19};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// d2 1d ff 00 00 01       rcr    BYTE PTR ds:0x10000ff,cl
	code = new uint8_t [6] {0xD2, 0x1D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 01; + r/m = 5; [EBP+off8]
	// d2 5d ff                rcr    BYTE PTR [ebp-0x1],cl
	code = new uint8_t [3] {0xD2, 0x5D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// d2 9a ff 00 00 01       rcr    BYTE PTR [edx+0x10000ff],cl
	code = new uint8_t [6] {0xD2, 0x9A, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 11; + r/m = 0; register (AL)
	// d2 d8                   rcr    al,cl
	code = new uint8_t [2] {0xD2, 0xD8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// --- RCR: D3 /3 ---

	// RCR r/m32,CL => D3 /3 []
	// mod = 00; + r/m = 1; [ECX]
	// d3 19                   rcr    DWORD PTR [ecx],cl
	code = new uint8_t [2] {0xD3, 0x19};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// d3 1d ff 00 00 01       rcr    DWORD PTR ds:0x10000ff,cl
	code = new uint8_t [6] {0xD3, 0x1D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 01; + r/m = 5; [EBP+off8]
	// d3 5d ff                rcr    DWORD PTR [ebp-0x1],cl
	code = new uint8_t [3] {0xD3, 0x5D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// d3 9a ff 00 00 01       rcr    DWORD PTR [edx+0x10000ff],cl
	code = new uint8_t [6] {0xD3, 0x9A, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 11; + r/m = 0; register (EAX)
	// d3 d8                   rcr    eax,cl
	code = new uint8_t [2] {0xD3, 0xD8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// --- RCR: C0 /3 ---

	// RCR r/m8,imm8   => C0 /3 [] [imm8]
	// mod = 00; + r/m = 0; [EAX]
	// c0 18 ff                rcr    BYTE PTR [eax],0xff
	code = new uint8_t [3] {0xC0, 0x18, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 1; [ECX+off8]
	// c0 59 ff 02             rcr    BYTE PTR [ecx-0x1],0x2
	code = new uint8_t [4] {0xC0, 0x59, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// c0 9a ff 00 00 01 ff    rcr    BYTE PTR [edx+0x10000ff],0xff
	code = new uint8_t [7] {0xC0, 0x9A, 0xFF, 0x00, 0x00, 0x01, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 2; register (DL)
	// c0 da ff                rcr    dl,0xff
	code = new uint8_t [3] {0xC0, 0xDA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// --- RCR: C1 /3 ---

	// RCR r/m32,imm8   => C1 /3 [] [imm8]
	// mod = 00; + r/m = 0; [EAX]
	// c1 18 ff                rcr    DWORD PTR [eax],0xff
	code = new uint8_t [3] {0xC1, 0x18, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 1; [ECX+off8]
	// c1 59 ff 02             rcr    DWORD PTR [ecx-0x1],0x2
	code = new uint8_t [4] {0xC1, 0x59, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// c1 9a ff 00 00 01 ff    rcr    DWORD PTR [edx+0x10000ff],0xff
	code = new uint8_t [7] {0xC1, 0x9A, 0xFF, 0x00, 0x00, 0x01, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 2; register (EDX)
	// c1 da ff                rcr    edx,0xff
	code = new uint8_t [3] {0xC1, 0xDA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

}

TEST(decode_RCL_RCR_16bit)
{

	mode32 = 0;

	// --- RCL: D0 /2 ---

	// RCL r/m8,1 => D0 /2 []
	// mod = 00; + r/m = 1; [BX+DI]
	// d0 11                   rcl    BYTE PTR [bx+di],1
	uint8_t *code = new uint8_t [2] {0xD0, 0x11};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// d0 16 ff 01       rcl    BYTE PTR ds:0x1ff,1
	code = new uint8_t [4] {0xD0, 0x16, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 5; [DI+off8]
	// d0 55 ff                rcl    BYTE PTR [di-0x1],1
	code = new uint8_t [3] {0xD0, 0x55, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// d0 92 ff 01       rcl    BYTE PTR [bp+si+0x1ff],1
	code = new uint8_t [4] {0xD0, 0x92, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 0; register (AL)
	// d0 d0                   rcl    al,1
	code = new uint8_t [2] {0xD0, 0xD0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// --- RCL: D1 /2 ---

	// RCL r/m16,1 => D1 /2 []
	// mod = 00; + r/m = 1; [BX+DI]
	// d1 11                   rcl    WORD PTR [bx+di],1
	code = new uint8_t [2] {0xD1, 0x11};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// d1 16 ff 01       rcl    WORD PTR ds:0x1ff,1
	code = new uint8_t [4] {0xD1, 0x16, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 5; [DI+off8]
	// d1 55 ff                rcl    WORD PTR [di-0x1],1
	code = new uint8_t [3] {0xD1, 0x55, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// d1 92 ff 01       rcl    WORD PTR [bp+si+0x1ff],1
	code = new uint8_t [4] {0xD1, 0x92, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 0; register (AX)
	// d1 d0                   rcl    ax,1
	code = new uint8_t [2] {0xD1, 0xD0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// --- RCL: D2 /2 ---

	// RCL r/m8,CL => D2 /2 []
	// mod = 00; + r/m = 1; [BX+DI]
	// d2 11                   rcl    BYTE PTR [bx+di],cl
	code = new uint8_t [2] {0xD2, 0x11};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// d2 16 ff 01       rcl    BYTE PTR ds:0x1ff,cl
	code = new uint8_t [4] {0xD2, 0x16, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 01; + r/m = 5; [DI+off8]
	// d2 55 ff                rcl    BYTE PTR [di-0x1],cl
	code = new uint8_t [3] {0xD2, 0x55, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// d2 92 ff 01       rcl    BYTE PTR [bp+si+0x1ff],cl
	code = new uint8_t [4] {0xD2, 0x92, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 11; + r/m = 0; register (AL)
	// d2 d0                   rcl    al,cl
	code = new uint8_t [2] {0xD2, 0xD0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// --- RCL: D3 /2 ---

	// RCL r/m16,CL => D3 /2 []
	// mod = 00; + r/m = 1; [BX+DI]
	// d3 11                   rcl    WORD PTR [bx+di],cl
	code = new uint8_t [2] {0xD3, 0x11};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// d3 16 ff 01       rcl    WORD PTR ds:0x1ff,cl
	code = new uint8_t [4] {0xD3, 0x16, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 01; + r/m = 5; [DI+off8]
	// d3 55 ff                rcl    WORD PTR [di-0x1],cl
	code = new uint8_t [3] {0xD3, 0x55, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// d3 92 ff 01       rcl    WORD PTR [bp+si+0x1ff],cl
	code = new uint8_t [4] {0xD3, 0x92, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 11; + r/m = 0; register (AX)
	// d3 d0                   rcl    ax,cl
	code = new uint8_t [2] {0xD3, 0xD0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// --- RCL: C0 /2 ---

	// RCL r/m8,imm8   => C0 /2 [] [imm8]
	// mod = 00; + r/m = 0; [BX+SI]
	// c0 10 ff                rcl    BYTE PTR [bx+si],0xff
	code = new uint8_t [3] {0xC0, 0x10, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 1; [BX+DI+off8]
	// c0 51 ff 02             rcl    BYTE PTR [bx+di-0x1],0x2
	code = new uint8_t [4] {0xC0, 0x51, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// c0 92 ff 01 ff    rcl    BYTE PTR [bp+si+0x1ff],0xff
	code = new uint8_t [5] {0xC0, 0x92, 0xFF, 0x01, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 2; register (DL)
	// c0 d2 ff                rcl    dl,0xff
	code = new uint8_t [3] {0xC0, 0xD2, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// --- RCL: C1 /2 ---

	// RCL r/m16,imm8   => C1 /2 [] [imm8]
	// mod = 00; + r/m = 0; [BX+SI]
	// c1 10 ff                rcl    WORD PTR [bx+si],0xff
	code = new uint8_t [3] {0xC1, 0x10, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 1; [BX+DI+off8]
	// c1 51 ff 02             rcl    WORD PTR [bx+di-0x1],0x2
	code = new uint8_t [4] {0xC1, 0x51, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// c1 92 ff 01 ff    rcl    WORD PTR [bp+si+0x1ff],0xff
	code = new uint8_t [5] {0xC1, 0x92, 0xFF, 0x01, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 2; register (DX)
	// c1 d2 ff                rcl    dx,0xff
	code = new uint8_t [3] {0xC1, 0xD2, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// --- RCR: D0 /3 ---

	// RCR r/m8,1 => D0 /3 []
	// mod = 00; + r/m = 1; [BX+DI]
	// d0 19                   rcr    BYTE PTR [bx+di],1
	code = new uint8_t [2] {0xD0, 0x19};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// d0 1e ff 01       rcr    BYTE PTR ds:0x1ff,1
	code = new uint8_t [4] {0xD0, 0x1E, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 5; [DI+off8]
	// d0 5d ff                rcr    BYTE PTR [di-0x1],1
	code = new uint8_t [3] {0xD0, 0x5D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// d0 9a ff 01       rcr    BYTE PTR [bp+si+0x1ff],1
	code = new uint8_t [4] {0xD0, 0x9A, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 0; register (AL)
	// d0 d8                   rcr    al,1
	code = new uint8_t [2] {0xD0, 0xD8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// --- RCR: D1 /3 ---

	// RCR r/m16,1 => D1 /3 []
	// mod = 00; + r/m = 1; [BX+DI]
	// d1 19                   rcr    WORD PTR [bx+di],1
	code = new uint8_t [2] {0xD1, 0x19};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// d1 1e ff 01       rcr    WORD PTR ds:0x1ff,1
	code = new uint8_t [4] {0xD1, 0x1E, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 5; [DI+off8]
	// d1 5d ff                rcr    WORD PTR [di-0x1],1
	code = new uint8_t [3] {0xD1, 0x5D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// d1 9a ff 01       rcr    WORD PTR [bp+si+0x1ff],1
	code = new uint8_t [4] {0xD1, 0x9A, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 0; register (AX)
	// d1 d8                   rcr    ax,1
	code = new uint8_t [2] {0xD1, 0xD8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// --- RCR: D2 /3 ---

	// RCR r/m8,CL => D2 /3 []
	// mod = 00; + r/m = 1; [BX+DI]
	// d2 19                   rcr    BYTE PTR [bx+di],cl
	code = new uint8_t [2] {0xD2, 0x19};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// d2 1e ff 01       rcr    BYTE PTR ds:0x1ff,cl
	code = new uint8_t [4] {0xD2, 0x1E, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 01; + r/m = 5; [DI+off8]
	// d2 5d ff                rcr    BYTE PTR [di-0x1],cl
	code = new uint8_t [3] {0xD2, 0x5D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// d2 9a ff 00 00 01       rcr    BYTE PTR [bp+si+0x1ff],cl
	code = new uint8_t [4] {0xD2, 0x9A, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 11; + r/m = 0; register (AL)
	// d2 d8                   rcr    al,cl
	code = new uint8_t [2] {0xD2, 0xD8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// --- RCR: D3 /3 ---

	// RCR r/m16,CL => D3 /3 []
	// mod = 00; + r/m = 1; [BX+DI]
	// d3 19                   rcr    WORD PTR [bx+di],cl
	code = new uint8_t [2] {0xD3, 0x19};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// d3 1e ff 01       rcr    WORD PTR ds:0x1ff,cl
	code = new uint8_t [4] {0xD3, 0x1E, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 01; + r/m = 5; [DI+off8]
	// d3 5d ff                rcr    DWORD PTR [di-0x1],cl
	code = new uint8_t [3] {0xD3, 0x5D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off32]
	// d3 9a ff 01       rcr    WORD PTR [bp+si+0x1ff],cl
	code = new uint8_t [4] {0xD3, 0x9A, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod = 11; + r/m = 0; register (AX)
	// d3 d8                   rcr    ax,cl
	code = new uint8_t [2] {0xD3, 0xD8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// --- RCR: C0 /3 ---

	// RCR r/m8,imm8   => C0 /3 [] [imm8]
	// mod = 00; + r/m = 0; [BX+SI]
	// c0 18 ff                rcr    BYTE PTR [bx+si],0xff
	code = new uint8_t [3] {0xC0, 0x18, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 1; [BX+DI+off8]
	// c0 59 ff 02             rcr    BYTE PTR [bx+di-0x1],0x2
	code = new uint8_t [4] {0xC0, 0x59, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// c0 9a ff 01 ff    rcr    BYTE PTR [bp+si+0x1ff],0xff
	code = new uint8_t [5] {0xC0, 0x9A, 0xFF, 0x01, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 2; register (DL)
	// c0 da ff                rcr    dl,0xff
	code = new uint8_t [3] {0xC0, 0xDA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// --- RCR: C1 /3 ---

	// RCR r/m16,imm8   => C1 /3 [] [imm8]
	// mod = 00; + r/m = 0; [BX+SI]
	// c1 18 ff                rcr    WORD PTR [bx+si],0xff
	code = new uint8_t [3] {0xC1, 0x18, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 1; [BX+DI+off8]
	// c1 59 ff 02             rcr    WORD PTR [bx+di-0x1],0x2
	code = new uint8_t [4] {0xC1, 0x59, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// c1 9a ff 01 ff    rcr    WORD PTR [bp+si+0x1ff],0xff
	code = new uint8_t [5] {0xC1, 0x9A, 0xFF, 0x01, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 2; register (DX)
	// c1 da ff                rcr    dx,0xff
	code = new uint8_t [3] {0xC1, 0xDA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RCR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

}

}
