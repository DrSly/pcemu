#include "main_test.h"

// Unit test functional groups:
//
// Memory:
// test_memory.cpp
//
// Debugger:
// test_debug.cpp
//
// CPU:
// test_cpu.cpp
//
// Keyboard controller:
// test_dev_8042.cpp
//
// DMA controller:
// test_dev_8237A.cpp
//
// Programmable Interval Timer (PIT):
// test_dev_8253.cpp
//
// Programmable Interrupt Controller (PIC):
// test_dev_8259.cpp
//
// UART:
// test_dev_16550.cpp
//
// PARALLEL PORT:
// test_dev_parallel.cpp
//
// DEBUG OUTPUT PORT:
// test_dev_debug.cpp
//
// SYSTEM CONTROL PORT
// test_dev_sysctrl.cpp
//
// Decode Opcodes:
// test_decode_MOV.cpp
// test_decode_PUSH_POP.cpp
// test_decode_LEA.cpp
// test_decode_ADD.cpp
// test_decode_SUB.cpp
// test_decode_INC_DEC.cpp
// test_decode_AND.cpp
// test_decode_OR.cpp
// test_decode_XOR.cpp
// test_decode_NOT_NEG.cpp
// test_decode_SHL_SHR.cpp
// test_decode_JMP.cpp
// test_decode_CMP.cpp
// test_decode_JCC.cpp
// test_decode_FLAGS.cpp
// test_decode_STRING.cpp
// test_decode_FUNCTIONS.cpp
// test_decode_PREFIX.cpp
// test_decode_XCHG.cpp
// test_decode_IN_OUT.cpp
// test_decode_OTHER.cpp
// test_decode_TEST.cpp
// test_decode_DIV_MUL.cpp
// test_decode_LOAD_PTR.cpp
// test_decode_MOVSX_MOVZX.cpp
// test_decode_ROL_ROR.cpp
// test_decode_RCL_RCR.cpp

// functions to test:
// in cpu.h
// mov => todo
// get_address => discarded
// get_value => needs work
// set_value => todo
// getregister => done
// getregistervalue => done
// setregister => todo
// in decode.h
// decode => needs work
// decode_modregrm => done
// assign_modregrm => todo
// decode_imm8 => done
// decode_imm32 => done

// comment out to disable full memory test
// #define FULL_MEMORY_TEST

namespace
{

// cpu.h

TEST(mov)
{
	// void mov(struct operand dest, struct operand source);
	CHECK_EQUAL(1, 1);
}

// cpu.h

TEST(get_value)
{
	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// int get_value(struct operand operand, enum datasize size);
	struct operand operand;
	int val = 0;
	set_mem(1, 0x0a, BYTE);
	set_mem(2, 0x0d, BYTE);
	set_mem(128, 0xff, BYTE);
	set_mem(536870913, 0xff, BYTE);	//2147483649
	reg_eax.value = 0x00000d0a;

	// test 8, 16 and 32 bit register
	memset(&operand, 0, sizeof(struct operand));
	operand.use_register = 1;
	operand.register_data = al_ax_eax;
	operand.register_size = DWORD;
	// register eax
	val = get_value(&operand, DWORD);
	CHECK_EQUAL(3338, val);
	operand.register_size = WORD;
	// register ax
	val = get_value(&operand, WORD);
	CHECK_EQUAL(3338, val);
	operand.register_size = BYTE;
	// register al
	val = get_value(&operand, BYTE);
	CHECK_EQUAL(10, val);
	operand.register_data = ah_sp_esp;
	operand.register_size = BYTE;
	// register ah
	val = get_value(&operand, BYTE);
	CHECK_EQUAL(13, val);

	// test indirect register - memory reference at [reg]
	memset(&operand, 0, sizeof(struct operand));
	operand.use_indregister = 1;
	operand.register_segment = seg_ds;
	operand.register_data = cl_cx_ecx;
	reg_ecx.value = 0x00000001;
	operand.register_size = DWORD;
	// dword @ 0001
	val = get_value(&operand, DWORD);
	CHECK_EQUAL(3338, val);
	// try a large address
	reg_ecx.value = 0x20000001;		//0x80000001;
	operand.register_size = DWORD;
	val = get_value(&operand, BYTE);
	CHECK_EQUAL(255, val);
	reg_ecx.value = 0x80000001;
	operand.register_size = WORD;
	// word @ 0001
	val = get_value(&operand, WORD);
	CHECK_EQUAL(3338, val);
	reg_ecx.value = 0x00008002;
	operand.register_size = BYTE;
	// byte @ 0002
	val = get_value(&operand, BYTE);
	CHECK_EQUAL(13, val);
	// test 8 bit register (high order value)
	memset(&operand, 0, sizeof(struct operand));
	operand.use_indregister = 1;
	operand.register_segment = seg_ds;
	operand.register_data = ch_bp_ebp;
	reg_ecx.value = 0x00008002;
	operand.register_size = BYTE;
	// byte @ 0080
	val = get_value(&operand, BYTE);
	CHECK_EQUAL(255, val);

	// test indirect register + offset is an extension of the above and should work
	memset(&operand, 0, sizeof(struct operand));
	operand.use_indregister = 1;
	operand.register_segment = seg_ds;
	operand.register_data = cl_cx_ecx;
	operand.register_size = DWORD;
	reg_ecx.value = 0x00000001;
	operand.use_offset = 1;
	operand.offset_data = 1;
	// dword @ 0002
	val = get_value(&operand, DWORD);
	CHECK_EQUAL(13, val);
	// try 0xFFFFFFFF for a negative offset
	reg_ecx.value = 0x00000002;
	operand.offset_data = 0xFFFFFFFF;
	// dword @ 0001
	val = get_value(&operand, DWORD);
	CHECK_EQUAL(3338, val);

	// test 32 bit displacement-only
	memset(&operand, 0, sizeof(struct operand));
	operand.use_disponly = 1;
	operand.register_segment = seg_ds;
	operand.disponly_data = 0x00000001;
	// dword @ 0001
	val = get_value(&operand, DWORD);
	CHECK_EQUAL(3338, val);
	// byte @ 0001
	val = get_value(&operand, BYTE);
	CHECK_EQUAL(10, val);
	// try a large address
	operand.disponly_data = 0x20000001;		//0x80000001;
	val = get_value(&operand, BYTE);
	CHECK_EQUAL(255, val);

	// todo: add other addressing modes
	// notably SIB

	// always remember to free memory
	free_memory();
}

// cpu.h

TEST(set_value)
{
	// void set_value(struct operand operand, enum datasize size);
	CHECK_EQUAL(1, 1);
}

// cpu.h

TEST(getregister)
{
	// void *getregister(enum rm_register reg, int bits);
	// check that the 8, 16 & 32 bit versions of the register are at the same address
	CHECK_EQUAL((int *)&reg_eax, (int *)&reg_eax.value);
	CHECK_EQUAL((int *)&reg_eax, (int *)&reg_eax.r16.value);
	CHECK_EQUAL((int *)&reg_eax, (int *)&reg_eax.r16.r8.rl);
	CHECK_EQUAL((int *)&reg_eax, (int *)(&(reg_eax.r16.r8.rh) - 1));

	int *regptr;
	regptr = (int *)getregister(al_ax_eax, DWORD);
	CHECK_EQUAL((int *)&reg_eax, regptr);
	regptr = (int *)getregister(al_ax_eax, WORD);
	CHECK_EQUAL((int *)&reg_eax, regptr);
	regptr = (int *)getregister(al_ax_eax, BYTE);
	CHECK_EQUAL((int *)&reg_eax, regptr);

	regptr = (int *)getregister(cl_cx_ecx, DWORD);
	CHECK_EQUAL((int *)&reg_ecx, regptr);

	regptr = (int *)getregister(dl_dx_edx, DWORD);
	CHECK_EQUAL((int *)&reg_edx, regptr);

	regptr = (int *)getregister(bl_bx_ebx, WORD);
	CHECK_EQUAL((int *)&reg_ebx, regptr);

	regptr = (int *)getregister(ah_sp_esp, WORD);
	CHECK_EQUAL((int *)&reg_esp, regptr);
	// esp 8 bit register is redirected to ah
	regptr = (int *)getregister(ah_sp_esp, BYTE);
	CHECK_EQUAL((int *)&reg_eax.r16.r8.rh, regptr);

	regptr = (int *)getregister(ch_bp_ebp, DWORD);
	CHECK_EQUAL((int *)&reg_ebp, regptr);
	// ebp 8 bit register is redirected to ch
	regptr = (int *)getregister(ch_bp_ebp, BYTE);
	CHECK_EQUAL((int *)&reg_ecx.r16.r8.rh, regptr);

	regptr = (int *)getregister(dh_si_esi, WORD);
	CHECK_EQUAL((int *)&reg_esi, regptr);
	// esi 8 bit register is redirected to dh
	regptr = (int *)getregister(dh_si_esi, BYTE);
	CHECK_EQUAL((int *)&reg_edx.r16.r8.rh, regptr);

	// edi 8 bit register is redirected to bh
	regptr = (int *)getregister(bh_di_edi, BYTE);
	CHECK_EQUAL((int *)&reg_ebx.r16.r8.rh, regptr);
}

// cpu.h

TEST(getregistervalue)
{
	// uint32_t getregistervalue(enum rm_register reg, enum datasize size);
	reg_eax.value = 0x00000001;
	reg_ecx.value = 0x00000002;
	reg_edx.value = 0x00008002;
	reg_ebx.value = 0x00000081;
	// check 32 bit register value
	CHECK_EQUAL(1, getregistervalue(al_ax_eax, DWORD));
	CHECK_EQUAL(2, getregistervalue(cl_cx_ecx, DWORD));
	CHECK_EQUAL(32770, getregistervalue(dl_dx_edx, DWORD));
	CHECK_EQUAL(129, getregistervalue(bl_bx_ebx, DWORD));
	// check 16 bit register value
	CHECK_EQUAL(1, getregistervalue(al_ax_eax, WORD));
	CHECK_EQUAL(2, getregistervalue(cl_cx_ecx, WORD));
	CHECK_EQUAL(32770, getregistervalue(dl_dx_edx, WORD));
	CHECK_EQUAL(129, getregistervalue(bl_bx_ebx, WORD));
	// check 8 bit register (low order value)
	CHECK_EQUAL(1, getregistervalue(al_ax_eax, BYTE));
	CHECK_EQUAL(2, getregistervalue(cl_cx_ecx, BYTE));
	CHECK_EQUAL(2, getregistervalue(dl_dx_edx, BYTE));
	CHECK_EQUAL(129, getregistervalue(bl_bx_ebx, BYTE));
	// check 8 bit register (high order value)
	CHECK_EQUAL(0, getregistervalue(ah_sp_esp, BYTE));
	CHECK_EQUAL(0, getregistervalue(ch_bp_ebp, BYTE));
	CHECK_EQUAL(128, getregistervalue(dh_si_esi, BYTE));
	CHECK_EQUAL(0, getregistervalue(bh_di_edi, BYTE));

	// now try with the high bit set to 1 (0x80...)
	// this presents issues when the value gets interpreted as a signed number
	// todo: big design decision here; do we treat values as signed or unsigned?
	reg_eax.value = 0x80000001;
	reg_ecx.value = 0x80000002;
	//CHECK_EQUAL(2147483649, getregistervalue(al_ax_eax, DWORD));	// unsigned
	CHECK_EQUAL(-2147483647, getregistervalue(al_ax_eax, DWORD));	// signed
	//CHECK_EQUAL(2147483650, getregistervalue(cl_cx_ecx, DWORD));	// unsigned
	CHECK_EQUAL(-2147483646, getregistervalue(cl_cx_ecx, DWORD));	// signed
}

// decode.h

TEST(decode_modregrm)
{
	//void decode_modregrm(uint8_t opcode, struct mod_reg_rm *modregrm_data);	// decode MOD-REG-R/M byte
	// nothing much to test here really, it's just a representation of the modregrm bitfield structure
	// just try a mix of various combinations for mod, reg & rm
	struct mod_reg_rm modregrm_data;
	decode_modregrm(0b00111000, &modregrm_data);
	CHECK_EQUAL(0, modregrm_data.mod);
	CHECK_EQUAL(7, modregrm_data.reg);
	CHECK_EQUAL(0, modregrm_data.rm);
	decode_modregrm(0b00111001, &modregrm_data);
	CHECK_EQUAL(0, modregrm_data.mod);
	CHECK_EQUAL(7, modregrm_data.reg);
	CHECK_EQUAL(1, modregrm_data.rm);
	decode_modregrm(0b00001001, &modregrm_data);
	CHECK_EQUAL(0, modregrm_data.mod);
	CHECK_EQUAL(1, modregrm_data.reg);
	CHECK_EQUAL(1, modregrm_data.rm);
	decode_modregrm(0b00001101, &modregrm_data);
	CHECK_EQUAL(0, modregrm_data.mod);
	CHECK_EQUAL(1, modregrm_data.reg);
	CHECK_EQUAL(5, modregrm_data.rm);
	//
	decode_modregrm(0b01101000, &modregrm_data);
	CHECK_EQUAL(1, modregrm_data.mod);
	CHECK_EQUAL(5, modregrm_data.reg);
	CHECK_EQUAL(0, modregrm_data.rm);
	decode_modregrm(0b01100001, &modregrm_data);
	CHECK_EQUAL(1, modregrm_data.mod);
	CHECK_EQUAL(4, modregrm_data.reg);
	CHECK_EQUAL(1, modregrm_data.rm);
	decode_modregrm(0b01001010, &modregrm_data);
	CHECK_EQUAL(1, modregrm_data.mod);
	CHECK_EQUAL(1, modregrm_data.reg);
	CHECK_EQUAL(2, modregrm_data.rm);
	decode_modregrm(0b01011110, &modregrm_data);
	CHECK_EQUAL(1, modregrm_data.mod);
	CHECK_EQUAL(3, modregrm_data.reg);
	CHECK_EQUAL(6, modregrm_data.rm);
	//
	decode_modregrm(0b10101000, &modregrm_data);
	CHECK_EQUAL(2, modregrm_data.mod);
	CHECK_EQUAL(5, modregrm_data.reg);
	CHECK_EQUAL(0, modregrm_data.rm);
	decode_modregrm(0b10010001, &modregrm_data);
	CHECK_EQUAL(2, modregrm_data.mod);
	CHECK_EQUAL(2, modregrm_data.reg);
	CHECK_EQUAL(1, modregrm_data.rm);
	decode_modregrm(0b10110111, &modregrm_data);
	CHECK_EQUAL(2, modregrm_data.mod);
	CHECK_EQUAL(6, modregrm_data.reg);
	CHECK_EQUAL(7, modregrm_data.rm);
	decode_modregrm(0b10011100, &modregrm_data);
	CHECK_EQUAL(2, modregrm_data.mod);
	CHECK_EQUAL(3, modregrm_data.reg);
	CHECK_EQUAL(4, modregrm_data.rm);
	//
	decode_modregrm(0b11000101, &modregrm_data);
	CHECK_EQUAL(3, modregrm_data.mod);
	CHECK_EQUAL(0, modregrm_data.reg);
	CHECK_EQUAL(5, modregrm_data.rm);
	decode_modregrm(0b11001100, &modregrm_data);
	CHECK_EQUAL(3, modregrm_data.mod);
	CHECK_EQUAL(1, modregrm_data.reg);
	CHECK_EQUAL(4, modregrm_data.rm);
	decode_modregrm(0b11010001, &modregrm_data);
	CHECK_EQUAL(3, modregrm_data.mod);
	CHECK_EQUAL(2, modregrm_data.reg);
	CHECK_EQUAL(1, modregrm_data.rm);
	decode_modregrm(0b11110011, &modregrm_data);
	CHECK_EQUAL(3, modregrm_data.mod);
	CHECK_EQUAL(6, modregrm_data.reg);
	CHECK_EQUAL(3, modregrm_data.rm);
}

// decode.h

TEST(assign_modregrm)
{
	//int assign_modregrm(struct mod_reg_rm *modregrm_data, uint8_t *extra_opcodes, int destination_bit, struct instruction *instr);
	// todo
	CHECK_EQUAL(1, 1);
}

// decode.h

TEST(decode_imm8)
{
	//uint8_t decode_imm8(uint8_t *opcode);
	// decode_imm8 converts just a single byte so I don't expect any issues here
	uint8_t *code = new uint8_t [5] {0x01, 0x0F, 0x10, 0x80, 0xFF};
	uint8_t u1 = decode_imm8(&code[0]);
	int i1 = decode_imm8(&code[0]);
	CHECK_EQUAL(1, u1);
	CHECK_EQUAL(1, i1);
	uint8_t u2 = decode_imm8(&code[1]);
	int i2 = decode_imm8(&code[1]);
	CHECK_EQUAL(15, u2);
	CHECK_EQUAL(15, i2);
	uint8_t u3 = decode_imm8(&code[2]);
	int i3 = decode_imm8(&code[2]);
	CHECK_EQUAL(16, u3);
	CHECK_EQUAL(16, i3);
	uint8_t u4 = decode_imm8(&code[3]);
	int i4 = decode_imm8(&code[3]);
	CHECK_EQUAL(128, u4);
	CHECK_EQUAL(128, i4);
	uint8_t u5 = decode_imm8(&code[4]);
	int i5 = decode_imm8(&code[4]);
	CHECK_EQUAL(255, u5);
	CHECK_EQUAL(255, i5);
}

// decode.h

TEST(decode_imm32)
{
	//uint32_t decode_imm32(uint8_t *opcode);
	// converts a 32 bit (4 byte) value, so endian-ness comes into play
	// intel cpu are little endian (store the least significant byte first)
	// code[0] { 01 00 02 00 = 0x00020001 = 131073
	// code[1] { 00 02 00 ff = 0xff000200 = -16776704 signed or 4278190592 unsigned
	uint8_t *code = new uint8_t [5] {0x01, 0x0, 0x02, 0x0, 0xFF};
	uint32_t u1 = decode_imm32(&code[0]);
	int i1 = decode_imm32(&code[0]);
	CHECK_EQUAL(131073, u1);
	CHECK_EQUAL(0x00020001, u1);
	CHECK_EQUAL(131073, i1);
	CHECK_EQUAL(0x00020001, i1);
	// here we can distinguish a difference between a signed and an unsigned integer
	// note my compiler defines int as 32 bit, the largest positive value would be 7f ff ff ff
	uint32_t u2 = decode_imm32(&code[1]);
	int i2 = decode_imm32(&code[1]);
	CHECK_EQUAL(4278190592, u2);
	CHECK_EQUAL(0xff000200, u2);
	CHECK_EQUAL(-16776704, i2);
	CHECK_EQUAL(0xff000200, i2);
}

// dummy

TEST(Sanity)
{
	CHECK_EQUAL(1, 1);
}

	int main()
	{
		return UnitTest::RunAllTests();
	}

}
