#include "main_test.h"

namespace
{

// decode.h

TEST(decode_ADD)
{

	mode32 = 1;

	// add al, imm8 => 04 imm8

	// 04 01                   add    al,0x1
	uint8_t *code = new uint8_t [2] {0x04, 0x1};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// add eax, imm32 => 05 imm32

	// 05 40 30 32 01          add    eax,0x1323040
	code = new uint8_t [5] {0x05, 0x40, 0x30, 0x32, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x1323040, opc.source.imm_word_data);
	delete code;

	// ADD reg8,r/m8   => 02 [mod/reg/rm]
	// ADD reg32,r/m32 => 03 [mod/reg/rm]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = 0D
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 1 (CL)

	// 02 0d 01 00 00 00       add    cl,BYTE PTR ds:0x1
	code = new uint8_t [6] {0x02, 0x0D, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 1D
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 3 (EBX)

	// 03 1d 01 00 00 00       add    ebx,DWORD PTR ds:0x1
	code = new uint8_t [6] {0x03, 0x1D, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 66
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 6 (ESI)

	// 02 66 01                add    ah,BYTE PTR [esi+0x1]
	code = new uint8_t [3] {0x02, 0x66, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 77
	// mod = 01; indirect register with one byte offset
	// reg = 6 (ESI); r/m = 7 (EDI)

	// 03 77 01                add    esi,DWORD PTR [edi+0x1]
	code = new uint8_t [3] {0x03, 0x77, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 9A
	// mod = 10; indirect register with four byte offset
	// reg = 3 (BL); r/m = 2 (EDX)

	// 02 9a 00 00 00 f0       add    bl,BYTE PTR [edx-0x10000000]
	code = new uint8_t [6] {0x02, 0x9A, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0xf0000000, opc.source.offset_data);
	CHECK_EQUAL(-268435456, opc.source.offset_data);
	delete code;

	// mod/reg/rm = AE
	// mod = 10; indirect register with four byte offset
	// reg = 5 (EBP); r/m = 6 (ESI)

	// 03 ae 00 00 00 f0       add    ebp,DWORD PTR [esi-0x10000000]
	code = new uint8_t [6] {0x03, 0xAE, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0xf0000000, opc.source.offset_data);
	CHECK_EQUAL(-268435456, opc.source.offset_data);
	delete code;

	// mod/reg/rm = F3
	// mod = 11; register addressing
	// reg = 6 (DH); r/m = 3 (BL)

	// 02 f3                   add    dh,bl
	code = new uint8_t [2] {0x02, 0xF3};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing
	// reg = 0 (EAX); r/m = 1 (ECX)

	// 03 c1                   add    eax,ecx
	code = new uint8_t [2] {0x03, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// --- r/m opcodes (80 /0, 81 /0, 83 /0) ---

	// ADD r/m8,imm8   => 80 /0 [] [imm8]
	// ADD r/m32,imm32 => 81 /0 [] [imm32]
	// ADD r/m32,imm8  => 83 /0 [] [imm8]
	// mod = 00; + r/m = 0; [EAX]
	// 80 00 ff                add    BYTE PTR [eax],0xff
	code = new uint8_t [3] {0x80, 0x00, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// 81 05 ff 00 00 01 0a 83 05 ff    add    DWORD PTR ds:0x10000ff,0xff05830a
	code = new uint8_t [10] {0x81, 0x05, 0xFF, 0x00, 0x00, 0x01, 0x0A, 0x83, 0x05, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff05830a, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// 83 05 ff 00 00 01 0a    add    DWORD PTR ds:0x10000ff,0xa
	code = new uint8_t [7] {0x83, 0x05, 0xFF, 0x00, 0x00, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 1; [ECX+off8]
	// 80 41 ff 02             add    BYTE PTR [ecx-0x1],0x2
	code = new uint8_t [4] {0x80, 0x41, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// 81 82 ff 00 00 01 0a 83 82 ff    add    DWORD PTR [edx+0x10000ff],0xff82830a
	code = new uint8_t [10] {0x81, 0x82, 0xFF, 0x00, 0x00, 0x01, 0x0A, 0x83, 0x82, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff82830a, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// 83 82 ff 00 00 01 0a    add    DWORD PTR [edx+0x10000ff],0xa
	code = new uint8_t [7] {0x83, 0x82, 0xFF, 0x00, 0x00, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 2; register (DL)
	// 80 c2 ff                add    dl,0xff
	code = new uint8_t [3] {0x80, 0xC2, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 2; [EDX]
	// 81 02 ff 00 00 01       add    DWORD PTR [edx],0x10000ff
	code = new uint8_t [6] {0x81, 0x02, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x10000ff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 2; [EDX]
	// 83 02 ff                add    DWORD PTR [edx],0xffffffff
	code = new uint8_t [3] {0x83, 0x02, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffffff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// 81 05 ff 00 00 01 0a 00 00 0f    add    DWORD PTR ds:0x10000ff,0xf00000a
	code = new uint8_t [10] {0x81, 0x05, 0xFF, 0x00, 0x00, 0x01, 0x0A, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf00000a, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 3; [EBX+off8]
	// 83 43 ff 02             add    DWORD PTR [ebx-0x1],0x2
	code = new uint8_t [4] {0x83, 0x43, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 5; [EBP+off32]
	// 81 85 ff 00 00 01 0a 00 00 0f    add    DWORD PTR [ebp+0x10000ff],0xf00000a
	code = new uint8_t [10] {0x81, 0x85, 0xFF, 0x00, 0x00, 0x01, 0x0A, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf00000a, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 6; register (ESI)
	// 83 c6 ff                add    esi,0xffffffff
	code = new uint8_t [3] {0x83, 0xC6, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffffff, opc.source.imm_word_data);
	delete code;

	// --- r/m opcode (82 /0) ---

	// is it even a real opcode ??

	// --- r/m opcodes (00, 01) ---

	// ADD r/m8,reg8   => 00 [mod/reg/rm]
	// ADD r/m32,reg32 => 01 [mod/reg/rm]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = 05
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 0 (AL)

	// 00 05 01 00 00 00       add    BYTE PTR ds:0x1,al
	code = new uint8_t [6] {0x00, 0x05, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 15
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 2 (EDX)

	// 01 15 01 00 00 00       add    DWORD PTR ds:0x1,edx
	code = new uint8_t [6] {0x01, 0x15, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 65
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 5 (EBP)

	// 00 65 01                add    BYTE PTR [ebp+0x1],ah
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [3] {0x00, 0x65, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 6F
	// mod = 01; indirect register with one byte offset
	// reg = 5 (EBP); r/m = 7 (EDI)

	// 01 6f 01                add    DWORD PTR [edi+0x1],ebp
	code = new uint8_t [3] {0x01, 0x6F, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 91
	// mod = 10; indirect register with four byte offset
	// reg = 2 (DL); r/m = 1 (ECX)

	// 00 91 00 00 00 f0       add    BYTE PTR [ecx-0x10000000],dl
	// todo: note how the offset is signed, check it works!!
	code = new uint8_t [6] {0x00, 0x91, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0xf0000000, opc.dest.offset_data);
	CHECK_EQUAL(-268435456, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = A3
	// mod = 10; indirect register with four byte offset
	// reg = 4 (ESP); r/m = 3 (EBX)

	// 01 a3 00 00 00 f0       add    DWORD PTR [ebx-0x10000000],esp
	code = new uint8_t [6] {0x01, 0xA3, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0xf0000000, opc.dest.offset_data);
	CHECK_EQUAL(-268435456, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = EC
	// mod = 11; register addressing
	// reg = 5 (CH); r/m = 4 (AH)

	// 00 ec                   add    ah,ch
	code = new uint8_t [2] {0x00, 0xEC};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = FA
	// mod = 11; register addressing
	// reg = 7 (EDI); r/m = 2 (EDX)

	// 01 fa                   add    edx,edi
	code = new uint8_t [2] {0x01, 0xFA};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

}

TEST(decode_ADD_16bit)
{

	mode32 = 0;

	// add ax, imm16 => 05 imm16

	// 05 32 01          add    ax,0x132
	uint8_t *code = new uint8_t [3] {0x05, 0x32, 0x1};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x132, opc.source.imm_word_data);
	delete code;

	// ADD reg8,r/m8   => 02 [mod/reg/rm]
	// ADD reg16,r/m16 => 03 [mod/reg/rm]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = 0E
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 1 (CL)

	// 02 0e 01 00       add    cl,BYTE PTR ds:0x1
	code = new uint8_t [4] {0x02, 0x0E, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 1E
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 3 (BX)

	// 03 1e 01 00       add    bx,WORD PTR ds:0x1
	code = new uint8_t [4] {0x03, 0x1E, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 66
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 6 (BP)

	// 02 66 01                add    ah,BYTE PTR [bp+0x1]
	code = new uint8_t [3] {0x02, 0x66, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 77
	// mod = 01; indirect register with one byte offset
	// reg = 6 (SI); r/m = 7 (BX)

	// 03 77 01                add    si,WORD PTR [bx+0x1]
	code = new uint8_t [3] {0x03, 0x77, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 9A
	// mod = 10; indirect register with four byte offset
	// reg = 3 (BL); r/m = 2 (BP+SI)

	// 02 9a 00 f0       add    bl,BYTE PTR [bp+si-0x1000]
	code = new uint8_t [4] {0x02, 0x9A, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-4096, opc.source.offset_data);
	delete code;

	// mod/reg/rm = AE
	// mod = 10; indirect register with four byte offset
	// reg = 5 (BP); r/m = 6 (BP)

	// 03 ae 00 f0       add    bp,WORD PTR [bp-0x1000]
	code = new uint8_t [4] {0x03, 0xAE, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-4096, opc.source.offset_data);
	delete code;

	// mod/reg/rm = F3
	// mod = 11; register addressing
	// reg = 6 (DH); r/m = 3 (BL)

	// 02 f3                   add    dh,bl
	code = new uint8_t [2] {0x02, 0xF3};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing
	// reg = 0 (AX); r/m = 1 (CX)

	// 03 c1                   add    ax,cx
	code = new uint8_t [2] {0x03, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// --- r/m opcodes (80 /0, 81 /0, 83 /0) ---

	// ADD r/m8,imm8   => 80 /0 [] [imm8]
	// ADD r/m16,imm16 => 81 /0 [] [imm16]
	// ADD r/m16,imm8  => 83 /0 [] [imm8]
	// mod = 00; + r/m = 0; [BX+SI]
	// 80 00 ff                add    BYTE PTR [bx+si],0xff
	code = new uint8_t [3] {0x80, 0x00, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// 81 06 ff 01 0a ff    add    WORD PTR ds:0x1ff,0xff0a
	code = new uint8_t [6] {0x81, 0x06, 0xFF, 0x01, 0x0A, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff0a, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// 83 06 ff 01 0a    add    WORD PTR ds:0x1ff,0xa
	code = new uint8_t [5] {0x83, 0x06, 0xFF, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 1; [BX+DI+off8]
	// 80 41 ff 02             add    BYTE PTR [bx+di-0x1],0x2
	code = new uint8_t [4] {0x80, 0x41, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// 81 82 ff 01 0a ff    add    WORD PTR [bp+si+0x1ff],0xff0a
	code = new uint8_t [6] {0x81, 0x82, 0xFF, 0x01, 0x0A, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff0a, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// 83 82 ff 01 0a    add    WORD PTR [bp+si+0x1ff],0xa
	code = new uint8_t [5] {0x83, 0x82, 0xFF, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 2; register (DL)
	// 80 c2 ff                add    dl,0xff
	code = new uint8_t [3] {0x80, 0xC2, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 2; [BP+SI]
	// 81 02 ff 01       add    WORD PTR [bp+si],0x1ff
	code = new uint8_t [4] {0x81, 0x02, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x1ff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 2; [BP+SI]
	// 83 02 ff                add    WORD PTR [bp+si],0xffff
	code = new uint8_t [3] {0x83, 0x02, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffffff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// 81 06 ff 01 0a 0f    add    WORD PTR ds:0x1ff,0xf0a
	code = new uint8_t [6] {0x81, 0x06, 0xFF, 0x01, 0x0A, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf0a, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 3; [BP+DI+off8]
	// 83 43 ff 02             add    WORD PTR [bp+di-0x1],0x2
	code = new uint8_t [4] {0x83, 0x43, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 5; [DI+off16]
	// 81 85 ff 01 0a 0f    add    WORD PTR [di+0x1ff],0xf0a
	code = new uint8_t [6] {0x81, 0x85, 0xFF, 0x01, 0x0A, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf0a, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 6; register (SI)
	// 83 c6 ff                add    si,0xffff
	code = new uint8_t [3] {0x83, 0xC6, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffffff, opc.source.imm_word_data);
	delete code;

	// --- r/m opcodes (00, 01) ---

	// ADD r/m8,reg8   => 00 [mod/reg/rm]
	// ADD r/m16,reg16 => 01 [mod/reg/rm]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = 06
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 0 (AL)

	// 00 06 01 00       add    BYTE PTR ds:0x1,al
	code = new uint8_t [4] {0x00, 0x06, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 16
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 2 (DX)

	// 01 16 01 00       add    WORD PTR ds:0x1,dx
	code = new uint8_t [4] {0x01, 0x16, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 65
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 5 (DI)

	// 00 65 01                add    BYTE PTR [di+0x1],ah
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [3] {0x00, 0x65, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 6F
	// mod = 01; indirect register with one byte offset
	// reg = 5 (BP); r/m = 7 (BX)

	// 01 6f 01                add    WORD PTR [bx+0x1],bp
	code = new uint8_t [3] {0x01, 0x6F, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 91
	// mod = 10; indirect register with four byte offset
	// reg = 2 (DL); r/m = 1 (BX+DI)

	// 00 91 00 f0       add    BYTE PTR [bx+di-0x1000],dl
	// todo: note how the offset is signed, check it works!!
	code = new uint8_t [4] {0x00, 0x91, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-4096, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = A3
	// mod = 10; indirect register with four byte offset
	// reg = 4 (SP); r/m = 3 (BP+DI)

	// 01 a3 00 f0       add    WORD PTR [bp+di-0x1000],sp
	code = new uint8_t [4] {0x01, 0xA3, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-4096, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = EC
	// mod = 11; register addressing
	// reg = 5 (CH); r/m = 4 (AH)

	// 00 ec                   add    ah,ch
	code = new uint8_t [2] {0x00, 0xEC};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = FA
	// mod = 11; register addressing
	// reg = 7 (DI); r/m = 2 (DX)

	// 01 fa                   add    dx,di
	code = new uint8_t [2] {0x01, 0xFA};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(ADD, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

}

}
