#include "main_test.h"

namespace
{

// decode.h

TEST(decode_SUB)
{

	mode32 = 1;

	// SUB AL,imm8 => 2C imm8

	// 2c 01                   sub    al,0x1
	uint8_t *code = new uint8_t [2] {0x2C, 0x1};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// SUB EAX,imm32 => 2D imm32

	// 2d 40 30 32 01          sub    eax,0x1323040
	code = new uint8_t [5] {0x2D, 0x40, 0x30, 0x32, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x1323040, opc.source.imm_word_data);
	delete code;

	// SUB reg8,r/m8   => 2A [mod/reg/rm]
	// SUB reg32,r/m32 => 2B [mod/reg/rm]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = 0D
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 1 (CL)

	// 2a 0d 01 00 00 00       sub    cl,BYTE PTR ds:0x1
	code = new uint8_t [6] {0x2A, 0x0D, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 1D
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 3 (EBX)

	// 2b 1d 01 00 00 00       sub    ebx,DWORD PTR ds:0x1
	code = new uint8_t [6] {0x2B, 0x1D, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 66
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 6 (ESI)

	// 2a 66 01                sub    ah,BYTE PTR [esi+0x1]
	code = new uint8_t [3] {0x2A, 0x66, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 77
	// mod = 01; indirect register with one byte offset
	// reg = 6 (ESI); r/m = 7 (EDI)

	// 2b 77 01                sub    esi,DWORD PTR [edi+0x1]
	code = new uint8_t [3] {0x2B, 0x77, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 9A
	// mod = 10; indirect register with four byte offset
	// reg = 3 (BL); r/m = 2 (EDX)

	// 2a 9a 00 00 00 f0       sub    bl,BYTE PTR [edx-0x10000000]
	code = new uint8_t [6] {0x2A, 0x9A, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0xf0000000, opc.source.offset_data);
	CHECK_EQUAL(-268435456, opc.source.offset_data);
	delete code;

	// mod/reg/rm = AE
	// mod = 10; indirect register with four byte offset
	// reg = 5 (EBP); r/m = 6 (ESI)

	// 2b ae 00 00 00 f0       sub    ebp,DWORD PTR [esi-0x10000000]
	code = new uint8_t [6] {0x2B, 0xAE, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0xf0000000, opc.source.offset_data);
	CHECK_EQUAL(-268435456, opc.source.offset_data);
	delete code;

	// mod/reg/rm = F3
	// mod = 11; register addressing
	// reg = 6 (DH); r/m = 3 (BL)

	// 2a f3                   sub    dh,bl
	code = new uint8_t [2] {0x2A, 0xF3};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing
	// reg = 0 (EAX); r/m = 1 (ECX)

	// 2b c1                   sub    eax,ecx
	code = new uint8_t [2] {0x2B, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// --- r/m opcodes (80 /5, 81 /5, 83 /5) ---

	// SUB r/m8,imm8   => 80 /5 [] [imm8]
	// SUB r/m32,imm32 => 81 /5 [] [imm32]
	// SUB r/m32,imm8  => 83 /5 [] [imm8]
	// mod = 00; + r/m = 0; [EAX]
	// 80 28 ff                sub    BYTE PTR [eax],0xff
	code = new uint8_t [3] {0x80, 0x28, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// 81 2d ff 00 00 01 0a 83 05 ff    sub    DWORD PTR ds:0x10000ff,0xff05830a
	code = new uint8_t [10] {0x81, 0x2D, 0xFF, 0x00, 0x00, 0x01, 0x0A, 0x83, 0x05, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff05830a, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// 83 2d ff 00 00 01 0a    sub    DWORD PTR ds:0x10000ff,0xa
	code = new uint8_t [7] {0x83, 0x2D, 0xFF, 0x00, 0x00, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 1; [ECX+off8]
	// 80 69 ff 02             sub    BYTE PTR [ecx-0x1],0x2
	code = new uint8_t [4] {0x80, 0x69, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// 81 aa ff 00 00 01 0a 83 82 ff    sub    DWORD PTR [edx+0x10000ff],0xff82830a
	code = new uint8_t [10] {0x81, 0xAA, 0xFF, 0x00, 0x00, 0x01, 0x0A, 0x83, 0x82, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff82830a, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// 83 aa ff 00 00 01 0a    sub    DWORD PTR [edx+0x10000ff],0xa
	code = new uint8_t [7] {0x83, 0xAA, 0xFF, 0x00, 0x00, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 2; register (DL)
	// 80 ea ff                sub    dl,0xff
	code = new uint8_t [3] {0x80, 0xEA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 2; [EDX]
	// 81 2a ff 00 00 01       sub    DWORD PTR [edx],0x10000ff
	code = new uint8_t [6] {0x81, 0x2A, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x10000ff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 2; [EDX]
	// 83 2a ff                sub    DWORD PTR [edx],0xffffffff
	code = new uint8_t [3] {0x83, 0x2A, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffffff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// 81 2d ff 00 00 01 0a 00 00 0f    sub    DWORD PTR ds:0x10000ff,0xf00000a
	code = new uint8_t [10] {0x81, 0x2D, 0xFF, 0x00, 0x00, 0x01, 0x0A, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf00000a, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 3; [EBX+off8]
	// 83 6b ff 02             sub    DWORD PTR [ebx-0x1],0x2
	code = new uint8_t [4] {0x83, 0x6B, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 5; [EBP+off32]
	// 81 ad ff 00 00 01 0a 00 00 0f    sub    DWORD PTR [ebp+0x10000ff],0xf00000a
	code = new uint8_t [10] {0x81, 0xAD, 0xFF, 0x00, 0x00, 0x01, 0x0A, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf00000a, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 6; register (ESI)
	// 83 ee ff                sub    esi,0xffffffff
	code = new uint8_t [3] {0x83, 0xEE, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffffff, opc.source.imm_word_data);
	delete code;

	// --- r/m opcodes (28, 29) ---

	// SUB r/m8,reg8   => 28 [mod/reg/rm]
	// SUB r/m32,reg32 => 29 [mod/reg/rm]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = 05
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 0 (AL)

	// 28 05 01 00 00 00       sub    BYTE PTR ds:0x1,al
	code = new uint8_t [6] {0x28, 0x05, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 15
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 2 (EDX)

	// 29 15 01 00 00 00       sub    DWORD PTR ds:0x1,edx
	code = new uint8_t [6] {0x29, 0x15, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 65
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 5 (EBP)

	// 28 65 01                sub    BYTE PTR [ebp+0x1],ah
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [3] {0x28, 0x65, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 6F
	// mod = 01; indirect register with one byte offset
	// reg = 5 (EBP); r/m = 7 (EDI)

	// 29 6f 01                sub    DWORD PTR [edi+0x1],ebp
	code = new uint8_t [3] {0x29, 0x6F, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 91
	// mod = 10; indirect register with four byte offset
	// reg = 2 (DL); r/m = 1 (ECX)

	// 28 91 00 00 00 f0       sub    BYTE PTR [ecx-0x10000000],dl
	// todo: note how the offset is signed, check it works!!
	code = new uint8_t [6] {0x28, 0x91, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0xf0000000, opc.dest.offset_data);
	CHECK_EQUAL(-268435456, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = A3
	// mod = 10; indirect register with four byte offset
	// reg = 4 (ESP); r/m = 3 (EBX)

	// 29 a3 00 00 00 f0       sub    DWORD PTR [ebx-0x10000000],esp
	code = new uint8_t [6] {0x29, 0xA3, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0xf0000000, opc.dest.offset_data);
	CHECK_EQUAL(-268435456, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = EC
	// mod = 11; register addressing
	// reg = 5 (CH); r/m = 4 (AH)

	// 28 ec                   sub    ah,ch
	code = new uint8_t [2] {0x28, 0xEC};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = FA
	// mod = 11; register addressing
	// reg = 7 (EDI); r/m = 2 (EDX)

	// 29 fa                   sub    edx,edi
	code = new uint8_t [2] {0x29, 0xFA};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

}

TEST(decode_SUB_16bit)
{

	mode32 = 0;

	// SUB AL,imm8 => 2C imm8

	// 2c 01                   sub    al,0x1
	uint8_t *code = new uint8_t [2] {0x2C, 0x1};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// SUB AX,imm16 => 2D imm16

	// 2d 32 01          sub    ax,0x132
	code = new uint8_t [3] {0x2D, 0x32, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x132, opc.source.imm_word_data);
	delete code;

	// SUB reg8,r/m8   => 2A [mod/reg/rm]
	// SUB reg16,r/m16 => 2B [mod/reg/rm]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = 0E
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 1 (CL)

	// 2a 0e 01 00       sub    cl,BYTE PTR ds:0x1
	code = new uint8_t [4] {0x2A, 0x0E, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 1E
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 3 (BX)

	// 2b 1e 01 00       sub    bx,WORD PTR ds:0x1
	code = new uint8_t [4] {0x2B, 0x1E, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 66
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 6 (BP)

	// 2a 66 01                sub    ah,BYTE PTR [bp+0x1]
	code = new uint8_t [3] {0x2A, 0x66, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 77
	// mod = 01; indirect register with one byte offset
	// reg = 6 (SI); r/m = 7 (BX)

	// 2b 77 01                sub    si,WORD PTR [bx+0x1]
	code = new uint8_t [3] {0x2B, 0x77, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 9A
	// mod = 10; indirect register with four byte offset
	// reg = 3 (BL); r/m = 2 (BP+SI)

	// 2a 9a 00 f0       sub    bl,BYTE PTR [bp+si-0x1000]
	code = new uint8_t [4] {0x2A, 0x9A, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-4096, opc.source.offset_data);
	delete code;

	// mod/reg/rm = AE
	// mod = 10; indirect register with four byte offset
	// reg = 5 (BP); r/m = 6 (BP)

	// 2b ae 00 f0       sub    bp,WORD PTR [bp-0x1000]
	code = new uint8_t [4] {0x2B, 0xAE, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-4096, opc.source.offset_data);
	delete code;

	// mod/reg/rm = F3
	// mod = 11; register addressing
	// reg = 6 (DH); r/m = 3 (BL)

	// 2a f3                   sub    dh,bl
	code = new uint8_t [2] {0x2A, 0xF3};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing
	// reg = 0 (AX); r/m = 1 (CX)

	// 2b c1                   sub    ax,cx
	code = new uint8_t [2] {0x2B, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// --- r/m opcodes (80 /5, 81 /5, 83 /5) ---

	// SUB r/m8,imm8   => 80 /5 [] [imm8]
	// SUB r/m16,imm16 => 81 /5 [] [imm16]
	// SUB r/m16,imm8  => 83 /5 [] [imm8]
	// mod = 00; + r/m = 0; [BX+SI]
	// 80 28 ff                sub    BYTE PTR [bx+si],0xff
	code = new uint8_t [3] {0x80, 0x28, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// 81 2e ff 01 0a ff    sub    WORD PTR ds:0x1ff,0xff0a
	code = new uint8_t [6] {0x81, 0x2E, 0xFF, 0x01, 0x0A, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff0a, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// 83 2e ff 01 0a    sub    WORD PTR ds:0x1ff,0xa
	code = new uint8_t [5] {0x83, 0x2E, 0xFF, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 1; [BX+DI+off8]
	// 80 69 ff 02             sub    BYTE PTR [bx+di-0x1],0x2
	code = new uint8_t [4] {0x80, 0x69, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// 81 aa ff 01 0a ff    sub    WORD PTR [bp+si+0x1ff],0xff0a
	code = new uint8_t [6] {0x81, 0xAA, 0xFF, 0x01, 0x0A, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff0a, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// 83 aa ff 01 0a    sub    WORD PTR [bp+si+0x1ff],0xa
	code = new uint8_t [5] {0x83, 0xAA, 0xFF, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 2; register (DL)
	// 80 ea ff                sub    dl,0xff
	code = new uint8_t [3] {0x80, 0xEA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 2; [BP+SI]
	// 81 2a ff 01       sub    WORD PTR [bp+si],0x1ff
	code = new uint8_t [4] {0x81, 0x2A, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x1ff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 2; [BP+SI]
	// 83 2a ff                sub    WORD PTR [bp+si],0xffff
	code = new uint8_t [3] {0x83, 0x2A, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffffff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// 81 2e ff 01 0a 0f    sub    WORD PTR ds:0x1ff,0xf0a
	code = new uint8_t [6] {0x81, 0x2E, 0xFF, 0x01, 0x0A, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf0a, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 3; [BP+DI+off8]
	// 83 6b ff 02             sub    WORD PTR [bp+di-0x1],0x2
	code = new uint8_t [4] {0x83, 0x6B, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 5; [DI+off16]
	// 81 ad ff 01 0a 0f    sub    WORD PTR [di+0x1ff],0xf0a
	code = new uint8_t [6] {0x81, 0xAD, 0xFF, 0x01, 0x0A, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf0a, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 6; register (SI)
	// 83 ee ff                sub    si,0xffff
	code = new uint8_t [3] {0x83, 0xEE, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffffff, opc.source.imm_word_data);
	delete code;

	// --- r/m opcodes (28, 29) ---

	// SUB r/m8,reg8   => 28 [mod/reg/rm]
	// SUB r/m16,reg16 => 29 [mod/reg/rm]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = 06
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 0 (AL)

	// 28 06 01 00       sub    BYTE PTR ds:0x1,al
	code = new uint8_t [4] {0x28, 0x06, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 16
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 2 (DX)

	// 29 16 01 00       sub    WORD PTR ds:0x1,dx
	code = new uint8_t [4] {0x29, 0x16, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 65
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 5 (DI)

	// 28 65 01                sub    BYTE PTR [di+0x1],ah
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [3] {0x28, 0x65, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 6F
	// mod = 01; indirect register with one byte offset
	// reg = 5 (BP); r/m = 7 (BX)

	// 29 6f 01                sub    WORD PTR [bx+0x1],bp
	code = new uint8_t [3] {0x29, 0x6F, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 91
	// mod = 10; indirect register with four byte offset
	// reg = 2 (DL); r/m = 1 (BX+DI)

	// 28 91 00 f0       sub    BYTE PTR [bx+di-0x1000],dl
	// todo: note how the offset is signed, check it works!!
	code = new uint8_t [4] {0x28, 0x91, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-4096, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = A3
	// mod = 10; indirect register with four byte offset
	// reg = 4 (SP); r/m = 3 (BP+DI)

	// 29 a3 00 f0       sub    WORD PTR [bp+di-0x1000],sp
	code = new uint8_t [4] {0x29, 0xA3, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-4096, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = EC
	// mod = 11; register addressing
	// reg = 5 (CH); r/m = 4 (AH)

	// 28 ec                   sub    ah,ch
	code = new uint8_t [2] {0x28, 0xEC};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = FA
	// mod = 11; register addressing
	// reg = 7 (DI); r/m = 2 (DX)

	// 29 fa                   sub    dx,di
	code = new uint8_t [2] {0x29, 0xFA};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(SUB, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

}

}
