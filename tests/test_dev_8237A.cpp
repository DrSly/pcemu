#include "main_test.h"

namespace
{

// dev_8237A.h

TEST(dev_8237A)
{

	// initialise cpu
	init_cpu();

	// initialise DMA controller
	CHECK_EQUAL(0, init_dma());

	// check ports are available
	CHECK(write_to_port[0x80]);

	// DMA1 master reset
	write_to_port[0x0d](0x0d, 0x00, BYTE);
	CHECK_EQUAL(0x0f, read_from_port[0x0f](0x0f, BYTE));

	// unmask channel 2
	write_to_port[0x0a](0x0a, 0x02, BYTE);
	CHECK_EQUAL(0x0b, read_from_port[0x0f](0x0f, BYTE));

	// unmask channels 1, 2, 3 and mask channel 0
	write_to_port[0x0f](0x0f, 0x01, BYTE);
	CHECK_EQUAL(0x01, read_from_port[0x0f](0x0f, BYTE));

	// mask channel 2
	write_to_port[0x0a](0x0a, 0x06, BYTE);
	CHECK_EQUAL(0x05, read_from_port[0x0f](0x0f, BYTE));

	// unmask channel 0
	write_to_port[0x0a](0x0a, 0x00, BYTE);
	CHECK_EQUAL(0x04, read_from_port[0x0f](0x0f, BYTE));

	// DMA2 master reset
	write_to_port[0xda](0xda, 0x00, BYTE);
	CHECK_EQUAL(0x0f, read_from_port[0xde](0xde, BYTE));

	// unmask channels 4, 6, 7 and mask channel 5
	write_to_port[0xde](0xde, 0x02, BYTE);
	CHECK_EQUAL(0x02, read_from_port[0xde](0xde, BYTE));

	// mask channels 6 and 7
	write_to_port[0xd4](0xd4, 0x06, BYTE);
	write_to_port[0xd4](0xd4, 0x07, BYTE);
	CHECK_EQUAL(0x0e, read_from_port[0xde](0xde, BYTE));

	// unmask channel 5
	write_to_port[0xd4](0xd4, 0x01, BYTE);
	CHECK_EQUAL(0x0c, read_from_port[0xde](0xde, BYTE));

	// shutdown
	shutdown_dma();

}

}
