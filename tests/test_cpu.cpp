#include "main_test.h"

namespace
{

// cpu.h

TEST(cpu_PUSH_POP)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	struct instruction opc;

	// b8 fe ff             mov    ax,0xfffe
	uint8_t *code = new uint8_t [3] {0xB8, 0xFE, 0xFF};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xfffe, (uint16_t)reg_eax.r16.value);

	// 89 c4                   mov    sp,ax
	code = new uint8_t [2] {0x89, 0xC4};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xfffe, (uint16_t)reg_esp.r16.value);

	// bb 65 01             mov    bx,0x165
	code = new uint8_t [3] {0xBB, 0x65, 0x01};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0x0165, (uint16_t)reg_ebx.r16.value);

	// 53                      push   bx
	code = new uint8_t [1] {0x53};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xfffc, (uint16_t)reg_esp.r16.value);
	CHECK_EQUAL(0x0165, get_mem((reg_ss << 4) + (uint16_t)reg_esp.r16.value, WORD));

	// bb 2b d8             mov    bx,0xd82b
	code = new uint8_t [3] {0xBB, 0x2B, 0xD8};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xd82b, (uint16_t)reg_ebx.r16.value);

	// 53                      push   bx
	code = new uint8_t [1] {0x53};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xfffa, (uint16_t)reg_esp.r16.value);
	CHECK_EQUAL(0xd82b, get_mem((reg_ss << 4) + (uint16_t)reg_esp.r16.value, WORD));

	// b8 04 00             mov    ax,0x4
	code = new uint8_t [3] {0xB8, 0x04, 0x00};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0x0004, (uint16_t)reg_eax.r16.value);

	// 50                      push   ax
	code = new uint8_t [1] {0x50};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xfff8, (uint16_t)reg_esp.r16.value);
	CHECK_EQUAL(0x0004, get_mem((reg_ss << 4) + (uint16_t)reg_esp.r16.value, WORD));

	// 5b                      pop    bx
	code = new uint8_t [1] {0x5B};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xfffa, (uint16_t)reg_esp.r16.value);
	CHECK_EQUAL(0x0004, (uint16_t)reg_ebx.r16.value);

	// 59                      pop    cx
	code = new uint8_t [1] {0x59};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xfffc, (uint16_t)reg_esp.r16.value);
	CHECK_EQUAL(0xd82b, (uint16_t)reg_ecx.r16.value);

	// 5a                      pop    dx
	code = new uint8_t [1] {0x5A};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xfffe, (uint16_t)reg_esp.r16.value);
	CHECK_EQUAL(0x0165, (uint16_t)reg_edx.r16.value);

	// shutdown
	free_memory();

}

// test operation of flags:
// ADD

TEST(cpu_FLAGS_ADD)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

//
// Listing of test program: testflags_add.bin
//
// 0000	8C C8		mov	ax,	cs		; setup data segment
// 0002	05 00 10	add	ax,	1000h	; we need a full 64Kb segment
// 0005	8E D8		mov	ds,	ax		; to store all the results
// 0007	31 C9		xor	cx,	cx
// 0009 outer_loop:
// 0009	31 DB		xor	bx,	bx
// 000B inner_loop:
// 000B	89 CE		mov	si,	cx		; destination address
// 000D	C1 E6 08	shl	si,	8		; si = (cx * 256) + bx
// 0010	01 DE		add	si,	bx		;
// 0012	89 C8		mov	ax,	cx
// 0014	02 C3		add	al,	bl		; test with the least significant byte
// 0016	9F			lahf			; load the flags (except overflow) into ah
// 0017	71 03		jno	short no_overflow
// 0019	80 CC 20	or	ah,	20h		; set bit 5 of ah to indicate overflow
// 001C no_overflow:
// 001C	80 E4 FF	and	ah,	0FFh	; we can mask out undefined flags if we need to
// 001F	88 24		mov	[si], ah	; output result at destination address
// 0021	43			inc	bx
// 0022	81 FB FF 00	cmp	bx,	0FFh	; we'll loop while bx <= 0xFF
// 0026	7E E3		jle	short inner_loop
// 0028	41			inc	cx
// 0029	81 F9 FF 00	cmp	cx,	0FFh	; we'll loop while cx <= 0xFF
// 002D	7E DA		jle	short outer_loop
// 002F	F4			hlt				; use hlt to signal completion
//

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_add.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\ADD_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// AND

TEST(cpu_FLAGS_AND)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_and.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\AND_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// CMP

TEST(cpu_FLAGS_CMP)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_cmp.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\CMP_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// DEC

TEST(cpu_FLAGS_DEC)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_dec.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\DEC_OUT", 0xe0000);

	for (int i=0; i<0x100; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// IMUL

TEST(cpu_FLAGS_IMUL)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_imul.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\IMUL_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// INC

TEST(cpu_FLAGS_INC)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

//
// Listing of test program: testflags_inc.bin
//
// 0000	8C C8		mov	ax,	cs		; setup data segment
// 0002	05 00 10	add	ax,	1000h	; we need a full 64Kb segment
// 0005	8E D8		mov	ds,	ax		; to store all the results
// 0007	31 C9		xor	cx,	cx
// 0009 loop:
// 000B	89 CE		mov	si,	cx		; destination address
// 0012	89 C8		mov	ax,	cx
// 0014	FE C0		inc al			; test with the least significant byte
// 0016	9F			lahf			; load the flags (except overflow) into ah
// 0017	71 03		jno	short no_overflow
// 0019	80 CC 20	or	ah,	20h		; set bit 5 of ah to indicate overflow
// 001C no_overflow:
// 001C	80 E4 FF	and	ah,	0FFh	; we can mask out undefined flags if we need to
// 001F	88 24		mov	[si], ah	; output result at destination address
// 0028	41			inc	cx
// 0029	81 F9 FF 00	cmp	cx,	0FFh	; we'll loop while cx <= 0xFF
// 002D	7E E8		jle	short loop
// 002F	F4			hlt				; use hlt to signal completion
//

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_inc.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	//save_to_file("memdmp", 0xf0000, 0x10000);
	load_from_file("..\\tests\\assets\\INC_OUT", 0xe0000);

	for (int i=0; i<0x100; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// MUL

TEST(cpu_FLAGS_MUL)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_mul.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\MUL_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// NEG

TEST(cpu_FLAGS_NEG)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_neg.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\NEG_OUT", 0xe0000);

	for (int i=0; i<0x100; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// OR

TEST(cpu_FLAGS_OR)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_or.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\OR_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// RCL

TEST(cpu_FLAGS_RCL)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_rcl.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\RCL_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// RCR

TEST(cpu_FLAGS_RCR)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_rcr.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\RCR_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// ROL

TEST(cpu_FLAGS_ROL)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_rol.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\ROL_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// ROR

TEST(cpu_FLAGS_ROR)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_ror.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\ROR_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// SHL

TEST(cpu_FLAGS_SHL)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_shl.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\SHL_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// SHR

TEST(cpu_FLAGS_SHR)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_shr.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\SHR_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// SUB

TEST(cpu_FLAGS_SUB)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_sub.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\SUB_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// TEST

TEST(cpu_FLAGS_TEST)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_test.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\TEST_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags:
// XOR

TEST(cpu_FLAGS_XOR)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	// load code segment from file
	load_from_file("..\\tests\\assets\\testflags_xor.bin", 0xe0000);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));

	// insert jump to program entry point
	// ea 5b e0 00 f0	jmp far	E000:0000
	uint8_t *code = new uint8_t [5] {0xEA, 0x00, 0x00, 0x00, 0xE0};
	copy_to_mem((reg_cs << 4) + reg_ip, code, 16);
	load_instr_from_memory(&opc);
	execute(&opc);
	delete code;

	CHECK_EQUAL(0xe000, reg_cs);
	CHECK_EQUAL(0x0000, reg_ip);

	int breakout = 0;
	code = new uint8_t [16];
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		execute(&opc);

	}

	// compare the memory contents with the solution file
	load_from_file("..\\tests\\assets\\XOR_OUT", 0xe0000);

	for (int i=0; i<0x10000; i+=4) {
		int expected = get_mem((0xE000 << 4) + i, DWORD);
		int actual = get_mem((0xF000 << 4) + i, DWORD);
		CHECK_EQUAL(expected, actual);
	}

	// shutdown
	free_memory();

}

// test operation of flags

TEST(cpu_FLAGS)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// initialise cpu
	init_cpu();

	// verify initial cpu state
	CHECK_EQUAL(0, mode32);
	CHECK_EQUAL(0xf000, reg_cs);
	CHECK_EQUAL(0xfff0, reg_ip);
	CHECK_EQUAL(0x0002, flags);

	struct instruction opc;

	// b8 f0 ff             mov    ax,0xfff0
	uint8_t *code = new uint8_t [3] {0xB8, 0xF0, 0xFF};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xfff0, (uint16_t)reg_eax.r16.value);

	// 83 c0 ee                add    ax,0xffee
	code = new uint8_t [3] {0x83, 0xC0, 0xEE};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xffde, (uint16_t)reg_eax.r16.value);
	CHECK_EQUAL(0x0087, flags);

	// b8 dc ff             mov    ax,0xffdc
	code = new uint8_t [3] {0xB8, 0xDC, 0xFF};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xffdc, (uint16_t)reg_eax.r16.value);

	// 89 c4                   mov    sp,ax
	code = new uint8_t [2] {0x89, 0xC4};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xffdc, (uint16_t)reg_esp.r16.value);

	// 44                      inc    esp
	code = new uint8_t [1] {0x44};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xffdd, (uint16_t)reg_esp.r16.value);
	CHECK_EQUAL(0x0087, flags);

	// b8 04 00          mov    ax,0x4
	code = new uint8_t [3] {0xB8, 0x04, 0x00};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0x0004, (uint16_t)reg_eax.r16.value);

	// 24 07                   and    al,0x7
	code = new uint8_t [3] {0x24, 0x07};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0x0004, (uint16_t)reg_eax.r16.value);
	CHECK_EQUAL(0x0002, flags);

	// 3c 07                   cmp    al,0x7
	code = new uint8_t [2] {0x3C, 0x07};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0x0004, (uint16_t)reg_eax.r16.value);
	CHECK_EQUAL(0x0093, flags);

	// b8 25 00          mov    ax,0x25
	code = new uint8_t [3] {0xB8, 0x25, 0x00};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0x0025, (uint16_t)reg_eax.r16.value);

	// 84 c0                   test   al,al
	code = new uint8_t [2] {0x84, 0xC0};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0x0025, (uint16_t)reg_eax.r16.value);
	CHECK_EQUAL(0x0002, flags);

	// 3c 25                   cmp    al,0x25
	code = new uint8_t [2] {0x3C, 0x25};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0x0025, (uint16_t)reg_eax.r16.value);
	CHECK_EQUAL(0x0046, flags);

	// 31 c0                   xor    ax,ax
	code = new uint8_t [2] {0x31, 0xC0};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0x0000, (uint16_t)reg_eax.r16.value);
	CHECK_EQUAL(0x0046, flags);

	// 3c 00                   cmp    al,0x0
	code = new uint8_t [2] {0x3C, 0x00};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0x0000, (uint16_t)reg_eax.r16.value);
	CHECK_EQUAL(0x0046, flags);

	// 3c fe                   cmp    al,0xfe
	code = new uint8_t [2] {0x3C, 0xFE};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0x0000, (uint16_t)reg_eax.r16.value);
	CHECK_EQUAL(0x0013, flags);

	// 31 c0                   xor    ax,ax
	code = new uint8_t [2] {0x31, 0xC0};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0x0000, (uint16_t)reg_eax.r16.value);
	CHECK_EQUAL(0x0046, flags);

	// b8 d0 ff             mov    ax,0xffd0
	code = new uint8_t [3] {0xB8, 0xD0, 0xFF};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xffd0, (uint16_t)reg_eax.r16.value);

	// 89 c4                   mov    sp,ax
	code = new uint8_t [2] {0x89, 0xC4};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xffd0, (uint16_t)reg_esp.r16.value);

	// 4c                      dec    sp
	code = new uint8_t [1] {0x4C};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xffcf, (uint16_t)reg_esp.r16.value);
	CHECK_EQUAL(0x0096, flags);

	// 4c                      dec    sp
	code = new uint8_t [1] {0x4C};
	for (int i=0; i<16; i++) set_mem((reg_cs << 4) + reg_ip + i, code[i], BYTE);
	delete code;
	load_instr_from_memory(&opc);
	execute(&opc);

	CHECK_EQUAL(0xffce, (uint16_t)reg_esp.r16.value);
	CHECK_EQUAL(0x0082, flags);

	// shutdown
	free_memory();

}

}
