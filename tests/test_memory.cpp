#include "main_test.h"

namespace
{

// memory.h

TEST(load_bios_file)
{

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// try to load Bochs default BIOS
	int memory_addr = 0xe0000;
	FILE *fp;
	uint8_t buffer[512];
	fp = fopen("..\\assets\\BIOS-bochs-latest", "rb");
	/*REQUIRE */CHECK(fp != 0);
	int bytesread = 0;
	int totalbytes = 0;
	while (!feof(fp)) {
		bytesread = fread(buffer, sizeof(uint8_t), 512, fp);
		copy_to_mem(memory_addr, buffer, bytesread);
		memory_addr += bytesread;
		totalbytes += bytesread;
	}
	fclose(fp);

	// the file is 131,072 bytes (128kb)
	CHECK_EQUAL(131072, totalbytes);

	// we loaded at 0xe0000 or 917,504 decimal
	// until 0xfffff or 1,048,575 decimal

	uint8_t *ptr = (uint8_t *)mem_ptr[0];
	CHECK_EQUAL(0, ptr[0]);
	CHECK_EQUAL(0x31, ptr[0xe0000]);
	CHECK_EQUAL(0xFA, ptr[0xe2000]);
	CHECK_EQUAL(0xFF, ptr[0xe4000]);
	CHECK_EQUAL(0xFF, ptr[0xe6000]);
	CHECK_EQUAL(0xFF, ptr[0xe8000]);
	CHECK_EQUAL(0xFF, ptr[0xea000]);
	CHECK_EQUAL(0xFF, ptr[0xec000]);
	CHECK_EQUAL(0xFF, ptr[0xee000]);
	CHECK_EQUAL(0x55, ptr[0xf0000]);
	CHECK_EQUAL(0x30, ptr[0xf2000]);
	CHECK_EQUAL(0xA1, ptr[0xf4000]);
	CHECK_EQUAL(0xE4, ptr[0xf6000]);
	CHECK_EQUAL(0x8A, ptr[0xf8000]);
	CHECK_EQUAL(0x00, ptr[0xfa000]);
	CHECK_EQUAL(0x00, ptr[0xfc000]);
	CHECK_EQUAL(0x00, ptr[0xfe000]);
	CHECK_EQUAL(0x7B, ptr[0xfffff]);

	// always remember to free memory
	free_memory();
}

// memory.h

TEST(misc_mem)
{
	//int get_mem(int addr, enum datasize size);

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// set up buffer for testing
	uint8_t tmp[5] = {0xaa, 0xaa, 0xaa, 0xaa, 0xaa};

	CHECK_EQUAL(0xaa, tmp[0]);
	CHECK_EQUAL(0xaa, tmp[1]);
	CHECK_EQUAL(0xaa, tmp[2]);
	CHECK_EQUAL(0xaa, tmp[3]);
	CHECK_EQUAL(0xaa, tmp[4]);
	// set some memory values
	set_mem(0, 255, BYTE);
	set_mem(1, 10, BYTE);
	set_mem(2, 13, BYTE);
	set_mem(3, 14, BYTE);
	// make sure copy_mem copies bytes
	test_copy_mem(0, tmp, 3);
	CHECK_EQUAL(255, tmp[0]);	// modified
	CHECK_EQUAL(10, tmp[1]);	// modified
	CHECK_EQUAL(13, tmp[2]);	// modified
	CHECK_EQUAL(0xaa, tmp[3]);	// unchanged
	CHECK_EQUAL(0xaa, tmp[4]);	// unchanged

	// try a large address
	set_mem(536870913, 5, BYTE);
	test_copy_mem(536870913, tmp, 1);
	CHECK_EQUAL(5, tmp[0]);
	CHECK_EQUAL(10, tmp[1]);
	CHECK_EQUAL(13, tmp[2]);
	CHECK_EQUAL(0xaa, tmp[3]);
	CHECK_EQUAL(0xaa, tmp[4]);

	// test memory wrap
	// memory segment size was 16777216 (or 0x01000000)
	// memory segment size is now 4194304 (or 0x00400000)
	CHECK_EQUAL(4194304, _1G / MEM_SEG_COUNT * sizeof(uint8_t));
	set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) + 3, 0xb4, BYTE);
	set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) + 2, 0xb3, BYTE);
	set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) + 1, 0xb2, BYTE);
	set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t), 0xb1, BYTE);
	set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 1, 0xaf, BYTE);	// this is the last memory address in the segment
	set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 2, 0xae, BYTE);
	set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 3, 0xad, BYTE);
	set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 4, 0xac, BYTE);
	// try to wrap the segment
	// word (2 bytes) at last memory address
	CHECK_EQUAL(1, set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 1, 0xfeff, WORD));
	CHECK_EQUAL(0xfe, get_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t), BYTE));
	CHECK_EQUAL(0xff, get_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 1, BYTE));
	// dword (4 bytes) at 3rd last memory address
	CHECK_EQUAL(1, set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 3, 0xfcfdfeff, DWORD));
	CHECK_EQUAL(0xfc, get_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t), BYTE));
	CHECK_EQUAL(0xfd, get_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 1, BYTE));
	CHECK_EQUAL(0xfe, get_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 2, BYTE));
	CHECK_EQUAL(0xff, get_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 3, BYTE));
	// dword (4 bytes) at 2nd last memory address
	CHECK_EQUAL(1, set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 2, 0xfcfdfeff, DWORD));
	CHECK_EQUAL(0xfc, get_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) + 1, BYTE));
	CHECK_EQUAL(0xfd, get_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t), BYTE));
	CHECK_EQUAL(0xfe, get_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 1, BYTE));
	CHECK_EQUAL(0xff, get_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 2, BYTE));
	// dword (4 bytes) at last memory address
	CHECK_EQUAL(1, set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 1, 0xfcfdfeff, DWORD));
	CHECK_EQUAL(0xfc, get_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) + 2, BYTE));
	CHECK_EQUAL(0xfd, get_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) + 1, BYTE));
	CHECK_EQUAL(0xfe, get_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t), BYTE));
	CHECK_EQUAL(0xff, get_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 1, BYTE));
	// this should not be wrapped, as it's the start of the 2nd segment
	CHECK_EQUAL(0, set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t), 0xfcfdfeff, DWORD));
	// dword (4 bytes) at 4th last memory address will fit
	CHECK_EQUAL(0, set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 4, 0xfcfdfeff, DWORD));
	// word (2 bytes) at 2nd last memory address will fit
	CHECK_EQUAL(0, set_mem(_1G / MEM_SEG_COUNT * sizeof(uint8_t) - 2, 0xfeff, WORD));

	// todo: add tests to cover operation of get_mem() over a segment boundary

	// check segments are correctly assigned
	for (unsigned long long i=0; i<MEM_SEG_COUNT; i++) {
		CHECK_EQUAL(i, test_get_segment(i * _1G / MEM_SEG_COUNT * sizeof(uint8_t)));
		CHECK_EQUAL(0, test_get_offset(i * _1G / MEM_SEG_COUNT * sizeof(uint8_t)));
		set_mem(i * _1G / MEM_SEG_COUNT * sizeof(uint8_t), i, BYTE);
		CHECK_EQUAL(i, get_mem(i * _1G / MEM_SEG_COUNT * sizeof(uint8_t), BYTE));
	}

	#ifdef FULL_MEMORY_TEST
	// this test may take some time or crash the system
	// comment out macro definition to disable
	for (unsigned long long i=0; i<_1G; i++) {
		set_mem(i, 0xff, BYTE);
		if ((i & 0x1fffffff) == 0x1fffffff) {printf("%lld\n", i); fflush(stdout);}
	}
	CHECK_EQUAL(0xff, get_mem(_1G - 1, BYTE));
	#endif // FULL_MEMORY_TEST

	// always remember to free memory
	free_memory();
}

// memory.h

TEST(get_mem)
{
	//int get_mem(int addr, enum datasize size);

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	//
	CHECK_EQUAL(1, 1);

	// always remember to free memory
	free_memory();
}

// memory.h

TEST(set_mem)
{
	//void set_mem(int addr, int value, enum datasize size);

	// initialise memory
	CHECK_EQUAL(0, init_memory());

	// set up buffer for testing
	uint8_t tmp[4] = {0xaa, 0xaa, 0xaa, 0xaa};

	// set some memory values
	set_mem(0, 255, BYTE);
	set_mem(1, 10, BYTE);
	set_mem(2, 13, BYTE);
	set_mem(3, 14, BYTE);
	test_copy_mem(0, tmp, 3);
	CHECK_EQUAL(255, tmp[0]);
	CHECK_EQUAL(10, tmp[1]);
	CHECK_EQUAL(13, tmp[2]);
	CHECK_EQUAL(0xaa, tmp[3]);	// unchanged

	// try a large address
	set_mem(536870913, 4, BYTE);
	test_copy_mem(536870913, tmp, 1);
	CHECK_EQUAL(4, tmp[0]);
	CHECK_EQUAL(10, tmp[1]);	// unchanged
	CHECK_EQUAL(13, tmp[2]);	// unchanged
	CHECK_EQUAL(0xaa, tmp[3]);	// unchanged
	// we can address memory over 1Gb, but the high order bits are ignored
	set_mem(2147483649, 88, BYTE);	// write to 2147483649
	test_copy_mem(1, tmp, 1);		// check result at 1
	CHECK_EQUAL(88, tmp[0]);

	// try WORD and WDORD values
	set_mem(0, 0x80af0f01, DWORD);
	test_copy_mem(0, tmp, 4);
	CHECK_EQUAL(1, tmp[0]);
	CHECK_EQUAL(15, tmp[1]);
	CHECK_EQUAL(175, tmp[2]);
	CHECK_EQUAL(128, tmp[3]);
	set_mem(0, 0x00ff, WORD);
	test_copy_mem(0, tmp, 2);
	CHECK_EQUAL(255, tmp[0]);
	CHECK_EQUAL(0, tmp[1]);
	CHECK_EQUAL(175, tmp[2]);	// unchanged
	CHECK_EQUAL(128, tmp[3]);	// unchanged

	// always remember to free memory
	free_memory();

}

}
