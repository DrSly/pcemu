#include "main_test.h"

namespace
{

// decode.h

TEST(decode_CMP)
{

	mode32 = 1;

	// CMP AL,imm8 => 3C imm8

	// 3c 01                   cmp    al,0x1
	uint8_t *code = new uint8_t [2] {0x3C, 0x1};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// CMP EAX,imm32 => 3D imm32

	// 3d 40 30 32 01          cmp    eax,0x1323040
	code = new uint8_t [5] {0x3D, 0x40, 0x30, 0x32, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x1323040, opc.source.imm_word_data);
	delete code;

	// CMP reg8,r/m8   => 3A [mod/reg/rm]
	// CMP reg32,r/m32 => 3B [mod/reg/rm]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = 0D
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 1 (CL)

	// 3a 0d 01 00 00 00       cmp    cl,BYTE PTR ds:0x1
	code = new uint8_t [6] {0x3A, 0x0D, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 1D
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 3 (EBX)

	// 3b 1d 01 00 00 00       cmp    ebx,DWORD PTR ds:0x1
	code = new uint8_t [6] {0x3B, 0x1D, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 66
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 6 (ESI)

	// 3a 66 01                cmp    ah,BYTE PTR [esi+0x1]
	code = new uint8_t [3] {0x3A, 0x66, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 77
	// mod = 01; indirect register with one byte offset
	// reg = 6 (ESI); r/m = 7 (EDI)

	// 3b 77 01                cmp    esi,DWORD PTR [edi+0x1]
	code = new uint8_t [3] {0x3B, 0x77, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 9A
	// mod = 10; indirect register with four byte offset
	// reg = 3 (BL); r/m = 2 (EDX)

	// 3a 9a 00 00 00 f0       cmp    bl,BYTE PTR [edx-0x10000000]
	code = new uint8_t [6] {0x3A, 0x9A, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0xf0000000, opc.source.offset_data);
	CHECK_EQUAL(-268435456, opc.source.offset_data);
	delete code;

	// mod/reg/rm = AE
	// mod = 10; indirect register with four byte offset
	// reg = 5 (EBP); r/m = 6 (ESI)

	// 3b ae 00 00 00 f0       cmp    ebp,DWORD PTR [esi-0x10000000]
	code = new uint8_t [6] {0x3B, 0xAE, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0xf0000000, opc.source.offset_data);
	CHECK_EQUAL(-268435456, opc.source.offset_data);
	delete code;

	// mod/reg/rm = F3
	// mod = 11; register addressing
	// reg = 6 (DH); r/m = 3 (BL)

	// 3a f3                   cmp    dh,bl
	code = new uint8_t [2] {0x3A, 0xF3};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing
	// reg = 0 (EAX); r/m = 1 (ECX)

	// 3b c1                   cmp    eax,ecx
	code = new uint8_t [2] {0x3B, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// --- r/m opcodes (80 /7, 81 /7, 83 /7) ---

	// CMP r/m8,imm8   => 80 /7 [] [imm8]
	// CMP r/m32,imm32 => 81 /7 [] [imm32]
	// CMP r/m32,imm8  => 83 /7 [] [imm8]
	// mod = 00; + r/m = 0; [EAX]
	// 80 38 ff                cmp    BYTE PTR [eax],0xff
	code = new uint8_t [3] {0x80, 0x38, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// 81 3d ff 00 00 01 0a 83 05 ff    cmp    DWORD PTR ds:0x10000ff,0xff05830a
	code = new uint8_t [10] {0x81, 0x3D, 0xFF, 0x00, 0x00, 0x01, 0x0A, 0x83, 0x05, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff05830a, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// 83 3d ff 00 00 01 0a    cmp    DWORD PTR ds:0x10000ff,0xa
	code = new uint8_t [7] {0x83, 0x3D, 0xFF, 0x00, 0x00, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 1; [ECX+off8]
	// 80 79 ff 02             cmp    BYTE PTR [ecx-0x1],0x2
	code = new uint8_t [4] {0x80, 0x79, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// 81 ba ff 00 00 01 0a 83 82 ff    cmp    DWORD PTR [edx+0x10000ff],0xff82830a
	code = new uint8_t [10] {0x81, 0xBA, 0xFF, 0x00, 0x00, 0x01, 0x0A, 0x83, 0x82, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff82830a, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// 83 ba ff 00 00 01 0a    cmp    DWORD PTR [edx+0x10000ff],0xa
	code = new uint8_t [7] {0x83, 0xBA, 0xFF, 0x00, 0x00, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 2; register (DL)
	// 80 fa ff                cmp    dl,0xff
	code = new uint8_t [3] {0x80, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 2; [EDX]
	// 81 3a ff 00 00 01       cmp    DWORD PTR [edx],0x10000ff
	code = new uint8_t [6] {0x81, 0x3A, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x10000ff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 2; [EDX]
	// 83 3a ff                cmp    DWORD PTR [edx],0xffffffff
	code = new uint8_t [3] {0x83, 0x3A, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffffff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// 81 3d ff 00 00 01 0a 00 00 0f    cmp    DWORD PTR ds:0x10000ff,0xf00000a
	code = new uint8_t [10] {0x81, 0x3D, 0xFF, 0x00, 0x00, 0x01, 0x0A, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf00000a, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 3; [EBX+off8]
	// 83 7b ff 02             cmp    DWORD PTR [ebx-0x1],0x2
	code = new uint8_t [4] {0x83, 0x7B, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 5; [EBP+off32]
	// 81 bd ff 00 00 01 0a 00 00 0f    cmp    DWORD PTR [ebp+0x10000ff],0xf00000a
	code = new uint8_t [10] {0x81, 0xBD, 0xFF, 0x00, 0x00, 0x01, 0x0A, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(10, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf00000a, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 6; register (ESI)
	// 83 fe ff                cmp    esi,0xffffffff
	code = new uint8_t [3] {0x83, 0xFE, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffffff, opc.source.imm_word_data);
	delete code;

	// --- r/m opcodes (38, 39) ---

	// CMP r/m8,reg8   => 38 [mod/reg/rm]
	// CMP r/m32,reg32 => 39 [mod/reg/rm]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = 05
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 0 (AL)

	// 38 05 01 00 00 00       cmp    BYTE PTR ds:0x1,al
	code = new uint8_t [6] {0x38, 0x05, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 15
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 2 (EDX)

	// 39 15 01 00 00 00       cmp    DWORD PTR ds:0x1,edx
	code = new uint8_t [6] {0x39, 0x15, 0x01, 0x00, 0x00, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 65
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 5 (EBP)

	// 38 65 01                cmp    BYTE PTR [ebp+0x1],ah
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [3] {0x38, 0x65, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 6F
	// mod = 01; indirect register with one byte offset
	// reg = 5 (EBP); r/m = 7 (EDI)

	// 39 6f 01                cmp    DWORD PTR [edi+0x1],ebp
	code = new uint8_t [3] {0x39, 0x6F, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 91
	// mod = 10; indirect register with four byte offset
	// reg = 2 (DL); r/m = 1 (ECX)

	// 38 91 00 00 00 f0       cmp    BYTE PTR [ecx-0x10000000],dl
	// todo: note how the offset is signed, check it works!!
	code = new uint8_t [6] {0x38, 0x91, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0xf0000000, opc.dest.offset_data);
	CHECK_EQUAL(-268435456, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = A3
	// mod = 10; indirect register with four byte offset
	// reg = 4 (ESP); r/m = 3 (EBX)

	// 39 a3 00 00 00 f0       cmp    DWORD PTR [ebx-0x10000000],esp
	code = new uint8_t [6] {0x39, 0xA3, 0x00, 0x00, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0xf0000000, opc.dest.offset_data);
	CHECK_EQUAL(-268435456, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = EC
	// mod = 11; register addressing
	// reg = 5 (CH); r/m = 4 (AH)

	// 38 ec                   cmp    ah,ch
	code = new uint8_t [2] {0x38, 0xEC};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = FA
	// mod = 11; register addressing
	// reg = 7 (EDI); r/m = 2 (EDX)

	// 39 fa                   cmp    edx,edi
	code = new uint8_t [2] {0x39, 0xFA};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

}

TEST(decode_CMP_16bit)
{

	mode32 = 0;

	// CMP AL,imm8 => 3C imm8

	// 3c 01                   cmp    al,0x1
	uint8_t *code = new uint8_t [2] {0x3C, 0x1};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x01, opc.source.imm_word_data);
	delete code;

	// CMP AX,imm16 => 3D imm16

	// 3d 32 01          cmp    ax,0x132
	code = new uint8_t [3] {0x3D, 0x32, 0x1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x132, opc.source.imm_word_data);
	delete code;

	// CMP reg8,r/m8   => 3A [mod/reg/rm]
	// CMP reg16,r/m16 => 3B [mod/reg/rm]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = 0E
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 1 (CL)

	// 3a 0e 01 00       cmp    cl,BYTE PTR ds:0x1
	code = new uint8_t [4] {0x3A, 0x0E, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 1E
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 3 (BX)

	// 3b 1e 01 00       cmp    bx,WORD PTR ds:0x1
	code = new uint8_t [4] {0x3B, 0x1E, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x01, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 66
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 6 (BP)

	// 3a 66 01                cmp    ah,BYTE PTR [bp+0x1]
	code = new uint8_t [3] {0x3A, 0x66, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 77
	// mod = 01; indirect register with one byte offset
	// reg = 6 (SI); r/m = 7 (BX)

	// 3b 77 01                cmp    si,WORD PTR [bx+0x1]
	code = new uint8_t [3] {0x3B, 0x77, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 9A
	// mod = 10; indirect register with four byte offset
	// reg = 3 (BL); r/m = 2 (BP+SI)

	// 3a 9a 00 f0       cmp    bl,BYTE PTR [bp+si-0x1000]
	code = new uint8_t [4] {0x3A, 0x9A, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-4096, opc.source.offset_data);
	delete code;

	// mod/reg/rm = AE
	// mod = 10; indirect register with four byte offset
	// reg = 5 (BP); r/m = 6 (BP)

	// 3b ae 00 f0       cmp    bp,WORD PTR [bp-0x1000]
	code = new uint8_t [4] {0x3B, 0xAE, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-4096, opc.source.offset_data);
	delete code;

	// mod/reg/rm = F3
	// mod = 11; register addressing
	// reg = 6 (DH); r/m = 3 (BL)

	// 3a f3                   cmp    dh,bl
	code = new uint8_t [2] {0x3A, 0xF3};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing
	// reg = 0 (AX); r/m = 1 (CX)

	// 3b c1                   cmp    ax,cx
	code = new uint8_t [2] {0x3B, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// --- r/m opcodes (80 /7, 81 /7, 83 /7) ---

	// CMP r/m8,imm8   => 80 /7 [] [imm8]
	// CMP r/m16,imm16 => 81 /7 [] [imm16]
	// CMP r/m16,imm8  => 83 /7 [] [imm8]
	// mod = 00; + r/m = 0; [BX+SI]
	// 80 38 ff                cmp    BYTE PTR [bx+si],0xff
	code = new uint8_t [3] {0x80, 0x38, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// 81 3e ff 01 0a ff    cmp    WORD PTR ds:0x1ff,0xff0a
	code = new uint8_t [6] {0x81, 0x3E, 0xFF, 0x01, 0x0A, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff0a, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// 83 3e ff 01 0a    cmp    WORD PTR ds:0x1ff,0xa
	code = new uint8_t [5] {0x83, 0x3E, 0xFF, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 1; [BX+DI+off8]
	// 80 79 ff 02             cmp    BYTE PTR [bx+di-0x1],0x2
	code = new uint8_t [4] {0x80, 0x79, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// 81 ba ff 01 0a ff    cmp    WORD PTR [bp+si+0x1ff],0xff0a
	code = new uint8_t [6] {0x81, 0xBA, 0xFF, 0x01, 0x0A, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff0a, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// 83 ba ff 01 0a    cmp    WORD PTR [bp+si+0x1ff],0xa
	code = new uint8_t [5] {0x83, 0xBA, 0xFF, 0x01, 0x0A};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x0a, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 2; register (DL)
	// 80 fa ff                cmp    dl,0xff
	code = new uint8_t [3] {0x80, 0xFA, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 2; [BP+SI]
	// 81 3a ff 01       cmp    WORD PTR [bp+si],0x1ff
	code = new uint8_t [4] {0x81, 0x3A, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x1ff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 2; [BP+SI]
	// 83 3a ff                cmp    WORD PTR [bp+si],0xffff
	code = new uint8_t [3] {0x83, 0x3A, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffffff, opc.source.imm_word_data);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// 81 3e ff 01 0a 0f    cmp    WORD PTR ds:0x1ff,0xf0a
	code = new uint8_t [6] {0x81, 0x3E, 0xFF, 0x01, 0x0A, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf0a, opc.source.imm_word_data);
	delete code;

	// mod = 01; + r/m = 3; [BP+DI+off8]
	// 83 7b ff 02             cmp    WORD PTR [bp+di-0x1],0x2
	code = new uint8_t [4] {0x83, 0x7B, 0xFF, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x02, opc.source.imm_word_data);
	delete code;

	// mod = 10; + r/m = 5; [DI+off16]
	// 81 bd ff 01 0a 0f    cmp    WORD PTR [di+0x1ff],0xf0a
	code = new uint8_t [6] {0x81, 0xBD, 0xFF, 0x01, 0x0A, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf0a, opc.source.imm_word_data);
	delete code;

	// mod = 11; + r/m = 6; register (SI)
	// 83 fe ff                cmp    si,0xffff
	code = new uint8_t [3] {0x83, 0xFE, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xffffffff, opc.source.imm_word_data);
	delete code;

	// --- r/m opcodes (38, 39) ---

	// CMP r/m8,reg8   => 38 [mod/reg/rm]
	// CMP r/m16,reg16 => 39 [mod/reg/rm]
	// cover all addressing modes
	// 00 + 000 + 101 (displacement only)
	// 01 + 000 + 000 + 1 byte offset
	// 10 + 000 + 000 + 4 byte offset
	// 11 + 000 + 000 (register addressing)

	// mod/reg/rm = 06
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 0 (AL)

	// 38 06 01 00       cmp    BYTE PTR ds:0x1,al
	code = new uint8_t [4] {0x38, 0x06, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 16
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 2 (DX)

	// 39 16 01 00       cmp    WORD PTR ds:0x1,dx
	code = new uint8_t [4] {0x39, 0x16, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x01, opc.dest.disponly_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 65
	// mod = 01; indirect register with one byte offset
	// reg = 4 (AH); r/m = 5 (DI)

	// 38 65 01                cmp    BYTE PTR [di+0x1],ah
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [3] {0x38, 0x65, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = 6F
	// mod = 01; indirect register with one byte offset
	// reg = 5 (BP); r/m = 7 (BX)

	// 39 6f 01                cmp    WORD PTR [bx+0x1],bp
	code = new uint8_t [3] {0x39, 0x6F, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 91
	// mod = 10; indirect register with four byte offset
	// reg = 2 (DL); r/m = 1 (BX+DI)

	// 38 91 00 f0       cmp    BYTE PTR [bx+di-0x1000],dl
	// todo: note how the offset is signed, check it works!!
	code = new uint8_t [4] {0x38, 0x91, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-4096, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = A3
	// mod = 10; indirect register with four byte offset
	// reg = 4 (SP); r/m = 3 (BP+DI)

	// 39 a3 00 f0       cmp    WORD PTR [bp+di-0x1000],sp
	code = new uint8_t [4] {0x39, 0xA3, 0x00, 0xF0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);		// indirect addressing means segment register size != instruction data size
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-4096, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = EC
	// mod = 11; register addressing
	// reg = 5 (CH); r/m = 4 (AH)

	// 38 ec                   cmp    ah,ch
	code = new uint8_t [2] {0x38, 0xEC};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// mod/reg/rm = FA
	// mod = 11; register addressing
	// reg = 7 (DI); r/m = 2 (DX)

	// 39 fa                   cmp    dx,di
	code = new uint8_t [2] {0x39, 0xFA};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

}

}
