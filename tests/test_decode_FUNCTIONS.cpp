#include "main_test.h"

namespace
{

// decode.h

TEST(decode_FUNCTIONS)
{

	mode32 = 1;

	// --- CALL: E8 ---

	// CALL imm   => E8 [imm32]
	// e8 01 00 00 10          call    0x10000006 [+268435457]
	uint8_t *code = new uint8_t [5] {0xE8, 0x01, 0x00, 0x00, 0x10};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(CALL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x10000001, opc.dest.imm_word_data);
	delete code;

	// CALL imm   => E8 [imm32]
	// e8 01 00 00 80          call    0x80000006 [-2147483647]
	code = new uint8_t [5] {0xE8, 0x01, 0x00, 0x00, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(CALL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-2147483647, opc.dest.imm_word_data);
	delete code;

	// --- CALL: FF /2 ---

	// CALL r/m32 => FF /2 []
	// mod = 00; + r/m = 1; [ECX]
	// ff 11                   call    DWORD PTR [ecx]
	code = new uint8_t [2] {0xFF, 0x11};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CALL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// ff 15 ff 00 00 01       call    DWORD PTR ds:0x10000ff
	code = new uint8_t [6] {0xFF, 0x15, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CALL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// mod = 01; + r/m = 5; [EBP+off8]
	// ff 55 ff                call    DWORD PTR [ebp-0x1]
	code = new uint8_t [3] {0xFF, 0x55, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CALL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// ff 92 ff 00 00 01       call    DWORD PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xFF, 0x92, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CALL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// mod = 11; + r/m = 0; register (EAX)
	// ff d0                   call    eax
	code = new uint8_t [2] {0xFF, 0xD0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CALL, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// --- CALL: FF /3 ---

	// CALL FAR mem32 => FF /3 []
	// mod = 00; + r/m = 1; [ECX]
	// ff 19                   call    FWORD PTR [ecx]
	code = new uint8_t [2] {0xFF, 0x19};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CALL_FAR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// ff 1d ff 00 00 01       call    FWORD PTR ds:0x10000ff
	code = new uint8_t [6] {0xFF, 0x1D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CALL_FAR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// mod = 01; + r/m = 5; [EBP+off8]
	// ff 5d ff                call    FWORD PTR [ebp-0x1]
	code = new uint8_t [3] {0xFF, 0x5D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CALL_FAR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// ff 9a ff 00 00 01       call    FWORD PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xFF, 0x9A, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(CALL_FAR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// mod = 11; + r/m = 0; register (EAX)
	// ff d8                   (bad)
	code = new uint8_t [2] {0xFF, 0xD8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// --- CALL: 9A ---

	// CALL imm:imm32 => 9A [offset:imm32] [segment:imm16]
	// 9a 01 20 30 40 f0 08    call    0x8f0:0x40302001
	code = new uint8_t [7] {0x9A, 0x01, 0x20, 0x30, 0x40, 0xF0, 0x08};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(CALL_FAR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x40302001, opc.dest.imm_word_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x8f0, opc.source.imm_word_data);
	delete code;

	// --- RET: C2, C3 ---

	// c2 01 20                ret    0x2001
	code = new uint8_t [3] {0xC2, 0x01, 0x20};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RET, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x2001, opc.dest.imm_word_data);
	delete code;

	// c3                      ret
	code = new uint8_t [3] {0xC3};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(RET, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.imm_word_data);
	delete code;

	// --- RETF: CA, CB ---

	// ca 01 00                retf   0x1
	code = new uint8_t [3] {0xCA, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RETF, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// cb                      ret
	code = new uint8_t [3] {0xCB};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(RETF, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.imm_word_data);
	delete code;

	// --- INT3: CC ---
	code = new uint8_t [1] {0xCC};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INT, opc.op);
	CHECK_EQUAL(NIL, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x03, opc.dest.imm_word_data);
	delete code;

	// --- INT X: CD ---
	code = new uint8_t [2] {0xCD, 0x21};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INT, opc.op);
	CHECK_EQUAL(NIL, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x21, opc.dest.imm_word_data);
	delete code;

	// --- INT1: F1 ---
	code = new uint8_t [1] {0xF1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INT, opc.op);
	CHECK_EQUAL(NIL, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// --- IRET: CF ---
	code = new uint8_t [1] {0xCF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(IRET, opc.op);
	CHECK_EQUAL(NIL, opc.datasize);
	delete code;

	mode32 = 0;

	// --- CALL: E8 ---

	// CALL imm   => E8 [imm16]
	// e8 01 10          call    0x1004 [+4097]
	code = new uint8_t [3] {0xE8, 0x01, 0x10};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CALL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x1001, opc.dest.imm_word_data);
	delete code;

	// CALL imm   => E8 [imm16]
	// e8 01 80          call    0x8004 [-32767]
	code = new uint8_t [3] {0xE8, 0x01, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CALL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-32767, (int)opc.dest.imm_word_data);
	delete code;

	// --- CALL: FF /2 ---

	// CALL r/m16 => FF /2 []
	// mod = 00; + r/m = 1; [BX+DI]
	// ff 11                   call    WORD PTR [bx+di]
	code = new uint8_t [2] {0xFF, 0x11};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CALL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// ff 16 ff 01       call    WORD PTR ds:0x1ff
	code = new uint8_t [4] {0xFF, 0x16, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CALL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	delete code;

	// mod = 01; + r/m = 5; [DI+off8]
	// ff 55 ff                call    WORD PTR [di-0x1]
	code = new uint8_t [3] {0xFF, 0x55, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CALL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// ff 92 ff 01       call    WORD PTR [bp+si+0x1ff]
	code = new uint8_t [4] {0xFF, 0x92, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CALL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	delete code;

	// mod = 11; + r/m = 0; register (AX)
	// ff d0                   call    ax
	code = new uint8_t [2] {0xFF, 0xD0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CALL, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// --- CALL: FF /3 ---

	// CALL FAR mem16 => FF /3 []
	// mod = 00; + r/m = 1; [BX+DI]
	// ff 19                   call    FAR WORD PTR [bx+di]
	code = new uint8_t [2] {0xFF, 0x19};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(CALL_FAR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// ff 1e ff 01       call    FAR WORD PTR ds:0x1ff
	code = new uint8_t [4] {0xFF, 0x1E, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CALL_FAR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	delete code;

	// mod = 01; + r/m = 5; [DI+off8]
	// ff 5d ff                call    FAR WORD PTR [di-0x1]
	code = new uint8_t [3] {0xFF, 0x5D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(CALL_FAR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// ff 9a ff 01       call    FAR WORD PTR [bp+si+0x1ff]
	code = new uint8_t [4] {0xFF, 0x9A, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(CALL_FAR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	delete code;

	// mod = 11; + r/m = 0; register (AX)
	// ff d8                   (bad)
	code = new uint8_t [2] {0xFF, 0xD8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);	// todo: check if this is indeed invalid, GRDB disassembles it as jmp ax
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// --- CALL: 9A ---

	// CALL imm:imm16 => 9A [offset:imm16] [segment:imm16]
	// 9a 01 20 f0 08    call    0x8f0:0x2001
	code = new uint8_t [5] {0x9A, 0x01, 0x20, 0xF0, 0x08};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(CALL_FAR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x2001, opc.dest.imm_word_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x8f0, opc.source.imm_word_data);
	delete code;

	// --- RET: C2, C3 ---

	// c2 01 20                ret    0x2001
	code = new uint8_t [3] {0xC2, 0x01, 0x20};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RET, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x2001, opc.dest.imm_word_data);
	delete code;

	// c3                      ret
	code = new uint8_t [3] {0xC3};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(RET, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.imm_word_data);
	delete code;

	// --- RETF: CA, CB ---

	// ca 01 00                retf   0x1
	code = new uint8_t [3] {0xCA, 0x01, 0x00};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(RETF, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// cb                      ret
	code = new uint8_t [3] {0xCB};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(RETF, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.imm_word_data);
	delete code;

	// --- INT3: CC ---
	code = new uint8_t [1] {0xCC};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INT, opc.op);
	CHECK_EQUAL(NIL, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x03, opc.dest.imm_word_data);
	delete code;

	// --- INT X: CD ---
	code = new uint8_t [2] {0xCD, 0x21};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INT, opc.op);
	CHECK_EQUAL(NIL, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x21, opc.dest.imm_word_data);
	delete code;

	// --- INT1: F1 ---
	code = new uint8_t [1] {0xF1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INT, opc.op);
	CHECK_EQUAL(NIL, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// --- IRET: CF ---
	code = new uint8_t [1] {0xCF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(IRET, opc.op);
	CHECK_EQUAL(NIL, opc.datasize);
	delete code;

}

}
