#include "main_test.h"

namespace
{

// dev_8259.h

TEST(dev_8259)
{

	// initialise cpu
	init_cpu();

	// initialise PIC controller
	CHECK_EQUAL(0, init_pic());

	// initialise IRQ lines
	for (int i=0; i<16; i++) {
		irq_request[i] = 0;
	}

	// check ports are available
	// write to port 20, 21, A0, A1
	CHECK(write_to_port[0x20]);
	CHECK(write_to_port[0x21]);
	CHECK(write_to_port[0xa0]);
	CHECK(write_to_port[0xa1]);
	// read from port 20, 21, A0, A1
	CHECK(read_from_port[0x20]);
	CHECK(read_from_port[0x21]);
	CHECK(read_from_port[0xa0]);
	CHECK(read_from_port[0xa1]);

	// no unmasked irq pending
	CHECK_EQUAL(0, check_irq_request());

	// acknowledge returns IRQ 7 (spurious)
	// note IRQ 7 = base vector offset (0x08) + 7 = 0x0f
	CHECK_EQUAL(0x0f, acknowledge_irq());

	// set an irq line
	irq_request[5] = 1;

	// IRQ 5 is unmasked
	CHECK_EQUAL(1, check_irq_request());

	// acknowledge returns IRQ 5
	// note IRQ 5 = base vector offset (0x08) + 5 = 0x0d
	CHECK_EQUAL(0x0d, acknowledge_irq());

	// once acknowledged, no unmasked irq pending
	CHECK_EQUAL(0, check_irq_request());

	// acknowledging more than once, returns IRQ 7 (spurious)
	CHECK_EQUAL(0x0f, acknowledge_irq());

	// send EOI
	write_to_port[0x20](0x20, 0x20, BYTE);

	// send EOI to 2nd PIC (not required but shouldn't do any harm)
	write_to_port[0xa0](0xa0, 0x20, BYTE);

	// mask IRQ 3
	int mask = read_from_port[0x21](0x21, BYTE);
	CHECK_EQUAL(0x00, mask);
	mask |= 0x08;
	write_to_port[0x21](0x21, mask, BYTE);
	// no unmasked irq pending
	irq_request[3] = 1;
	CHECK_EQUAL(0, check_irq_request());
	// irq 5 pending
	irq_request[5] = 1;
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x0d, acknowledge_irq());
	// send EOI
	write_to_port[0x20](0x20, 0x20, BYTE);
	write_to_port[0xa0](0xa0, 0x20, BYTE);

	// unmask IRQ 3
	mask = 0x00;
	write_to_port[0x21](0x21, mask, BYTE);
	// set irq line(s)
	irq_request[3] = 1;
	irq_request[12] = 1;
	// IRQ 12 has higher priority
	CHECK_EQUAL(1, check_irq_request());
	// note IRQ 12 = base vector offset (0x70) + 4 = 0x74
	CHECK_EQUAL(0x74, acknowledge_irq());
	// send EOI
	write_to_port[0x20](0x20, 0x20, BYTE);
	write_to_port[0xa0](0xa0, 0x20, BYTE);
	// IRQ 3 is unmasked
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x0b, acknowledge_irq());
	// send EOI
	write_to_port[0x20](0x20, 0x20, BYTE);
	write_to_port[0xa0](0xa0, 0x20, BYTE);

	// no unmasked irq pending
	CHECK_EQUAL(0, check_irq_request());

	// acknowledge returns IRQ 7 (spurious)
	CHECK_EQUAL(0x0f, acknowledge_irq());

	// mask IRQ 2
	mask = read_from_port[0x21](0x21, BYTE);
	CHECK_EQUAL(0x00, mask);
	mask |= 0x04;
	write_to_port[0x21](0x21, mask, BYTE);
	// set irq line(s)
	irq_request[2] = 1;
	irq_request[8] = 1;
	irq_request[9] = 1;
	irq_request[10] = 1;
	irq_request[11] = 1;
	irq_request[12] = 1;
	irq_request[13] = 1;
	irq_request[14] = 1;
	irq_request[15] = 1;
	// no unmasked irq pending
	CHECK_EQUAL(0, check_irq_request());
	// acknowledge returns IRQ 7 (spurious)
	CHECK_EQUAL(0x0f, acknowledge_irq());
	// set an irq line on the master PIC
	irq_request[0] = 1;
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x08, acknowledge_irq());
	// send EOI
	write_to_port[0x20](0x20, 0x20, BYTE);
	write_to_port[0xa0](0xa0, 0x20, BYTE);
	// no unmasked irq pending
	CHECK_EQUAL(0, check_irq_request());
	// acknowledge returns IRQ 7 (spurious)
	CHECK_EQUAL(0x0f, acknowledge_irq());
	// unmask IRQ 2
	mask = 0x00;
	write_to_port[0x21](0x21, mask, BYTE);
	// irq 2 pending
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x0a, acknowledge_irq());
	// send EOI
	write_to_port[0x20](0x20, 0x20, BYTE);
	write_to_port[0xa0](0xa0, 0x20, BYTE);

	// set all IRQ lines
	for (int i=0; i<16; i++) {
		irq_request[i] = 1;
	}
	// IRQ 0
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x08, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	// IRQ 1
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x09, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	// IRQ 2
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x0a, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	// IRQ 8
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x70, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	write_to_port[0xa0](0xa0, 0x20, BYTE);
	// IRQ 9
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x71, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	write_to_port[0xa0](0xa0, 0x20, BYTE);
	// IRQ 10
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x72, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	write_to_port[0xa0](0xa0, 0x20, BYTE);
	// IRQ 11
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x73, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	write_to_port[0xa0](0xa0, 0x20, BYTE);
	// IRQ 12
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x74, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	write_to_port[0xa0](0xa0, 0x20, BYTE);
	// IRQ 13
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x75, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	write_to_port[0xa0](0xa0, 0x20, BYTE);
	// IRQ 14
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x76, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	write_to_port[0xa0](0xa0, 0x20, BYTE);
	// IRQ 15
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x77, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	write_to_port[0xa0](0xa0, 0x20, BYTE);
	// IRQ 3
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x0b, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	// IRQ 4
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x0c, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	// IRQ 5
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x0d, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	// IRQ 6
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x0e, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);
	// IRQ 7
	CHECK_EQUAL(1, check_irq_request());
	CHECK_EQUAL(0x0f, acknowledge_irq());
	write_to_port[0x20](0x20, 0x20, BYTE);

	// no unmasked irq pending
	CHECK_EQUAL(0, check_irq_request());

	// acknowledge returns IRQ 7 (spurious)
	// note IRQ 7 = base vector offset (0x08) + 7 = 0x0f
	CHECK_EQUAL(0x0f, acknowledge_irq());

	// shutdown
	shutdown_pic();

}

}
