#include "main_test.h"

namespace
{

// decode.h

TEST(decode_JMP)
{

	mode32 = 1;

	// --- JMP: EB, E9 ---

	// JMP SHORT imm   => EB [imm8]
	// eb 01                   jmp    0x3 [+1] (new address is calculated from the start of the following instruction)
	uint8_t *code = new uint8_t [2] {0xEB, 0x01};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JMP SHORT imm   => EB [imm8]
	// eb 80                   jmp    0xffffff82 [-128]
	code = new uint8_t [2] {0xEB, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JMP imm   => E9 [imm32]
	// e9 01 00 00 10          jmp    0x10000006 [+268435457]
	code = new uint8_t [5] {0xE9, 0x01, 0x00, 0x00, 0x10};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x10000001, opc.dest.imm_word_data);
	delete code;

	// JMP imm   => E9 [imm32]
	// e9 01 00 00 80          jmp    0x80000006 [-2147483647]
	code = new uint8_t [5] {0xE9, 0x01, 0x00, 0x00, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-2147483647, opc.dest.imm_word_data);
	delete code;

	// --- JMP: FF /4 ---

	// JMP r/m32 => FF /4 []
	// mod = 00; + r/m = 1; [ECX]
	// ff 21                   jmp    DWORD PTR [ecx]
	code = new uint8_t [2] {0xFF, 0x21};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// ff 25 ff 00 00 01       jmp    DWORD PTR ds:0x10000ff
	code = new uint8_t [6] {0xFF, 0x25, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// mod = 01; + r/m = 5; [EBP+off8]
	// ff 65 ff                jmp    DWORD PTR [ebp-0x1]
	code = new uint8_t [3] {0xFF, 0x65, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// ff a2 ff 00 00 01       jmp    DWORD PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xFF, 0xA2, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// mod = 11; + r/m = 0; register (EAX)
	// ff e0                   jmp    eax
	code = new uint8_t [2] {0xFF, 0xE0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// --- JMP: FF /5 ---

	// JMP FAR mem32 => FF /5 []
	// mod = 00; + r/m = 1; [ECX]
	// ff 29                   jmp    FWORD PTR [ecx]
	code = new uint8_t [2] {0xFF, 0x29};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JMP_FAR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// mod = 00; + r/m = 5; 32 bit displacement only
	// ff 2d ff 00 00 01       jmp    FWORD PTR ds:0x10000ff
	code = new uint8_t [6] {0xFF, 0x2D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JMP_FAR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// mod = 01; + r/m = 5; [EBP+off8]
	// ff 6d ff                jmp    FWORD PTR [ebp-0x1]
	code = new uint8_t [3] {0xFF, 0x6D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(JMP_FAR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// mod = 10; + r/m = 2; [EDX+off32]
	// ff aa ff 00 00 01       jmp    FWORD PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xFF, 0xAA, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(JMP_FAR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// mod = 11; + r/m = 0; register (EAX)
	// ff e8                   (bad)
	code = new uint8_t [2] {0xFF, 0xE8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// --- JMP: EA ---

	// JMP imm:imm32 => EA [offset:imm32] [segment:imm16]
	// ea 01 20 30 40 f0 08    jmp    0x8f0:0x40302001
	code = new uint8_t [7] {0xEA, 0x01, 0x20, 0x30, 0x40, 0xF0, 0x08};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(JMP_FAR, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x40302001, opc.dest.imm_word_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x8f0, opc.source.imm_word_data);
	delete code;

}

TEST(decode_JMP_16bit)
{

	mode32 = 0;

	// --- JMP: EB, E9 ---

	// JMP SHORT imm   => EB [imm8]
	// eb 01                   jmp    0x3 [+1] (new address is calculated from the start of the following instruction)
	uint8_t *code = new uint8_t [2] {0xEB, 0x01};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x01, opc.dest.imm_word_data);
	delete code;

	// JMP SHORT imm   => EB [imm8]
	// eb 80                   jmp    0xff82 [-128]
	code = new uint8_t [2] {0xEB, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-128, opc.dest.imm_word_data);
	delete code;

	// JMP imm   => E9 [imm16]
	// e9 01 10          jmp    0x1004 [+4097]
	code = new uint8_t [3] {0xE9, 0x01, 0x10};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x1001, opc.dest.imm_word_data);
	delete code;

	// JMP imm   => E9 [imm16]
	// e9 01 80          jmp    0x8004 [-32767]
	code = new uint8_t [3] {0xE9, 0x01, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(-32767, (int)opc.dest.imm_word_data);
	delete code;

	// --- JMP: FF /4 ---

	// JMP r/m16 => FF /4 []
	// mod = 00; + r/m = 1; [BX+DI]
	// ff 21                   jmp    WORD PTR [bx+di]
	code = new uint8_t [2] {0xFF, 0x21};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// ff 26 ff 01       jmp    WORD PTR ds:0x1ff
	code = new uint8_t [4] {0xFF, 0x26, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	delete code;

	// mod = 01; + r/m = 5; [DI+off8]
	// ff 65 ff                jmp    WORD PTR [di-0x1]
	code = new uint8_t [3] {0xFF, 0x65, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// ff a2 ff 01       jmp    WORD PTR [bp+si+0x1ff]
	code = new uint8_t [4] {0xFF, 0xA2, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	delete code;

	// mod = 11; + r/m = 0; register (AX)
	// ff e0                   jmp    ax
	code = new uint8_t [2] {0xFF, 0xE0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JMP, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// --- JMP: FF /5 ---

	// JMP FAR mem16 => FF /5 []
	// mod = 00; + r/m = 1; [BX+DI]
	// ff 29                   jmp    FAR WORD PTR [bx+di]
	code = new uint8_t [2] {0xFF, 0x29};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(JMP_FAR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// mod = 00; + r/m = 6; 16 bit displacement only
	// ff 2e ff 01       jmp    FAR WORD PTR ds:0x1ff
	code = new uint8_t [4] {0xFF, 0x2E, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JMP_FAR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	delete code;

	// mod = 01; + r/m = 5; [DI+off8]
	// ff 6d ff                jmp    FAR WORD PTR [di-0x1]
	code = new uint8_t [3] {0xFF, 0x6D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(JMP_FAR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// mod = 10; + r/m = 2; [BP+SI+off16]
	// ff aa ff 01       jmp    FAR WORD PTR [bp+si+0x1ff]
	code = new uint8_t [4] {0xFF, 0xAA, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(JMP_FAR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	delete code;

	// mod = 11; + r/m = 0; register (AX)
	// ff e8                   (bad)
	code = new uint8_t [2] {0xFF, 0xE8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);	// todo: check if this is indeed invalid, GRDB disassembles it as jmp ax
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// --- JMP: EA ---

	// JMP imm:imm16 => EA [offset:imm16] [segment:imm16]
	// ea 01 20 f0 08    jmp    0x8f0:0x2001
	code = new uint8_t [5] {0xEA, 0x01, 0x20, 0xF0, 0x08};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(JMP_FAR, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_imm_word);
	CHECK_EQUAL(0x2001, opc.dest.imm_word_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0x8f0, opc.source.imm_word_data);
	delete code;

}

}
