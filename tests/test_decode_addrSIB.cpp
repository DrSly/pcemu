#include "main_test.h"

namespace
{

// decode.h

TEST(decode_addr_SIB)
{
	// test cases for 32 bit SIB addressing modes

	mode32 = 1;

	// --- SIB:
	// mod/rm = 00/100: SIB Mode
	// mod/rm = 01/100: SIB + disp8
	// mod/rm = 10/100: SIB + disp32

	// SIB Mode
	// mod = 00; rm = 100
	// s = 00; index = 000 (eax); base = 111 (edi)
	// 80 24 07 ff             and    BYTE PTR [edi+eax*1],0xff
	uint8_t *code = new uint8_t [4] {0x80, 0x24, 0x07, 0xFF};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.dest.sib_index);
	CHECK_EQUAL(1, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// SIB  + disp8
	// mod = 01; rm = 100
	// s = 01; index = 000 (eax); base = 111 (edi)
	// 80 64 47 01 ff          and    BYTE PTR [edi+eax*2+0x1],0xff
	code = new uint8_t [5] {0x80, 0x64, 0x47, 0x01, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.dest.sib_index);
	CHECK_EQUAL(2, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// SIB  + disp32
	// mod = 10; rm = 100
	// s = 10; index = 001 (ecx); base = 100 (esp)
	// 80 a4 8c 01 00 00 0f ff    and    BYTE PTR [esp+ecx*4+0xf000001],0xff
	code = new uint8_t [8] {0x80, 0xA4, 0x8C, 0x01, 0x00, 0x00, 0x0F, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(8, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.sib_index);
	CHECK_EQUAL(4, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0xf000001, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// index*scale  + disp32
	// mod = 00; rm = 100
	// s = 11; index = 000 (eax); base = 101 (ebp*)
	// * mod = 00 + base = 101 interpreted as displacement only
	// 80 24 c5 01 00 00 0f ff    and    BYTE PTR [eax*8+0xf000001],0xff
	code = new uint8_t [8] {0x80, 0x24, 0xC5, 0x01, 0x00, 0x00, 0x0F, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(8, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0xf000001, opc.dest.disponly_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.dest.sib_index);
	CHECK_EQUAL(8, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// SIB  + disp8 (ebp)
	// mod = 01; rm = 100
	// s = 10; index = 010 (edx); base = 101 (ebp)
	// 80 64 95 01 ff          and    BYTE PTR [ebp+edx*4+0x1],0xff
	code = new uint8_t [5] {0x80, 0x64, 0x95, 0x01, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(dl_dx_edx, opc.dest.sib_index);
	CHECK_EQUAL(4, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// base only (eiz)
	// mod = 00; rm = 100
	// s = 10; index = 100 (esp*); base = 001 (ecx)
	// * index = 100 interpreted as no index (base only)
	// 80 24 a1 ff             and    BYTE PTR [ecx+eiz*4],0xff
	code = new uint8_t [4] {0x80, 0x24, 0xA1, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(ah_sp_esp, opc.dest.sib_index);
	CHECK_EQUAL(0, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// base  + disp8 (eiz)
	// mod = 01; rm = 100
	// s = 10; index = 100 (esp*); base = 001 (ecx)
	// * index = 100 interpreted as no index (base only)
	// 80 64 a1 01 ff          and    BYTE PTR [ecx+eiz*4+0x1],0xff
	code = new uint8_t [5] {0x80, 0x64, 0xA1, 0x01, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(ah_sp_esp, opc.dest.sib_index);
	CHECK_EQUAL(0, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xff, opc.source.imm_word_data);
	delete code;

	// SIB Mode
	// mod = 00; rm = 100
	// s = 00; index = 000 (eax); base = 111 (edi)
	// 81 24 07 01 00 00 0f    and    DWORD PTR [edi+eax*1],0xf000001
	code = new uint8_t [7] {0x81, 0x24, 0x07, 0x01, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.dest.sib_index);
	CHECK_EQUAL(1, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf000001, opc.source.imm_word_data);
	delete code;

	// SIB  + disp8
	// mod = 01; rm = 100
	// s = 01; index = 000 (eax); base = 111 (edi)
	// 81 64 47 01 01 00 00 0f    and    DWORD PTR [edi+eax*2+0x1],0xf000001
	code = new uint8_t [8] {0x81, 0x64, 0x47, 0x01, 0x01, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(8, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.dest.sib_index);
	CHECK_EQUAL(2, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf000001, opc.source.imm_word_data);
	delete code;

	// SIB  + disp32
	// mod = 10; rm = 100
	// s = 10; index = 001 (ecx); base = 100 (esp)
	// 81 a4 8c 01 00 00 80 01 00 00 0f    and    DWORD PTR [esp+ecx*4-0x7fffffff],0xf000001
	code = new uint8_t [11] {0x81, 0xA4, 0x8C, 0x01, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(11, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.sib_index);
	CHECK_EQUAL(4, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x80000001, opc.dest.offset_data);
	CHECK_EQUAL(-2147483647, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf000001, opc.source.imm_word_data);
	delete code;

	// index*scale  + disp32
	// mod = 00; rm = 100
	// s = 11; index = 000 (eax); base = 101 (ebp*)
	// * mod = 00 + base = 101 interpreted as displacement only
	// 81 24 c5 01 00 00 80 01 00 00 0f    and    DWORD PTR [eax*8-0x7fffffff],0xf000001
	code = new uint8_t [11] {0x81, 0x24, 0xC5, 0x01, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(11, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x80000001, opc.dest.disponly_data);
	CHECK_EQUAL(-2147483647, opc.dest.disponly_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.dest.sib_index);
	CHECK_EQUAL(8, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf000001, opc.source.imm_word_data);
	delete code;

	// SIB  + disp32 (ebp)
	// mod = 10; rm = 100
	// s = 10; index = 011 (ebx); base = 101 (ebp)
	// 81 a4 9d 01 00 00 80 01 00 00 0f    and    DWORD PTR [ebp+ebx*4-0x7fffffff],0xf000001
	code = new uint8_t [11] {0x81, 0xA4, 0x9D, 0x01, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(11, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.sib_index);
	CHECK_EQUAL(4, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x80000001, opc.dest.offset_data);
	CHECK_EQUAL(-2147483647, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf000001, opc.source.imm_word_data);
	delete code;

	// base only (eiz)
	// mod = 00; rm = 100
	// s = 10; index = 100 (esp*); base = 011 (ebx)
	// * index = 100 interpreted as no index (base only)
	// 81 24 a3 01 00 00 0f    and    DWORD PTR [ebx+eiz*4],0xf000001
	code = new uint8_t [7] {0x81, 0x24, 0xA3, 0x01, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(ah_sp_esp, opc.dest.sib_index);
	CHECK_EQUAL(0, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf000001, opc.source.imm_word_data);
	delete code;

	// base  + disp32 (eiz)
	// mod = 10; rm = 100
	// s = 10; index = 100 (esp*); base = 011 (ebx)
	// * index = 100 interpreted as no index (base only)
	// 81 a4 a3 01 00 00 80 01 00 00 0f    and    DWORD PTR [ebx+eiz*4-0x7fffffff],0xf000001
	code = new uint8_t [11] {0x81, 0xA4, 0xA3, 0x01, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(11, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(ah_sp_esp, opc.dest.sib_index);
	CHECK_EQUAL(0, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x80000001, opc.dest.offset_data);
	CHECK_EQUAL(-2147483647, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_imm_word);
	CHECK_EQUAL(0xf000001, opc.source.imm_word_data);
	delete code;

	// SIB Mode
	// mod = 00; rm = 100
	// s = 00; index = 000 (eax); base = 111 (edi)
	// 20 0c 07                and    BYTE PTR [edi+eax*1],cl
	code = new uint8_t [3] {0x20, 0x0C, 0x07};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.dest.sib_index);
	CHECK_EQUAL(1, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// SIB  + disp8
	// mod = 01; rm = 100
	// s = 01; index = 000 (eax); base = 111 (edi)
	// 20 54 47 01             and    BYTE PTR [edi+eax*2+0x1],dl
	code = new uint8_t [4] {0x20, 0x54, 0x47, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.dest.sib_index);
	CHECK_EQUAL(2, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// SIB  + disp32
	// mod = 10; rm = 100
	// s = 10; index = 001 (ecx); base = 100 (esp)
	// 20 9c 8c 01 00 00 0f    and    BYTE PTR [esp+ecx*4+0xf000001],bl
	code = new uint8_t [7] {0x20, 0x9C, 0x8C, 0x01, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.sib_index);
	CHECK_EQUAL(4, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0xf000001, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// index*scale  + disp32
	// mod = 00; rm = 100
	// s = 11; index = 000 (eax); base = 101 (ebp*)
	// * mod = 00 + base = 101 interpreted as displacement only
	// 20 24 c5 01 00 00 0f    and    BYTE PTR [eax*8+0xf000001],ah
	code = new uint8_t [7] {0x20, 0x24, 0xC5, 0x01, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0xf000001, opc.dest.disponly_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.dest.sib_index);
	CHECK_EQUAL(8, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// SIB  + disp8 (ebp)
	// mod = 01; rm = 100
	// s = 10; index = 010 (edx); base = 101 (ebp)
	// 20 6c 95 01             and    BYTE PTR [ebp+edx*4+0x1],ch
	code = new uint8_t [4] {0x20, 0x6C, 0x95, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(dl_dx_edx, opc.dest.sib_index);
	CHECK_EQUAL(4, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// base only (eiz)
	// mod = 00; rm = 100
	// s = 10; index = 100 (esp*); base = 001 (ecx)
	// * index = 100 interpreted as no index (base only)
	// 20 34 a1                and    BYTE PTR [ecx+eiz*4],dh
	code = new uint8_t [3] {0x20, 0x34, 0xA1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(ah_sp_esp, opc.dest.sib_index);
	CHECK_EQUAL(0, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// base  + disp8 (eiz)
	// mod = 01; rm = 100
	// s = 10; index = 100 (esp*); base = 001 (ecx)
	// * index = 100 interpreted as no index (base only)
	// 20 7c a1 01             and    BYTE PTR [ecx+eiz*4+0x1],bh
	code = new uint8_t [4] {0x20, 0x7C, 0xA1, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(ah_sp_esp, opc.dest.sib_index);
	CHECK_EQUAL(0, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(BYTE, opc.source.register_size);
	delete code;

	// SIB Mode
	// mod = 00; rm = 100
	// s = 00; index = 000 (eax); base = 111 (edi)
	// 21 04 07                and    DWORD PTR [edi+eax*1],eax
	code = new uint8_t [3] {0x21, 0x04, 0x07};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.dest.sib_index);
	CHECK_EQUAL(1, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// SIB  + disp8
	// mod = 01; rm = 100
	// s = 01; index = 000 (eax); base = 111 (edi)
	// 21 4c 47 01             and    DWORD PTR [edi+eax*2+0x1],ecx
	code = new uint8_t [4] {0x21, 0x4C, 0x47, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.dest.sib_index);
	CHECK_EQUAL(2, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x01, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// SIB  + disp32
	// mod = 10; rm = 100
	// s = 10; index = 001 (ecx); base = 100 (esp)
	// 21 94 8c 01 00 00 80    and    DWORD PTR [esp+ecx*4-0x7fffffff],edx
	code = new uint8_t [7] {0x21, 0x94, 0x8C, 0x01, 0x00, 0x00, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.sib_index);
	CHECK_EQUAL(4, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x80000001, opc.dest.offset_data);
	CHECK_EQUAL(-2147483647, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// index*scale  + disp32
	// mod = 00; rm = 100
	// s = 11; index = 000 (eax); base = 101 (ebp*)
	// * mod = 00 + base = 101 interpreted as displacement only
	// 21 1c c5 01 00 00 80    and    DWORD PTR [eax*8-0x7fffffff],ebx
	code = new uint8_t [7] {0x21, 0x1C, 0xC5, 0x01, 0x00, 0x00, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x80000001, opc.dest.disponly_data);
	CHECK_EQUAL(-2147483647, opc.dest.disponly_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.dest.sib_index);
	CHECK_EQUAL(8, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// SIB  + disp32 (ebp)
	// mod = 10; rm = 100
	// s = 10; index = 011 (ebx); base = 101 (ebp)
	// 21 a4 9d 01 00 00 80    and    DWORD PTR [ebp+ebx*4-0x7fffffff],esp
	code = new uint8_t [7] {0x21, 0xA4, 0x9D, 0x01, 0x00, 0x00, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.sib_index);
	CHECK_EQUAL(4, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x80000001, opc.dest.offset_data);
	CHECK_EQUAL(-2147483647, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// base only (eiz)
	// mod = 00; rm = 100
	// s = 10; index = 100 (esp*); base = 011 (ebx)
	// * index = 100 interpreted as no index (base only)
	// 21 2c a3                and    DWORD PTR [ebx+eiz*4],ebp
	code = new uint8_t [3] {0x21, 0x2C, 0xA3};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(ah_sp_esp, opc.dest.sib_index);
	CHECK_EQUAL(0, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// base  + disp32 (eiz)
	// mod = 10; rm = 100
	// s = 10; index = 100 (esp*); base = 011 (ebx)
	// * index = 100 interpreted as no index (base only)
	// 21 b4 a3 01 00 00 80    and    DWORD PTR [ebx+eiz*4-0x7fffffff],esi
	code = new uint8_t [7] {0x21, 0xB4, 0xA3, 0x01, 0x00, 0x00, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_sib);
	CHECK_EQUAL(ah_sp_esp, opc.dest.sib_index);
	CHECK_EQUAL(0, opc.dest.sib_scale);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x80000001, opc.dest.offset_data);
	CHECK_EQUAL(-2147483647, opc.dest.offset_data);
	CHECK_EQUAL(1, opc.source.use_register);
	CHECK_EQUAL(dh_si_esi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// SIB Mode
	// mod = 00; rm = 100
	// s = 00; index = 000 (eax); base = 111 (edi)
	// 22 3c 07                and    bh,BYTE PTR [edi+eax*1]
	code = new uint8_t [3] {0x22, 0x3C, 0x07};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.source.sib_index);
	CHECK_EQUAL(1, opc.source.sib_scale);
	delete code;

	// SIB  + disp8
	// mod = 01; rm = 100
	// s = 01; index = 000 (eax); base = 111 (edi)
	// 22 44 47 01             and    al,BYTE PTR [edi+eax*2+0x1]
	code = new uint8_t [4] {0x22, 0x44, 0x47, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.source.sib_index);
	CHECK_EQUAL(2, opc.source.sib_scale);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// SIB  + disp32
	// mod = 10; rm = 100
	// s = 10; index = 001 (ecx); base = 100 (esp)
	// 22 8c 8c 01 00 00 0f    and    cl,BYTE PTR [esp+ecx*4+0xf000001]
	code = new uint8_t [7] {0x22, 0x8C, 0x8C, 0x01, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_sib);
	CHECK_EQUAL(cl_cx_ecx, opc.source.sib_index);
	CHECK_EQUAL(4, opc.source.sib_scale);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0xf000001, opc.source.offset_data);
	delete code;

	// index*scale  + disp32
	// mod = 00; rm = 100
	// s = 11; index = 000 (eax); base = 101 (ebp*)
	// * mod = 00 + base = 101 interpreted as displacement only
	// 22 14 c5 01 00 00 0f    and    dl,BYTE PTR [eax*8+0xf000001]
	code = new uint8_t [7] {0x22, 0x14, 0xC5, 0x01, 0x00, 0x00, 0x0F};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0xf000001, opc.source.disponly_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.source.sib_index);
	CHECK_EQUAL(8, opc.source.sib_scale);
	delete code;

	// SIB  + disp8 (ebp)
	// mod = 01; rm = 100
	// s = 10; index = 010 (edx); base = 101 (ebp)
	// 22 5c 95 01             and    bl,BYTE PTR [ebp+edx*4+0x1]
	code = new uint8_t [4] {0x22, 0x5C, 0x95, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_sib);
	CHECK_EQUAL(dl_dx_edx, opc.source.sib_index);
	CHECK_EQUAL(4, opc.source.sib_scale);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// base only (eiz)
	// mod = 00; rm = 100
	// s = 10; index = 100 (esp*); base = 001 (ecx)
	// * index = 100 interpreted as no index (base only)
	// 22 24 a1                and    ah,BYTE PTR [ecx+eiz*4]
	code = new uint8_t [3] {0x22, 0x24, 0xA1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_sib);
	CHECK_EQUAL(ah_sp_esp, opc.source.sib_index);
	CHECK_EQUAL(0, opc.source.sib_scale);
	delete code;

	// base  + disp8 (eiz)
	// mod = 01; rm = 100
	// s = 10; index = 100 (esp*); base = 001 (ecx)
	// * index = 100 interpreted as no index (base only)
	// 22 6c a1 01             and    ch,BYTE PTR [ecx+eiz*4+0x1]
	code = new uint8_t [4] {0x22, 0x6C, 0xA1, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_sib);
	CHECK_EQUAL(ah_sp_esp, opc.source.sib_index);
	CHECK_EQUAL(0, opc.source.sib_scale);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// SIB Mode
	// mod = 00; rm = 100
	// s = 00; index = 000 (eax); base = 111 (edi)
	// 23 34 07                and    esi,DWORD PTR [edi+eax*1]
	code = new uint8_t [3] {0x23, 0x34, 0x07};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dh_si_esi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.source.sib_index);
	CHECK_EQUAL(1, opc.source.sib_scale);
	delete code;

	// SIB  + disp8
	// mod = 01; rm = 100
	// s = 01; index = 000 (eax); base = 111 (edi)
	// 23 7c 47 01             and    edi,DWORD PTR [edi+eax*2+0x1]
	code = new uint8_t [4] {0x23, 0x7C, 0x47, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bh_di_edi, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bh_di_edi, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.source.sib_index);
	CHECK_EQUAL(2, opc.source.sib_scale);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x01, opc.source.offset_data);
	delete code;

	// SIB  + disp32
	// mod = 10; rm = 100
	// s = 10; index = 001 (ecx); base = 100 (esp)
	// 23 84 8c 01 00 00 80    and    eax,DWORD PTR [esp+ecx*4-0x7fffffff]
	code = new uint8_t [7] {0x23, 0x84, 0x8C, 0x01, 0x00, 0x00, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ah_sp_esp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_sib);
	CHECK_EQUAL(cl_cx_ecx, opc.source.sib_index);
	CHECK_EQUAL(4, opc.source.sib_scale);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x80000001, opc.source.offset_data);
	CHECK_EQUAL(-2147483647, opc.source.offset_data);
	delete code;

	// index*scale  + disp32
	// mod = 00; rm = 100
	// s = 11; index = 000 (eax); base = 101 (ebp*)
	// * mod = 00 + base = 101 interpreted as displacement only
	// 23 0c c5 01 00 00 80    and    ecx,DWORD PTR [eax*8-0x7fffffff]
	code = new uint8_t [7] {0x23, 0x0C, 0xC5, 0x01, 0x00, 0x00, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x80000001, opc.source.disponly_data);
	CHECK_EQUAL(-2147483647, opc.source.disponly_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_sib);
	CHECK_EQUAL(al_ax_eax, opc.source.sib_index);
	CHECK_EQUAL(8, opc.source.sib_scale);
	delete code;

	// SIB  + disp32 (ebp)
	// mod = 10; rm = 100
	// s = 10; index = 011 (ebx); base = 101 (ebp)
	// 23 94 9d 01 00 00 80    and    edx,DWORD PTR [ebp+ebx*4-0x7fffffff]
	code = new uint8_t [7] {0x23, 0x94, 0x9D, 0x01, 0x00, 0x00, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_sib);
	CHECK_EQUAL(bl_bx_ebx, opc.source.sib_index);
	CHECK_EQUAL(4, opc.source.sib_scale);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x80000001, opc.source.offset_data);
	CHECK_EQUAL(-2147483647, opc.source.offset_data);
	delete code;

	// base only (eiz)
	// mod = 00; rm = 100
	// s = 10; index = 100 (esp*); base = 011 (ebx)
	// * index = 100 interpreted as no index (base only)
	// 23 1c a3                and    ebx,DWORD PTR [ebx+eiz*4]
	code = new uint8_t [3] {0x23, 0x1C, 0xA3};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_sib);
	CHECK_EQUAL(ah_sp_esp, opc.source.sib_index);
	CHECK_EQUAL(0, opc.source.sib_scale);
	delete code;

	// base  + disp32 (eiz)
	// mod = 10; rm = 100
	// s = 10; index = 100 (esp*); base = 011 (ebx)
	// * index = 100 interpreted as no index (base only)
	// 23 a4 a3 01 00 00 80    and    esp,DWORD PTR [ebx+eiz*4-0x7fffffff]
	code = new uint8_t [7] {0x23, 0xA4, 0xA3, 0x01, 0x00, 0x00, 0x80};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(AND, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(ah_sp_esp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_sib);
	CHECK_EQUAL(ah_sp_esp, opc.source.sib_index);
	CHECK_EQUAL(0, opc.source.sib_scale);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x80000001, opc.source.offset_data);
	CHECK_EQUAL(-2147483647, opc.source.offset_data);
	delete code;

}

}
