#include "main_test.h"

namespace
{

// dev_16550.h

TEST(dev_16550)
{

	// initialise cpu
	init_cpu();

	// initialise UART
	CHECK_EQUAL(0, init_uart());

	// check ports are available
	for (int i=0; i<8; i++) {
		CHECK(write_to_port[0x3f8 + i]);
		CHECK(read_from_port[0x3f8 + i]);
	}
	for (int i=0; i<8; i++) {
		CHECK(write_to_port[0x2f8 + i]);
		CHECK(read_from_port[0x2f8 + i]);
	}
	for (int i=0; i<8; i++) {
		CHECK(write_to_port[0x3e8 + i]);
		CHECK(read_from_port[0x3e8 + i]);
	}
	for (int i=0; i<8; i++) {
		CHECK(write_to_port[0x2e8 + i]);
		CHECK(read_from_port[0x2e8 + i]);
	}

	// BASE + 0 (DLAB bit set); Divisor Latch (low)
	// BASE + 1 (DLAB bit set); Divisor Latch (high)
	// first set the DLAB in the Line Control Register
	int dlab = read_from_port[0x3f8+3](0x3f8+3, BYTE);
	dlab |= 0x80;
	write_to_port[0x3f8+3](0x3f8+3, dlab, BYTE);
	dlab = read_from_port[0x2f8+3](0x2f8+3, BYTE);
	dlab |= 0x80;
	write_to_port[0x2f8+3](0x2f8+3, dlab, BYTE);
	dlab = read_from_port[0x3e8+3](0x3e8+3, BYTE);
	dlab |= 0x80;
	write_to_port[0x3e8+3](0x3e8+3, dlab, BYTE);
	dlab = read_from_port[0x2e8+3](0x2e8+3, BYTE);
	dlab |= 0x80;
	write_to_port[0x2e8+3](0x2e8+3, dlab, BYTE);
	// 0006 = 19200 baud
	write_to_port[0x3f8+0](0x3f8+0, 0x06, BYTE);
	write_to_port[0x3f8+1](0x3f8+1, 0x00, BYTE);
	CHECK_EQUAL(0x06, read_from_port[0x3f8+0](0x3f8+0, BYTE));
	CHECK_EQUAL(0x00, read_from_port[0x3f8+1](0x3f8+1, BYTE));
	// 0180 = 300 baud
	write_to_port[0x2f8+0](0x2f8+0, 0x80, BYTE);
	write_to_port[0x2f8+1](0x2f8+1, 0x01, BYTE);
	CHECK_EQUAL(0x80, read_from_port[0x2f8+0](0x2f8+0, BYTE));
	CHECK_EQUAL(0x01, read_from_port[0x2f8+1](0x2f8+1, BYTE));
	// 0003 = 38400 baud
	write_to_port[0x3e8+0](0x3e8+0, 0x06, BYTE);
	write_to_port[0x3e8+1](0x3e8+1, 0x00, BYTE);
	CHECK_EQUAL(0x06, read_from_port[0x3e8+0](0x3e8+0, BYTE));
	CHECK_EQUAL(0x00, read_from_port[0x3e8+1](0x3e8+1, BYTE));
	// 0900 = 50 baud
	write_to_port[0x2e8+0](0x2e8+0, 0x00, BYTE);
	write_to_port[0x2e8+1](0x2e8+1, 0x09, BYTE);
	CHECK_EQUAL(0x00, read_from_port[0x2e8+0](0x2e8+0, BYTE));
	CHECK_EQUAL(0x09, read_from_port[0x2e8+1](0x2e8+1, BYTE));
	// finally clear the DLAB in the Line Control Register
	dlab = read_from_port[0x3f8+3](0x3f8+3, BYTE);
	dlab &= 0x7f;
	write_to_port[0x3f8+3](0x3f8+3, dlab, BYTE);
	dlab = read_from_port[0x2f8+3](0x2f8+3, BYTE);
	dlab &= 0x7f;
	write_to_port[0x2f8+3](0x2f8+3, dlab, BYTE);
	dlab = read_from_port[0x3e8+3](0x3e8+3, BYTE);
	dlab &= 0x7f;
	write_to_port[0x3e8+3](0x3e8+3, dlab, BYTE);
	dlab = read_from_port[0x2e8+3](0x2e8+3, BYTE);
	dlab &= 0x7f;
	write_to_port[0x2e8+3](0x2e8+3, dlab, BYTE);

	// BASE + 1; Interrupt Enable Register
	write_to_port[0x3f8+1](0x3f8+1, 0x0c, BYTE);
	write_to_port[0x2f8+1](0x2f8+1, 0x0d, BYTE);
	write_to_port[0x3e8+1](0x3e8+1, 0x0e, BYTE);
	write_to_port[0x2e8+1](0x2e8+1, 0x0f, BYTE);
	CHECK_EQUAL(0x0c, read_from_port[0x3f8+1](0x3f8+1, BYTE));
	CHECK_EQUAL(0x0d, read_from_port[0x2f8+1](0x2f8+1, BYTE));
	CHECK_EQUAL(0x0e, read_from_port[0x3e8+1](0x3e8+1, BYTE));
	CHECK_EQUAL(0x0f, read_from_port[0x2e8+1](0x2e8+1, BYTE));

	// BASE + 2; Interrupt Identification Register (read only)
	CHECK_EQUAL(0x01, read_from_port[0x3f8+2](0x3f8+2, BYTE));
	CHECK_EQUAL(0x01, read_from_port[0x2f8+2](0x2f8+2, BYTE));
	CHECK_EQUAL(0x01, read_from_port[0x3e8+2](0x3e8+2, BYTE));
	CHECK_EQUAL(0x01, read_from_port[0x2e8+2](0x2e8+2, BYTE));

	// BASE + 2; FIFO Control Register (write only)
	// 0x03: Clear Receive FIFO
	write_to_port[0x3f8+2](0x3f8+2, 0x03, BYTE);
	// 0x05: Clear Transmit FIFO
	write_to_port[0x2f8+2](0x2f8+2, 0x05, BYTE);
	// 0x07: Clear Transmit and Receive FIFO
	write_to_port[0x3e8+2](0x3e8+2, 0x07, BYTE);
	// 0x00: disable FIFOs
	write_to_port[0x2e8+2](0x2e8+2, 0x00, BYTE);

	// BASE + 3; Line Control Register
	write_to_port[0x3f8+3](0x3f8+3, 0x00, BYTE);
	write_to_port[0x2f8+3](0x2f8+3, 0x03, BYTE);
	write_to_port[0x3e8+3](0x3e8+3, 0x0e, BYTE);
	write_to_port[0x2e8+3](0x2e8+3, 0x2b, BYTE);
	CHECK_EQUAL(0x00, read_from_port[0x3f8+3](0x3f8+3, BYTE));
	CHECK_EQUAL(0x03, read_from_port[0x2f8+3](0x2f8+3, BYTE));
	CHECK_EQUAL(0x0e, read_from_port[0x3e8+3](0x3e8+3, BYTE));
	CHECK_EQUAL(0x2b, read_from_port[0x2e8+3](0x2e8+3, BYTE));

	// BASE + 4; Modem Control Register
	write_to_port[0x3f8+4](0x3f8+4, 0x1c, BYTE);
	write_to_port[0x2f8+4](0x2f8+4, 0x1d, BYTE);
	write_to_port[0x3e8+4](0x3e8+4, 0x1e, BYTE);
	write_to_port[0x2e8+4](0x2e8+4, 0x1f, BYTE);
	CHECK_EQUAL(0x1c, read_from_port[0x3f8+4](0x3f8+4, BYTE));
	CHECK_EQUAL(0x1d, read_from_port[0x2f8+4](0x2f8+4, BYTE));
	CHECK_EQUAL(0x1e, read_from_port[0x3e8+4](0x3e8+4, BYTE));
	CHECK_EQUAL(0x1f, read_from_port[0x2e8+4](0x2e8+4, BYTE));

	// BASE + 5; Line Status Register (read only)
	CHECK_EQUAL(0x00, read_from_port[0x3f8+5](0x3f8+5, BYTE));
	CHECK_EQUAL(0x00, read_from_port[0x2f8+5](0x2f8+5, BYTE));
	CHECK_EQUAL(0x00, read_from_port[0x3e8+5](0x3e8+5, BYTE));
	CHECK_EQUAL(0x00, read_from_port[0x2e8+5](0x2e8+5, BYTE));

	// BASE + 6; Modem Status Register (read only)
	CHECK_EQUAL(0x00, read_from_port[0x3f8+6](0x3f8+6, BYTE));
	CHECK_EQUAL(0x00, read_from_port[0x2f8+6](0x2f8+6, BYTE));
	CHECK_EQUAL(0x00, read_from_port[0x3e8+6](0x3e8+6, BYTE));
	CHECK_EQUAL(0x00, read_from_port[0x2e8+6](0x2e8+6, BYTE));

	// BASE + 7; scratch register
	write_to_port[0x3f8+7](0x3f8+7, 0xa7, BYTE);
	write_to_port[0x2f8+7](0x2f8+7, 0xa8, BYTE);
	write_to_port[0x3e8+7](0x3e8+7, 0xa9, BYTE);
	write_to_port[0x2e8+7](0x2e8+7, 0xaa, BYTE);
	CHECK_EQUAL(0xa7, read_from_port[0x3f8+7](0x3f8+7, BYTE));
	CHECK_EQUAL(0xa8, read_from_port[0x2f8+7](0x2f8+7, BYTE));
	CHECK_EQUAL(0xa9, read_from_port[0x3e8+7](0x3e8+7, BYTE));
	CHECK_EQUAL(0xaa, read_from_port[0x2e8+7](0x2e8+7, BYTE));

	// shutdown
	shutdown_uart();

}

}
