#include "main_test.h"

namespace
{

// dev_8253.h

TEST(dev_8253)
{

	// initialise cpu
	init_cpu();

	// initialise PIT controller
	CHECK_EQUAL(0, init_pit());

	// for testing assume gate inputs are high
	gate_input[0] = 1;
	gate_input[1] = 1;
	gate_input[2] = 1;

	// check ports are available
	// write to port 40..43
	CHECK(write_to_port[0x40]);
	CHECK(write_to_port[0x41]);
	CHECK(write_to_port[0x42]);
	CHECK(write_to_port[0x43]);
	// read from port 40..42
	CHECK(read_from_port[0x40]);
	CHECK(read_from_port[0x41]);
	CHECK(read_from_port[0x42]);

	// Mode 0 - Interrupt On Terminal Count
	// when the mode/command register is written the output signal goes low
	// after the reload register has been set, the current count will be set to the reload value on the next falling edge of the (1.193182 MHz) input signal
	// subsequent falling edges of the input signal will decrement the current count (if the gate input is high on the preceding rising edge of the input signal)
	// when the current count decrements from one to zero, the output goes high and remains high until another mode/command register is written or the reload register is set again

	// set command register
	// 0x30: channel 0; lobyte/hibyte access; mode 0
	write_to_port[0x43](0x43, 0x30, BYTE);
	CHECK_EQUAL(0, pit_output[0]);

	// set reload value
	// 0x40: channel 0 data port
	write_to_port[0x40](0x40, 0x02, BYTE);	// low byte
	write_to_port[0x40](0x40, 0x00, BYTE);	// high byte
	tick();		// current count loaded (0x02)
	CHECK_EQUAL(0, pit_output[0]);
	tick();		// current count = 0x01
	CHECK_EQUAL(0, pit_output[0]);
	tick();		// current count = 0x00
	CHECK_EQUAL(1, pit_output[0]);
	tick();		// current count = 0xff
	CHECK_EQUAL(1, pit_output[0]);

	// try resetting the reload value
	// set reload value
	// 0x40: channel 0 data port
	write_to_port[0x40](0x40, 0x02, BYTE);	// low byte
	tick();
	write_to_port[0x40](0x40, 0x00, BYTE);	// high byte
	tick();		// current count loaded (0x02)
	CHECK_EQUAL(0, pit_output[0]);
	tick();		// current count = 0x01
	CHECK_EQUAL(0, pit_output[0]);
	tick();		// current count = 0x00
	CHECK_EQUAL(1, pit_output[0]);
	tick();		// current count = 0xff
	CHECK_EQUAL(1, pit_output[0]);

	// Mode 1 - Hardware Re-triggerable One-shot
	// similar to mode 0, however counting doesn't start until a rising edge of the gate input is detected. For this reason it is not usable for PIT channels 0 or 1 (where the gate input can't be changed)
	// When the mode/command register is written the output signal goes high until the next rising edge of the gate input. Once this occurs, the output signal will go low and the current count will be set to the reload value on the next falling edge of the (1.193182 MHz) input signal
	// If the gate input signal goes low it will have no effect. However, if the gate input goes high again it will cause the current count to be reloaded from the reload register on the next falling edge of the input signal, and restart the count again (the same as when counting first started).
	// The reload value can be changed at any time, however the new value will not affect the current count until the current count is reloaded (on the next rising edge of the gate input). So if you want to do this, clear and then reset bit 0 of IO port 0x61, after modifying the reload value.

	// set command register
	// 0xb2: channel 2; lobyte/hibyte access; mode 1
	write_to_port[0x43](0x43, 0xb2, BYTE);
	CHECK_EQUAL(1, pit_output[2]);

	// set reload value
	// 0x42: channel 2 data port
	write_to_port[0x42](0x42, 0x02, BYTE);	// low byte
	write_to_port[0x42](0x42, 0x00, BYTE);	// high byte
	tick();
	CHECK_EQUAL(1, pit_output[2]);
	gate_input[2] = 0;
	tick();
	CHECK_EQUAL(1, pit_output[2]);
	gate_input[2] = 1;
	tick();		// current count loaded (0x02)
	CHECK_EQUAL(0, pit_output[2]);
	tick();		// current count = 0x01
	CHECK_EQUAL(0, pit_output[2]);
	tick();		// current count = 0x00
	CHECK_EQUAL(1, pit_output[2]);
	tick();		// current count = 0xff
	CHECK_EQUAL(1, pit_output[2]);

	// Mode 2 - Rate Generator
	// frequency = 1193182 / reload_value Hz
	// when the mode/command register is written the output signal goes high
	// after the reload register has been set, the current count will be set to the reload value on the next falling edge of the (1.193182 MHz) input signal
	// subsequent falling edges of the input signal will decrement the current count (if the gate input is high on the preceding rising edge of the input signal)
	// when the current count decrements from two to one, the output goes low, and on the next falling edge of the (1.193182 MHz) input signal it will go high again and the current count will be set to the reload value and counting will continue
	// if the gate input goes low, counting stops and the output goes high immediately. Once the gate input has returned high, the next falling edge on input signal will cause the current count to be set to the reload value and operation will continue
	// the reload value can be changed at any time, however the new value will not effect the current count until the current count is reloaded (when it is decreased from two to one, or the gate input going low then high)
	// a reload value (or divisor) of one must not be used with this mode

	// set command register
	// 0xb4: channel 2; lobyte/hibyte access; mode 2
	write_to_port[0x43](0x43, 0xb4, BYTE);
	CHECK_EQUAL(1, pit_output[2]);

	// set reload value
	// 0x42: channel 2 data port
	write_to_port[0x42](0x42, 0x03, BYTE);	// low byte
	write_to_port[0x42](0x42, 0x00, BYTE);	// high byte
	tick();		// current count loaded (0x03)
	CHECK_EQUAL(1, pit_output[2]);
	tick();		// current count = 0x02
	CHECK_EQUAL(1, pit_output[2]);
	tick();		// current count = 0x01
	CHECK_EQUAL(0, pit_output[2]);
	tick();		// current count = 0x00/0x03
	CHECK_EQUAL(1, pit_output[2]);
	tick();		// current count = 0x02
	CHECK_EQUAL(1, pit_output[2]);
	tick();		// current count = 0x01
	CHECK_EQUAL(0, pit_output[2]);
	tick();		// current count = 0x00/0x03
	CHECK_EQUAL(1, pit_output[2]);

	// Mode 3 - Square Wave Generator
	// operates as a frequency divider like mode 2, however the output signal is fed into an internal "flip flop" to produce a square wave (rather than a short pulse)
	// this causes the actual output to change state half as often, so to compensate for this the current count is decremented twice on each falling edge of the input signal (instead of once), and the current count is set to the reload value twice as often
	// when the mode/command register is written the output signal goes high
	// after the reload register has been set, the current count will be set to the reload value on the next falling edge of the (1.193182 MHz) input signal
	// subsequent falling edges of the input signal will decrement the current count twice (if the gate input is high on the preceding rising edge of the input signal)
	// for odd reload values, the current count is always set to one less than the reload value
	// when the current count decrements from two to zero the output of the flop-flop changes state; the current count will be reset to the reload value and counting will continue
	// On channel 2, if the gate input goes low, counting stops and the output goes high immediately. Once the gate input has returned high, the next falling edge on input signal will cause the current count to be set to the reload value and operation will continue (with the output left high)
	// The reload value can be changed at any time, however the new value will not effect the current count until the current count is reloaded (when it is decreased from two to zero, or the gate input going low then high). When this occurs counting will continue using the new reload value
	// A reload value (or divisor) of one must not be used with this mode

	// set command register
	// 0x36: channel 0; lobyte/hibyte access; mode 3
	write_to_port[0x43](0x43, 0x36, BYTE);
	CHECK_EQUAL(1, pit_output[0]);

	// set reload value
	// 0x40: channel 0 data port
	write_to_port[0x40](0x40, 0x03, BYTE);	// low byte
	write_to_port[0x40](0x40, 0x00, BYTE);	// high byte
	// note: odd values are decremented by 1 before being loaded
	tick();		// current count loaded (0x02)
	CHECK_EQUAL(1, pit_output[0]);
	tick();		// current count = 0x00/0x02
	CHECK_EQUAL(0, pit_output[0]);
	tick();		// current count loaded (0x02)
	CHECK_EQUAL(1, pit_output[0]);
	tick();		// current count = 0x00/0x02
	CHECK_EQUAL(0, pit_output[0]);

	// try resetting the reload value
	// set reload value
	// 0x40: channel 0 data port
	write_to_port[0x40](0x40, 0x04, BYTE);	// low byte
	write_to_port[0x40](0x40, 0x00, BYTE);	// high byte
	CHECK_EQUAL(0, pit_output[0]);
	gate_input[0] = 0;
	tick();
	CHECK_EQUAL(1, pit_output[0]);
	gate_input[0] = 1;
	tick();		// current count loaded (0x04)
	CHECK_EQUAL(1, pit_output[0]);
	tick();		// current count = 0x02
	CHECK_EQUAL(1, pit_output[0]);
	tick();		// current count = 0x00/0x04
	CHECK_EQUAL(0, pit_output[0]);
	tick();		// current count = 0x02
	CHECK_EQUAL(0, pit_output[0]);
	tick();		// current count = 0x00/0x04
	CHECK_EQUAL(1, pit_output[0]);

	// Mode 4 - Software Triggered Strobe
	// operates as a retriggerable delay, and generates a pulse when the current count reaches zero
	// when the mode/command register is written the output signal goes high
	// after the reload register has been set, the current count will be set to the reload value on the next falling edge of the (1.193182 MHz) input signal
	// subsequent falling edges of the input signal will decrement the current count (if the gate input is high on the preceding rising edge of the input signal)
	// When the current count decrements from one to zero, the output goes low for one cycle of the input signal (0.8381 uS). The current count will wrap around to 0xFFFF (or 0x9999 in BCD mode) and continue to decrement until the mode/command register or the reload register are set, however this will not affect the output state
	// If the gate input goes low, counting stops but the output will not be affected and the current count will not be reset to the reload value
	// The reload value can be changed at any time. When the new value has been set (both bytes for "lobyte/hibyte" access mode) it will be loaded into the current count on the next falling edge of the (1.193182 MHz) input signal, and counting will continue using the new reload value

	// set command register
	// 0x38: channel 0; lobyte/hibyte access; mode 4
	write_to_port[0x43](0x43, 0x38, BYTE);
	CHECK_EQUAL(1, pit_output[0]);

	// set reload value
	// 0x40: channel 0 data port
	write_to_port[0x40](0x40, 0x02, BYTE);	// low byte
	write_to_port[0x40](0x40, 0x00, BYTE);	// high byte
	//
	tick();		// current count loaded (0x02)
	CHECK_EQUAL(1, pit_output[0]);
	tick();		// current count = 0x01
	CHECK_EQUAL(1, pit_output[0]);
	tick();		// current count = 0x00
	CHECK_EQUAL(0, pit_output[0]);
	tick();		// current count = 0xff
	CHECK_EQUAL(1, pit_output[0]);
	// check pit_output stays high
	for (int i=0; i< 0xffff; i++) {
		tick();
		CHECK_EQUAL(1, pit_output[0]);
	}
	tick();
	CHECK_EQUAL(1, pit_output[0]);

	// Mode 5 - Hardware Triggered Strobe
	// similar to mode 4, except that it waits for the rising edge of the gate input to trigger (or re-trigger) the delay period (like mode 1). For this reason it is not usable for PIT channels 0 or 1 (where the gate input can't be changed)
	// When the mode/command register is written the output signal goes high
	// After the reload register has been set the PIT will wait for the next rising edge of the gate input. Once this occurs, the current count will be set to the reload value on the next falling edge of the (1.193182 MHz) input signal
	// Subsequent falling edges of the input signal will decrement the current count
	// When the current count decrements from one to zero, the output goes low for one cycle of the input signal (0.8381 uS). The current count will wrap around to 0xFFFF (or 0x9999 in BCD mode) and continue to decrement until the mode/command register or the reload register are set, however this will not effect the output state
	// If the gate input signal goes low during this process it will have no effect. However, if the gate input goes high again it will cause the current count to be reloaded from the reload register on the next falling edge of the input signal, and restart the count again (the same as when counting first started)
	// The reload value can be changed at any time, however the new value will not affect the current count until the current count is reloaded (on the next rising edge of the gate input). When this occurs counting will continue using the new reload value

	// set command register
	// 0xba: channel 2; lobyte/hibyte access; mode 5
	write_to_port[0x43](0x43, 0xba, BYTE);
	CHECK_EQUAL(1, pit_output[2]);

	// set reload value
	// 0x42: channel 2 data port
	write_to_port[0x42](0x42, 0x02, BYTE);	// low byte
	write_to_port[0x42](0x42, 0x00, BYTE);	// high byte
	//tick();
	CHECK_EQUAL(1, pit_output[2]);
	gate_input[2] = 0;
	tick();
	CHECK_EQUAL(1, pit_output[2]);
	gate_input[2] = 1;
	tick();		// current count loaded (0x02)
	CHECK_EQUAL(1, pit_output[2]);
	tick();		// current count = 0x01
	CHECK_EQUAL(1, pit_output[2]);
	tick();		// current count = 0x00
	CHECK_EQUAL(0, pit_output[2]);
	tick();		// current count = 0xff
	CHECK_EQUAL(1, pit_output[2]);
	// check pit_output stays high
	for (int i=0; i< 0xffff; i++) {
		tick();
		CHECK_EQUAL(1, pit_output[2]);
	}
	tick();
	CHECK_EQUAL(1, pit_output[2]);

	// shutdown
	shutdown_pit();

}

}
