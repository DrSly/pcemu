#include "main_test.h"

namespace
{

// decode.h

TEST(decode_NOT_NEG)
{

	mode32 = 1;

	// --- NOT: F6 /2 ---

	// NOT r/m8 => F6 /2 []
	// mod = 00; + r/m = 1; [ECX]
	// f6 11                   not    BYTE PTR [ecx]
	uint8_t *code = new uint8_t [2] {0xF6, 0x11};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// NOT r/m8 => F6 /2 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// f6 15 ff 00 00 01       not    BYTE PTR ds:0x10000ff
	code = new uint8_t [6] {0xF6, 0x15, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// NOT r/m8 => F6 /2 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// f6 55 ff                not    BYTE PTR [ebp-0x1]
	code = new uint8_t [3] {0xF6, 0x55, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// NOT r/m8 => F6 /2 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// f6 92 ff 00 00 01       not    BYTE PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xF6, 0x92, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// NOT r/m8 => F6 /2 []
	// mod = 11; + r/m = 0; register (AL)
	// f6 d0                   not    al
	code = new uint8_t [2] {0xF6, 0xD0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	delete code;

	// --- NOT: F7 /2 ---

	// NOT r/m32 => F7 /2 []
	// mod = 00; + r/m = 1; [ECX]
	// f7 11                   not    DWORD PTR [ecx]
	code = new uint8_t [2] {0xF7, 0x11};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// NOT r/m32 => F7 /2 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// f7 15 ff 00 00 01       not    DWORD PTR ds:0x10000ff
	code = new uint8_t [6] {0xF7, 0x15, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// NOT r/m32 => F7 /2 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// f7 55 ff                not    DWORD PTR [ebp-0x1]
	code = new uint8_t [3] {0xF7, 0x55, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// NOT r/m32 => F7 /2 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// f7 92 ff 00 00 01       not    DWORD PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xF7, 0x92, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// NOT r/m32 => F7 /2 []
	// mod = 11; + r/m = 0; register (EAX)
	// f7 d0                   not    eax
	code = new uint8_t [2] {0xF7, 0xD0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// --- NEG: F6 /3 ---

	// NEG r/m8 => F6 /3 []
	// mod = 00; + r/m = 1; [ECX]
	// f6 19                   neg    BYTE PTR [ecx]
	code = new uint8_t [2] {0xF6, 0x19};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// NEG r/m8 => F6 /3 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// f6 1d ff 00 00 01       neg    BYTE PTR ds:0x10000ff
	code = new uint8_t [6] {0xF6, 0x1D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// NEG r/m8 => F6 /3 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// f6 5d ff                neg    BYTE PTR [ebp-0x1]
	code = new uint8_t [3] {0xF6, 0x5D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// NEG r/m8 => F6 /3 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// f6 9a ff 00 00 01       neg    BYTE PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xF6, 0x9A, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// NEG r/m8 => F6 /3 []
	// mod = 11; + r/m = 0; register (AL)
	// f6 d8                   neg    al
	code = new uint8_t [2] {0xF6, 0xD8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	delete code;

	// --- NEG: F7 /3 ---

	// NEG r/m32 => F7 /3 []
	// mod = 00; + r/m = 1; [ECX]
	// f7 19                   neg    DWORD PTR [ecx]
	code = new uint8_t [2] {0xF7, 0x19};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

	// NEG r/m32 => F7 /3 []
	// mod = 00; + r/m = 5; 32 bit displacement only
	// f7 1d ff 00 00 01       neg    DWORD PTR ds:0x10000ff
	code = new uint8_t [6] {0xF7, 0x1D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x10000ff, opc.dest.disponly_data);
	delete code;

	// NEG r/m32 => F7 /3 []
	// mod = 01; + r/m = 5; [EBP+off8]
	// f7 5d ff                neg    DWORD PTR [ebp-0x1]
	code = new uint8_t [3] {0xF7, 0x5D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ch_bp_ebp, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// NEG r/m32 => F7 /3 []
	// mod = 10; + r/m = 2; [EDX+off32]
	// f7 9a ff 00 00 01       neg    DWORD PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xF7, 0x9A, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x10000ff, opc.dest.offset_data);
	delete code;

	// NEG r/m32 => F7 /3 []
	// mod = 11; + r/m = 0; register (AEX)
	// f7 d8                   neg    eax
	code = new uint8_t [2] {0xF7, 0xD8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	delete code;

}

TEST(decode_NOT_NEG_16bit)
{

	mode32 = 0;

	// --- NOT: F6 /2 ---

	// NOT r/m8 => F6 /2 []
	// mod = 00; + r/m = 1; [BX+DI]
	// f6 11                   not    BYTE PTR [bx+di]
	uint8_t *code = new uint8_t [2] {0xF6, 0x11};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// NOT r/m8 => F6 /2 []
	// mod = 00; + r/m = 6; 16 bit displacement only
	// f6 16 ff 01       not    BYTE PTR ds:0x1ff
	code = new uint8_t [4] {0xF6, 0x16, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	delete code;

	// NOT r/m8 => F6 /2 []
	// mod = 01; + r/m = 5; [DI+off8]
	// f6 55 ff                not    BYTE PTR [di-0x1]
	code = new uint8_t [3] {0xF6, 0x55, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// NOT r/m8 => F6 /2 []
	// mod = 10; + r/m = 2; [BP+SI+off16]
	// f6 92 ff 01       not    BYTE PTR [bp+si+0x1ff]
	code = new uint8_t [4] {0xF6, 0x92, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	delete code;

	// NOT r/m8 => F6 /2 []
	// mod = 11; + r/m = 0; register (AL)
	// f6 d0                   not    al
	code = new uint8_t [2] {0xF6, 0xD0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	delete code;

	// --- NOT: F7 /2 ---

	// NOT r/m16 => F7 /2 []
	// mod = 00; + r/m = 1; [BX+DI]
	// f7 11                   not    WORD PTR [bx+di]
	code = new uint8_t [2] {0xF7, 0x11};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// NOT r/m16 => F7 /2 []
	// mod = 00; + r/m = 6; 16 bit displacement only
	// f7 16 ff 01       not    WORD PTR ds:0x1ff
	code = new uint8_t [4] {0xF7, 0x16, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	delete code;

	// NOT r/m16 => F7 /2 []
	// mod = 01; + r/m = 5; [DI+off8]
	// f7 55 ff                not    WORD PTR [di-0x1]
	code = new uint8_t [3] {0xF7, 0x55, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// NOT r/m16 => F7 /2 []
	// mod = 10; + r/m = 2; [BP+SI+off16]
	// f7 92 ff 01       not    WORD PTR [bp+si+0x1ff]
	code = new uint8_t [4] {0xF7, 0x92, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	delete code;

	// NOT r/m16 => F7 /2 []
	// mod = 11; + r/m = 0; register (AX)
	// f7 d0                   not    ax
	code = new uint8_t [2] {0xF7, 0xD0};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NOT, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// --- NEG: F6 /3 ---

	// NEG r/m8 => F6 /3 []
	// mod = 00; + r/m = 1; [BX+DI]
	// f6 19                   neg    BYTE PTR [bx+di]
	code = new uint8_t [2] {0xF6, 0x19};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// NEG r/m8 => F6 /3 []
	// mod = 00; + r/m = 6; 16 bit displacement only
	// f6 1e ff 01       neg    BYTE PTR ds:0x1ff
	code = new uint8_t [4] {0xF6, 0x1E, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	delete code;

	// NEG r/m8 => F6 /3 []
	// mod = 01; + r/m = 5; [DI+off8]
	// f6 5d ff                neg    BYTE PTR [di-0x1]
	code = new uint8_t [3] {0xF6, 0x5D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// NEG r/m8 => F6 /3 []
	// mod = 10; + r/m = 2; [BP+SI+off16]
	// f6 9a ff 01       neg    BYTE PTR [bp+si+0x1ff]
	code = new uint8_t [4] {0xF6, 0x9A, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	delete code;

	// NEG r/m8 => F6 /3 []
	// mod = 11; + r/m = 0; register (AL)
	// f6 d8                   neg    al
	code = new uint8_t [2] {0xF6, 0xD8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(BYTE, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(BYTE, opc.dest.register_size);
	delete code;

	// --- NEG: F7 /3 ---

	// NEG r/m16 => F7 /3 []
	// mod = 00; + r/m = 1; [BX+DI]
	// f7 19                   neg    WORD PTR [bx+di]
	code = new uint8_t [2] {0xF7, 0x19};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

	// NEG r/m16 => F7 /3 []
	// mod = 00; + r/m = 6; 16 bit displacement only
	// f7 1e ff 01       neg    WORD PTR ds:0x1ff
	code = new uint8_t [4] {0xF7, 0x1E, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_disponly);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(0x1ff, opc.dest.disponly_data);
	delete code;

	// NEG r/m16 => F7 /3 []
	// mod = 01; + r/m = 5; [DI+off8]
	// f7 5d ff                neg    WORD PTR [di-0x1]
	code = new uint8_t [3] {0xF7, 0x5D, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ds, opc.dest.register_segment);
	CHECK_EQUAL(ind_di, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(-1, opc.dest.offset_data);
	delete code;

	// NEG r/m16 => F7 /3 []
	// mod = 10; + r/m = 2; [BP+SI+off16]
	// f7 9a ff 01       neg    WORD PTR [bp+si+0x1ff]
	code = new uint8_t [4] {0xF7, 0x9A, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_indregister);
	CHECK_EQUAL(seg_ss, opc.dest.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.dest.use_offset);
	CHECK_EQUAL(0x1ff, opc.dest.offset_data);
	delete code;

	// NEG r/m16 => F7 /3 []
	// mod = 11; + r/m = 0; register (AX)
	// f7 d8                   neg    ax
	code = new uint8_t [2] {0xF7, 0xD8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(NEG, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	delete code;

}

}
