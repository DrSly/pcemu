#include "main_test.h"

namespace
{

// dev_parallel.h

TEST(dev_parallel)
{

	// initialise cpu
	init_cpu();

	// initialise parallel port
	CHECK_EQUAL(0, init_parallel());

	// check ports are available
	for (int i=0; i<3; i++) {
		CHECK(write_to_port[0x378 + i]);
		CHECK(read_from_port[0x378 + i]);
	}
	for (int i=0; i<3; i++) {
		CHECK(write_to_port[0x278 + i]);
		CHECK(read_from_port[0x278 + i]);
	}

	// LPT1 = 0x378
	uint8_t lpt1 = read_from_port[0x378+2](0x378+2, BYTE);
	CHECK_EQUAL(0x0c, lpt1);
	lpt1 &= 0xdf;
	write_to_port[0x378+2](0x378+2, lpt1, BYTE);
	write_to_port[0x378+0](0x378+0, 0xaa, BYTE);
	lpt1 = read_from_port[0x378+0](0x378+0, BYTE);
	CHECK_EQUAL(0xaa, lpt1);

	// LPT2 = 0x278
	uint8_t lpt2 = read_from_port[0x278+2](0x278+2, BYTE);
	CHECK_EQUAL(0xff, lpt2);
	lpt2 &= 0xdf;
	write_to_port[0x278+2](0x278+2, lpt2, BYTE);
	write_to_port[0x278+0](0x278+0, 0xaa, BYTE);
	lpt2 = read_from_port[0x278+0](0x278+0, BYTE);
	CHECK_EQUAL(0xff, lpt2);

	// shutdown
	shutdown_parallel();

}

}
