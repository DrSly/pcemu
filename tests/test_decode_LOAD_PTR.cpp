#include "main_test.h"

namespace
{

// decode.h

TEST(decode_LDS_LES_LFS_LGS_LSS)
{

	mode32 = 1;

	// LDS reg32,mem => C5 [mod/reg/rm] []

	// mod/reg/rm = 01
	// mod = 00; + r/m = 1; [ECX]
	// reg = 0 (EAX)

	// c5 01                   lds    eax,FWORD PTR [ecx]
	uint8_t *code = new uint8_t [2] {0xC5, 0x01};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LDS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 08
	// mod = 00; + r/m = 0; [EAX]
	// reg = 1 (ECX)

	// c5 08                   lds    ecx,FWORD PTR [eax]
	code = new uint8_t [2] {0xC5, 0x08};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LDS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 05
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 0 (EAX)

	// c5 05 ff 00 00 01       lds    eax,FWORD PTR ds:0x10000ff
	code = new uint8_t [6] {0xC5, 0x05, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(LDS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x10000ff, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 42
	// mod = 01; indirect register with one byte offset
	// reg = 0 (EAX); r/m = 2 (EDX)

	// c5 42 ff                lds    eax,FWORD PTR [edx-0x1]
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [3] {0xC5, 0x42, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LDS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-1, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 82
	// mod = 10; indirect register with four byte offset
	// reg = 0 (EAX); r/m = 2 (EDX)

	// c5 82 ff 00 00 01       lds    eax,FWORD PTR [edx+0x10000ff]
	code = new uint8_t [6] {0xC5, 0x82, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(LDS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x10000ff, opc.source.offset_data);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing not allowed
	// reg = 0 (EAX); r/m = 1 (ECX)

	// c5 c1                   (bad) => disassembles to a kmovb
	code = new uint8_t [2] {0xC5, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);
	CHECK_EQUAL(0, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_register);
	CHECK_EQUAL(0, opc.dest.use_indregister);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.use_offset);
	CHECK_EQUAL(0, opc.dest.use_disponly);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_indregister);
	CHECK_EQUAL(0, opc.source.use_imm_word);
	CHECK_EQUAL(0, opc.source.use_offset);
	CHECK_EQUAL(0, opc.source.use_disponly);
	delete code;

	// LES reg32,mem => C4 [mod/reg/rm] []

	// mod/reg/rm = 01
	// mod = 00; + r/m = 2; [EDX]
	// reg = 0 (EAX)

	// c4 02                   les    eax,FWORD PTR [edx]
	code = new uint8_t [2] {0xC4, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LES, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 08
	// mod = 00; + r/m = 0; [EAX]
	// reg = 3 (EBX)

	// c4 18                   les    ebx,FWORD PTR [eax]
	code = new uint8_t [2] {0xC4, 0x18};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LES, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 05
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 1 (ECX)

	// c4 0d ff 00 00 01       les    ecx,FWORD PTR ds:0x10000ff
	code = new uint8_t [6] {0xC4, 0x0D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(LES, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x10000ff, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 42
	// mod = 01; indirect register with one byte offset
	// reg = 2 (EDX); r/m = 2 (EDX)

	// c4 52 ff                les    edx,FWORD PTR [edx-0x1]
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [3] {0xC4, 0x52, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LES, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-1, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 82
	// mod = 10; indirect register with four byte offset
	// reg = 0 (EAX); r/m = 3 (EBX)

	// c4 83 ff 00 00 01       les    eax,FWORD PTR [ebx+0x10000ff]
	code = new uint8_t [6] {0xC4, 0x83, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(6, a);
	CHECK_EQUAL(LES, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x10000ff, opc.source.offset_data);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing not allowed
	// reg = 0 (EAX); r/m = 1 (ECX)

	// c4 c1                   (bad)
	code = new uint8_t [2] {0xC4, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);
	CHECK_EQUAL(0, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_register);
	CHECK_EQUAL(0, opc.dest.use_indregister);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.use_offset);
	CHECK_EQUAL(0, opc.dest.use_disponly);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_indregister);
	CHECK_EQUAL(0, opc.source.use_imm_word);
	CHECK_EQUAL(0, opc.source.use_offset);
	CHECK_EQUAL(0, opc.source.use_disponly);
	delete code;

	// LFS reg32,mem => 0FB4 [mod/reg/rm] []

	// mod/reg/rm = 01
	// mod = 00; + r/m = 1; [ECX]
	// reg = 0 (EAX)

	// 0f b4 01                lfs    eax,FWORD PTR [ecx]
	code = new uint8_t [3] {0x0F, 0xB4, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LFS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 08
	// mod = 00; + r/m = 0; [EAX]
	// reg = 1 (ECX)

	// 0f b4 08                lfs    ecx,FWORD PTR [eax]
	code = new uint8_t [3] {0x0F, 0xB4, 0x08};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LFS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 05
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 0 (EAX)

	// 0f b4 05 ff 00 00 01    lfs    eax,FWORD PTR ds:0x10000ff
	code = new uint8_t [7] {0x0F, 0xB4, 0x05, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(LFS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x10000ff, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 42
	// mod = 01; indirect register with one byte offset
	// reg = 0 (EAX); r/m = 2 (EDX)

	// 0f b4 42 ff             lfs    eax,FWORD PTR [edx-0x1]
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [4] {0x0F, 0xB4, 0x42, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(LFS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-1, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 82
	// mod = 10; indirect register with four byte offset
	// reg = 0 (EAX); r/m = 2 (EDX)

	// 0f b4 82 ff 00 00 01    lfs    eax,FWORD PTR [edx+0x10000ff]
	code = new uint8_t [7] {0x0F, 0xB4, 0x82, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(LFS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x10000ff, opc.source.offset_data);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing not allowed
	// reg = 0 (EAX); r/m = 1 (ECX)

	// 0f b4 c1                   (bad)
	code = new uint8_t [3] {0x0F, 0xB4, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);
	CHECK_EQUAL(0, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_register);
	CHECK_EQUAL(0, opc.dest.use_indregister);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.use_offset);
	CHECK_EQUAL(0, opc.dest.use_disponly);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_indregister);
	CHECK_EQUAL(0, opc.source.use_imm_word);
	CHECK_EQUAL(0, opc.source.use_offset);
	CHECK_EQUAL(0, opc.source.use_disponly);
	delete code;

	// LGS reg32,mem => 0FB5 [mod/reg/rm] []

	// mod/reg/rm = 01
	// mod = 00; + r/m = 2; [EDX]
	// reg = 0 (EAX)

	// 0f b5 02                lgs    eax,FWORD PTR [edx]
	code = new uint8_t [3] {0x0F, 0xB5, 0x02};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LGS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 08
	// mod = 00; + r/m = 0; [EAX]
	// reg = 3 (EBX)

	// 0f b5 18                lgs    ebx,FWORD PTR [eax]
	code = new uint8_t [3] {0x0F, 0xB5, 0x18};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LGS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(bl_bx_ebx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 05
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 1 (ECX)

	// 0f b5 0d ff 00 00 01    lgs    ecx,FWORD PTR ds:0x10000ff
	code = new uint8_t [7] {0x0F, 0xB5, 0x0D, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(LGS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x10000ff, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 42
	// mod = 01; indirect register with one byte offset
	// reg = 2 (EDX); r/m = 2 (EDX)

	// 0f b5 52 ff             lgs    edx,FWORD PTR [edx-0x1]
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [4] {0x0F, 0xB5, 0x52, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(LGS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(dl_dx_edx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-1, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 82
	// mod = 10; indirect register with four byte offset
	// reg = 0 (EAX); r/m = 3 (EBX)

	// 0f b5 83 ff 00 00 01    lgs    eax,FWORD PTR [ebx+0x10000ff]
	code = new uint8_t [7] {0x0F, 0xB5, 0x83, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(LGS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(bl_bx_ebx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x10000ff, opc.source.offset_data);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing not allowed
	// reg = 0 (EAX); r/m = 1 (ECX)

	// 0f b5 c1                   (bad)
	code = new uint8_t [3] {0x0F, 0xB5, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);
	CHECK_EQUAL(0, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_register);
	CHECK_EQUAL(0, opc.dest.use_indregister);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.use_offset);
	CHECK_EQUAL(0, opc.dest.use_disponly);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_indregister);
	CHECK_EQUAL(0, opc.source.use_imm_word);
	CHECK_EQUAL(0, opc.source.use_offset);
	CHECK_EQUAL(0, opc.source.use_disponly);
	delete code;

	// LSS reg32,mem => 0FB2 [mod/reg/rm] []

	// mod/reg/rm = 01
	// mod = 00; + r/m = 1; [ECX]
	// reg = 0 (EAX)

	// 0f b2 01                lss    eax,FWORD PTR [ecx]
	code = new uint8_t [3] {0x0F, 0xB2, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LSS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(cl_cx_ecx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 08
	// mod = 00; + r/m = 0; [EAX]
	// reg = 1 (ECX)

	// 0f b2 08                lss    ecx,FWORD PTR [eax]
	code = new uint8_t [3] {0x0F, 0xB2, 0x08};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LSS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(al_ax_eax, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 05
	// mod = 00 + r/m = 5; 32 bit displacement only
	// reg = 0 (EAX)

	// 0f b2 05 ff 00 00 01    lss    eax,FWORD PTR ds:0x10000ff
	code = new uint8_t [7] {0x0F, 0xB2, 0x05, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(LSS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x10000ff, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 42
	// mod = 01; indirect register with one byte offset
	// reg = 0 (EAX); r/m = 2 (EDX)

	// 0f b2 42 ff             lss    eax,FWORD PTR [edx-0x1]
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [4] {0x0F, 0xB2, 0x42, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(LSS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-1, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 82
	// mod = 10; indirect register with four byte offset
	// reg = 0 (EAX); r/m = 2 (EDX)

	// 0f b2 82 ff 00 00 01    lss    eax,FWORD PTR [edx+0x10000ff]
	code = new uint8_t [7] {0x0F, 0xB2, 0x82, 0xFF, 0x00, 0x00, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(7, a);
	CHECK_EQUAL(LSS, opc.op);
	CHECK_EQUAL(DWORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(DWORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(dl_dx_edx, opc.source.register_data);
	CHECK_EQUAL(DWORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x10000ff, opc.source.offset_data);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing not allowed
	// reg = 0 (EAX); r/m = 1 (ECX)

	// 0f b2 c1                   (bad)
	code = new uint8_t [3] {0x0F, 0xB2, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);
	CHECK_EQUAL(0, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_register);
	CHECK_EQUAL(0, opc.dest.use_indregister);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.use_offset);
	CHECK_EQUAL(0, opc.dest.use_disponly);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_indregister);
	CHECK_EQUAL(0, opc.source.use_imm_word);
	CHECK_EQUAL(0, opc.source.use_offset);
	CHECK_EQUAL(0, opc.source.use_disponly);
	delete code;

}

TEST(decode_LDS_LES_LFS_LGS_LSS_16bit)
{

	mode32 = 0;

	// LDS reg16,mem => C5 [mod/reg/rm] []

	// mod/reg/rm = 01
	// mod = 00; + r/m = 1; [BX+DI]
	// reg = 0 (AX)

	// c5 01                   lds    ax,[bx+di]
	uint8_t *code = new uint8_t [2] {0xC5, 0x01};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LDS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 08
	// mod = 00; + r/m = 0; [BX+SI]
	// reg = 1 (CX)

	// c5 08                   lds    cx,[bx+si]
	code = new uint8_t [2] {0xC5, 0x08};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LDS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 06
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 0 (AX)

	// c5 06 ff 01       lds    ax,ds:0x1ff
	code = new uint8_t [4] {0xC5, 0x06, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(LDS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x1ff, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 42
	// mod = 01; indirect register with one byte offset
	// reg = 0 (AX); r/m = 2 (BP+SI)

	// c5 42 ff                lds    ax,[bp+si-0x1]
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [3] {0xC5, 0x42, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LDS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-1, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 82
	// mod = 10; indirect register with four byte offset
	// reg = 0 (AX); r/m = 2 (BP+SI)

	// c5 82 ff 01       lds    ax,[bp+si+0x1ff]
	code = new uint8_t [4] {0xC5, 0x82, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(LDS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x1ff, opc.source.offset_data);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing not allowed
	// reg = 0 (AX); r/m = 1 (CX)

	// c5 c1                   (bad)
	code = new uint8_t [2] {0xC5, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);
	CHECK_EQUAL(0, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_register);
	CHECK_EQUAL(0, opc.dest.use_indregister);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.use_offset);
	CHECK_EQUAL(0, opc.dest.use_disponly);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_indregister);
	CHECK_EQUAL(0, opc.source.use_imm_word);
	CHECK_EQUAL(0, opc.source.use_offset);
	CHECK_EQUAL(0, opc.source.use_disponly);
	delete code;

	// LES reg16,mem => C4 [mod/reg/rm] []

	// mod/reg/rm = 01
	// mod = 00; + r/m = 1; [BX+DI]
	// reg = 0 (AX)

	// c4 01                   les    ax,[bx+di]
	code = new uint8_t [2] {0xC4, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LES, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 08
	// mod = 00; + r/m = 0; [BX+SI]
	// reg = 1 (CX)

	// c4 08                   les    cx,[bx+si]
	code = new uint8_t [2] {0xC4, 0x08};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(LES, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 06
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 0 (AX)

	// c4 06 ff 01       les    ax,ds:0x1ff
	code = new uint8_t [4] {0xC4, 0x06, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(LES, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x1ff, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 42
	// mod = 01; indirect register with one byte offset
	// reg = 0 (AX); r/m = 2 (BP+SI)

	// c4 42 ff                les    ax,[bp+si-0x1]
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [3] {0xC4, 0x42, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LES, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-1, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 82
	// mod = 10; indirect register with four byte offset
	// reg = 0 (AX); r/m = 2 (BP+SI)

	// c4 82 ff 01       les    ax,[bp+si+0x1ff]
	code = new uint8_t [4] {0xC4, 0x82, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(LES, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x1ff, opc.source.offset_data);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing not allowed
	// reg = 0 (AX); r/m = 1 (CX)

	// c4 c1                   (bad)
	code = new uint8_t [2] {0xC4, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);
	CHECK_EQUAL(0, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_register);
	CHECK_EQUAL(0, opc.dest.use_indregister);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.use_offset);
	CHECK_EQUAL(0, opc.dest.use_disponly);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_indregister);
	CHECK_EQUAL(0, opc.source.use_imm_word);
	CHECK_EQUAL(0, opc.source.use_offset);
	CHECK_EQUAL(0, opc.source.use_disponly);
	delete code;

	// LFS reg16,mem => 0FB4 [mod/reg/rm] []

	// mod/reg/rm = 01
	// mod = 00; + r/m = 1; [BX+DI]
	// reg = 0 (AX)

	// 0f b4 01                   lfs    ax,[bx+di]
	code = new uint8_t [3] {0x0F, 0xB4, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LFS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 08
	// mod = 00; + r/m = 0; [BX+SI]
	// reg = 1 (CX)

	// 0f b4 08                   lfs    cx,[bx+si]
	code = new uint8_t [3] {0x0F, 0xB4, 0x08};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LFS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 06
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 0 (AX)

	// 0f b4 06 ff 01       lfs    ax,ds:0x1ff
	code = new uint8_t [5] {0x0F, 0xB4, 0x06, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(LFS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x1ff, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 42
	// mod = 01; indirect register with one byte offset
	// reg = 0 (AX); r/m = 2 (BP+SI)

	// 0f b4 42 ff                lfs    ax,[bp+si-0x1]
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [4] {0x0F, 0xB4, 0x42, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(LFS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-1, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 82
	// mod = 10; indirect register with four byte offset
	// reg = 0 (AX); r/m = 2 (BP+SI)

	// 0f b4 82 ff 01       lfs    ax,[bp+si+0x1ff]
	code = new uint8_t [5] {0x0F, 0xB4, 0x82, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(LFS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x1ff, opc.source.offset_data);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing not allowed
	// reg = 0 (AX); r/m = 1 (CX)

	// 0f b4 c1                   (bad)
	code = new uint8_t [3] {0x0F, 0xB4, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);
	CHECK_EQUAL(0, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_register);
	CHECK_EQUAL(0, opc.dest.use_indregister);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.use_offset);
	CHECK_EQUAL(0, opc.dest.use_disponly);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_indregister);
	CHECK_EQUAL(0, opc.source.use_imm_word);
	CHECK_EQUAL(0, opc.source.use_offset);
	CHECK_EQUAL(0, opc.source.use_disponly);
	delete code;

	// LGS reg16,mem => 0FB5 [mod/reg/rm] []

	// mod/reg/rm = 01
	// mod = 00; + r/m = 1; [BX+DI]
	// reg = 0 (AX)

	// 0f b5 01                   lgs    ax,[bx+di]
	code = new uint8_t [3] {0x0F, 0xB5, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LGS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 08
	// mod = 00; + r/m = 0; [BX+SI]
	// reg = 1 (CX)

	// 0f b5 08                   lgs    cx,[bx+si]
	code = new uint8_t [3] {0x0F, 0xB5, 0x08};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LGS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 06
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 0 (AX)

	// 0f b5 06 ff 01       lgs    ax,ds:0x1ff
	code = new uint8_t [5] {0x0F, 0xB5, 0x06, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(LGS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x1ff, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 42
	// mod = 01; indirect register with one byte offset
	// reg = 0 (AX); r/m = 2 (BP+SI)

	// 0f b5 42 ff                lgs    ax,[bp+si-0x1]
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [4] {0x0F, 0xB5, 0x42, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(LGS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-1, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 82
	// mod = 10; indirect register with four byte offset
	// reg = 0 (AX); r/m = 2 (BP+SI)

	// 0f b5 82 ff 01       lgs    ax,[bp+si+0x1ff]
	code = new uint8_t [5] {0x0F, 0xB5, 0x82, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(LGS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x1ff, opc.source.offset_data);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing not allowed
	// reg = 0 (AX); r/m = 1 (CX)

	// 0f b5 c1                   (bad)
	code = new uint8_t [3] {0x0F, 0xB5, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);
	CHECK_EQUAL(0, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_register);
	CHECK_EQUAL(0, opc.dest.use_indregister);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.use_offset);
	CHECK_EQUAL(0, opc.dest.use_disponly);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_indregister);
	CHECK_EQUAL(0, opc.source.use_imm_word);
	CHECK_EQUAL(0, opc.source.use_offset);
	CHECK_EQUAL(0, opc.source.use_disponly);
	delete code;

	// LSS reg16,mem => 0FB2 [mod/reg/rm] []

	// mod/reg/rm = 01
	// mod = 00; + r/m = 1; [BX+DI]
	// reg = 0 (AX)

	// 0f b2 01                   lss    ax,[bx+di]
	code = new uint8_t [3] {0x0F, 0xB2, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LSS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx_di, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 08
	// mod = 00; + r/m = 0; [BX+SI]
	// reg = 1 (CX)

	// 0f b2 08                   lss    cx,[bx+si]
	code = new uint8_t [3] {0x0F, 0xB2, 0x08};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(3, a);
	CHECK_EQUAL(LSS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(cl_cx_ecx, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(ind_bx_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	delete code;

	// mod/reg/rm = 06
	// mod = 00 + r/m = 6; 16 bit displacement only
	// reg = 0 (AX)

	// 0f b2 06 ff 01       lss    ax,ds:0x1ff
	code = new uint8_t [5] {0x0F, 0xB2, 0x06, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(LSS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_disponly);
	CHECK_EQUAL(seg_ds, opc.source.register_segment);
	CHECK_EQUAL(0x1ff, opc.source.disponly_data);
	delete code;

	// mod/reg/rm = 42
	// mod = 01; indirect register with one byte offset
	// reg = 0 (AX); r/m = 2 (BP+SI)

	// 0f b2 42 ff                lss    ax,[bp+si-0x1]
	// todo: note the offset is signed, check it works for negative!!
	code = new uint8_t [4] {0x0F, 0xB2, 0x42, 0xFF};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(4, a);
	CHECK_EQUAL(LSS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(-1, opc.source.offset_data);
	delete code;

	// mod/reg/rm = 82
	// mod = 10; indirect register with four byte offset
	// reg = 0 (AX); r/m = 2 (BP+SI)

	// 0f b2 82 ff 01       lss    ax,[bp+si+0x1ff]
	code = new uint8_t [5] {0x0F, 0xB2, 0x82, 0xFF, 0x01};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(5, a);
	CHECK_EQUAL(LSS, opc.op);
	CHECK_EQUAL(WORD, opc.datasize);
	CHECK_EQUAL(1, opc.dest.use_register);
	CHECK_EQUAL(al_ax_eax, opc.dest.register_data);
	CHECK_EQUAL(WORD, opc.dest.register_size);
	CHECK_EQUAL(1, opc.source.use_indregister);
	CHECK_EQUAL(seg_ss, opc.source.register_segment);
	CHECK_EQUAL(ind_bp_si, opc.source.register_data);
	CHECK_EQUAL(WORD, opc.source.register_size);
	CHECK_EQUAL(1, opc.source.use_offset);
	CHECK_EQUAL(0x1ff, opc.source.offset_data);
	delete code;

	// mod/reg/rm = C1
	// mod = 11; register addressing not allowed
	// reg = 0 (AX); r/m = 1 (CX)

	// 0f b2 c1                   (bad)
	code = new uint8_t [3] {0x0F, 0xB2, 0xC1};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(2, a);
	CHECK_EQUAL(INVALID_OPCODE, opc.op);
	CHECK_EQUAL(0, opc.datasize);
	CHECK_EQUAL(0, opc.dest.use_register);
	CHECK_EQUAL(0, opc.dest.use_indregister);
	CHECK_EQUAL(0, opc.dest.use_imm_word);
	CHECK_EQUAL(0, opc.dest.use_offset);
	CHECK_EQUAL(0, opc.dest.use_disponly);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_register);
	CHECK_EQUAL(0, opc.source.use_indregister);
	CHECK_EQUAL(0, opc.source.use_imm_word);
	CHECK_EQUAL(0, opc.source.use_offset);
	CHECK_EQUAL(0, opc.source.use_disponly);
	delete code;

}

}
