#include "main_test.h"

namespace
{

// dev_8042.h

TEST(dev_8042)
{

	// initialise cpu
	init_cpu();

	// initialise ps2 keyboard and mouse
	CHECK_EQUAL(0, init_ps2_keyboard());
	CHECK_EQUAL(0, init_ps2_mouse());

	// initialise keyboard controller
	CHECK_EQUAL(0, init_keybd());

	//REQUIRE CHECK(write_to_port[0x60]);
	CHECK(write_to_port[0x60]);
	CHECK(write_to_port[0x64]);

	// read controller status register
	CHECK_EQUAL(0x18, read_from_port[0x64](0x64, BYTE));

	// Test PS/2 Controller
	write_to_port[0x64](0x64, 0xaa, BYTE);
	CHECK_EQUAL(0x55, read_from_port[0x60](0x60, BYTE));

	// Test first PS/2 port
	write_to_port[0x64](0x64, 0xab, BYTE);
	CHECK_EQUAL(0x00, read_from_port[0x60](0x60, BYTE));

	// Read Controller Output Port
	write_to_port[0x64](0x64, 0xd0, BYTE);
	CHECK_EQUAL(0x00, read_from_port[0x60](0x60, BYTE));

	// Write byte to Controller Output Port
	write_to_port[0x64](0x64, 0xd1, BYTE);
	write_to_port[0x60](0x60, 0xaa, BYTE);

	// Read Controller Output Port
	write_to_port[0x64](0x64, 0xd0, BYTE);
	CHECK_EQUAL(0xaa, read_from_port[0x60](0x60, BYTE));

	// Write byte to internal RAM
	write_to_port[0x64](0x64, 0x60, BYTE);
	write_to_port[0x60](0x60, 0xee, BYTE);

	// Read byte from internal RAM
	write_to_port[0x64](0x64, 0x20, BYTE);
	CHECK_EQUAL(0xee, read_from_port[0x60](0x60, BYTE));

	// shutdown
	shutdown_ps2_keyboard();
	shutdown_ps2_mouse();
	shutdown_keybd();

}

TEST(dev_8042_init)
{

	// initialise cpu
	init_cpu();

	// initialise ps2 keyboard and mouse
	CHECK_EQUAL(0, init_ps2_keyboard());
	CHECK_EQUAL(0, init_ps2_mouse());

	// initialise keyboard controller
	CHECK_EQUAL(0, init_keybd());

	// read controller status register
	for (int i=0; i<8192; i++) {
		CHECK_EQUAL(0x18, read_from_port[0x64](0x64, BYTE));
	}

	// write BYTE to port: 0x64 value: 0xaa			<= test PS/2 controller
	write_to_port[0x64](0x64, 0xaa, BYTE);
	// read BYTE from port: 0x64 value: 0x1d
	CHECK_EQUAL(0x1d, read_from_port[0x64](0x64, BYTE));
	// read BYTE from port: 0x64 value: 0x1d
	CHECK_EQUAL(0x1d, read_from_port[0x64](0x64, BYTE));
	// read BYTE from port: 0x60 value: 0x55
	CHECK_EQUAL(0x55, read_from_port[0x60](0x60, BYTE));

	// write BYTE to port: 0x64 value: 0xab			<= Test first PS/2 port
	write_to_port[0x64](0x64, 0xab, BYTE);
	// read BYTE from port: 0x64 value: 0x1d
	CHECK_EQUAL(0x1d, read_from_port[0x64](0x64, BYTE));
	// read BYTE from port: 0x64 value: 0x1d
	CHECK_EQUAL(0x1d, read_from_port[0x64](0x64, BYTE));
	// read BYTE from port: 0x60 value: 0x00
	CHECK_EQUAL(0x00, read_from_port[0x60](0x60, BYTE));

	// write BYTE to port: 0x64 value: 0xae			<= Enable first PS/2 port
	write_to_port[0x64](0x64, 0xae, BYTE);
	// write BYTE to port: 0x64 value: 0xa8			<= Enable second PS/2 port
	write_to_port[0x64](0x64, 0xa8, BYTE);
	// write BYTE to port: 0x60 value: 0xff
	write_to_port[0x60](0x60, 0xff, BYTE);
	// read BYTE from port: 0x64 value: 0x14 (x11)
	for (int i=0; i<11; i++) CHECK_EQUAL(0x14, read_from_port[0x64](0x64, BYTE));
	// read BYTE from port: 0x64 value: 0x15
	CHECK_EQUAL(0x15, read_from_port[0x64](0x64, BYTE));
	// read 0xFA from port 60: ack
	CHECK_EQUAL(0xfa, read_from_port[0x60](0x60, BYTE));
	// read BYTE from port: 0x64 value: 0x14 (x25)
	for (int i=0; i<25; i++) CHECK_EQUAL(0x14, read_from_port[0x64](0x64, BYTE));
	// read BYTE from port: 0x64 value: 0x15
	CHECK_EQUAL(0x15, read_from_port[0x64](0x64, BYTE));
	// read 0xAA from port 60: self-test passed
	CHECK_EQUAL(0xaa, read_from_port[0x60](0x60, BYTE));

	// write BYTE to port: 0x60 value: 0xf5
	write_to_port[0x60](0x60, 0xf5, BYTE);
	// read BYTE from port: 0x64 value: 0x14 (x25)
	for (int i=0; i<25; i++) CHECK_EQUAL(0x14, read_from_port[0x64](0x64, BYTE));
	// read BYTE from port: 0x64 value: 0x15
	CHECK_EQUAL(0x15, read_from_port[0x64](0x64, BYTE));
	// read 0xFA from port 60: ack
	CHECK_EQUAL(0xfa, read_from_port[0x60](0x60, BYTE));

	// write BYTE to port: 0x64 value: 0x60
	// next byte will be "Controller Configuration Byte"
	write_to_port[0x64](0x64, 0x60, BYTE);
	// read BYTE from port: 0x64 value: 0x1c
	CHECK_EQUAL(0x1c, read_from_port[0x64](0x64, BYTE));
	// write BYTE to port: 0x60 value: 0x61 [ 0110 0001 ]
	// 0 - First PS/2 port interrupt (1 = enabled)
	// 1 - Second PS/2 port interrupt (0 = disabled)
	// 4 - First PS/2 port clock (0 = enabled)
	// 6 - First PS/2 port translation (1 = enabled)
	write_to_port[0x60](0x60, 0x61, BYTE);
	// read BYTE from port: 0x64 value: 0x10
	CHECK_EQUAL(0x10, read_from_port[0x64](0x64, BYTE));

	// write BYTE to port: 0x60 value: 0xf4
	write_to_port[0x60](0x60, 0xf4, BYTE);
	// read BYTE from port: 0x64 value: 0x10 (x24)
	for (int i=0; i<24; i++) CHECK_EQUAL(0x10, read_from_port[0x64](0x64, BYTE));
	// read BYTE from port: 0x64 value: 0x11
	CHECK_EQUAL(0x11, read_from_port[0x64](0x64, BYTE));
	// read 0xFA from port 60: ack
	CHECK_EQUAL(0xfa, read_from_port[0x60](0x60, BYTE));

	// shutdown
	shutdown_ps2_keyboard();
	shutdown_ps2_mouse();
	shutdown_keybd();

}

}
