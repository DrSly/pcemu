#include "main_test.h"

namespace
{

// decode.h

TEST(decode_FLAGS)
{

	mode32 = 1;

	// --- CMC: F5 ---
	uint8_t *code = new uint8_t [1] {0xF5};
	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	int a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(CMC, opc.op);
	delete code;

	// --- CLC: F8 ---
	code = new uint8_t [1] {0xF8};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(CLC, opc.op);
	delete code;

	// --- STC: F9 ---
	code = new uint8_t [1] {0xF9};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(STC, opc.op);
	delete code;

	// --- CLD: FC ---
	code = new uint8_t [1] {0xFC};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(CLD, opc.op);
	delete code;

	// --- STD: FD ---
	code = new uint8_t [1] {0xFD};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(STD, opc.op);
	delete code;

	// --- CLI: FA ---
	code = new uint8_t [1] {0xFA};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(CLI, opc.op);
	delete code;

	// --- STI: FB ---
	code = new uint8_t [1] {0xFB};
	memset(&opc, 0, sizeof(struct instruction));
	a = decode(code, &opc);
	CHECK_EQUAL(1, a);
	CHECK_EQUAL(STI, opc.op);
	delete code;

}

}
