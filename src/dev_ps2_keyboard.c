#include <stdint.h>

#include "dev_8042.h"
#include "dev_ps2_keyboard.h"

//
// dev_ps2_keyboard.c
//
// stub for the keyboard hardware interface implementation
//

void init_ps2kbd(void);
uint8_t read_from_ps2kbd(void);
void write_to_ps2kbd(uint8_t data);
int has_data_ps2kbd(void);

int delay;		// don't send response until delay = 0
uint8_t response;
int response_set;
int reset_flag;

int init_ps2_keyboard() {

	delay = 0;
	response = 0;
	response_set = 0;
	reset_flag = 0;

	register_ps2_device(DEV_KEYBD, &init_ps2kbd, &read_from_ps2kbd, &write_to_ps2kbd, &has_data_ps2kbd);

	return 0;

}

void shutdown_ps2_keyboard() {

	unregister_ps2_device(DEV_KEYBD);

}

void init_ps2kbd(void) {

}

uint8_t read_from_ps2kbd(void) {

	uint8_t data = response;

	if (reset_flag == 1) {
		delay = 26;
		response = 0xaa;
		response_set = 1;
		reset_flag--;
	}
	else {
		response_set = 0;
	}

	return data;

}

void write_to_ps2kbd(uint8_t data) {

	switch (data) {
	case 0xf4:		// Enable scanning
		delay = 25;
		response = 0xfa;
		response_set = 1;
		break;
	case 0xf5:		// Disable scanning
		delay = 26;
		response = 0xfa;
		response_set = 1;
		break;
	case 0xff:		// Reset and self-test
		reset_flag = 1;
		delay = 12;
		response = 0xfa;
		response_set = 1;
		break;
	}

}

int has_data_ps2kbd(void) {

	if (!response_set)
		return 0;

	delay--;
	if (delay <= 0) {
		return 1;
	}
	return 0;

}
