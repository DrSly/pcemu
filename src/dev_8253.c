#include <stdio.h>

#include "cpu.h"
#include "dev_8253.h"

#define PIT_DATA0 0x40		// Channel 0 data port
#define PIT_DATA1 0x41		// Channel 1 data port
#define PIT_DATA2 0x42		// Channel 2 data port
#define PIT_CMD 0x43		// Mode/Command Register

enum ACCESS_MODE {
	LATCH_COUNT = 0,
	LO_BYTE = 1,
	HI_BYTE = 2,
	LO_HI_BYTE = 3
};

enum OPERATING_MODE {
	Interrupt_On_Terminal_Count = 0,
	Hardware_Retriggerable_Oneshot = 1,
	Rate_Generator = 2,
	Square_Wave_Generator = 3,
	Software_Triggered_Strobe = 4,
	Hardware_Triggered_Strobe = 5
};

int read_pit_port(int port, enum datasize);
void write_pit_port(int port, int data, enum datasize);
void set_access_mode(int channel, enum ACCESS_MODE access_mode);
void set_operating_mode(int channel, enum OPERATING_MODE operating_mode);
void set_latch_register(int channel);
void set_status_flag(int channel);
void set_reload_value(int channel, uint8_t new_value);
int read_counter_value(int channel);

// pit channels
struct PIT_CHANNEL {
	enum OPERATING_MODE operating_mode;	// operating modes 0..5
	enum ACCESS_MODE access_mode;		// access modes 0..3
	uint16_t reload_value;		// value to load when counter reaches zero
	uint16_t current_value;		// current counter value
	uint16_t latch_value;		// counter latch register
	int reload_set;				// 0 = reload value is in the process of being set; 1 = reload value is set
	int latch_set;				// 1 = latch_value has been set
	int read_status;			// 1 = read status flag has been set
	int flipflop;				// 0 = low byte; 1 = high byte
	int gate_low;				// 1 = input gate was previously low; used to detect rising edge of input gate
	int strobe_triggerred;		// for operating modes 4 & 5, set to 1 if counter has already reached zero
} pit_channel[3];

int init_pit() {

	// initialise internal pit registers
	for (int i=0; i<3; i++) {
		pit_channel[i].operating_mode = Interrupt_On_Terminal_Count;
		pit_channel[i].access_mode = LATCH_COUNT;
		pit_channel[i].reload_value = 0xffff;
		pit_channel[i].current_value = 0xffff;
		pit_channel[i].latch_value = 0xffff;
		pit_channel[i].reload_set = 0;
		pit_channel[i].latch_set = 0;
		pit_channel[i].read_status = 0;
		pit_channel[i].flipflop = 0;
		pit_channel[i].gate_low = 0;
		pit_channel[i].strobe_triggerred = 0;
	}

	// initialise pit output signal
	for (int i=0; i<3; i++) {
		pit_output[i] = 1;
	}

	// register io handler
	register_io_handler(PIT_DATA0, 1, &read_pit_port, &write_pit_port);
	register_io_handler(PIT_DATA1, 1, &read_pit_port, &write_pit_port);
	register_io_handler(PIT_DATA2, 1, &read_pit_port, &write_pit_port);
	register_io_handler(PIT_CMD, 1, NULL, &write_pit_port);

	return 0;
}

void shutdown_pit() {

	// register io handler
	unregister_io_handler(PIT_DATA0, 1);
	unregister_io_handler(PIT_DATA1, 1);
	unregister_io_handler(PIT_DATA2, 1);
	unregister_io_handler(PIT_CMD, 1);

}

// input clock frequency 1.193182 MHz
// Note: The PIT channel 2 gate is controlled by IO port 0x61, bit 0
// this can be used to prevent ticks from affecting the counter
void tick() {

	// the action always happens at the falling edge of the input clock signal
	for (int i=0; i<3; i++) {

		//
		// Interrupt_On_Terminal_Count = 0
		//

		if (pit_channel[i].operating_mode == Interrupt_On_Terminal_Count) {

			if (pit_channel[i].reload_set) {

				pit_channel[i].current_value = pit_channel[i].reload_value;
				pit_channel[i].reload_set = 0;
				// set pit output signal low
				pit_output[i] = 0;

			}
			else {

				if (gate_input[i] == 1) pit_channel[i].current_value--;
				if (pit_channel[i].current_value == 0) {
					pit_output[i] = 1;
				}

			}

		}

		//
		// Hardware_Retriggerable_Oneshot = 1
		//

		if (pit_channel[i].operating_mode == Hardware_Retriggerable_Oneshot) {

			if ((pit_channel[i].reload_set) && (pit_channel[i].gate_low) && (gate_input[i] == 1)) {

				pit_channel[i].current_value = pit_channel[i].reload_value;
				//pit_channel[i].reload_set = 0;	don't reset this flag as the current_value should be reloaded every time the gate input changes to 1
				pit_channel[i].gate_low = 0;
				// set pit output signal low
				pit_output[i] = 0;

			}
			else {

				pit_channel[i].current_value--;
				if (pit_channel[i].current_value == 0) {
					// set pit output signal high
					pit_output[i] = 1;
				}

			}

		}

		//
		// Mode 2 - Rate_Generator
		//

		if (pit_channel[i].operating_mode == Rate_Generator) {

			if ((pit_channel[i].reload_set) && (pit_channel[i].gate_low) && (gate_input[i] == 1)) {

				pit_channel[i].current_value = pit_channel[i].reload_value;
				//pit_channel[i].reload_set = 0;	don't reset this flag as the current_value should be reloaded every time the gate input changes to 1
				pit_channel[i].gate_low = 0;

			}
			else {

				if (gate_input[i] == 1) pit_channel[i].current_value--;
				if (pit_channel[i].current_value == 1) {
					// set pit output signal low
					pit_output[i] = 0;
				}
				if (pit_channel[i].current_value == 0) {
					// set pit output signal high
					pit_output[i] = 1;
					pit_channel[i].current_value = pit_channel[i].reload_value;
				}
				// set pit output signal high immediately if the gate input goes low
				if (gate_input[i] == 0) pit_output[i] = 1;

			}

		}

		//
		// Square_Wave_Generator = 3
		//

		if (pit_channel[i].operating_mode == Square_Wave_Generator) {

			if ((pit_channel[i].reload_set) && (pit_channel[i].gate_low) && (gate_input[i] == 1)) {

				pit_channel[i].current_value = pit_channel[i].reload_value;
				if (pit_channel[i].reload_value % 2 == 1) pit_channel[i].current_value--;
				//pit_channel[i].reload_set = 0;	don't reset this flag as the current_value should be reloaded every time the gate input changes to 1
				pit_channel[i].gate_low = 0;

			}
			else {

				if (gate_input[i] == 1) pit_channel[i].current_value -= 2;
				if (pit_channel[i].current_value == 0) {
					// toggle the pit output signal
					if (pit_output[i] == 1)
						pit_output[i] = 0;
					else
						pit_output[i] = 1;
					pit_channel[i].current_value = pit_channel[i].reload_value;
					if (pit_channel[i].reload_value % 2 == 1) pit_channel[i].current_value--;
				}
				// set pit output signal high immediately if the gate input goes low
				if (gate_input[i] == 0) pit_output[i] = 1;

			}

		}

		//
		// Software_Triggered_Strobe = 4
		//

		if (pit_channel[i].operating_mode == Software_Triggered_Strobe) {

			// in mode 4 the output should be low for one cycle only
			if (pit_output[i] == 0) {
				pit_channel[i].strobe_triggerred = 1;
				pit_output[i] = 1;
			}

			if (pit_channel[i].reload_set) {

				pit_channel[i].current_value = pit_channel[i].reload_value;
				pit_channel[i].reload_set = 0;

			}
			else {

				if (gate_input[i] == 1) pit_channel[i].current_value--;
				if ((pit_channel[i].current_value == 0) && (!pit_channel[i].strobe_triggerred)) {
					// set pit output signal low for one tick
					pit_output[i] = 0;
				}

			}

		}

		//
		// Hardware_Triggered_Strobe = 5
		//

		if (pit_channel[i].operating_mode == Hardware_Triggered_Strobe) {

			// in mode 5 the output should be low for one cycle only
			if (pit_output[i] == 0) {
				pit_channel[i].strobe_triggerred = 1;
				pit_output[i] = 1;
			}

			if ((pit_channel[i].reload_set) && (pit_channel[i].gate_low) && (gate_input[i] == 1)) {

				pit_channel[i].current_value = pit_channel[i].reload_value;
				//pit_channel[i].reload_set = 0;	don't reset this flag as the current_value should be reloaded every time the gate input changes to 1
				pit_channel[i].gate_low = 0;

			}
			else {

				// note: the documentation suggests the gate input doesn't affect counting
				// todo: test on real hardware!!
				if (gate_input[i] == 1) pit_channel[i].current_value--;
				if ((pit_channel[i].current_value == 0) && (!pit_channel[i].strobe_triggerred)) {
					// set pit output signal low for one tick
					pit_output[i] = 0;
				}

			}

		}

		// set flag if input gate goes low
		if (gate_input[i] == 0) {
			pit_channel[i].gate_low = 1;
		}

	}	// for (int i=0; i<3; i++)

}

int read_pit_port(int port, enum datasize datasize) {

	int val = 0;

	switch (port) {
	case PIT_DATA0:	// Channel 0 data port
		val = read_counter_value(0);
		break;
	case PIT_DATA1:	// Channel 1 data port
		val = read_counter_value(1);
		break;
	case PIT_DATA2:	// Channel 2 data port
		val = read_counter_value(2);
		break;
	}

	return val;

}

void write_pit_port(int port, int data, enum datasize datasize) {

	int bcd_mode = data & 0x01;					// bit 0
	int operating_mode = (data >> 1) & 0x07;	// bit 1..3
	enum ACCESS_MODE access_mode = (data >> 4) & 0x03;	// bit 4..5
	int channel = (data >> 6) & 0x03;			// bit 6..7

	switch (port) {
	case PIT_DATA0:	// Channel 0 data port
		set_reload_value(0, data);
		break;
	case PIT_DATA1:	// Channel 1 data port
		set_reload_value(1, data);
		break;
	case PIT_DATA2:	// Channel 2 data port
		set_reload_value(2, data);
		break;
	case PIT_CMD:	// Mode/Command Register
		// Counter Latch
		if ((operating_mode == 0) && (access_mode == LATCH_COUNT)) {
			set_latch_register(channel);
			break;
		}
		// Read Back Status Byte (8254 only)
		if (channel == 3) {
			int latch_count_flag = data & 0x20;
			int latch_status_flag = data & 0x10;
			int ch2 = (data >> 3) & 0x01;
			int ch1 = (data >> 2) & 0x01;
			int ch0 = (data >> 1) & 0x01;
			if (ch0 != 0) {
				if (latch_count_flag == 0) set_latch_register(0);
				if (latch_status_flag == 0) set_status_flag(0);
			}
			if (ch1 != 0) {
				if (latch_count_flag == 0) set_latch_register(1);
				if (latch_status_flag == 0) set_status_flag(1);
			}
			if (ch2 != 0) {
				if (latch_count_flag == 0) set_latch_register(2);
				if (latch_status_flag == 0) set_status_flag(2);
			}
			break;
		}
		if (access_mode != LATCH_COUNT) {
			set_access_mode(channel, access_mode);
		}
		set_operating_mode(channel, operating_mode);
		break;
	}

}

void set_access_mode(int channel, enum ACCESS_MODE access_mode) {

	if ((channel < 0) || (channel > 2)) return;

	pit_channel[channel].access_mode = access_mode;

}

void set_operating_mode(int channel, enum OPERATING_MODE operating_mode) {

	if ((channel < 0) || (channel > 2)) return;

	pit_channel[channel].operating_mode = operating_mode;

	// set pit output signal

	if (pit_channel[channel].operating_mode == Interrupt_On_Terminal_Count)
		pit_output[channel] = 0;

	if (pit_channel[channel].operating_mode == Hardware_Retriggerable_Oneshot)
		pit_output[channel] = 1;

	if (pit_channel[channel].operating_mode == Rate_Generator) {
		// set the gate_low flag to force the reload of the current count
		pit_channel[channel].gate_low = 1;
		pit_output[channel] = 1;
	}

	if (pit_channel[channel].operating_mode == Square_Wave_Generator) {
		// set the gate_low flag to force the reload of the current count
		pit_channel[channel].gate_low = 1;
		pit_output[channel] = 1;
	}

	if (pit_channel[channel].operating_mode == Software_Triggered_Strobe) {
		// reset the strobe_triggerred flag
		pit_channel[channel].strobe_triggerred = 0;
		pit_output[channel] = 1;
	}

	if (pit_channel[channel].operating_mode == Hardware_Triggered_Strobe) {
		// set the gate_low flag to force the reload of the current count
		pit_channel[channel].gate_low = 1;
		// reset the strobe_triggerred flag
		pit_channel[channel].strobe_triggerred = 0;
		pit_output[channel] = 1;
	}

}

void set_latch_register(int channel) {

	if ((channel < 0) || (channel > 2)) return;

	pit_channel[channel].latch_value = pit_channel[channel].current_value;
	pit_channel[channel].latch_set = 1;

}

void set_status_flag(int channel) {

	if ((channel < 0) || (channel > 2)) return;

	pit_channel[channel].read_status = 1;

}

void set_reload_value(int channel, uint8_t new_value) {

	if ((channel < 0) || (channel > 2)) return;

	if (pit_channel[channel].access_mode == LO_BYTE) {

		uint16_t val = pit_channel[channel].reload_value;
		val &= 0xff00;
		val |= new_value;
		pit_channel[channel].reload_value = val;
		pit_channel[channel].reload_set = 1;

	}

	if (pit_channel[channel].access_mode == HI_BYTE) {

		uint16_t val = pit_channel[channel].reload_value;
		val &= 0x00ff;
		val |= (new_value << 8);
		pit_channel[channel].reload_value = val;
		pit_channel[channel].reload_set = 1;

	}

	if (pit_channel[channel].access_mode == LO_HI_BYTE) {

		uint16_t val = pit_channel[channel].reload_value;
		// check the flip-flop to determine which byte to set
		if (pit_channel[channel].flipflop == 0) {	// set the low byte first
			val &= 0xff00;
			val |= new_value;
			pit_channel[channel].reload_value = val;
			pit_channel[channel].reload_set = 0;
			pit_channel[channel].flipflop = 1;
		}
		else {		// set the high byte
			val &= 0x00ff;
			val |= (new_value << 8);
			pit_channel[channel].reload_value = val;
			pit_channel[channel].reload_set = 1;
			pit_channel[channel].flipflop = 0;
		}

	}

}

int read_counter_value(int channel) {

	if ((channel < 0) || (channel > 2)) return 0;

	uint16_t val;

	// use the latch value if set
	val = pit_channel[channel].current_value;
	if (pit_channel[channel].latch_set != 0) {
		val = pit_channel[channel].latch_value;
	}

	if (pit_channel[channel].access_mode == LO_BYTE) {

		val &= 0x00ff;
		pit_channel[channel].latch_set = 0;

	}

	if (pit_channel[channel].access_mode == HI_BYTE) {

		val &= 0xff00;
		val = val >> 8;
		pit_channel[channel].latch_set = 0;

	}

	if (pit_channel[channel].access_mode == LO_HI_BYTE) {

		// check the flip-flop to determine which byte to return
		if (pit_channel[channel].flipflop == 0) {	// return the low byte first
			val &= 0x00ff;
			pit_channel[channel].flipflop = 1;
		}
		else {		// return the high byte
			val &= 0xff00;
			val = val >> 8;
			pit_channel[channel].flipflop = 0;
			pit_channel[channel].latch_set = 0;
		}

	}

	return val;

}
