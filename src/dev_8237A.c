#include <stdio.h>
#include <string.h>		// for memset()

#include "cpu.h"
#include "dev_8237A.h"

#define DMA_1 0x00		// 1st dma controller
#define DMARANGE_1 16	// 1st dma controller
#define DMA_2 0xC0		// 2nd dma controller
#define DMARANGE_2 32	// 2nd dma controller
#define DMA_PAGE_REGISTER 0x80	// dma page registers
#define PAGE_REGISTER_COUNT 16	// dma page registers

int read_dma_port(int port, enum datasize);
void write_dma_port(int port, int data, enum datasize);
int read_dma_page_register(int page_register, enum datasize);
void write_dma_page_register(int page_register, int data, enum datasize);

// port 80 is described as part of the dma page registers but it's purpose is unclear
// it is described as "Manufacturer systems checkpoint port (used during POST)"
// http://stanislavs.org/helppc/ports.html
uint8_t port0x80;

// dma1 controller
int dma1_disabled;	// 1 = dma controller disabled
int dma1_flipflop;	// 0 = low byte; 1 = high byte

// dma2 controller
int dma2_disabled;	// 1 = dma controller disabled
int dma2_flipflop;	// 0 = low byte; 1 = high byte

// dma channels
struct DMA_CHANNEL {
	uint8_t page_register;
	uint16_t start_address;
	uint16_t transfer_bytes;
	uint8_t transfer_direction;		// 1 = write to memory; 2 = read from memory
	uint8_t auto_reset;				// 1 = reset start_address and transfer_bytes on completion
	uint8_t reverse_memory;			// 1 = reverse (memory address decremented)
	uint8_t transfer_mode;			// 0 = on demand; 1 = single; 2 = block; 3 = cascade
	enum datasize transfer_size;
	int is_masked;					// 1 = channel masked (disabled)
	int request_pending;			// 1 = dma request is in progress
	int transfer_complete;			// 1 = dma transfer is complete
} dma_channel[8];

int init_dma() {

	// zero out dma_channel structure
	memset(dma_channel, 0, sizeof(struct DMA_CHANNEL) * 8);

	// register io handler
	register_io_handler(DMA_1, DMARANGE_1, &read_dma_port, &write_dma_port);
	register_io_handler(DMA_2, DMARANGE_2, &read_dma_port, &write_dma_port);
	register_io_handler(DMA_PAGE_REGISTER, PAGE_REGISTER_COUNT, &read_dma_page_register, &write_dma_page_register);

	return 0;
}

void shutdown_dma() {

	// register io handler
	unregister_io_handler(DMA_1, DMARANGE_1);
	unregister_io_handler(DMA_2, DMARANGE_2);
	unregister_io_handler(DMA_PAGE_REGISTER, PAGE_REGISTER_COUNT);

}

int read_dma_port(int port, enum datasize datasize) {

	int val = 0;

	switch (port) {
	case 0x08:	// DMA1 Status Register
		for (int i=3; i>=0; i--) {
			val += (dma_channel[i].request_pending & 0x01);
			val = val << 1;
		}
		for (int i=3; i>=0; i--) {
			val += (dma_channel[i].transfer_complete & 0x01);
			val = val << 1;
			dma_channel[i].transfer_complete = 0;	// reset transfer_complete
		}
		break;
	case 0x0f:	// DMA1 MultiChannel Mask Register
		for (int i=3; i>=0; i--) {
			val += (dma_channel[i].is_masked & 0x01);
			if (i > 0) val = val << 1;
		}
		break;
	case 0xd0:	// DMA2 Status Register
		for (int i=3; i>=0; i--) {
			val += (dma_channel[i + 4].request_pending & 0x01);
			if (i > 0) val = val << 1;
		}
		for (int i=3; i>=0; i--) {
			val += (dma_channel[i + 4].transfer_complete & 0x01);
			if (i > 0) val = val << 1;
			dma_channel[i + 4].transfer_complete = 0;	// reset transfer_complete
		}
		break;
	case 0xde:	// DMA2 MultiChannel Mask Register
		for (int i=3; i>=0; i--) {
			val += (dma_channel[i + 4].is_masked & 0x01);
			if (i > 0) val = val << 1;
		}
		break;
	}

	return val;
}

void write_dma_port(int port, int data, enum datasize datasize) {

	switch (port) {
	case 0x00:	// DMA1 Start Address Register channel 0
		if (dma1_flipflop == 0) {
			dma_channel[0].start_address = data;
			dma1_flipflop = 1;
		}
		else {
			dma_channel[0].start_address += (data << 8);
			dma1_flipflop = 0;
		}
		break;
	case 0x01:	// DMA1 Count Register channel 0
		if (dma1_flipflop == 0) {
			dma_channel[0].transfer_bytes = data;
			dma1_flipflop = 1;
		}
		else {
			dma_channel[0].transfer_bytes += (data << 8);
			dma1_flipflop = 0;
		}
		break;
	case 0x02:	// DMA1 Start Address Register channel 1
		if (dma1_flipflop == 0) {
			dma_channel[1].start_address = data;
			dma1_flipflop = 1;
		}
		else {
			dma_channel[1].start_address += (data << 8);
			dma1_flipflop = 0;
		}
		break;
	case 0x03:	// DMA1 Count Register channel 1
		if (dma1_flipflop == 0) {
			dma_channel[1].transfer_bytes = data;
			dma1_flipflop = 1;
		}
		else {
			dma_channel[1].transfer_bytes += (data << 8);
			dma1_flipflop = 0;
		}
		break;
	case 0x04:	// DMA1 Start Address Register channel 2
		if (dma1_flipflop == 0) {
			dma_channel[2].start_address = data;
			dma1_flipflop = 1;
		}
		else {
			dma_channel[2].start_address += (data << 8);
			dma1_flipflop = 0;
		}
		break;
	case 0x05:	// DMA1 Count Register channel 2
		if (dma1_flipflop == 0) {
			dma_channel[2].transfer_bytes = data;
			dma1_flipflop = 1;
		}
		else {
			dma_channel[2].transfer_bytes += (data << 8);
			dma1_flipflop = 0;
		}
		break;
	case 0x06:	// DMA1 Start Address Register channel 3
		if (dma1_flipflop == 0) {
			dma_channel[3].start_address = data;
			dma1_flipflop = 1;
		}
		else {
			dma_channel[3].start_address += (data << 8);
			dma1_flipflop = 0;
		}
		break;
	case 0x07:	// DMA1 Count Register channel 3
		if (dma1_flipflop == 0) {
			dma_channel[3].transfer_bytes = data;
			dma1_flipflop = 1;
		}
		else {
			dma_channel[3].transfer_bytes += (data << 8);
			dma1_flipflop = 0;
		}
		break;
	case 0x08:	// DMA1 Command Register
		dma1_disabled = (data & 0x04) >> 2;
		break;
	case 0x0a:	// DMA1 Single Channel Mask Register
		dma_channel[data & 0x03].is_masked = (data & 0x04) ? 1 : 0;
		break;
	case 0x0b:	// DMA1 Mode Register
		dma_channel[data & 0x03].transfer_direction = (data & 0x0c) >> 2;
		dma_channel[data & 0x03].auto_reset = (data & 0x10) >> 4;
		dma_channel[data & 0x03].reverse_memory = (data & 0x20) >> 5;
		dma_channel[data & 0x03].transfer_mode = (data & 0xc0) >> 6;
		break;
	case 0x0c:	// DMA1 Flip-Flop Reset Register
		dma1_flipflop = 0;
		break;
	case 0x0d:	// DMA1 Master Reset Register
		dma1_flipflop = 0;
		for (int i=0; i<4; i++) {
			dma_channel[i].request_pending = 0;
			dma_channel[i].transfer_complete = 0;
			dma_channel[i].is_masked = 1;
		}
		break;
	case 0x0e:	// DMA1 Mask Reset Register
		for (int i=0; i<4; i++) {
			dma_channel[i].is_masked = 0;
		}
		break;
	case 0x0f:	// DMA1 MultiChannel Mask Register
		for (int i=0; i<4; i++) {
			dma_channel[i].is_masked = data & 0x01;
			data = data >> 1;
		}
		break;
	case 0xc0:	// DMA2 Start Address Register channel 4
		if (dma2_flipflop == 0) {
			dma_channel[4].start_address = data;
			dma2_flipflop = 1;
		}
		else {
			dma_channel[4].start_address += (data << 8);
			dma2_flipflop = 0;
		}
		break;
	case 0xc2:	// DMA2 Count Register channel 4
		if (dma2_flipflop == 0) {
			dma_channel[4].transfer_bytes = data;
			dma2_flipflop = 1;
		}
		else {
			dma_channel[4].transfer_bytes += (data << 8);
			dma2_flipflop = 0;
		}
		break;
	case 0xc4:	// DMA2 Start Address Register channel 5
		if (dma2_flipflop == 0) {
			dma_channel[5].start_address = data;
			dma2_flipflop = 1;
		}
		else {
			dma_channel[5].start_address += (data << 8);
			dma2_flipflop = 0;
		}
		break;
	case 0xc6:	// DMA2 Count Register channel 5
		if (dma2_flipflop == 0) {
			dma_channel[5].transfer_bytes = data;
			dma2_flipflop = 1;
		}
		else {
			dma_channel[5].transfer_bytes += (data << 8);
			dma2_flipflop = 0;
		}
		break;
	case 0xc8:	// DMA2 Start Address Register channel 6
		if (dma2_flipflop == 0) {
			dma_channel[6].start_address = data;
			dma2_flipflop = 1;
		}
		else {
			dma_channel[6].start_address += (data << 8);
			dma2_flipflop = 0;
		}
		break;
	case 0xca:	// DMA2 Count Register channel 6
		if (dma2_flipflop == 0) {
			dma_channel[6].transfer_bytes = data;
			dma2_flipflop = 1;
		}
		else {
			dma_channel[6].transfer_bytes += (data << 8);
			dma2_flipflop = 0;
		}
		break;
	case 0xcc:	// DMA2 Start Address Register channel 7
		if (dma2_flipflop == 0) {
			dma_channel[7].start_address = data;
			dma2_flipflop = 1;
		}
		else {
			dma_channel[7].start_address += (data << 8);
			dma2_flipflop = 0;
		}
		break;
	case 0xce:	// DMA2 Count Register channel 7
		if (dma2_flipflop == 0) {
			dma_channel[7].transfer_bytes = data;
			dma2_flipflop = 1;
		}
		else {
			dma_channel[7].transfer_bytes += (data << 8);
			dma2_flipflop = 0;
		}
		break;
	case 0xd0:	// DMA2 Command Register
		dma2_disabled = (data & 0x04) >> 2;
		break;
	case 0xd4:	// DMA2 Single Channel Mask Register
		dma_channel[(data & 0x03) + 4].is_masked = (data & 0x04) ? 1 : 0;
		break;
	case 0xd6:	// DMA2 Mode Register
		dma_channel[(data & 0x03) + 4].transfer_direction = (data & 0x0c) >> 2;
		dma_channel[(data & 0x03) + 4].auto_reset = (data & 0x10) >> 4;
		dma_channel[(data & 0x03) + 4].reverse_memory = (data & 0x20) >> 5;
		dma_channel[(data & 0x03) + 4].transfer_mode = (data & 0xc0) >> 6;
		break;
	case 0xd8:	// DMA2 Flip-Flop Reset Register
		dma2_flipflop = 0;
		break;
	case 0xda:	// DMA2 Master Reset Register
		dma2_flipflop = 0;
		for (int i=0; i<4; i++) {
			dma_channel[i + 4].request_pending = 0;
			dma_channel[i + 4].transfer_complete = 0;
			dma_channel[i + 4].is_masked = 1;
		}
		break;
	case 0xdc:	// DMA2 Mask Reset Register
		for (int i=0; i<4; i++) {
			dma_channel[i + 4].is_masked = 0;
		}
		break;
	case 0xde:	// DMA2 MultiChannel Mask Register
		for (int i=0; i<4; i++) {
			dma_channel[i + 4].is_masked = data & 0x01;
			data = data >> 1;
		}
		break;
	}

}

int read_dma_page_register(int page_register, enum datasize datasize) {

	int val = 0;

	switch (page_register) {
	case 0x80:
		val = port0x80;
		break;
	case 0x81:
		val = dma_channel[2].page_register;
		break;
	case 0x82:
		val = dma_channel[3].page_register;
		break;
	case 0x83:
		val = dma_channel[1].page_register;
		break;
	case 0x87:
		val = dma_channel[0].page_register;
		break;
	case 0x89:
		val = dma_channel[6].page_register;
		break;
	case 0x8a:
		val = dma_channel[7].page_register;
		break;
	case 0x8b:
		val = dma_channel[5].page_register;
		break;
	case 0x8f:
		val = dma_channel[4].page_register;
		break;
	}

	return val;
}

void write_dma_page_register(int page_register, int data, enum datasize datasize) {

	switch (page_register) {
	case 0x80:
		port0x80 = data;
		break;
	case 0x81:
		dma_channel[2].page_register = data;
		break;
	case 0x82:
		dma_channel[3].page_register = data;
		break;
	case 0x83:
		dma_channel[1].page_register = data;
		break;
	case 0x87:
		dma_channel[0].page_register = data;
		break;
	case 0x89:
		dma_channel[6].page_register = data;
		break;
	case 0x8a:
		dma_channel[7].page_register = data;
		break;
	case 0x8b:
		dma_channel[5].page_register = data;
		break;
	case 0x8f:
		dma_channel[4].page_register = data;
		break;
	}

}
