#include <stdio.h>

#include "zlog.h"
#include "cpu.h"
#include "dev_8042.h"

#define KEYBD_DATA 0x60		// data port
#define KEYBD_CTL 0x64		// Status/Command Register

int read_keybd_port(int port, enum datasize);
void write_keybd_port(int port, int data, enum datasize);

// PS/2 device interface
void init_ps2_device(int dev_id);
uint8_t read_from_ps2_device(int dev_id);
void write_to_ps2_device(int dev_id, uint8_t data);
int can_read_ps2_device(int dev_id);

// controller contains 32 bytes of internal memory
uint8_t keybd_ram[32];
// status register contains various flags that indicate the state of the controller
uint8_t keybd_status_register;
// controller output port is tied to various functions eg. cpu reset line and A20 gate
uint8_t keybd_output_port;
// output buffer - read via KEYBD_DATA port 0x60
uint8_t output_buffer;
// input buffer - sent to either the controller or the attached device
uint8_t input_buffer;
// device to write to - DEV_KEYBD = 1st PS/2 port; DEV_MOUSE = 2nd PS/2 port
int current_device;

int init_keybd() {

	// initialise internal ram and status register
	for (int i=0; i<32; i++)
		keybd_ram[i] = 0;

	// initialise status register:
	// 0x08 = data written to port 60 for PS/2 controller command
	// 0x10 = unknown (may be "keyboard lock")
	keybd_status_register = 0x18;

	keybd_output_port = 0x00;

	// register io handler
	register_io_handler(KEYBD_DATA, 1, &read_keybd_port, &write_keybd_port);
	register_io_handler(KEYBD_CTL, 1, &read_keybd_port, &write_keybd_port);

	// initialise PS/2 devices
	for (int i=0; i<2; i++) {
		init_ps2_device(i);
	}

	// todo: implement Second PS/2 Port for mouse
	current_device = DEV_KEYBD;

	return 0;
}

void shutdown_keybd() {

	// register io handler
	unregister_io_handler(KEYBD_CTL, 1);
	unregister_io_handler(KEYBD_DATA, 1);

}

void register_ps2_device(int dev_id, void (fn_init)(void), uint8_t (fn_read)(void), void (fn_write)(uint8_t), int (fn_canread)(void)) {

	// register PS/2 device handler(s)
	init_device[dev_id] = fn_init;
	read_from_device[dev_id] = fn_read;
	write_to_device[dev_id] = fn_write;
	device_has_data[dev_id] = fn_canread;

}

void unregister_ps2_device(int dev_id) {

	// unregister PS/2 device handler(s)
	init_device[dev_id] = NULL;
	read_from_device[dev_id] = NULL;
	write_to_device[dev_id] = NULL;
	device_has_data[dev_id] = NULL;

}

int read_keybd_port(int port, enum datasize datasize) {

	int val = 0;

	if (port == KEYBD_CTL) {

		if ((keybd_status_register & 0x08) == 0) {

			// read data from PS/2 device

			// todo: we should also check that the port is enabled!
			if (can_read_ps2_device(current_device)) {
				output_buffer = read_from_ps2_device(current_device);
				keybd_status_register |= 0x01;		// output buffer is full
			}
			else {
				keybd_status_register &= 0xfe;		// output buffer is empty
			}

		}

		// return the value of controller status register
		val = keybd_status_register;

	}
	else if (port == KEYBD_DATA) {

		// read keyboard / controller data
		keybd_status_register &= 0xfe;		// output buffer is empty
		val = output_buffer;

	}

	return val;
}

void write_keybd_port(int port, int data, enum datasize datasize) {

	if (port == KEYBD_CTL) {

		if ((data & 0xe0) == 0x20) {
			// Read "byte N" from internal RAM (where 'N' is the command byte & 0x1F)
			// byte will be returned on next read from port 60
			output_buffer = keybd_ram[data & 0x1f];
			keybd_status_register |= 0x01;	// output buffer is full
			input_buffer = data;
			keybd_status_register &= 0xfd;	// input buffer is empty
		}
		else if ((data & 0xe0) == 0x60) {
			// Write next byte to "byte N" of internal RAM (where 'N' is the command byte & 0x1F)
			// next byte should be written to port 60
			keybd_status_register |= 0x08;	// data in input buffer is for controller
			input_buffer = data;			// remember to consume data in input_buffer before overwriting
			keybd_status_register &= 0xfd;	// input buffer is empty
		}
		else if (data == 0xa7) {
			// Disable second PS/2 port
		}
		else if (data == 0xa8) {
			// Enable second PS/2 port
		}
		else if (data == 0xa9) {
			// Test second PS/2 port
			// 0x00 = test passed
			// result will be returned on next read from port 60
			output_buffer = 0x00;
			keybd_status_register |= 0x01;	// output buffer is full
			input_buffer = data;
			keybd_status_register &= 0xfd;	// input buffer is empty
		}
		else if (data == 0xaa) {
			// Test PS/2 Controller
			// 0x55 = test passed; 0xFC test failed
			// result will be returned on next read from port 60
			output_buffer = 0x55;
			keybd_status_register |= 0x04;	// set system flag - POST OK
			keybd_status_register |= 0x01;	// output buffer is full
			input_buffer = data;
			keybd_status_register &= 0xfd;	// input buffer is empty
		}
		else if (data == 0xab) {
			// Test first PS/2 port
			// 0x00 = test passed
			// result will be returned on next read from port 60
			output_buffer = 0x00;
			keybd_status_register |= 0x01;	// output buffer is full
			input_buffer = data;
			keybd_status_register &= 0xfd;	// input buffer is empty
		}
		else if (data == 0xad) {
			// Disable first PS/2 port
		}
		else if (data == 0xae) {
			// Enable first PS/2 port
			keybd_status_register &= 0xf7;		// data written to input buffer is for PS/2 device
			keybd_status_register &= 0xfe;		// output buffer is empty
		}
		else if (data == 0xd0) {
			// Read Controller Output Port
			// byte will be returned on next read from port 60
			output_buffer = keybd_output_port;
			keybd_status_register |= 0x01;	// output buffer is full
			input_buffer = data;
			keybd_status_register &= 0xfd;	// input buffer is empty
		}
		else if (data == 0xd1) {
			// Write next byte to Controller Output Port
			// next byte should be written to port 60
			keybd_status_register |= 0x08;	// data in input buffer is for controller
			input_buffer = data;			// remember to consume data in input_buffer before overwriting
			keybd_status_register &= 0xfd;	// input buffer is empty
		}
		else if (data == 0xd2) {
			// Write next byte to first PS/2 port output buffer
			// (makes it look like the byte written was received from the first PS/2 port)
		}
		else if (data == 0xd3) {
			// todo: implement Second PS/2 Port for mouse
			// Write next byte to second PS/2 port output buffer
			// (makes it look like the byte written was received from the second PS/2 port)
		}
		else if (data == 0xd4) {
			// Write next byte to second PS/2 port input buffer
			// (sends next byte to the second PS/2 port)
		}
		else if ((data & 0xf0) == 0xf0) {
			// todo: this will reset the cpu if data = 0xFE
			input_buffer = data;			// remember to consume data in input_buffer before overwriting
			keybd_status_register &= 0xfd;	// input buffer is empty
		}

	}
	else if (port == KEYBD_DATA) {

		if ((keybd_status_register & 0x08) == 0) {

			write_to_ps2_device(current_device, data);

		}
		else if ((input_buffer & 0xe0) == 0x60) {

			// write to internal RAM
			keybd_ram[input_buffer & 0x1f] = data;
			keybd_status_register &= 0xf7;	// data in input buffer is for device
			keybd_status_register &= 0xfd;	// input buffer is empty
			// if we're writing to byte [0], copy bit 2 (System Flag) into keybd_status_register
			if ((input_buffer & 0x1f) == 0x00) {
				keybd_status_register = (keybd_status_register & ~0x04) | (data & 0x04);
			}

		}
		else if (input_buffer == 0xd1) {

			// write to controller output port
			keybd_output_port = data;
			keybd_status_register &= 0xf7;	// data in input buffer is for device
			input_buffer = data;
			keybd_status_register &= 0xfd;	// input buffer is empty

		}

	}

}

void init_ps2_device(int dev_id) {

	if (init_device[dev_id]) {
		init_device[dev_id]();
	}
	else {
		zlogf("[t=%I64d] attempt to initialise unknown PS/2 device: 0x%x\n", tick_count, dev_id);
	}

}

uint8_t read_from_ps2_device(int dev_id) {

	uint8_t val;

	if (read_from_device[dev_id]) {
		val = read_from_device[dev_id]();
	}
	else {
		zlogf("[t=%I64d] attempt to read from unknown PS/2 device: 0x%x\n", tick_count, dev_id);
	}

	return val;

}

void write_to_ps2_device(int dev_id, uint8_t data) {

	if (write_to_device[dev_id]) {
		write_to_device[dev_id](data);
	}
	else {
		zlogf("[t=%I64d] attempt to write to unknown PS/2 device: 0x%x\n", tick_count, dev_id);
	}

}

int can_read_ps2_device(int dev_id) {

	int val;

	if (device_has_data[dev_id]) {
		val = device_has_data[dev_id]();
	}
	else {
		zlogf("[t=%I64d] attempt to access unknown PS/2 device: 0x%x\n", tick_count, dev_id);
	}

	return val;

}
