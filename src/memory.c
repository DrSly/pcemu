#include <stdio.h>
#include <stdlib.h>
#include <string.h>		// for memset(), memcpy()

#include "zlog.h"
#include "memory.h"

extern long long tick_count;

int init_memory() {
	zlogf("[t=%I64d] initialising %d memory blocks (block size = %d)\n", tick_count, MEM_SEG_COUNT, _1G / MEM_SEG_COUNT);
	for (int i=0; i<MEM_SEG_COUNT; i++) {
		mem_ptr[i] = malloc(_1G / MEM_SEG_COUNT * sizeof(uint8_t));
		if (!mem_ptr[i]) return -1;
	}
	return 0;
}

void free_memory() {
	zlogf("[t=%I64d] releasing memory blocks\n", tick_count);
	for (int i=0; i<MEM_SEG_COUNT; i++) {
		free(mem_ptr[i]);
	}
}

// copy size bytes from memory into buffer
void copy_from_mem(int addr, uint8_t *buffer, int size) {
	//zlogf_time("copy_from_mem: addr: %d [0x%x] buffer: 0x%x size: %d\n", addr, addr, buffer, size);
	for (int i=0; i<size; i++) {
		int ind = (addr >> 22) & 0x000000ff;
		uint8_t *ptr = mem_ptr[ind];
		if ((addr & 0x003fffff) + i > (_1G / MEM_SEG_COUNT) - 1) {
			ptr = mem_ptr[ind + 1];
			buffer[i] = ptr[(addr & 0x003fffff) + i - (_1G / MEM_SEG_COUNT)];
		}
		else {
			buffer[i] = ptr[(addr & 0x003fffff) + i];
		}
	}
}

// copy size bytes from buffer into memory
void copy_to_mem(int addr, uint8_t *buffer, int size) {
	//zlogf_time("copy_to_mem: addr: %d [0x%x] buffer: 0x%x size: %d\n", addr, addr, buffer, size);
	for (int i=0; i<size; i++) {
		int ind = (addr >> 22) & 0x000000ff;
		uint8_t *ptr = mem_ptr[ind];
		if ((addr & 0x003fffff) + i > (_1G / MEM_SEG_COUNT) - 1) {
			ptr = mem_ptr[ind + 1];
			uint8_t *mem_ptr = &ptr[(addr & 0x003fffff) + i - (_1G / MEM_SEG_COUNT)];
			*mem_ptr = buffer[i];
		}
		else {
			uint8_t *mem_ptr = &ptr[(addr & 0x003fffff) + i];
			*mem_ptr = buffer[i];
		}
	}
}

int save_to_file(char *filename, int memory_addr, int bytes) {

	FILE *fp;
	uint8_t buffer[512];

	fp=fopen(filename, "wb");

	if (!fp) {
		printf("Error creating file: %s\n", filename);
		return 1;
	}

	int bytestowrite = 512;
	int bytesremaining = bytes;

	while (bytesremaining > 0) {

		if (bytesremaining < 512) bytestowrite = bytesremaining;
		copy_from_mem(memory_addr, buffer, bytestowrite);
		fwrite(buffer, sizeof(uint8_t), bytestowrite, fp);
		memory_addr += bytestowrite;
		bytesremaining -= bytestowrite;

	}

	fclose(fp);

	return 0;

}

int load_from_file(char *filename, int memory_addr) {

	FILE *fp;
	uint8_t buffer[512];

	fp=fopen(filename, "rb");

	if (!fp) {
		printf("Error opening file: %s\n", filename);
		return 1;
	}

	int bytesread = 0;
	int totalbytes = 0;

	while (!feof(fp)) {

		bytesread = fread(buffer, sizeof(uint8_t), 512, fp);
		copy_to_mem(memory_addr, buffer, bytesread);
		memory_addr += bytesread;
		totalbytes += bytesread;

	}

	fclose(fp);

	return 0;

}

int get_mem(int addr, enum datasize size) {
	// handle the special case where the requested address wraps past the end of mem_ptr[ind]
	if ((size == DWORD) && ((addr & 0x003fffff) + 4 > _1G / MEM_SEG_COUNT * sizeof(uint8_t))) {
		// calculate value based on the individual bytes
		return get_mem(addr, BYTE) + 256 * get_mem(addr + 1, BYTE) + 256 * 256 * get_mem(addr + 2, BYTE) + 256 * 256 * 256 * get_mem(addr + 3, BYTE);
	}
	else if ((size == WORD) && ((addr & 0x003fffff) + 2 > _1G / MEM_SEG_COUNT * sizeof(uint8_t))) {
		// calculate value based on the individual bytes
		return get_mem(addr, BYTE) + 256 * get_mem(addr + 1, BYTE);
	}
	else {
		int ind = (addr >> 22) & 0x000000ff;
		uint8_t *ptr = mem_ptr[ind];
		// log memory read
		if (addr < 0) {
			zlogf("[t=%I64d] Warning, signed representation of address 0x%x is less than zero (%d)\n", tick_count, addr, addr);
		}
		if (size == DWORD) zlogf("[t=%I64d] read DWORD at memory address: 0x%x value: 0x%08x\n", tick_count, addr, *((uint32_t *)&ptr[addr & 0x003fffff]));
		if (size == WORD) zlogf("[t=%I64d] read WORD at memory address: 0x%x value: 0x%04x\n", tick_count, addr, *((uint16_t *)&ptr[addr & 0x003fffff]));
		if (size == BYTE) zlogf("[t=%I64d] read BYTE at memory address: 0x%x value: 0x%02x\n", tick_count, addr, *((uint8_t *)&ptr[addr & 0x003fffff]));
		if (size == DWORD) return *((uint32_t *)&ptr[addr & 0x003fffff]);
		if (size == WORD) return *((uint16_t *)&ptr[addr & 0x003fffff]);
		if (size == BYTE) return *((uint8_t *)&ptr[addr & 0x003fffff]);
		// as a failsafe, return without cast
		zlogf("[t=%I64d] Warning, read UNKNOWN SIZE at memory address: 0x%x value: 0x%x\n", tick_count, addr, ptr[addr & 0x003fffff]);
		return ptr[addr & 0x003fffff];
	}
}

int set_mem(int addr, int value, enum datasize size) {
	// handle the special case where the requested address wraps past the end of mem_ptr[ind]
	if ((size == DWORD) && ((addr & 0x003fffff) + 4 > _1G / MEM_SEG_COUNT * sizeof(uint8_t))) {
		// set memory based on the individual bytes
		set_mem(addr, value & 0xff, BYTE);
		set_mem(addr + 1, (value >> 8) & 0xff, BYTE);
		set_mem(addr + 2, (value >> 16) & 0xff, BYTE);
		set_mem(addr + 3, (value >> 24) & 0xff, BYTE);
		return 1;
	}
	else if ((size == WORD) && ((addr & 0x003fffff) + 2 > _1G / MEM_SEG_COUNT * sizeof(uint8_t))) {
		// set memory based on the individual bytes
		set_mem(addr, value & 0xff, BYTE);
		set_mem(addr + 1, (value >> 8) & 0xff, BYTE);
		return 1;
	}
	else {
		int ind = (addr >> 22) & 0x000000ff;
		uint8_t *ptr = mem_ptr[ind];
		// log memory write
		if (addr < 0) {
			zlogf("[t=%I64d] Warning, signed representation of address 0x%x is less than zero (%d)\n", tick_count, addr, addr);
		}
		if (size == DWORD) zlogf("[t=%I64d] write DWORD at memory address: 0x%x value: 0x%08x\n", tick_count, addr, (uint32_t)value);
		if (size == WORD) zlogf("[t=%I64d] write WORD at memory address: 0x%x value: 0x%04x\n", tick_count, addr, (uint16_t)value);
		if (size == BYTE) zlogf("[t=%I64d] write BYTE at memory address: 0x%x value: 0x%02x\n", tick_count, addr, (uint8_t)value);
		void *mem_ptr = &ptr[addr & 0x003fffff];
		if (size == DWORD) *(uint32_t *)mem_ptr = (uint32_t)value;
		if (size == WORD) *(uint16_t *)mem_ptr = (uint16_t)value;
		if (size == BYTE) *(uint8_t *)mem_ptr = (uint8_t)value;
		return 0;
	}
}

// function to copy bytes to facillitate testing
void test_copy_mem(int addr, uint8_t *dest, int num_bytes) {
	// copy num_bytes from addr to dest
	int ind = (addr >> 22) & 0x000000ff;
	uint8_t *ptr = mem_ptr[ind];
	memcpy(dest, &ptr[addr & 0x003fffff], num_bytes);
}

// function to return segment index to facillitate testing
int test_get_segment(int addr) {
	return (addr >> 22) & 0x000000ff;
}

// function to return offset to facillitate testing
int test_get_offset(int addr) {
	return addr & 0x003fffff;
}
