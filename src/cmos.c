#include <stdio.h>

#include "cpu.h"
#include "cmos.h"

#define RTC_REGC 0x0C
#define RTC_REGD 0x0D

int load_cmos(void);
int read_cmos_port(int port, enum datasize);
void write_cmos_port(int port, int data, enum datasize);

// cmos is usually 64 or 128 bytes
uint8_t cmos_data[128];
int cmos_size;
int cmos_curr;		// a read or write to port #71 will get or set cmos_data at this location

int init_cmos() {

	if (load_cmos() != 0) {
		printf("Error loading cmos\n");
		cmos_size = 0;
		return 1;
	}

	cmos_curr = 0x0d;

	// register io handler
	register_io_handler(0x70, 2, &read_cmos_port, write_cmos_port);

	return 0;
}

void shutdown_cmos() {

	// register io handler
	unregister_io_handler(0x70, 2);

}

int read_cmos_port(int port, enum datasize datasize) {

	int val = 0;

	if (port == 0x71) {
		val = cmos_data[cmos_curr];
		cmos_curr = 0x0d;
	}

	return val;
}

void write_cmos_port(int port, int data, enum datasize datasize) {

	if (port == 0x70) {
		// todo: write to port 70 sets nmi to (data & 0x80)
		cmos_curr = data & 0x7f;
	}
	else {
		// cmos registers RTC_REGC and RTC_REGD are read only
		if ((cmos_curr == RTC_REGC) || (cmos_curr == RTC_REGD)) {
			return;
		}
		cmos_data[cmos_curr] = data;
		cmos_curr = 0x0d;
	}
}

int load_cmos() {
	FILE *fp;
	fp=fopen("..\\assets\\430vx.nvr", "rb");
	if (!fp) {
		printf("Error opening 430vx.nvr\n");
		return 1;
	}
	cmos_size = fread(cmos_data, sizeof(uint8_t), 128, fp);
	printf("load_cmos: Read %d bytes.\n", cmos_size);
	fclose(fp);
	return 0;
}
