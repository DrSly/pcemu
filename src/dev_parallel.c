#include <stdio.h>

#include "zlog.h"
#include "cpu.h"
#include "dev_parallel.h"

#define LPT1	0x378
#define LPT2	0x278

int read_parallel_port(int port, enum datasize);
void write_parallel_port(int port, int data, enum datasize);
struct LPT_PORT *getLptPort(int base_address);

// parallel port registers
struct LPT_PORT {
	int base_address;
	int enabled;
	uint8_t data;
	uint8_t status;
	uint8_t control;
} parallel_port[2];

int init_parallel(void) {

	// initialise internal uart registers
	for (int i=0; i<2; i++) {

		parallel_port[i].data = 0;
		parallel_port[i].status = 0;
		parallel_port[i].control = 0x0c;

	}

	// initialise parallel port address
	parallel_port[0].base_address = LPT1;
	parallel_port[1].base_address = LPT2;

	// lpt2 is disabled
	parallel_port[0].enabled = 1;
	parallel_port[1].enabled = 0;

	// register io handler
	register_io_handler(LPT1, 3, &read_parallel_port, &write_parallel_port);
	register_io_handler(LPT2, 3, &read_parallel_port, &write_parallel_port);

	return 0;
}

void shutdown_parallel(void) {

	// register io handler
	unregister_io_handler(LPT1, 3);
	unregister_io_handler(LPT2, 3);

}

int read_parallel_port(int port, enum datasize datasize) {

	int val = 0;
	int base_address = port & 0xfff8;
	int offset = port & 0x07;

	struct LPT_PORT *lpt = getLptPort(base_address);
	if (lpt == NULL) {
		zlogf("[t=%I64d] PARALLEL PORT [%04x] address not valid\n", tick_count, port);
		return 0;
	}

	// check if port is enabled
	if (!lpt->enabled) {
		return -1;
	}

	switch (offset) {
	case 0:		// Data Register
		val = lpt->data;
		break;
	case 1:		// Status Register
		val = lpt->status;
		break;
	case 2:		// Control Register
		val = lpt->control;
		break;
	}

	return val;

}

void write_parallel_port(int port, int data, enum datasize datasize) {

	int base_address = port & 0xfff8;
	int offset = port & 0x07;

	struct LPT_PORT *lpt = getLptPort(base_address);
	if (lpt == NULL) {
		zlogf("[t=%I64d] PARALLEL PORT [%04x] address not valid\n", tick_count, port);
		return;
	}

	// check if port is enabled
	if (!lpt->enabled) {
		return;
	}

	switch (offset) {
	case 0:		// Data Register
		lpt->data = data;
		break;
	case 1:		// Status Register
		lpt->status = data;
		break;
	case 2:		// Control Register
		lpt->control = data;
		break;
	}

}

struct LPT_PORT *getLptPort(int base_address) {

	for (int i=0; i<2; i++) {
		if (parallel_port[i].base_address == base_address)
			return &parallel_port[i];
	}

	return NULL;

}
