#ifndef _DEV8237A_H_
#define _DEV8237A_H_

// DEV8237A = DMA controller
// http://wiki.osdev.org/DMA
//
// DMA Registers
// 00 - DMA1 Start Address Register channel 0
// 01 - DMA1 Count Register channel 0
// 02 - DMA1 Start Address Register channel 1
// 03 - DMA1 Count Register channel 1
// 04 - DMA1 Start Address Register channel 2
// 05 - DMA1 Count Register channel 2
// 06 - DMA1 Start Address Register channel 3
// 07 - DMA1 Count Register channel 3
// 08 - DMA1 Status Register / Command Register
// 09 - DMA1 Request Register
// 0A - DMA1 Single Channel Mask Register
// 0B - DMA1 Mode Register
// 0C - DMA1 Flip-Flop Reset Register
// 0D - DMA1 Intermediate Register / Master Reset Register
// 0E - DMA1 Mask Reset Register
// 0F - DMA1 MultiChannel Mask Register
//
// C0 - DMA2 Start Address Register channel 4
// C2 - DMA2 Count Register channel 4
// C4 - DMA2 Start Address Register channel 5
// C6 - DMA2 Count Register channel 5
// C8 - DMA2 Start Address Register channel 6
// CA - DMA2 Count Register channel 6
// CC - DMA2 Start Address Register channel 7
// CE - DMA2 Count Register channel 7
// D0 - DMA2 Status Register / Command Register
// D2 - DMA2 Request Register
// D4 - DMA2 Single Channel Mask Register
// D6 - DMA2 Mode Register
// D8 - DMA2 Flip-Flop Reset Register
// DA - DMA2 Intermediate Register / Master Reset Register
// DC - DMA2 Mask Reset Register
// DE - DMA2 MultiChannel Mask Register
//
// DMA Page Registers
//
// 81 - Channel 2 Page Address Register
// 82 - Channel 3 Page Address Register
// 83 - Channel 1 Page Address Register
// 87 - Channel 0 Page Address Register
// 89 - Channel 6 Page Address Register
// 8A - Channel 7 Page Address Register
// 8B - Channel 5 Page Address Register
// 8F - Channel 4 Page Address Register

#ifdef __cplusplus
extern "C" {
#endif
int init_dma(void);
void shutdown_dma(void);
#ifdef __cplusplus
}
#endif

#endif
