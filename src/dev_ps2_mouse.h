#ifndef _DEVPS2MOUSE_H_
#define _DEVPS2MOUSE_H_

//
// dev_ps2_mouse.h
//
// stub for the mouse hardware interface implementation
//

#ifdef __cplusplus
extern "C" {
#endif
int init_ps2_mouse(void);
void shutdown_ps2_mouse(void);
#ifdef __cplusplus
}
#endif

#endif
