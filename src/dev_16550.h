#ifndef _DEV16550_H_
#define _DEV16550_H_

// 16550 = UART Serial Port
// https://wiki.osdev.org/Serial_Ports
// https://en.wikibooks.org/wiki/Serial_Programming/8250_UART_Programming
//
// Serial Ports
//
// COM1	0x3F8
// COM2	0x2F8
// COM3	0x3E8
// COM4	0x2E8
//
// Line Protocol
// The serial data transmitted across the wire can have a number of different parameters set. As a rule,
// the sending device and the receiving device require the same protocol parameter values written to each
// serial controller in order for communication to be successful.
// These days you could consider 8N1 (8 bits, no parity, one stop bit) pretty much the default.
//
// Baud Rate
// The serial controller (UART) has an internal clock which runs at 115200 ticks per second and a clock
// divisor which is used to control the baud rate. In order to set the speed of the port, calculate the
// divisor required for the given baud rate and program that in to the divisor register. For example, a
// divisor of 1 will give 115200 baud, a divisor of 2 will give 57600 baud, 3 will give 38400 baud, etc.
//
// Device Registers
//
// Base +0 = Data register
// Reading this registers read from the Receive buffer. Writing to this register writes to the Transmit buffer.
//
// Base +1 = Interrupt Enable Register
// Determines which interrupts should be enabled (0 = disabled, 1 = enabled)
// 7 .. 4	Unused
// 3		Status change
// 2		Break/error
// 1		Transmitter empty
// 0		Data available
//
// Base +2 = Interrupt Identification and FIFO Control registers
//
// Read: Interrupt Identification Register
// 7 .. 6	FIFO:
//		0 0 = No FIFO on chip
//		0 1 = Reserved condition
//		1 0 = FIFO enabled, but not functioning
//		1 1 = FIFO enabled
// 5	Reserved
// 4	Reserved
// 3 .. 1	Interrupt Identification
//		0 0 0 = Modem Status Interrupt => Read Modem Status Register(MSR)
//		0 0 1 = Transmitter Holding Register Empty Interrupt => Read Interrupt Identification Register(IIR) or
//																Write to Transmit Holding Buffer(THR)
//		0 1 0 = Received Data Available Interrupt => Read Receive Buffer Register(RBR)
//		0 1 1 = Receiver Line Status Interrupt => Reading Line Status Register(LSR)
//		1 0 0 = Reserved
//		1 0 1 = Reserved
//		1 1 0 = Time-out Interrupt Pending (16550 & later) => Read Receive Buffer Register(RBR)
//		1 1 1 = Reserved
// 0	Interrupt Pending Flag:
//		0 = this particular UART is triggering an interrupt
//		1 = interrupt has already been processed or this particular UART was not the triggering device
//
// Write: FIFO Control Register
// 7 .. 6	FIFO Interrupt Trigger Level (16 byte)
//		0 0 = 1 Byte
//		0 1 = 4 Bytes
//		1 0 = 8 Bytes
//		1 1 = 14 Bytes
// 5	Reserved
// 4	Reserved
// 3	DMA Mode Select
// 2	Clear Transmit FIFO
// 1	Clear Receive FIFO
// 0	Enable FIFOs
//
// Base +3 = Line Control Register
// 7	DLAB*:
//		When this bit is set, offsets at Base +0 and Base +1 are mapped to different registers (see below)
// 6	Set Break Enable:
//		Bit 6, when set to 1, causes TX wire to go logical "0" and stay that way, which is interpreted as
//		a long stream of "0" bits by the receiving UART - the "break condition"
// 5 .. 3	Parity:
//		x x 0 = NONE
//		0 0 1 = ODD
//		0 1 1 = EVEN
//		1 0 1 = MARK
//		1 1 1 = SPACE
// 2	Stop bits:
//		0 = 1 stop bit
//		1 = 1.5 / 2 stop bits (depending on character length)
// 1 and 0	Data Bits:
//		0 0 = 5
//		0 1 = 6
//		1 0 = 7
//		1 1 = 8
//
// Base +4 = Modem Control Register
// 7	Reserved
// 6	Reserved
// 5	Reserved
// 4	Loopback Mode
// 3	Auxiliary Output 2
// 2	Auxiliary Output 1
// 1	Request To Send
// 0	Data Terminal Ready
//
// Base +5 = Line Status Register
// 7	Error in Received FIFO
// 6	Empty Data Holding Registers
// 5	Empty Transmitter Holding Register
// 4	Break Interrupt
// 3	Framing Error
// 2	Parity Error
// 1	Overrun Error
// 0	Data Ready
//
// Base +6 = Modem Status Register
// 7	Carrier Detect
// 6	Ring Indicator
// 5	Data Set Ready
// 4	Clear To Send
// 3	Delta Data Carrier Detect
// 2	Trailing Edge Ring Indicator
// 1	Delta Data Set Ready
// 0	Delta Clear To Send
//
// Base +7 = Scratch Register
//
// *Base +0 (With DLAB set to 1) = divisor value for setting the baud rate (least significant byte)
//
// *Base +1 (With DLAB set to 1) = divisor value for setting the baud rate (most significant byte)


#ifdef __cplusplus
extern "C" {
#endif
int init_uart(void);
void shutdown_uart(void);
#ifdef __cplusplus
}
#endif

#endif
