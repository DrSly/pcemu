#ifndef _DEVSYSCTRL_H_
#define _DEVSYSCTRL_H_

// System Control Port A
// Port 0x92 = PS/2 system control port A  (port B is at 0061)
// bit 7-6	any bit set to 1 turns activity light on
// bit 5	reserved
// bit 4	watchdog timout
// bit 3	0 = RTC/CMOS security lock (on password area) unlocked
//			1 = CMOS locked (done by POST)
// bit 2	reserved
// bit 1	1 = indicates A20 active
// bit 0	1 = CPU reset

#ifdef __cplusplus
extern "C" {
#endif
int init_sysctrl(void);
void shutdown_sysctrl(void);
#ifdef __cplusplus
}
#endif

#endif
