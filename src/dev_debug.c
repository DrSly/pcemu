#include <stdio.h>
#include <string.h>		// for memset()
#include <time.h>

#include "zlog.h"
#include "cpu.h"
#include "dev_debug.h"

#define DBG_OUT 	0x402		// debug output port
#define DBG_FILE	"debug.log"	// debug output file

void write_dbg_port(int port, int data, enum datasize);
char *current_datetime(void);

// file handle to write to
FILE *fdbg;
// file buffer
uint8_t buffer[512];
// file buffer position
int buffer_pos;

int init_debug() {

	// initialise output buffer
	memset(buffer, 0, sizeof(uint8_t) * 512);
	buffer_pos = 0;

	// open the file in append mode
	fdbg = fopen(DBG_FILE, "a");

	if (!fdbg) {
		printf("Error opening debug output file: %s\n", DBG_FILE);
		zlogf("[t=%I64d] error opening debug output file: %s\n", tick_count, DBG_FILE);
		return 1;
	}

	fprintf(fdbg, "START: %s", current_datetime());

	// register io handler
	register_io_handler(DBG_OUT, 1, NULL, &write_dbg_port);

	return 0;
}

void shutdown_debug() {

	// write the remaining buffer contents and close the file
	if (buffer_pos > 0) {
		fwrite(buffer, sizeof(uint8_t), buffer_pos, fdbg);
	}

	fprintf(fdbg, "END: %s", current_datetime());

	fclose(fdbg);

	// register io handler
	unregister_io_handler(DBG_OUT, 1);

}

void write_dbg_port(int port, int data, enum datasize datasize) {

	// write out the buffer contents if the buffer is full
	if (buffer_pos == 512) {
		fwrite(buffer, sizeof(uint8_t), 512, fdbg);
		buffer_pos = 0;
	}

	buffer[buffer_pos] = data;
	buffer_pos++;

}

char *current_datetime() {

	time_t t;
	time(&t);

	return ctime(&t);

}
