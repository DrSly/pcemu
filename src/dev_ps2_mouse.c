#include <stdint.h>

#include "dev_8042.h"
#include "dev_ps2_mouse.h"

//
// dev_ps2_mouse.c
//
// stub for the mouse hardware interface implementation
//

void init_ps2ms(void);
uint8_t read_from_ps2ms(void);
void write_to_ps2ms(uint8_t data);
int has_data_ps2ms(void);

int init_ps2_mouse() {

	register_ps2_device(DEV_MOUSE, &init_ps2ms, &read_from_ps2ms, &write_to_ps2ms, &has_data_ps2ms);

	return 0;

}

void shutdown_ps2_mouse() {

	unregister_ps2_device(DEV_MOUSE);

}

void init_ps2ms(void) {

}

uint8_t read_from_ps2ms(void) {

	return 0;

}

void write_to_ps2ms(uint8_t data) {

}

int has_data_ps2ms(void) {

	return 0;

}
