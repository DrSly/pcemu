#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "zlog.h"
#include "memory.h"
#include "decode.h"
#include "cpu.h"
#include "debug.h"


#ifndef __cplusplus

// define string representations for enums

// 8 bit registers
char *byte_register_names[] = {
	"AL", "CL", "DL", "BL", "AH", "CH", "DH", "BH",
	"ES", "CS", "SS", "DS", "FS", "GS",
	"flags", "ip", "BX+SI", "BX+DI", "BP+SI", "BP+DI", "SI", "DI", "BP", "BX"
};

// 16 bit registers
char *word_register_names[] = {
	"AX", "CX", "DX", "BX", "SP", "BP", "SI", "DI",
	"ES", "CS", "SS", "DS", "FS", "GS",
	"flags", "ip", "BX+SI", "BX+DI", "BP+SI", "BP+DI", "SI", "DI", "BP", "BX"
};

// 32 bit registers
char *dword_register_names[] = {
	"EAX", "ECX", "EDX", "EBX", "ESP", "EBP", "ESI", "EDI",
	"ES", "CS", "SS", "DS", "FS", "GS",
	"flags", "ip", "BX+SI", "BX+DI", "BP+SI", "BP+DI", "SI", "DI", "BP", "BX"
};

// define string representations of opcodes

char *opcode_names[] = {
	"ADD", "SUB", "INC", "DEC", "DIV", "IDIV", "MUL", "IMUL",
	"AND", "OR", "XOR", "NOT", "NEG", "TEST",
	"SHL", "SHR", "ROL", "ROR", "RCL", "RCR",
	"JMP", "JMP_FAR",
	"JZ", "JNZ", "JC", "JNC", "JS", "JNS", "JO", "JNO", "JA", "JNA",
	"JGE", "JG", "JL", "JLE", "JP", "JNP", "JCXZ", "JECXZ",
	"CMP", "CMC", "CLC", "STC", "CLI", "STI", "CLD", "STD",
	"MOV", "MOVS", "CMPS", "STOS", "LODS", "SCAS",
	"REP MOVS", "REPNE CMPS", "REPE CMPS", "REP STOS", "REP LODS", "REPNE SCAS", "REPE SCAS",
	"PUSH", "PUSHA", "PUSHF",
	"POP", "POPA", "POPF",
	"NOP", "XCHG", "LEA", "LDS", "LES", "LFS", "LGS", "LSS",
	"CALL", "CALL_FAR", "RET", "RETF", "INT", "IRET",
	"IN", "OUT", "INS", "OUTS", "REP_INS", "REP_OUTS",
	"SAHF", "LAHF",
	"CBW", "CWD", "CWDE", "CDQ",
	"MOVSX", "MOVZX",
	"LOOP", "LOOPE", "LOOPNE",
	// temporary flags to designate an unsupported feature:
	"NOT_IMPL", "INVALID_OPCODE"
};

#endif

// break when stepcount reaches zero
int stepcount = 1;

// break when stepcount reaches zero
int breakpoints[20];


// debugger main entry: input and process commands from user
int debug_main_entry(struct instruction *opc) {

	// return -1 if exit requested
	int success = 0;

	static long long prev_tc = -1;

	// decrement step count by the number of ticks executed
	stepcount -= tick_count - prev_tc;
	prev_tc = tick_count;

	if ((stepcount > 0) && (check_breakpoint(reg_cs, reg_ip) == 0)) return success;

	// log the breakpoint
	zlogf("[t=%I64d] [%04x:%08x] BREAK\n", tick_count, reg_cs, reg_ip);

	// output registers and current intruction
	dump_regs();
	print_opcode(opc, reg_cs, reg_ip);

	// initialise a buffer
	const int buffer_size = 64;
	char buffer[buffer_size];
	char tmp_buff[buffer_size];
	char *tkn[buffer_size];
	memset(tkn, 0, sizeof(tkn[buffer_size]));

	success = 1;
	while ((success != 0) && (success != -1)) {

		// read input from stdin
		printf("-> ");
		getline(buffer, buffer_size);

		// process user input
		strcpy(tmp_buff, buffer);

		// copy each word token into an array of char *
		int pos = 0;
		char *p_str = strtok(tmp_buff, " \n");
		while (p_str != NULL) {
			tkn[pos] = strdup(p_str);
			pos++;
			p_str = strtok (NULL, " \n");
		}

		if (pos == 0) {
			printf("No user input!! Enter ? for help\n");
			continue;
		}

		// commands: [s] step, [c] continue, [bp] set breakpoint,
		// [m] dump memory, [d] disassemble, [r] show registers,
		// [?] display commands
		if (strcmp("s", tkn[0]) == 0) {
			stepcount = 1;
			success = 0;
			// set step count if user supplied a positive integer
			if (pos == 2) {
				int tmp_count = strtol(tkn[1], NULL, 10);
				if (tmp_count <= 0) {
					printf("Parse error, expected s [count]\n");
					success = 1;
				}
				else {
					stepcount = tmp_count;
				}
			}
		}
		else if (strcmp("c", tkn[0]) == 0) {
			printf("Continue\n");
			stepcount = 0;
			success = 0;
		}
		else if (strcmp("bp", tkn[0]) == 0) {
			// set breakpoint = parse out address [0000:0000]
			int seg, offs;
			if ((pos > 1) && (parse_address(tkn[1], &seg, &offs) == 0)) {
				printf("Set breakpoint at %x:%x\n", seg, offs);
				set_breakpoint(seg, offs);
			}
			else {
				printf("Parse error, expected bp [seg]:[offset]\n");
			}
			success = 1;
		}
		else if (strcmp("m", tkn[0]) == 0) {
			// dump memory = parse out address [0000:0000]
			int seg, offs;
			if ((pos > 1) && (parse_address(tkn[1], &seg, &offs) == 0)) {
				printf("Dumping %x:%x\n", seg, offs);
				dump_mem(seg, offs);
			}
			else {
				printf("Parse error, expected m [seg]:[offset]\n");
			}
			success = 1;
		}
		else if (strcmp("d", tkn[0]) == 0) {
			// disassemble = parse out address [0000:0000]
			int seg, offs;
			if ((pos > 1) && (parse_address(tkn[1], &seg, &offs) == 0)) {
				printf("Disassemble %x:%x\n", seg, offs);
				disassemble(seg, offs);
			}
			else {
				printf("Parse error, expected d [seg]:[offset]\n");
			}
			success = 1;
		}
		else if (strcmp("r", tkn[0]) == 0) {
			dump_regs();
			print_opcode(opc, reg_cs, reg_ip);
			success = 1;
		}
		else if (strcmp("?", tkn[0]) == 0) {
			printf("\n");
			printf("[s] step, [c] continue, [bp] set breakpoint, [m] dump memory, [d] disassemble\n");
			success = 1;
		}
		else if (strcmp("x", tkn[0]) == 0) {
			// terminate program
			printf("Exit\n");
			success = -1;
		}
		else {
			printf("Unknown command: %s\n", buffer);
			success = 1;
		}

		// free malloc'd strings
		for (int i=0; i<pos; i++) {
			free(tkn[i]);
			tkn[i] = NULL;
		}

	}

	return success;

}

// read up to size - 1 characters from stdin, remove the newline (\n) from output,
// if no newline was read flush remaining input
// return pointer to buffer on success; if EOF is encountered and no characters
// have been read, buffer is unchanged and a null pointer is returned
char *getline(char *buffer, int size) {

	char *out = fgets(buffer, size, stdin);

	if (out != NULL) {
		int length = strlen(buffer);
		if (length > 0 && buffer[length - 1] != '\n') {
			// no EOL character was read; flush stdin
			int ch;
			while ( ((ch = getchar()) != '\n') && (ch != EOF) );
		}
		else {
			// replace the newline character with \0
			buffer[length - 1] = 0;
		}
	}

	return out;

}

// parse string in the form <int>:<int> into two integers
// return 0 on success, or -1 on failure
int parse_address(char *address_str, int *segment, int *offset) {

	// make local copy of address_str since strtok will mess it up
	char *parse_str = strdup(address_str);
	char *segment_str = NULL;
	char *offset_str = NULL;
	int success = 0;

	// look for ':'
	int pos = 0;
	char *p_str = strtok(parse_str, ":");
	while (p_str != NULL) {
		if (pos == 0) segment_str = strdup(p_str);
		if (pos == 1) offset_str = strdup(p_str);
		pos++;
		p_str = strtok (NULL, ":");
	}

	// parse_str is not needed after this point
	free(parse_str);
	parse_str = NULL;

	// make sure there are two elements
	if (pos != 2) success = -1;

	int tmp_segment, tmp_offset;
	if (success == 0) {
		tmp_segment = strtol(segment_str, NULL, 16);
		if (errno == ERANGE) success = -1;
		tmp_offset = strtol(offset_str, NULL, 16);
		if (errno == ERANGE) success = -1;
	}

	// if no errors assign output parameters
	if (success == 0) {
		*segment = tmp_segment;
		*offset = tmp_offset;
	}

	if (segment_str != NULL) free(segment_str);
	if (offset_str != NULL) free(offset_str);

	return success;

}

// break before the next instruction
void break_next() {

	stepcount = 1;

}

// dump the contents of memory
void dump_mem(int segment, int offset) {

	// print header
	printf("              ");
	for (int i=0; i<16; i++)
		printf(" %02x", i);
	printf("\n");

	// get the base address
	uint8_t prefetch[16];
	int base_offset = offset & 0xfffffff0;
	copy_from_mem((segment << 4) + base_offset, prefetch, 16);
	for (int i=0; i<8; i++) {
		printf("%04x:%08x ", segment, base_offset);
		// dump the memory contents
		for (int i=0; i<16; i++)
			printf(" %02x", prefetch[i]);
		printf("\n");
		base_offset+= 16;
		copy_from_mem((segment << 4) + base_offset, prefetch, 16);
	}

}

// disassemble
void disassemble(int segment, int offset) {

	uint8_t prefetch[16];
	struct instruction opc;

	for (int i=0; i<8; i++) {
		// load instruction
		copy_from_mem((segment << 4) + offset, prefetch, 16);
		memset(&opc, 0, sizeof(struct instruction));
		decode(prefetch, &opc);
		print_opcode(&opc, segment, offset);
		offset += opc.length;
	}

}

// dump the contents of registers
void dump_regs() {

	printf("\n");

	printf("eax:%08x ebx:%08x ecx:%08x edx:%08x esi:%08x edi:%08x \n",
			reg_eax.value, reg_ebx.value, reg_ecx.value, reg_edx.value,
			reg_esi.value, reg_edi.value);

	printf("ebp:%08x esp:%08x eip:%08x flags:%08x \n",
			reg_ebp.value, reg_esp.value, reg_ip, flags);

	printf("ds:%04x es:%04x fs:%04x gs:%04x ss:%04x cs:%04x          %20I64d\n\n",
			reg_ds, reg_es, reg_fs, reg_gs, reg_ss, reg_cs, tick_count);

}

void set_breakpoint(int segment, int offset) {

	static int bp_ind = 0;
	breakpoints[bp_ind] = (segment << 4) + offset;
	bp_ind++;
	if (bp_ind == 20) bp_ind = 0;

}

int check_breakpoint(int segment, int offset) {

	// return 1 if segment:offset is in the list of breakpoints
	int laddr = (segment << 4) + offset;
	for (int i=0; i<20; i++) {
		if (breakpoints[i] == laddr) return 1;
	}

	return 0;

}

void print_opcode(struct instruction *opc, int segment, int offset) {

	char buff_1[50];
	char buff_2[50];

	printf("%04x:%08x\t", segment, offset);

	for (int i=0; i<opc->length; i++)
		printf("%02x ", opc->bytes[i]);

	printf("    \t");

	switch (opc->op) {
	case ADD:
	case SUB:
	case AND:
	case OR:
	case XOR:
	case TEST:
	case CMP:
	case MOV:
	case XCHG:
	case LEA:
	case LDS:
	case LES:
	case LFS:
	case LGS:
	case LSS:
	case IN:
	case OUT:
	case MOVSX:
	case MOVZX:
		zlogf("[t=%I64d] [%04x:%08x] %s %s %s\n", tick_count, reg_cs, reg_ip, opcode_names[opc->op], printOperand(buff_1, &opc->dest), printOperand(buff_2, &opc->source));
		printf("%s %s %s", opcode_names[opc->op], printOperand(buff_1, &opc->dest), printOperand(buff_2, &opc->source));
		break;
	case INC:
	case DEC:
	case DIV:
	case IDIV:
	case MUL:
	case NOT:
	case NEG:
	case PUSH:
	case POP:
		zlogf("[t=%I64d] [%04x:%08x] %s %s\n", tick_count, reg_cs, reg_ip, opcode_names[opc->op], printOperand(buff_1, &opc->dest));
		printf("%s %s", opcode_names[opc->op], printOperand(buff_1, &opc->dest));
		break;
	case IMUL:
		// a special case; IMUL can have one, two, or three operands
		// todo: do not print twice if source and dest are both the same register
		if (opc->source.use_imm_word == 1) {
			zlogf("[t=%I64d] [%04x:%08x] %s %s %s %x\n", tick_count, reg_cs, reg_ip, opcode_names[opc->op], printOperand(buff_1, &opc->dest), printOperand(buff_2, &opc->source), opc->source.imm_word_data);
			printf("%s %s %s %x", opcode_names[opc->op], printOperand(buff_1, &opc->dest), printOperand(buff_2, &opc->source), opc->source.imm_word_data);
		}
		else {
			zlogf("[t=%I64d] [%04x:%08x] %s %s\n", tick_count, reg_cs, reg_ip, opcode_names[opc->op], printOperand(buff_1, &opc->dest));
			printf("%s %s", opcode_names[opc->op], printOperand(buff_1, &opc->dest));
		}
		break;
	case SHL:
	case SHR:
	case ROL:
	case ROR:
	case RCL:
	case RCR:
		zlogf("[t=%I64d] [%04x:%08x] %s %s %s\n", tick_count, reg_cs, reg_ip, opcode_names[opc->op], printOperand(buff_1, &opc->dest), printOperand(buff_2, &opc->source));
		printf("%s %s %s", opcode_names[opc->op], printOperand(buff_1, &opc->dest), printOperand(buff_2, &opc->source));
		break;
	case JMP:
	case CALL:
		zlogf("[t=%I64d] [%04x:%08x] %s %s\n", tick_count, reg_cs, reg_ip, opcode_names[opc->op], calcDestAddress(buff_1, &opc->dest, reg_ip + opc->length));
		printf("%s %s", opcode_names[opc->op], calcDestAddress(buff_1, &opc->dest, reg_ip + opc->length));
		break;
	case JMP_FAR:
	case CALL_FAR:
		zlogf("[t=%I64d] [%04x:%08x] %s %s:%s\n", tick_count, reg_cs, reg_ip, opcode_names[opc->op], printOperand(buff_1, &opc->source), printOperand(buff_2, &opc->dest));
		printf("%s %s:%s", opcode_names[opc->op], printOperand(buff_1, &opc->source), printOperand(buff_2, &opc->dest));
		break;
	case JZ:
	case JNZ:
	case JC:
	case JNC:
	case JS:
	case JNS:
	case JO:
	case JNO:
	case JA:
	case JNA:
	case JGE:
	case JG:
	case JL:
	case JLE:
	case JP:
	case JNP:
	case JCXZ:
	case JECXZ:
	case LOOP:
	case LOOPE:
	case LOOPNE:
		zlogf("[t=%I64d] [%04x:%08x] %s %s\n", tick_count, reg_cs, reg_ip, opcode_names[opc->op], calcDestAddress(buff_1, &opc->dest, reg_ip + opc->length));
		printf("%s %s", opcode_names[opc->op], calcDestAddress(buff_1, &opc->dest, reg_ip + opc->length));
		break;
	case CMC:
	case CLC:
	case STC:
	case CLI:
	case STI:
	case CLD:
	case STD:
	case MOVS:
	case CMPS:
	case STOS:
	case LODS:
	case SCAS:
	case REP_MOVS:
	case REPNE_CMPS:
	case REPE_CMPS:
	case REP_STOS:
	case REP_LODS:
	case REPNE_SCAS:
	case REPE_SCAS:
	case PUSHA:
	case PUSHF:
	case POPA:
	case POPF:
	case NOP:
	case IRET:
	case SAHF:
	case LAHF:
	case CBW:
	case CWD:
	case CWDE:
	case CDQ:
	case INS:
	case OUTS:
	case REP_INS:
	case REP_OUTS:
		zlogf("[t=%I64d] [%04x:%08x] %s\n", tick_count, reg_cs, reg_ip, opcode_names[opc->op]);
		printf("%s", opcode_names[opc->op]);
		break;
	case RET:
	case RETF:
	case INT:
		zlogf("[t=%I64d] [%04x:%08x] %s %s\n", tick_count, reg_cs, reg_ip, opcode_names[opc->op], printOperand(buff_1, &opc->dest));
		printf("%s %s", opcode_names[opc->op], printOperand(buff_1, &opc->dest));
		break;
	case NOT_IMPL:
		zlogf("[t=%I64d] [%04x:%08x] %s\n", tick_count, reg_cs, reg_ip, opcode_names[opc->op]);
		printf("%s", opcode_names[opc->op]);
		break;
	case INVALID_OPCODE:
		zlogf("[t=%I64d] [%04x:%08x] INVALID_OPCODE: %x\n", tick_count, reg_cs, reg_ip, opc->bytes[0]);
		printf("INVALID_OPCODE");
		break;
	}

	printf("\n");

}

const char *printOperand(char *strout, struct operand *operand) {

	if (operand->use_sib) {
		if (operand->use_disponly) {
			// BYTE PTR [eax*8+0xf000001]
			// todo: should be data size not register size here!
			if (operand->register_size == BYTE)
				sprintf(strout, "BYTE PTR [%s*%i+%x]", byte_register_names[operand->sib_index], operand->sib_scale, operand->disponly_data);
			if (operand->register_size == WORD)
				sprintf(strout, "WORD PTR [%s*%i+%x]", word_register_names[operand->sib_index], operand->sib_scale, operand->disponly_data);
			if (operand->register_size == DWORD)
				sprintf(strout, "DWORD PTR [%s*%i+%x]", dword_register_names[operand->sib_index], operand->sib_scale, operand->disponly_data);
		}
		else {
			if ((operand->sib_scale == 0) && (!operand->use_offset)) {
				// BYTE PTR [ecx+eiz*1]
				if (operand->register_size == BYTE)
					sprintf(strout, "BYTE PTR [%s]", byte_register_names[operand->register_data]);
				if (operand->register_size == WORD)
					sprintf(strout, "WORD PTR [%s]", word_register_names[operand->register_data]);
				if (operand->register_size == DWORD)
					sprintf(strout, "DWORD PTR [%s]", dword_register_names[operand->register_data]);
			}
			if ((operand->sib_scale == 0) && (operand->use_offset)) {
				// BYTE PTR [ecx+eiz*1+0x1]
				if (operand->register_size == BYTE)
					sprintf(strout, "BYTE PTR [%s+%x]", byte_register_names[operand->register_data], operand->offset_data);
				if (operand->register_size == WORD)
					sprintf(strout, "WORD PTR [%s+%x]", word_register_names[operand->register_data], operand->offset_data);
				if (operand->register_size == DWORD)
					sprintf(strout, "DWORD PTR [%s+%x]", dword_register_names[operand->register_data], operand->offset_data);
			}
			if ((operand->sib_scale != 0) && (!operand->use_offset)) {
				// BYTE PTR [edi+eax*1]
				if (operand->register_size == BYTE)
					sprintf(strout, "BYTE PTR [%s+%s*%i]", byte_register_names[operand->register_data], byte_register_names[operand->sib_index], operand->sib_scale);
				if (operand->register_size == WORD)
					sprintf(strout, "WORD PTR [%s+%s*%i]", word_register_names[operand->register_data], word_register_names[operand->sib_index], operand->sib_scale);
				if (operand->register_size == DWORD)
					sprintf(strout, "DWORD PTR [%s+%s*%i]", dword_register_names[operand->register_data], dword_register_names[operand->sib_index], operand->sib_scale);
			}
			if ((operand->sib_scale != 0) && (operand->use_offset)) {
				// BYTE PTR [esp+ecx*4+0xf000001]
				if (operand->register_size == BYTE)
					sprintf(strout, "BYTE PTR [%s+%s*%i+%x]", byte_register_names[operand->register_data], byte_register_names[operand->sib_index], operand->sib_scale, operand->offset_data);
				if (operand->register_size == WORD)
					sprintf(strout, "WORD PTR [%s+%s*%i+%x]", word_register_names[operand->register_data], word_register_names[operand->sib_index], operand->sib_scale, operand->offset_data);
				if (operand->register_size == DWORD)
					sprintf(strout, "DWORD PTR [%s+%s*%i+%x]", dword_register_names[operand->register_data], dword_register_names[operand->sib_index], operand->sib_scale, operand->offset_data);
			}
		}
		return strout;
	}
	if (operand->use_register) {
		if (operand->register_size == BYTE) return byte_register_names[operand->register_data];
		if (operand->register_size == WORD) return word_register_names[operand->register_data];
		if (operand->register_size == DWORD) return dword_register_names[operand->register_data];
	}
	if (operand->use_indregister) {
		if (operand->use_offset) {
			if (operand->register_size == BYTE)
				sprintf(strout, "BYTE PTR [%s+%x]", byte_register_names[operand->register_data], operand->offset_data);
			if (operand->register_size == WORD)
				sprintf(strout, "WORD PTR [%s+%x]", word_register_names[operand->register_data], operand->offset_data);
			if (operand->register_size == DWORD)
				sprintf(strout, "DWORD PTR [%s+%x]", dword_register_names[operand->register_data], operand->offset_data);
		}
		else {
			if (operand->register_size == BYTE)
				sprintf(strout, "BYTE PTR [%s]", byte_register_names[operand->register_data]);
			if (operand->register_size == WORD)
				sprintf(strout, "WORD PTR [%s]", word_register_names[operand->register_data]);
			if (operand->register_size == DWORD)
				sprintf(strout, "DWORD PTR [%s]", dword_register_names[operand->register_data]);
		}
	}
	if (operand->use_disponly) {
		sprintf(strout, "%s:%08x", word_register_names[operand->register_segment], operand->disponly_data);
	}
	if (operand->use_imm_word) {
		sprintf(strout, "%x", operand->imm_word_data);
	}
	return strout;
}

const char *calcDestAddress(char *strout, struct operand *operand, int32_t base_address) {

	if (operand->use_register) {
		if (operand->register_size == BYTE) return byte_register_names[operand->register_data];
		if (operand->register_size == WORD) return word_register_names[operand->register_data];
		if (operand->register_size == DWORD) return dword_register_names[operand->register_data];
	}
	if (operand->use_indregister) {
		if (operand->use_offset) {
			if (operand->register_size == BYTE)
				sprintf(strout, "BYTE PTR [%s+%x]", byte_register_names[operand->register_data], operand->offset_data);
			if (operand->register_size == WORD)
				sprintf(strout, "WORD PTR [%s+%x]", word_register_names[operand->register_data], operand->offset_data);
			if (operand->register_size == DWORD)
				sprintf(strout, "DWORD PTR [%s+%x]", dword_register_names[operand->register_data], operand->offset_data);
		}
		else {
			if (operand->register_size == BYTE)
				sprintf(strout, "BYTE PTR [%s]", byte_register_names[operand->register_data]);
			if (operand->register_size == WORD)
				sprintf(strout, "WORD PTR [%s]", word_register_names[operand->register_data]);
			if (operand->register_size == DWORD)
				sprintf(strout, "DWORD PTR [%s]", dword_register_names[operand->register_data]);
		}
	}
	if (operand->use_disponly) {
		sprintf(strout, "%s:%08x", word_register_names[operand->register_segment], operand->disponly_data);
	}
	if (operand->use_imm_word) {
		sprintf(strout, "%x", operand->imm_word_data + base_address);
	}
	return strout;
}
