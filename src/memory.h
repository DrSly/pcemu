#ifndef _MEMORY_H_
#define _MEMORY_H_

#include "x86_types.h"

// size of memory should reflect the size of the address bus (!)
#define _1G 1073741824
#define _4G 4294967296

// segment memory into 256 seperate blocks
#define MEM_SEG_COUNT 256

#ifndef __cplusplus
// represent memory as 256 element array pointers to malloc'd unsigned bytes
void * mem_ptr[MEM_SEG_COUNT];
#endif

#ifdef __cplusplus
extern "C" {
#endif
int init_memory();
void free_memory();
void copy_from_mem(int addr, uint8_t *buffer, int size);
void copy_to_mem(int addr, uint8_t *buffer, int size);
int save_to_file(char *filename, int memory_addr, int bytes);
int load_from_file(char *filename, int memory_addr);
int get_mem(int addr, enum datasize size);
int set_mem(int addr, int value, enum datasize size);	// actually not a return value, but for testing
						// we're flagging whether the memory access 'wrapped' over a segment boundary
void test_copy_mem(int addr, uint8_t *dest, int num_bytes);		// for testing
int test_get_segment(int addr);	// for testing
int test_get_offset(int addr);	// for testing
#ifdef __cplusplus
}
#endif

#endif
