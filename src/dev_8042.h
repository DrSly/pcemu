#ifndef _DEV8042_H_
#define _DEV8042_H_

// 8042 = Keyboard controller
// http://wiki.osdev.org/%228042%22_PS/2_Controller
//
// Controller IO Ports:
// IO Port	Access Type	Purpose
// 0x60		Read/Write	Data Port
// 0x64		Read		Status Register
// 0x64		Write		Command Register
//
// Data Port - used for reading/writing to a PS/2 device or the controller itself
//
// Status Register - contains various flags that indicate the state of the PS/2 controller
//
// 0	Output buffer status (0 = empty, 1 = full)
//		(must be set before attempting to read data from IO port 0x60)
// 1	Input buffer status (0 = empty, 1 = full)
//		(must be clear before attempting to write data to IO port 0x60 or IO port 0x64)
// 2	System Flag
//		Meant to be cleared on reset and set by firmware (via. PS/2 Controller Configuration Byte) if the system passes self tests (POST)
// 3	Command/data (0 = data written to input buffer is data for PS/2 device, 1 = data written to input buffer is data for PS/2 controller command)
// 4	Unknown (chipset specific)
//		May be "keyboard lock" (more likely unused on modern systems)
// 5	Unknown (chipset specific)
//		May be "receive time-out" or "second PS/2 port output buffer full"
// 6	Time-out error (0 = no error, 1 = time-out error)
// 7	Parity error (0 = no error, 1 = parity error)
//
// Command Register
//
// To send a command to the controller, simply write the command byte to IO port 0x64. If there is a "next byte" (the command
// is 2 bytes) then the next byte needs to be written to IO Port 0x60 after making sure that the controller is ready for it (by
// making sure bit 1 of the Status Register is clear). If there is a response byte, then the response byte needs to be read from
// IO Port 0x60 after making sure that it has arrived (by making sure bit 0 of the Status Register is set).
//
// Byte		Meaning										Response Byte
// 0x20		Read "byte 0" from internal RAM				Controller Configuration Byte
// 0x21		Read "byte N" from internal RAM				Unknown
// to 0x3F	(where 'N' is the command byte & 0x1F)
// 0x60		Write next byte to "byte 0" of internal		None
//			RAM (Controller Configuration Byte)
// 0x61		Write next byte to "byte N" of internal		None
// to 0x7F	RAM (where 'N' is the command byte & 0x1F)
// 0xA7		Disable second PS/2 port (only if 2 PS/2 	None
// 			ports supported)
// 0xA8		Enable second PS/2 port (only if 2 PS/2		None
//			ports supported)
// 0xA9		Test second PS/2 port						0x00 test passed; 0x01 clock line stuck low;
//			(only if 2 PS/2 ports supported)			0x02 clock line stuck high; 0x03 data line stuck low;
//														0x04 data line stuck high
// 0xAA		Test PS/2 Controller						0x55 test passed; 0xFC test failed
// 0xAB		Test first PS/2 port						0x00 test passed; 0x01 clock line stuck low;
//														0x02 clock line stuck high;0x03 data line stuck low;
//														0x04 data line stuck high
// 0xAC		Diagnostic dump (real all bytes of			Unknown
//			internal RAM)
// 0xAD		Disable first PS/2 port						None
// 0xAE		Enable first PS/2 port						None
// 0xC0		Read controller input port					Unknown
// 0xC1		Copy bits 0 to 3 of input port to status	None
//			bits 4 to 7
// 0xC2		Copy bits 4 to 7 of input port to status	None
//			bits 4 to 7
// 0xD0		Read Controller Output Port	Controller
//			Output Port
// 0xD1		Write next byte to Controller Output Port	None
//			Note: Check if output buffer is empty first
// 0xD2		Write next byte to first PS/2 port output	None
//			buffer (only if 2 PS/2 ports supported)
//			(makes it look like the byte written was
//			received from the first PS/2 port)
// 0xD3		Write next byte to second PS/2 port output	None
//			buffer (only if 2 PS/2 ports supported)
//			(makes it look like the byte written was
//			received from the second PS/2 port)
// 0xD4		Write next byte to second PS/2 port input	None
//			buffer (only if 2 PS/2 ports supported)
//			(sends next byte to the second PS/2 port)
// 0xF0		Pulse output line low for 6 ms. Bits 0		None
// to 0xFF	to 3 are used as a mask (0 = pulse line, 1 = don't
//			pulse line) and correspond to 4 different output
//			lines. Note: Bit 0 corresponds to the "reset" line.
//			The other output lines don't have a standard/defined
//			purpose.
//
// Controller Configuration Byte:
//
// 0	First PS/2 port interrupt (1 = enabled, 0 = disabled)
// 1	Second PS/2 port interrupt (1 = enabled, 0 = disabled, only if 2 PS/2 ports supported)
// 2	System Flag (1 = system passed POST, 0 = your OS shouldn't be running)
// 3	Should be zero
// 4	First PS/2 port clock (1 = disabled, 0 = enabled)
// 5	Second PS/2 port clock (1 = disabled, 0 = enabled, only if 2 PS/2 ports supported)
// 6	First PS/2 port translation (1 = enabled, 0 = disabled)
// 7	Must be zero
//
// Controller Output Port
//
// 0	System reset (output)
//		WARNING always set to '1'. You need to pulse the reset line (e.g. using command 0xFE), and setting this bit to '0'
//		can lock the computer up ("reset forever").
// 1	A20 gate (output)
// 2	Second PS/2 port clock (output, only if 2 PS/2 ports supported)
// 3	Second PS/2 port data (output, only if 2 PS/2 ports supported)
// 4	Output buffer full with byte from first PS/2 port (connected to IRQ1)
// 5	Output buffer full with byte from second PS/2 port (connected to IRQ12, only if 2 PS/2 ports supported)
// 6	First PS/2 port clock (output)
// 7	First PS/2 port data (output)


// PS/2 Keyboard device id
#define DEV_KEYBD 0
// PS/2 Mouse device id
#define DEV_MOUSE 1


#ifndef __cplusplus
// PS/2 device interface
void (*init_device[2])(void);				// initialise the PS/2 device
uint8_t (*read_from_device[2])(void);		// read from the PS/2 device
void (*write_to_device[2])(uint8_t data);	// write data to the PS/2 device
int (*device_has_data[2])(void);			// check if the device has data to read
#endif


#ifdef __cplusplus
extern "C" {
#endif
int init_keybd(void);
void shutdown_keybd(void);
void register_ps2_device(int dev_id, void (fn_init)(void), uint8_t (fn_read)(void), void (fn_write)(uint8_t), int (fn_canread)(void));
void unregister_ps2_device(int dev_id);
#ifdef __cplusplus
}
#endif

#endif
