#ifndef _DEVPS2KEYBOARD_H_
#define _DEVPS2KEYBOARD_H_

//
// dev_ps2_keyboard.h
//
// stub for the keyboard hardware interface implementation
//

#ifdef __cplusplus
extern "C" {
#endif
int init_ps2_keyboard(void);
void shutdown_ps2_keyboard(void);
#ifdef __cplusplus
}
#endif

#endif
