#include <stdio.h>

#include "cpu.h"
#include "dev_sysctrl.h"

#define SYSCTRL_A 	0x92		// System Control Port A

int read_sysctrl_port(int port, enum datasize);
void write_sysctrl_port(int port, int data, enum datasize);

// system control / status register
uint8_t sysctrl;
// todo: A20 address line gate should link to memory controller
int a20_enabled;

int init_sysctrl() {

	// initialise system control port A
	sysctrl = 0;

	// a20 address line disabled
	a20_enabled = 0;

	// register io handler
	register_io_handler(SYSCTRL_A, 1, &read_sysctrl_port, &write_sysctrl_port);

	return 0;

}

void shutdown_sysctrl() {

	// register io handler
	unregister_io_handler(SYSCTRL_A, 1);

}

int read_sysctrl_port(int port, enum datasize datasize) {

	int val = 0;

	switch (port) {
	case 0x92:		// system control / status register
		val = sysctrl;
		break;
	}

	return val;

}

void write_sysctrl_port(int port, int data, enum datasize datasize) {

	switch (port) {
	case 0x92:		// system control / status register
		sysctrl = data;
		// set A20 address line gate input
		a20_enabled = (sysctrl & 0x02) == 0x02;
		break;
	}

}
