#ifndef _DEBUG_H_
#define _DEBUG_H_

#include "x86_types.h"

#ifdef __cplusplus
extern "C" {
#endif
int debug_main_entry(struct instruction *opc);
char *getline(char *buffer, int size);
int parse_address(char *address_str, int *segment, int *offset);
void break_next();
void dump_mem(int segment, int offset);
void disassemble(int segment, int offset);
void dump_regs();
void set_breakpoint(int segment, int offset);
int check_breakpoint(int segment, int offset);
void print_opcode(struct instruction *opc, int segment, int offset);
const char *printOperand(char *strout, struct operand *operand);
const char *calcDestAddress(char *strout, struct operand *operand, int32_t base_address);
#ifdef __cplusplus
}
#endif

#endif
