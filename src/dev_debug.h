#ifndef _DEVDEBUG_H_
#define _DEVDEBUG_H_

// Debug Output
// Log bytes written to port 0x402 into a file

#ifdef __cplusplus
extern "C" {
#endif
int init_debug(void);
void shutdown_debug(void);
#ifdef __cplusplus
}
#endif

#endif
