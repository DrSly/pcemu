#ifndef _CMOS_H_
#define _CMOS_H_

// The format of the ten clock data registers (bytes 00h-09h) is:
//
// 00h Seconds       (BCD 00-59, Hex 00-3B) Note: Bit 7 is read only
// 01h Second Alarm  (BCD 00-59, Hex 00-3B; "don't care" if C0-FF)
// 02h Minutes       (BCD 00-59, Hex 00-3B)
// 03h Minute Alarm  (BCD 00-59, Hex 00-3B; "don't care" if C0-FF))
// 04h Hours         (BCD 00-23, Hex 00-17 if 24 hr mode)
//						 (BCD 01-12, Hex 01-0C if 12 hr am)
//						 (BCD 81-92. Hex 81-8C if 12 hr pm)
// 05h Hour Alarm    (same as hours; "don't care" if C0-FF))
// 06h Day of Week   (01-07 Sunday=1)
// 07h Date of Month (BCD 01-31, Hex 01-1F)
// 08h Month         (BCD 01-12, Hex 01-0C)
// 09h Year          (BCD 00-99, Hex 00-63)
//
// BCD/Hex selection depends on Bit 2 of register B (0Bh)
// 12/24 Hr selection depends on Bit 1 of register B (0Bh)
// Alarm will trigger when contents of all three Alarm byte registers
// match their companions.
//
// The following is the on-chip status register information.
// 0Ah Status Register A (read/write)
// 0Bh Status Register B (read/write)
// 0Ch Status Register C (Read only)
// 0Dh Status Register D (read only)
//
// see also: http://www.bioscentral.com/misc/cmosmap.htm

#ifdef __cplusplus
extern "C" {
#endif
int init_cmos(void);
void shutdown_cmos(void);
#ifdef __cplusplus
}
#endif

#endif
