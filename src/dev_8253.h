#ifndef _DEV8253_H_
#define _DEV8253_H_

// 8253/8254 = Programmable Interval Timers (PIT)
// http://wiki.osdev.org/Programmable_Interval_Timer
//
// Channel 0 is connected directly to IRQ0
// Channel 1 is unusable, and may not even exist.
// Channel 2 is connected to the PC speaker, but can be used for other purposes.
//
// Mode/Command register:
// 6 and 7	Select channel:
//		0 0 = Channel 0
//		0 1 = Channel 1
//		1 0 = Channel 2
//		1 1 = Read-back command (8254 only)
// 4 and 5	Access mode:
//		0 0 = Latch count value command
//		0 1 = Access mode: lobyte only
//		1 0 = Access mode: hibyte only
//		1 1 = Access mode: lobyte/hibyte
// 1 to 3	Operating mode:
//		0 0 0 = Mode 0 (interrupt on terminal count)
//		0 0 1 = Mode 1 (hardware re-triggerable one-shot)
//		0 1 0 = Mode 2 (rate generator)
//		0 1 1 = Mode 3 (square wave generator)
//		1 0 0 = Mode 4 (software triggered strobe)
//		1 0 1 = Mode 5 (hardware triggered strobe)
//		1 1 0 = Mode 2 (rate generator, same as 010b)
//		1 1 1 = Mode 3 (square wave generator, same as 011b)
// 0		BCD/Binary mode:
//		0 = 16-bit binary, 1 = four-digit BCD
//
// Counter Latch Command:
// 6 and 7	Select channel (same as previous)
// 0 to 5	Set to all zeros
// Copies the current count for the specific channel into a 'latch register' which can then be read via the data port
// corresponding to the selected channel (I/O ports 0x40 to 0x42). The value kept in the latch register remains the
// same until it has been fully read, or until a new mode/command register is written.
//
// Read Back Command (8254 only):
// 6 and 7	Set to 1 1
// 5		Latch count flag (0 = latch count, 1 = don't latch count)
// 4		Latch status flag (0 = latch status, 1 = don't latch status)
// 3		Read back timer channel 2 (1 = yes, 0 = no)
// 2		Read back timer channel 1 (1 = yes, 0 = no)
// 1		Read back timer channel 0 (1 = yes, 0 = no)
// 0		Reserved (should be clear)
//
// Bits 1 to 3 of the read back command select which PIT channels are affected, and allow multiple channels to be
// selected at the same time
// If bit 5 is clear, then any/all PIT channels selected with bits 1 to 3 will have their current count copied into
// their latch register (similar to sending the latch command, except it works for multiple channels with one command).
// If bit 4 is clear, then for any/all PIT channels selected with bits 1 to 3, the next read of the corresponding
// data port will return a status byte.
//
// Read Back Status Byte:
// 7	Output pin state
// 6	Null count flags
// 4 and 5	Access mode (see previous)
// 1 to 3	Operating mode (see previous)
// 0		BCD/Binary mode (see previous)
//
// Bit 7 indicates the state of the PIT channel's output pin at the moment that the read-back command was issued.
// Bit 6 indicates whether a newly-programmed divisor value has been loaded into the current count yet.


#ifndef __cplusplus
// PIT output signal
int pit_output[3];
// PIT channel 2 gate is controlled by IO port 0x61, bit 0
// PIT channels 0 and 1 gates are not connected
// this should really be part of the keyboard controller
// but we'll put it here for now
int gate_input[3];
#endif


#ifdef __cplusplus
extern "C" {
#endif
int init_pit(void);
void shutdown_pit(void);
void tick(void);
#ifdef __cplusplus
}
#endif

#endif
