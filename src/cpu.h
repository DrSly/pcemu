#ifndef _CPU_H_
#define _CPU_H_

#include "x86_types.h"

#ifndef __cplusplus
// 32 bit registers
union register32 reg_eax, reg_ebx, reg_ecx, reg_edx, reg_esp, reg_esi, reg_ebp, reg_edi;
int32_t reg_cs, reg_ds, reg_es, reg_ss, reg_fs, reg_gs;
int32_t reg_ip;
int32_t flags;
long long tick_count;
// io ports
int (*read_from_port[0x10000])(int port, enum datasize size);
void (*write_to_port[0x10000])(int port, int data, enum datasize size);
#endif

#ifdef __cplusplus
extern "C" {
#endif
void init_cpu();
void register_io_handler(int port, int count, int (fn_read)(int, enum datasize), void (fn_write)(int, int, enum datasize));
void unregister_io_handler(int port, int count);
uint8_t *load_instr_from_memory(struct instruction *opc);
void mov(struct operand *dest, struct operand *source, enum datasize size);
void push(struct operand *operand, enum datasize size);
void pusha(enum datasize size);
void pushf(enum datasize size);
void pop(struct operand *operand, enum datasize size);
void popa(enum datasize size);
void popf(enum datasize size);
void nop();
void xchg(struct operand *dest, struct operand *source, enum datasize size);
void lea(struct operand *dest, struct operand *source, enum datasize size);
void ldfptr(enum opcode opc, struct operand *dest, struct operand *source, enum datasize size);
void add(struct operand *dest, struct operand *source, enum datasize size);
void sub(struct operand *dest, struct operand *source, enum datasize size);
void inc(struct operand *operand, enum datasize size);
void dec(struct operand *operand, enum datasize size);
void udiv(struct operand *operand, enum datasize size);
void idiv(struct operand *operand, enum datasize size);
void mul(struct operand *operand, enum datasize size);
void imul(struct operand *dest, struct operand *source, enum datasize size);
void bitwise_and(struct operand *dest, struct operand *source, enum datasize size);
void bitwise_or(struct operand *dest, struct operand *source, enum datasize size);
void bitwise_xor(struct operand *dest, struct operand *source, enum datasize size);
void bitwise_not(struct operand *operand, enum datasize size);
void neg(struct operand *operand, enum datasize size);
void test(struct operand *dest, struct operand *source, enum datasize size);
void shl(struct operand *dest, struct operand *source, enum datasize size);
void shr(struct operand *dest, struct operand *source, enum datasize size);
void rol(struct operand *dest, struct operand *source, enum datasize size);
void ror(struct operand *dest, struct operand *source, enum datasize size);
void rcl(struct operand *dest, struct operand *source, enum datasize size);
void rcr(struct operand *dest, struct operand *source, enum datasize size);
void jmp(struct operand *operand, enum datasize size);
void jmp_far(struct operand *offset, struct operand *segment, enum datasize size);
void jz(struct operand *operand, enum datasize size);
void jnz(struct operand *operand, enum datasize size);
void jc(struct operand *operand, enum datasize size);
void jnc(struct operand *operand, enum datasize size);
void js(struct operand *operand, enum datasize size);
void jns(struct operand *operand, enum datasize size);
void jo(struct operand *operand, enum datasize size);
void jno(struct operand *operand, enum datasize size);
void ja(struct operand *operand, enum datasize size);
void jna(struct operand *operand, enum datasize size);
void jge(struct operand *operand, enum datasize size);
void jg(struct operand *operand, enum datasize size);
void jl(struct operand *operand, enum datasize size);
void jle(struct operand *operand, enum datasize size);
void jp(struct operand *operand, enum datasize size);
void jnp(struct operand *operand, enum datasize size);
void jcxz(struct operand *operand, enum datasize datasize, enum datasize address_size);
void cmp(struct operand *dest, struct operand *source, enum datasize size);
void compl_c();
void clear_c();
void set_c();
void clear_d();
void set_d();
void clear_i();
void set_i();
void movs(struct operand *dest, struct operand *source, enum datasize size);
void cmps(struct operand *dest, struct operand *source, enum datasize size);
void stos(struct operand *dest, struct operand *source, enum datasize size);
void lods(struct operand *dest, struct operand *source, enum datasize size);
void scas(struct operand *dest, struct operand *source, enum datasize size);
void rep_movs(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size);
void repne_cmps(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size);
void repe_cmps(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size);
void rep_stos(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size);
void rep_lods(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size);
void repne_scas(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size);
void repe_scas(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size);
void call(struct operand *operand, enum datasize size);
void call_far(struct operand *offset, struct operand *segment, enum datasize size);
void ret(struct operand *operand, enum datasize size);
void retf(struct operand *operand, enum datasize size);
void interrupt(struct operand *operand, enum datasize size);
void iret(enum datasize size);
void in(struct operand *dest, struct operand *source, enum datasize size);
void out(struct operand *dest, struct operand *source, enum datasize size);
void sahf();
void lahf();
void cbw();
void cwd();
void cwde();
void cdq();
void movsx(struct operand *dest, struct operand *source, enum datasize size);
void movzx(struct operand *dest, struct operand *source, enum datasize size);
void loop(struct operand *operand, enum datasize datasize, enum datasize address_size);
void loope(struct operand *operand, enum datasize datasize, enum datasize address_size);
void loopne(struct operand *operand, enum datasize datasize, enum datasize address_size);
void ins(struct operand *dest, struct operand *source, enum datasize size);
void outs(struct operand *dest, struct operand *source, enum datasize size);
void rep_ins(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size);
void rep_outs(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size);
void execute(struct instruction *instr);
int checkparity(int value);
void setflags(int flag_bitfields);
void clearflags(int flag_bitfields);
int get_value(struct operand *operand, enum datasize size);
void set_value(struct operand *operand, int value, enum datasize size);
void *getregister(enum rm_register reg, enum datasize size);
int getregistervalue(enum rm_register reg, enum datasize size);
void setregister(enum rm_register reg, int value, enum datasize size);
int sign_extend(int value, enum datasize size);
#ifdef __cplusplus
}
#endif

#endif
