#ifndef _DECODE_H_
#define _DECODE_H_

#include <stdbool.h>
#include "x86_types.h"


#define imm_byte_count(override) (( !(mode32) != !(override) ) ? 4 : 2)

#define WORD_SIZE(override) (( !(mode32) != !(override) ) ? DWORD : WORD)

#define assign_modregrm(modregrm_data, extra_opcodes, destination_bit, operandsize_bit, instr) (( !(mode32) != !(instr->prefix.address_size) ) ? (assign_modregrm32(modregrm_data, extra_opcodes, destination_bit, operandsize_bit, instr)) : (assign_modregrm16(modregrm_data, extra_opcodes, destination_bit, operandsize_bit, instr)))

#define assign_modrm(modregrm_data, extra_opcodes, destination_bit, operandsize_bit, instr) (( !(mode32) != !(instr->prefix.address_size) ) ? (assign_modrm32(modregrm_data, extra_opcodes, destination_bit, operandsize_bit, instr)) : (assign_modrm16(modregrm_data, extra_opcodes, destination_bit, operandsize_bit, instr)))

#define get_segment_register(register, segment_override) (( !(mode32) != !(instr->prefix.address_size) ) ? (get_segment_register32(register, segment_override)) : (get_segment_register16(register, segment_override)))

#define decode_imm(opcode, override) (( !(mode32) != !(override) ) ? (decode_imm32(opcode)) : (decode_imm16(opcode)))

#define decode_imm_sign_extend(opcode, override) (( !(mode32) != !(override) ) ? (decode_imm32_sign_extend(opcode)) : (decode_imm16_sign_extend(opcode)))


#ifdef __cplusplus
extern "C" {
#endif
int decode(uint8_t *opcodes, struct instruction *opc);
uint8_t check_prefix(uint8_t *opcodes);
void decode_modregrm(uint8_t opcode, struct mod_reg_rm *modregrm_data);	// decode MOD-REG-R/M byte
void decode_sib(uint8_t opcode, struct sib *sib_data);
int assign_modregrm32(struct mod_reg_rm *modregrm_data, uint8_t *extra_opcodes, int destination_bit, int operandsize_bit, struct instruction *instr);
int assign_modrm32(struct mod_reg_rm *modregrm_data, uint8_t *extra_opcodes, int destination_bit, int operandsize_bit, struct instruction *instr);
int assign_modregrm16(struct mod_reg_rm *modregrm_data, uint8_t *extra_opcodes, int destination_bit, int operandsize_bit, struct instruction *instr);
int assign_modrm16(struct mod_reg_rm *modregrm_data, uint8_t *extra_opcodes, int destination_bit, int operandsize_bit, struct instruction *instr);
enum rm_register get_segment_register32(enum rm_register rm, int seg_override);
enum rm_register get_segment_register16(enum rm_register rm, int seg_override);
uint8_t decode_imm8(uint8_t *opcode);
uint16_t decode_imm16(uint8_t *opcode);
uint32_t decode_imm32(uint8_t *opcode);
int decode_imm8_sign_extend(uint8_t *opcode);
int decode_imm16_sign_extend(uint8_t *opcode);
int decode_imm32_sign_extend(uint8_t *opcode);
#ifdef __cplusplus
}
#endif

// opcodes sourced from https://courses.engr.illinois.edu/ece390/books/labmanual/inst-ref-general.html
//
// B.4.1 AAA, AAS, AAM, AAD: ASCII Adjustments
// B.4.2 ADC: Add with Carry
// B.4.3 ADD: Add Integers = 00, 01, 02, 03, 04, 05, 80, 81, 82(?), 83
// B.4.4 AND: Bitwise AND = 20, 21, 22, 23, 24, 25, 80, 81, 83
// B.4.5 ARPL: Adjust RPL Field of Selector
// B.4.6 BOUND: Check Array Index against Bounds
// B.4.7 BSF, BSR: Bit Scan
// B.4.8 BSWAP: Byte Swap
// B.4.9 BT, BTC, BTR, BTS: Bit Test
// B.4.10 CALL: Call Subroutine = E8, 9A, FF
// B.4.11 CBW, CWD, CDQ, CWDE: Sign Extensions = 98, 99
// B.4.12 CLC, CLD, CLI, CLTS: Clear Flags = F8, FA, FC, (0F06)
// ...

// B.4.14 CMC: Complement Carry Flag = F5
// ...

// B.4.16 CMP: Compare Integers = 38, 39, 3A, 3B, 3C, 3D, 80, 81, 83
// B.4.17 CMPSB, CMPSW, CMPSD: Compare Strings = A6, A7

// B.4.22 DEC: Decrement Integer = 48-4F, EF, FF
// B.4.23 DIV: Unsigned Integer Divide = F6, F7

// ...

// B.4.75 IDIV: Signed Integer Divide = F6, F7
// B.4.76 IMUL: Signed Integer Multiply = 69, 6B, F6, F7, 0FAF
// B.4.77 IN: Input from I/O Port = E4, E5, EC, ED
// B.4.78 INC: Increment Integer = 40-47, FE, FF
// B.4.79 INSB, INSW, INSD: Input String from I/O Port = 6C, 6D
// B.4.80 INT: Software Interrupt = CD
// B.4.81 INT3, INT1, ICEBP, INT01: Breakpoints = CC, F1

// ...

// B.4.85 IRET, IRETW, IRETD: Return from Interrupt = CF
// B.4.86 JCXZ, JECXZ: Jump if CX/ECX Zero = E3
// B.4.87 Jcc: Conditional Branch = 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 7A, 7B, 7C, 7D, 7E, 7F,
// 		0F80, 0F81, 0F82, 0F83, 0F84, 0F85, 0F86, 0F87, 0F88, 0F89, 0F8A, 0F8B, 0F8C, 0F8D, 0F8E, 0F8F
// B.4.88 JMP: Jump = E9, EB, EA, FF
// B.4.89 LAHF: Load AH from Flags = 9F
// ...
// B.4.91 LDS, LES, LFS, LGS, LSS: Load Far Pointer = C4, C5, 0FB2, 0FB4, 0FB5
// B.4.92 LEA: Load Effective Address = 8D

// ...

// B.4.97 LODSB, LODSW, LODSD: Load from String = AC, AD
// B.4.98 LOOP, LOOPE, LOOPZ, LOOPNE, LOOPNZ: Loop with Counter = E0, E1, E2

// ...

// B.4.102 MOV: Move Data = 88, 89, 8A, 8B, B0, B8, C6, C7, A0, A1, A2, A3, 8C, 8E, 0F20, 0F21, 0F22, 0F23, 0F24, 0F26
// B.4.103 MOVSB, MOVSW, MOVSD: Move String = A4, A5
// B.4.104 MOVSX, MOVZX: Move Data with Sign or Zero Extend = 0FB6, 0FB7, 0FBE, 0FBF
// B.4.105 MUL: Unsigned Integer Multiply = F6, F7
// B.4.106 NEG, NOT: Two's and One's Complement = F6, F7
// B.4.107 NOP: No Operation = 90
// B.4.108 OR: Bitwise OR = 08, 09, 0A, 0B, 0C, 0D, 80, 81, 83
// B.4.109 OUT: Output Data to I/O Port = E6, E7, EE, EF
// B.4.110 OUTSB, OUTSW, OUTSD: Output String to I/O Port = 6E, 6F
// ...
// B.4.112 POP: Pop Data from Stack = 58, 8F, 1F, 07, 17, 0FA1, 0FA9
// B.4.113 POPAx: Pop All General-Purpose Registers = 61
// B.4.114 POPFx: Pop Flags Register = 9D

// B.4.116 PUSH: Push Data on Stack = 50, FF, 0E, 1E, 06, 16, 0FA0, 0FA8, 6A, 68
// B.4.117 PUSHAx: Push All General-Purpose Registers = 60
// B.4.118 PUSHFx: Push Flags Register = 9C
// B.4.119 RCL, RCR: Bitwise Rotate through Carry Bit = C0, C1, D0, D1, D2, D3
// ...

// B.4.123 RET, RETF, RETN: Return from Procedure Call = C2, C3, CA, CB
// B.4.124 ROL, ROR: Bitwise Rotate = C0, C1, D0, D1, D2, D3

// ...

// B.4.126 SAHF: Store AH to Flags = 9E

// ...

// B.4.130 SCASB, SCASW, SCASD: Scan String = AE, AF

// B.4.134 SHL, SHR: Bitwise Logical Shifts = C0, C1, D0, D1, D2, D3

// ...

// B.4.137 STC, STD, STI: Set Flags = F9, FB, FD
// B.4.138 STOSB, STOSW, STOSD: Store Byte to String = AA, AB

//B.4.140 SUB: Subtract Integers = 28, 29, 2A, 2B, 2C, 2D, 80, 81, 83

// ...

// B.4.145 TEST: Test Bits (notional bitwise AND) = 84, 85, A8, A9, F6, F7

// ...

// B.4.151 XCHG: Exchange = 86, 87, 90
// ...

// B.4.153 XOR: Bitwise Exclusive OR = 30, 31, 32, 33, 34, 35, 80, 81, 83

#endif
