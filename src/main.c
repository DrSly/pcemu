#include <stdio.h>
#include <signal.h>
#include <conio.h>		// for getch()
#include <stdlib.h>
#include <string.h>		// for memset()

#include "zlog.h"
#include "x86_types.h"
#include "memory.h"
#include "cpu.h"
#include "debug.h"
#include "dev_debug.h"
#include "dev_sysctrl.h"
#include "dev_8259.h"
#include "cmos.h"
#include "dev_ps2_keyboard.h"
#include "dev_ps2_mouse.h"
#include "dev_8042.h"
#include "dev_8237A.h"
#include "dev_8253.h"
#include "dev_16550.h"
#include "dev_parallel.h"

int load_bios();
void outputRegister(enum rm_register reg);
void SignalHandler(int signal);

struct x {
	// registers
	union register32 eax;
	union register32 ebx;
	union register32 ecx;
	union register32 edx;
};

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {

	// initialise logging
	zlog_init("pcemu.log");

	zlogf("[t=%I64d] pcemu start\n", tick_count);

	// initialise memory
	if (init_memory() != 0) {
		zlogf("[t=%I64d] Error initialising memory subsystem\n", tick_count);
		printf("Error initialising memory subsystem\n");
		return 1;
	}

	// initialise cpu
	zlogf("[t=%I64d] initialising cpu\n", tick_count);
	init_cpu();

	// initialise devices
	zlogf("[t=%I64d] initialising devices\n", tick_count);
	init_debug();
	init_sysctrl();
	init_pic();
	init_cmos();
	init_ps2_keyboard();
	init_ps2_mouse();
	init_keybd();
	init_dma();
	init_pit();
	init_uart();
	init_parallel();

	// load bios into memory
	if (load_bios() != 0) {
		zlogf("[t=%I64d] Error loading bios\n", tick_count);
		printf("Error loading bios\n");
		return 1;
	}

	// set up handler to catch Ctrl-C
	signal(SIGINT, SignalHandler);

	struct instruction opc;
	memset(&opc, 0, sizeof(struct instruction));
	uint8_t *code = 0;
	int breakout = 0;
	while (!breakout) {

		// load 16 bytes from memory
		code = load_instr_from_memory(&opc);
		// exit on HLT instruction
		if (code[0] == 0xf4) breakout = 1;

		if (debug_main_entry(&opc) == -1) breakout = 1;

		execute(&opc);

	}

	// shut down
	shutdown_parallel();
	shutdown_uart();
	shutdown_pit();
	shutdown_dma();
	shutdown_keybd();
	shutdown_ps2_mouse();
	shutdown_ps2_keyboard();
	shutdown_cmos();
	shutdown_pic();
	shutdown_sysctrl();
	shutdown_debug();

	free_memory();

	zlogf("[t=%I64d] pcemu finish\n", tick_count);

	zlog_finish();

	//getch();
	return 0;
}

int load_bios() {
	//int memory_addr = 0x7c00;
	int memory_addr = 0xe0000;
	FILE *fp;
	uint8_t buffer[512];
	// Bochs default BIOS
	fp=fopen("..\\assets\\BIOS-bochs-latest", "rb");
	// PCem BIOS
	//fp=fopen("..\\assets\\55XWUQ0E.BIN", "rb");
	if (!fp) {
		printf("Error opening BIOS ROM image\n");
		return 1;
	}
	int bytesread = 0;
	int totalbytes = 0;
	while (!feof(fp)) {
		bytesread = fread(buffer, sizeof(uint8_t), 512, fp);
		copy_to_mem(memory_addr, buffer, bytesread);
		memory_addr+= bytesread;
		totalbytes += bytesread;
	}
	printf("load_bios: Read %d bytes.\n", totalbytes);
	fclose(fp);
	return 0;
}

void outputRegister(enum rm_register reg) {
	switch (reg) {
		case al_ax_eax:
			printf("al_ax_eax ");
		break;
		case cl_cx_ecx:
			printf("cl_cx_ecx ");
		break;
		case dl_dx_edx:
			printf("dl_dx_edx ");
		break;
		case bl_bx_ebx:
			printf("bl_bx_ebx ");
		break;
		case ah_sp_esp:
			printf("ah_sp_esp ");
		break;
		case ch_bp_ebp:
			printf("ch_bp_ebp ");
		break;
		case dh_si_esi:
			printf("dh_si_esi ");
		break;
		case bh_di_edi:
			printf("bh_di_edi ");
		break;
		case seg_es:
			printf("es : ");
		break;
		case seg_cs:
			printf("cs : ");
		break;
		case seg_ss:
			printf("ss : ");
		break;
		case seg_ds:
			printf("ds : ");
		break;
		case seg_fs:
			printf("fs : ");
		break;
		case seg_gs:
			printf("gs : ");
		break;
		case rm_flags:
			printf("flags ");
		break;
		case ip:
			printf("ip ");
		break;
	}
}

void SignalHandler(int sig) {
	// tell the debugger to break before the next instruction
	break_next();
	signal(SIGINT, SignalHandler);
}
