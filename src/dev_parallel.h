#ifndef _DEVPARALLEL_H_
#define _DEVPARALLEL_H_

// Parallel Port
// https://wiki.osdev.org/Parallel_port
//
// LPT1	0x378
// LPT2	0x278
//
// Data Register = Base Address + 0
// Any byte writen to this register is put on pins 2 through 9 of the port.
// Any read from this register reflects the state of the port.
//
// Status Register = Base Address + 1
// 7	BUSY
// 6	ACK
// 5	PAPER_OUT
// 4	SELECT_IN
// 3	ERROR
// 2	IRQ
// 1	Reserved
// 0	Reserved
//
// Control Register = Base Address + 2
// 7	Unused
// 6	Unused
// 5	BIDI
// 4	IRQACK
// 3	SELECT
// 2	INITIALISE
// 1	AUTO_LF
// 0	STROBE


#ifdef __cplusplus
extern "C" {
#endif
int init_parallel(void);
void shutdown_parallel(void);
#ifdef __cplusplus
}
#endif

#endif
