#ifndef _DEV8259_H_
#define _DEV8259_H_

// 8259 = Programmable Interrupt Controller (PIC)
// http://wiki.osdev.org/8259_PIC
// http://retired.beyondlogic.org/interrupts/interupt.htm
//
// Initialization Control Words (ICW)
// These must be sent to both PIC controllers' command ports (0x20 and 0xA0)
//
// ICW 1
// To initialise a PIC, send a value of 0x11 (ICW1) to each of the PIC controllers' command ports, followed by ICW2, ICW3
// and ICW4 to their data ports. Both controllers must be initialised at the same time.
//
// 5-7	must be 0
//   4	Initialization bit. Set 1 if PIC is to be initialized
//   3	LTIM: Edge triggered (0)/Level triggered (1) mode
//   2	ADI: CALL address interval. Set to 0
//   1	SNGL: PIC cascading bit. If cleared, PIC is cascaded with slave PICs, and ICW3 must be sent to controller
//   0	IC4: If set, the PIC expects to recieve IC4 during initialization
//
// ICW 2
// This control word is used to map the base address of the IVT of which the PIC are to use.
// This must be sent to each of the controllers' data ports (0x21 and 0xA1)
//
// 3-7 In 80x86 mode, specifies the interrupt vector address
// 0-2 Not used
//
// Since the lowest three bits are not used, the base address must be divisible by 8.
//
// ICW 3
// This control word is used to let the PICs know what IRQ lines to use when communicating with each other.
// This must be sent to each of the controllers' data ports (0x21 and 0xA1)
// The format differs between the slave and master
//
// master:
// 0-7	Specifies what Interrupt Request (IRQ) is connected to slave PIC (each bit represents an IRQ)
//
// slave:
// 3-7	Reserved, must be 0
// 0-2	IRQ number the master PIC uses to connect to (In binary notation)
//
// The 80x86 architecture uses IRQ line 2 to connect the master PIC to the slave PIC.
// Therefore the master controller receives 00000100 (0x4) and the slave controller receives 0x2.
//
// ICW 4
// Required if bit 0 was set in ICW 1.
// This must be sent to each of the controllers' data ports (0x21 and 0xA1)
//
// 5-7	must be 0
//   4	SFNM: Set to 0
//   3	BUF: Set to 0
//   2	M/S: Set to 0
//   1	AEOI: Set to 0
//   0	uPM: MCS-80/86 (0)/80x86 (1) mode
//
// OCW2 command word
// This is the primary control word used to control the PIC. To signal EOI to the controller, send 0x20 (Non specific EOI)
// to the pic controller's command port.
//
// 5-7	OCW2 Commands:
//		0 0 0 = Rotate in Automatic EOI mode (CLEAR)
//		0 0 1 = Non specific EOI command
//		0 1 0 = No operation
//		0 1 1 = Specific EOI command
//		1 0 0 = Rotate in Automatic EOI mode (SET)
//		1 0 1 = Rotate on non specific EOI
//		1 1 0 = Set priority command
//		1 1 1 = Rotate on specific EOI
// 3-4	Reserved
// 0-2	Interrupt level upon which the controller must react
//
// OCW3 command word
// The ISR and IRR can be read via the OCW3 command word.
// To read the IRR, write 0x0a to the command port, and then read the command port (not the data port).
// To read the ISR, write 0x0b to the command port, and then read the command port (not the data port).


#ifndef __cplusplus
// interrupt request lines
int irq_request[16];
#endif


#ifdef __cplusplus
extern "C" {
#endif
int init_pic(void);
void shutdown_pic(void);
int check_irq_request();
int acknowledge_irq();
#ifdef __cplusplus
}
#endif

#endif
