#ifndef _X86TYPES_H_
#define _X86TYPES_H_

#include <stdint.h>

// define the x86 registers

struct reg16 {
	int8_t rl;
	int8_t rh;
};

union register16 {
	struct reg16 r8;
	int16_t value;
};

union register32 {
	union register16 r16;
	int32_t value;
};

// register values as defined in reg and rm parts of the mod reg r/m byte
// note the actual register being used is dependent on whether the instruction is
// operating on 8, 16 or 32 bit data; eg AH (8 bit) corresponds with ESP (32 bit)

#define _16BITMODEOFFSET 16

enum rm_register {
	al_ax_eax = 0,
	cl_cx_ecx = 1,
	dl_dx_edx = 2,
	bl_bx_ebx = 3,
	ah_sp_esp = 4,
	ch_bp_ebp = 5,
	dh_si_esi = 6,
	bh_di_edi = 7,
	// segment registers use the same values in the reg field, so we set bit 4 to differentiate them
	// see MOV (8c, 8e) for example
	seg_es = 8,		// 000
	seg_cs = 9,		// 001
	seg_ss = 10,	// 010
	seg_ds = 11,	// 011
	seg_fs = 12,	// 100
	seg_gs = 13,	// 101
	// used internally by cpu.c
	rm_flags = 14,
	ip = 15,
	// 16 bit addressing modes use the same values as the general purpose registers,
	// we add _16BITMODEOFFSET to differentiate them
	ind_bx_si = 0 + _16BITMODEOFFSET,
	ind_bx_di = 1 + _16BITMODEOFFSET,
	ind_bp_si = 2 + _16BITMODEOFFSET,
	ind_bp_di = 3 + _16BITMODEOFFSET,
	ind_si = 4 + _16BITMODEOFFSET,
	ind_di = 5 + _16BITMODEOFFSET,
	ind_bp = 6 + _16BITMODEOFFSET,
	ind_bx = 7 + _16BITMODEOFFSET
};

// flags

enum enum_flags {
	CF = 0x0001,	// carry
	PF = 0x0004,	// parity
	AF = 0x0010,	// adjust
	ZF = 0x0040,	// zero
	SF = 0x0080,	// sign
	TF = 0x0100,	// trap
	IF = 0x0200,	// interrupt enable
	DF = 0x0400,	// direction
	OF = 0x0800,	// overflow
	//IOPL = bits 12+13,	// I/O privilege level (286+)
	NT = 0x4000,	// Nested task flag (286+)
};

// define operand size

enum datasize {
	BYTE = 8,
	WORD = 16,
	DWORD = 32,
	NIL = 0
};

// define supported opcodes

enum opcode {
	ADD, SUB, INC, DEC, DIV, IDIV, MUL, IMUL,
	AND, OR, XOR, NOT, NEG, TEST,
	SHL, SHR, ROL, ROR, RCL, RCR,
	JMP, JMP_FAR,
	JZ, JNZ, JC, JNC, JS, JNS, JO, JNO, JA, JNA,
	JGE, JG, JL, JLE, JP, JNP, JCXZ, JECXZ,
	CMP, CMC, CLC, STC, CLI, STI, CLD, STD,
	MOV, MOVS, CMPS, STOS, LODS, SCAS,
	REP_MOVS, REPNE_CMPS, REPE_CMPS, REP_STOS, REP_LODS, REPNE_SCAS, REPE_SCAS,
	PUSH, PUSHA, PUSHF,
	POP, POPA, POPF,
	NOP, XCHG, LEA, LDS, LES, LFS, LGS, LSS,
	CALL, CALL_FAR, RET, RETF, INT, IRET,
	IN, OUT, INS, OUTS, REP_INS, REP_OUTS,
	SAHF, LAHF,
	CBW, CWD, CWDE, CDQ,
	MOVSX, MOVZX,
	LOOP, LOOPE, LOOPNE,
	// temporary flags to designate an unsupported feature:
	NOT_IMPL,
	INVALID_OPCODE = 0xFFFF
};

// opcode prefixes
struct prefix {
	int extended_opcode;	// 0F: extended 2 byte opcode
	int segment_override;	// 26, 2E, 36, 3E, 64, 65: segment override (ES/CS/SS/DS/FS/GS)
	int operand_size;		// 66: operand size override
	int address_size;		// 67: address size override
	int lock_rep;			// F0, F2, F3: lock/repne/repe
};

// operand could be an immediate value, memory reference, or register
// we need sufficient detail to encapsulate all the different addressing modes of the x86 cpu
// for more info see: http://www.c-jump.com/CIS77/CPU/x86/lecture.html#X77_0090_addressing_modes

struct operand {
	int use_register;
	int use_indregister;
	enum rm_register register_segment;
	enum rm_register register_data;
	enum datasize register_size;
	int use_sib;
	int sib_scale;
	enum rm_register sib_index;
	int use_imm_word;
	uint32_t imm_word_data;
	int use_offset;
	int offset_data;
	int use_disponly;
	int disponly_data;
};

// The MOD-REG-R/M byte specifies instruction operands and their addressing mode

struct mod_reg_rm {
	int mod;
	enum rm_register rm;	// rm register
	enum rm_register reg;	// reg field
};

// The SIB byte is used in Scaled indexed addressing mode

struct sib {
	int scale;			// 00 = index*1, 01 = index*2, 10 = index*4, 11 = index*8
	enum rm_register index;	// index register, multiplied by scale
	enum rm_register base;	// base register
};

// decoded instruction

struct instruction {
	enum opcode op;	// opcode
	enum datasize datasize;
	struct operand source;
	struct operand dest;
	struct prefix prefix;	// instruction prefixes
	// for debugging purposes
	int length;
	uint8_t bytes[15];
};


#ifndef __cplusplus
int mode32;
#endif


#endif
