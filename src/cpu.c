#include <stdio.h>
#include <string.h>		// for memset()

#include "zlog.h"
#include "memory.h"
#include "decode.h"
#include "cpu.h"

// store the current instruction
uint8_t prefetch[16];

void init_cpu() {

	// initialise CPU
	reg_eax.value = 0;	reg_ebx.value = 0;
	reg_ecx.value = 0;	reg_edx.value = 0;
	reg_esp.value = 0;	reg_esi.value = 0;
	reg_ebp.value = 0;	reg_edi.value = 0;

	//reg_cs = 0;	reg_ds = 0;
	reg_cs = 0xf000;	reg_ds = 0;
	reg_es = 0;	reg_ss = 0;
	reg_fs = 0;	reg_gs = 0;
	//reg_ip = 0x7c00;
	reg_ip = 0xfff0;
	flags = 0x0002;
	tick_count = 0;

	// cpu begins operating in 16 bit mode
	mode32 = 0;

	// initialise io ports
	for (int i=0; i<0x10000; i++) {
		read_from_port[i] = NULL;
		write_to_port[i] = NULL;
	}

}

void register_io_handler(int port, int count, int (fn_read)(int, enum datasize), void (fn_write)(int, int, enum datasize)) {

	// reset io handler
	for (int i=port; i<port+count; i++) {
		read_from_port[i] = fn_read;
		write_to_port[i] = fn_write;
	}

}

void unregister_io_handler(int port, int count) {

	// reset io handler
	for (int i=port; i<port+count; i++) {
		read_from_port[i] = NULL;
		write_to_port[i] = NULL;
	}

}

uint8_t *load_instr_from_memory(struct instruction *opc) {

	// instruction can be up to 16 bytes
	copy_from_mem((reg_cs << 4) + reg_ip, prefetch, 16);
	memset(opc, 0, sizeof(struct instruction));
	decode(prefetch, opc);
	return prefetch;
}

void mov(struct operand *dest, struct operand *source, enum datasize size) {

	// copy source to dest
	// either may be register, memory reference, or
	// source may be an immediate value

	int val = get_value(source, size);

	set_value(dest, val, size);

}

void push(struct operand *operand, enum datasize size) {

	// decrement SP by the size of the operand
	// copy one word from source to the stack top (SS:SP)

	struct operand stack_top;
	memset(&stack_top, 0, sizeof(struct operand));
	stack_top.use_indregister = 1;
	stack_top.register_segment = seg_ss;
	stack_top.register_data = ah_sp_esp;
	stack_top.register_size = size;

	// decrement the stack pointer
	if (size == BYTE) reg_esp.value -= 2;
	if (size == WORD) reg_esp.value -= 2;
	if (size == DWORD) reg_esp.value -= 4;

	int val = get_value(operand, size);

	set_value(&stack_top, val, size);

}

// push all
void pusha(enum datasize size) {

	// push on the stack in the following order: AX, CX, DX, BX, SP, BP, SI and DI
	struct operand p_register;
	memset(&p_register, 0, sizeof(struct operand));
	p_register.use_register = 1;
	p_register.register_data = al_ax_eax;
	push(&p_register, size);
	p_register.use_register = 1;
	p_register.register_data = cl_cx_ecx;
	push(&p_register, size);
	p_register.use_register = 1;
	p_register.register_data = dl_dx_edx;
	push(&p_register, size);
	p_register.use_register = 1;
	p_register.register_data = bl_bx_ebx;
	push(&p_register, size);
	p_register.use_register = 1;
	p_register.register_data = ah_sp_esp;
	push(&p_register, size);
	p_register.use_register = 1;
	p_register.register_data = ch_bp_ebp;
	push(&p_register, size);
	p_register.use_register = 1;
	p_register.register_data = dh_si_esi;
	push(&p_register, size);
	p_register.use_register = 1;
	p_register.register_data = bh_di_edi;
	push(&p_register, size);

}

// push flags
void pushf(enum datasize size) {

	struct operand reg_flags;
	memset(&reg_flags, 0, sizeof(struct operand));
	reg_flags.use_register = 1;
	reg_flags.register_data = rm_flags;
	push(&reg_flags, size);

}

void pop(struct operand *operand, enum datasize size) {

	// copy a value from the stack (SS:SP) to destination specified in operand
	// increment SP by the size of the operand

	struct operand stack_top;
	memset(&stack_top, 0, sizeof(struct operand));
	stack_top.use_indregister = 1;
	stack_top.register_segment = seg_ss;
	stack_top.register_data = ah_sp_esp;
	stack_top.register_size = size;

	int val = get_value(&stack_top, size);

	set_value(operand, val, size);

	// update the stack pointer
	if (size == BYTE) reg_esp.value += 2;
	if (size == WORD) reg_esp.value += 2;
	if (size == DWORD) reg_esp.value += 4;

}

// pop all
void popa(enum datasize size) {

	// pop from the stack in the following order: DI, SI, BP, (SP), BX, DX, CX and AX
	// note the value for SP is discarded
	struct operand p_register;
	memset(&p_register, 0, sizeof(struct operand));
	p_register.use_register = 1;
	p_register.register_data = bh_di_edi;
	pop(&p_register, size);
	p_register.use_register = 1;
	p_register.register_data = dh_si_esi;
	pop(&p_register, size);
	p_register.use_register = 1;
	p_register.register_data = ch_bp_ebp;
	pop(&p_register, size);
	p_register.use_register = 1;
	p_register.register_data = bl_bx_ebx;	// we need to discard ah_sp_esp, so we're placing the value into another register which is subsequently overwritten
	pop(&p_register, size);
	p_register.use_register = 1;
	p_register.register_data = bl_bx_ebx;
	pop(&p_register, size);
	p_register.use_register = 1;
	p_register.register_data = dl_dx_edx;
	pop(&p_register, size);
	p_register.use_register = 1;
	p_register.register_data = cl_cx_ecx;
	pop(&p_register, size);
	p_register.use_register = 1;
	p_register.register_data = al_ax_eax;
	pop(&p_register, size);

}

// pop flags
void popf(enum datasize size) {

	struct operand reg_flags;
	memset(&reg_flags, 0, sizeof(struct operand));
	reg_flags.use_register = 1;
	reg_flags.register_data = rm_flags;
	pop(&reg_flags, size);

}

void nop() {
}

void xchg(struct operand *dest, struct operand *source, enum datasize size) {

	// swap source and dest

	int val = get_value(source, size);
	int val2 = get_value(dest, size);

	set_value(dest, val, size);
	set_value(source, val2, size);

}

void lea(struct operand *dest, struct operand *source, enum datasize size) {

	// calculates the memory address specified by source and loads it into dest

	int val;

	// cannot load a register!
	if (source->use_register) {
		return;
	}
	if (source->use_indregister && source->use_offset) {
		val = getregistervalue(source->register_data, source->register_size) + source->offset_data;
		set_value(dest, val, size);
		return;
	}
	if (source->use_indregister) {
		val = getregistervalue(source->register_data, source->register_size);
		set_value(dest, val, size);
		return;
	}
	// cannot load an immediate value!
	if (source->use_imm_word) {
		return;
	}
	if (source->use_disponly) {
		val = source->disponly_data;
		set_value(dest, val, size);
		return;
	}

}

// Load Far Pointer (handles LDS, LES, LFS, LGS, LSS)
void ldfptr(enum opcode opc, struct operand *dest, struct operand *source, enum datasize size) {

	// load an entire far pointer (16 or 32 bits of offset, plus 16 bits of segment) from memory
	// first load 16 or 32 bits from the given memory address (source) into the given register (dest)
	// then load the next 16 bits from memory into DS, ES, FS, GS or SS

	int val = get_value(source, size);

	set_value(dest, val, size);

	// calculate the next address to read
	int step = 0;
	if (size == BYTE) step = 1;
	if (size == WORD) step = 2;
	if (size == DWORD) step = 4;

	struct operand next;
	memcpy(&next, source, sizeof(struct operand));
	if (next.use_indregister && next.use_offset) {
		next.offset_data += step;
	}
	else if (next.use_indregister) {
		next.use_offset = 1;
		next.offset_data = step;
	}
	else if (next.use_disponly) {
		next.disponly_data += step;
	}

	// load the next 16 bits into DS, ES, FS, GS or SS
	val = get_value(&next, WORD);

	switch (opc) {
	case LDS:
		setregister(seg_ds, val, WORD);
		break;
	case LES:
		setregister(seg_es, val, WORD);
		break;
	case LFS:
		setregister(seg_fs, val, WORD);
		break;
	case LGS:
		setregister(seg_gs, val, WORD);
		break;
	case LSS:
		setregister(seg_ss, val, WORD);
		break;
	default:
		break;
	}

}

void add(struct operand *dest, struct operand *source, enum datasize size) {

	// add the value of source to dest
	// if the source size is smaller than dest, it is sign extended
	// either may be a register or memory reference, or
	// source may be a register, memory reference, or an immediate value

	unsigned int val_source = get_value(source, size);
	unsigned int val_dest = get_value(dest, size);

	unsigned long val = val_source + val_dest;
	set_value(dest, val, size);

	// signed representation of the result; used for setting flags
	int val_s = sign_extend(val, size);

	// SET FLAGS REGISTER
	// Modifies: AF CF OF PF SF ZF
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// ADJUST (AKA AUXILLARY) FLAG
	// for info on AF see https://en.wikipedia.org/wiki/Half-carry_flag
	int valnibble = val_source & 0x0F;
	valnibble += (val_dest & 0x0F);
	if (valnibble > 0x0F)
		set_flags |= AF;
	else
		clear_flags |= AF;

	// CARRY FLAG
	if (size == DWORD) {
		if ((val | 0xFFFFFFFF) != 0xFFFFFFFF) set_flags |= CF;
		else clear_flags |= CF;
	}
	if (size == WORD) {
		if ((val | 0xFFFF) != 0xFFFF) set_flags |= CF;
		else clear_flags |= CF;
	}
	if (size == BYTE) {
		if ((val | 0xFF) != 0xFF) set_flags |= CF;
		else clear_flags |= CF;
	}

	// OVERFLOW FLAG
	// for info on OF see http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
	int val1 = sign_extend(val_dest, size);
	int val2 = sign_extend(val_source, size);
	if ((val1 >= 0) && (val2 >= 0)) {
		if (val_s < 0) set_flags |= OF;
		else clear_flags |= OF;
	}
	else if ((val1 < 0) && (val2 < 0)) {
		if (val_s >= 0) set_flags |= OF;
		else clear_flags |= OF;
	}
	else clear_flags |= OF;

	// PARITY FLAG
	if (checkparity(val)) set_flags |= PF;
	else clear_flags |= PF;

	// SIGN FLAG
	if (val_s < 0) set_flags |= SF;
	else clear_flags |= SF;

	// ZERO FLAG
	if (val_s == 0) set_flags |= ZF;
	else clear_flags |= ZF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void sub(struct operand *dest, struct operand *source, enum datasize size) {

	// subtract the value of source from dest
	// if the source size is smaller than dest, it is sign extended
	// either may be a register or memory reference, or
	// source may be a register, memory reference, or an immediate value

	unsigned int val_source = get_value(source, size);
	unsigned int val_dest = get_value(dest, size);

	unsigned int val = val_dest - val_source;
	set_value(dest, val, size);

	// signed representation of the result; used for setting flags
	int val_s = sign_extend(val, size);

	// SET FLAGS REGISTER
	// Modifies: AF CF OF PF SF ZF
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// ADJUST (AKA AUXILLARY) FLAG
	// for info on AF see https://en.wikipedia.org/wiki/Half-carry_flag
	int valnibble = val_dest & 0x0F;
	valnibble -= (val_source & 0x0F);
	if (valnibble < 0)
		set_flags |= AF;
	else
		clear_flags |= AF;

	// CARRY FLAG
	if (val_source > val_dest)
		set_flags |= CF;
	else
		clear_flags |= CF;

	// OVERFLOW FLAG
	// for info on OF see http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
	int val1 = sign_extend(val_dest, size);
	int val2 = sign_extend(val_source, size);
	if ((val1 >= 0) && (val2 < 0)) {
		if (val_s < 0) set_flags |= OF;
		else clear_flags |= OF;
	}
	else if ((val1 < 0) && (val2 >= 0)) {
		if (val_s >= 0) set_flags |= OF;
		else clear_flags |= OF;
	}
	else clear_flags |= OF;

	// PARITY FLAG
	if (checkparity(val)) set_flags |= PF;
	else clear_flags |= PF;

	// SIGN FLAG
	if (val_s < 0) set_flags |= SF;
	else clear_flags |= SF;

	// ZERO FLAG
	if (val == 0) set_flags |= ZF;
	else clear_flags |= ZF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void inc(struct operand *operand, enum datasize size) {

	// add 1 to the operand

	int val_source = get_value(operand, size);

	int val = val_source + 1;
	set_value(operand, val, size);

	// signed representation of the result; used for setting flags
	int val_s = sign_extend(val, size);

	// SET FLAGS REGISTER
	// Modifies: AF OF PF SF ZF
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// ADJUST (AKA AUXILLARY) FLAG
	// for info on AF see https://en.wikipedia.org/wiki/Half-carry_flag
	int valnibble = val_source & 0x0F;
	valnibble += 1;
	if (valnibble > 0x0F)
		set_flags |= AF;
	else
		clear_flags |= AF;

	// OVERFLOW FLAG
	// for info on OF see http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
	int val1 = sign_extend(val_source, size);
	if ((val1 >= 0) && (val_s < 0)) set_flags |= OF;
	else clear_flags |= OF;

	// PARITY FLAG
	if (checkparity(val)) set_flags |= PF;
	else clear_flags |= PF;

	// SIGN FLAG
	if (val_s < 0) set_flags |= SF;
	else clear_flags |= SF;

	// ZERO FLAG
	if (val_s == 0) set_flags |= ZF;
	else clear_flags |= ZF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void dec(struct operand *operand, enum datasize size) {

	// subtract 1 from the operand

	int val_source = get_value(operand, size);

	int val = val_source - 1;
	set_value(operand, val, size);

	// signed representation of the result; used for setting flags
	int val_s = sign_extend(val, size);

	// SET FLAGS REGISTER
	// Modifies: AF OF PF SF ZF
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// ADJUST (AKA AUXILLARY) FLAG
	// for info on AF see https://en.wikipedia.org/wiki/Half-carry_flag
	int valnibble = val_source & 0x0F;
	valnibble -= 1;
	if (valnibble < 0)
		set_flags |= AF;
	else
		clear_flags |= AF;

	// OVERFLOW FLAG
	// for info on OF see http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
	int val1 = sign_extend(val_source, size);
	if ((val1 < 0) && (val_s >= 0)) set_flags |= OF;
	else clear_flags |= OF;

	// PARITY FLAG
	if (checkparity(val)) set_flags |= PF;
	else clear_flags |= PF;

	// SIGN FLAG
	if (val_s < 0) set_flags |= SF;
	else clear_flags |= SF;

	// ZERO FLAG
	if (val == 0) set_flags |= ZF;
	else clear_flags |= ZF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

// DIV (Unsigned Integer Divide)
void udiv(struct operand *operand, enum datasize size) {

	// DIV performs unsigned integer division. The explicit operand provided is the divisor; the dividend and destination operands are implicit,
	// For DIV r/m8, AX is divided by the given operand; the quotient is stored in AL and the remainder in AH
	// For DIV r/m16, DX:AX is divided by the given operand; the quotient is stored in AX and the remainder in DX
	// For DIV r/m32, EDX:EAX is divided by the given operand; the quotient is stored in EAX and the remainder in EDX

	if (size == BYTE) {
		// dividend is AX
		unsigned int dividend = getregistervalue(al_ax_eax, WORD);
		unsigned int divisor = get_value(operand, size);
		// the quotient is stored in AL and the remainder in AH
		unsigned int quotient = dividend / divisor;
		setregister(al_ax_eax, quotient, BYTE);
		unsigned int remainder = dividend % divisor;
		setregister(ah_sp_esp, remainder, BYTE);
	}
	if (size == WORD) {
		// dividend is DX:AX
		unsigned int dividend = (getregistervalue(dl_dx_edx, WORD) << 16) + getregistervalue(al_ax_eax, WORD);
		unsigned int divisor = get_value(operand, size);
		// the quotient is stored in AX and the remainder in DX
		unsigned int quotient = dividend / divisor;
		setregister(al_ax_eax, quotient, WORD);
		unsigned int remainder = dividend % divisor;
		setregister(dl_dx_edx, remainder, WORD);
	}
	if (size == DWORD) {
		// dividend is EDX:EAX
		uint64_t dividend = ((uint64_t)getregistervalue(dl_dx_edx, DWORD) << 32) + getregistervalue(al_ax_eax, DWORD);
		unsigned int divisor = get_value(operand, size);
		// the quotient is stored in EAX and the remainder in EDX
		uint64_t quotient = dividend / divisor;
		setregister(al_ax_eax, quotient, DWORD);
		uint64_t remainder = dividend % divisor;
		setregister(dl_dx_edx, remainder, DWORD);
	}

}

// IDIV (Signed Integer Divide)
void idiv(struct operand *operand, enum datasize size) {

	// IDIV performs signed integer division. The explicit operand provided is the divisor; the dividend and destination operands are implicit,
	// For IDIV r/m8, AX is divided by the given operand; the quotient is stored in AL and the remainder in AH
	// For IDIV r/m16, DX:AX is divided by the given operand; the quotient is stored in AX and the remainder in DX
	// For IDIV r/m32, EDX:EAX is divided by the given operand; the quotient is stored in EAX and the remainder in EDX

	if (size == BYTE) {
		// dividend is AX
		int dividend = getregistervalue(al_ax_eax, WORD);
		int divisor = get_value(operand, size);
		// the quotient is stored in AL and the remainder in AH
		int quotient = dividend / divisor;
		setregister(al_ax_eax, quotient, BYTE);
		int remainder = dividend % divisor;
		setregister(ah_sp_esp, remainder, BYTE);
	}
	if (size == WORD) {
		// dividend is DX:AX
		int dividend = (getregistervalue(dl_dx_edx, WORD) << 16) + getregistervalue(al_ax_eax, WORD);
		int divisor = get_value(operand, size);
		// the quotient is stored in AX and the remainder in DX
		int quotient = dividend / divisor;
		setregister(al_ax_eax, quotient, WORD);
		int remainder = dividend % divisor;
		setregister(dl_dx_edx, remainder, WORD);
	}
	if (size == DWORD) {
		// dividend is EDX:EAX
		int64_t dividend = ((uint64_t)getregistervalue(dl_dx_edx, DWORD) << 32) + getregistervalue(al_ax_eax, DWORD);
		int divisor = get_value(operand, size);
		// the quotient is stored in EAX and the remainder in EDX
		int64_t quotient = dividend / divisor;
		setregister(al_ax_eax, quotient, DWORD);
		int64_t remainder = dividend % divisor;
		setregister(dl_dx_edx, remainder, DWORD);
	}

}

// MUL (Unsigned Integer Multiply)
void mul(struct operand *operand, enum datasize size) {

	// MUL performs unsigned integer multiplication. The other operand to the multiplication, and the destination operand, are implicit,
	// For MUL r/m8, AL is multiplied by the given operand; the product is stored in AX
	// For MUL r/m16, AX is multiplied by the given operand; the product is stored in DX:AX
	// For MUL r/m32, EAX is multiplied by the given operand; the product is stored in EDX:EAX

	unsigned int val = get_value(operand, size);
	unsigned int val2 = getregistervalue(al_ax_eax, size);
	uint64_t product = val * val2;

	if (size == BYTE) {
		// the product is stored in AX
		setregister(al_ax_eax, product, WORD);
	}
	if (size == WORD) {
		// the product is stored in DX:AX
		setregister(al_ax_eax, product & 0xffff, WORD);
		product = product >> 16;
		setregister(dl_dx_edx, product & 0xffff, WORD);
	}
	if (size == DWORD) {
		// the product is stored in EDX:EAX
		setregister(al_ax_eax, product & 0xffffffff, DWORD);
		product = product >> 32;
		setregister(dl_dx_edx, product & 0xffffffff, DWORD);
	}

	// SET FLAGS REGISTER
	// Modifies: CF OF (AF, PF SF ZF undefined)
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// ADJUST FLAG IS UNDEFINED
	clear_flags |= AF;

	// CARRY FLAG
	// The OF and CF flags are set to 0 if the upper half of the result is 0; otherwise, they are set to 1
	if (size == BYTE) {
		// 16 bit result
		if ((product & 0xff00) != 0) set_flags |= CF;
		else clear_flags |= CF;
	}
	if (size == WORD) {
		// 32 bit result
		if ((product & 0xffff0000) != 0) set_flags |= CF;
		else clear_flags |= CF;
	}
	if (size == DWORD) {
		// 64 bit result
		if ((product & 0xffffffff00000000) != 0) set_flags |= CF;
		else clear_flags |= CF;
	}

	// OVERFLOW FLAG
	// The OF and CF flags are set to 0 if the upper half of the result is 0; otherwise, they are set to 1
	if (size == BYTE) {
		// 16 bit result
		if ((product & 0xff00) != 0) set_flags |= OF;
		else clear_flags |= OF;
	}
	if (size == WORD) {
		// 32 bit result
		if ((product & 0xffff0000) != 0) set_flags |= OF;
		else clear_flags |= OF;
	}
	if (size == DWORD) {
		// 64 bit result
		if ((product & 0xffffffff00000000) != 0) set_flags |= OF;
		else clear_flags |= OF;
	}

	// PARITY FLAG IS UNDEFINED
	clear_flags |= PF;

	// SIGN FLAG IS UNDEFINED
	clear_flags |= SF;

	// ZERO FLAG IS UNDEFINED
	clear_flags |= ZF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

// IMUL (Signed Integer Multiply)
void imul(struct operand *dest, struct operand *source, enum datasize size) {

	// IMUL performs signed integer multiplication. For the single-operand form, the other operand and destination are implicit,
	// For IMUL r/m8, AL is multiplied by the given operand; the product is stored in AX
	// For IMUL r/m16, AX is multiplied by the given operand; the product is stored in DX:AX
	// For IMUL r/m32, EAX is multiplied by the given operand; the product is stored in EDX:EAX
	//
	// For the two-operand form multiplies its two operands and stores the result in the destination (first) operand
	// For the three-operand form multiplies its last two operands and stores the result in the first operand

	int val;
	int val2;
	int64_t product;

	if (source->use_imm_word == 1) {
		// three-operand form multiplies two values from source and stores the result in dest
		val = get_value(source, size);
		val2 = source->imm_word_data;
		val = sign_extend(val, size);
		val2 = sign_extend(val2, size);
		product = val * val2;
		set_value(dest, product, size);
	}
	else {
		// single-operand form
		val = get_value(source, size);
		val2 = getregistervalue(al_ax_eax, size);
		val = sign_extend(val, size);
		val2 = sign_extend(val2, size);
		product = val * val2;

		if (size == BYTE) {
			// the product is stored in AX
			setregister(al_ax_eax, product, WORD);
		}
		if (size == WORD) {
			// the product is stored in DX:AX
			setregister(al_ax_eax, product & 0xffff, WORD);
			setregister(dl_dx_edx, (product >> 16) & 0xffff, WORD);
		}
		if (size == DWORD) {
			// the product is stored in EDX:EAX
			setregister(al_ax_eax, product & 0xffffffff, DWORD);
			setregister(dl_dx_edx, (product >> 32) & 0xffffffff, DWORD);
		}
	}

	// SET FLAGS REGISTER
	// Modifies: CF OF (AF, PF SF ZF undefined)
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// ADJUST FLAG IS UNDEFINED
	clear_flags |= AF;

	// CARRY FLAG
	// The OF and CF flags are set to 0 if the result fits in the lower half of the result; otherwise, they are set to 1
	if (size == BYTE) {
		// 16 bit result - set CF if result is more than 8 bits
		if ((product < -128) || (product > 127)) set_flags |= CF;
		else clear_flags |= CF;
	}
	if (size == WORD) {
		// 32 bit result - set CF if result is more than 16 bits
		if ((product < -32768) || (product > 32767)) set_flags |= CF;
		else clear_flags |= CF;
	}
	if (size == DWORD) {
		// 64 bit result - set CF if result is more than 32 bits
		if ((product < -2147483648) || (product > 2147483647)) set_flags |= CF;
		else clear_flags |= CF;
	}

	// OVERFLOW FLAG
	// The OF and CF flags are set to 0 if the result fits in the lower half of the result; otherwise, they are set to 1
	if (size == BYTE) {
		// 16 bit result - set OF if result is more than 8 bits
		if ((product < -128) || (product > 127)) set_flags |= OF;
		else clear_flags |= OF;
	}
	if (size == WORD) {
		// 32 bit result - set OF if result is more than 16 bits
		if ((product < -32768) || (product > 32767)) set_flags |= OF;
		else clear_flags |= OF;
	}
	if (size == DWORD) {
		// 64 bit result - set OF if result is more than 32 bits
		if ((product < -2147483648) || (product > 2147483647)) set_flags |= OF;
		else clear_flags |= OF;
	}

	// PARITY FLAG IS UNDEFINED
	clear_flags |= PF;

	// SIGN FLAG IS UNDEFINED
	clear_flags |= SF;

	// ZERO FLAG IS UNDEFINED
	clear_flags |= ZF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void bitwise_and(struct operand *dest, struct operand *source, enum datasize size) {

	// logical AND of the two operands, placing the result in dest

	int val_source = get_value(source, size);
	int val_dest = get_value(dest, size);

	int val = val_source & val_dest;
	set_value(dest, val, size);

	// signed representation of the result; used for setting flags
	int val_s = sign_extend(val, size);

	// SET FLAGS REGISTER
	// Modifies: CF OF PF SF ZF (AF undefined)
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// ADJUST FLAG IS UNDEFINED
	clear_flags |= AF;

	// CARRY FLAG
	// clear the carry flag for logical operations
	clear_flags |= CF;

	// OVERFLOW FLAG
	// for info on OF see http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
	clear_flags |= OF;

	// PARITY FLAG
	if (checkparity(val)) set_flags |= PF;
	else clear_flags |= PF;

	// SIGN FLAG
	if (val_s < 0) set_flags |= SF;
	else clear_flags |= SF;

	// ZERO FLAG
	if (val == 0) set_flags |= ZF;
	else clear_flags |= ZF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void bitwise_or(struct operand *dest, struct operand *source, enum datasize size) {

	// logical OR of the two operands, placing the result in dest

	int val_source = get_value(source, size);
	int val_dest = get_value(dest, size);

	int val = val_source | val_dest;
	set_value(dest, val, size);

	// signed representation of the result; used for setting flags
	int val_s = sign_extend(val, size);

	// SET FLAGS REGISTER
	// Modifies: CF OF PF SF ZF (AF undefined)
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// ADJUST FLAG IS UNDEFINED
	clear_flags |= AF;

	// CARRY FLAG
	// clear the carry flag for logical operations
	clear_flags |= CF;

	// OVERFLOW FLAG
	// for info on OF see http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
	clear_flags |= OF;

	// PARITY FLAG
	if (checkparity(val)) set_flags |= PF;
	else clear_flags |= PF;

	// SIGN FLAG
	if (val_s < 0) set_flags |= SF;
	else clear_flags |= SF;

	// ZERO FLAG
	if (val == 0) set_flags |= ZF;
	else clear_flags |= ZF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void bitwise_xor(struct operand *dest, struct operand *source, enum datasize size) {

	// logical XOR of the two operands, placing the result in dest

	int val_source = get_value(source, size);
	int val_dest = get_value(dest, size);

	int val = val_source ^ val_dest;
	set_value(dest, val, size);

	// signed representation of the result; used for setting flags
	int val_s = sign_extend(val, size);

	// SET FLAGS REGISTER
	// Modifies: CF OF PF SF ZF (AF undefined)
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// ADJUST FLAG IS UNDEFINED
	clear_flags |= AF;

	// CARRY FLAG
	// clear the carry flag for logical operations
	clear_flags |= CF;

	// OVERFLOW FLAG
	// for info on OF see http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
	clear_flags |= OF;

	// PARITY FLAG
	if (checkparity(val)) set_flags |= PF;
	else clear_flags |= PF;

	// SIGN FLAG
	if (val_s < 0) set_flags |= SF;
	else clear_flags |= SF;

	// ZERO FLAG
	if (val == 0) set_flags |= ZF;
	else clear_flags |= ZF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void bitwise_not(struct operand *operand, enum datasize size) {

	// one's complement

	int val = get_value(operand, size);
	val = ~val;

	set_value(operand, val, size);

}

void neg(struct operand *operand, enum datasize size) {

	// negate (subtract operand from zero)

	int val_source = get_value(operand, size);
	val_source = sign_extend(val_source, size);
	int val = 0 - val_source;
	val = sign_extend(val, size);

	set_value(operand, val, size);

	// SET FLAGS REGISTER
	// Modifies: AF CF OF PF SF ZF
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// ADJUST (AKA AUXILLARY) FLAG
	// for info on AF see https://en.wikipedia.org/wiki/Half-carry_flag
	int valnibble = 0;
	valnibble = 0 - (val_source & 0x0F);
	if (valnibble < 0)
		set_flags |= AF;
	else
		clear_flags |= AF;

	// CARRY FLAG
	if (val != 0) set_flags |= CF;
	else clear_flags |= CF;

	// OVERFLOW FLAG
	// for info on OF see http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
	// Negating a byte containing -128, a word containing -32,768, or a double word containing -2,147,483,648 does not change the operand, but will set the overflow flag
	if ((val == val_source) && (val != 0)) set_flags |= OF;
	else clear_flags |= OF;

	// PARITY FLAG
	if (checkparity(val)) set_flags |= PF;
	else clear_flags |= PF;

	// SIGN FLAG
	if (val < 0) set_flags |= SF;
	else clear_flags |= SF;

	// ZERO FLAG
	if (val == 0) set_flags |= ZF;
	else clear_flags |= ZF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void test(struct operand *dest, struct operand *source, enum datasize size) {

	// Performs a logical AND of the two operands updating the flags register without saving the result

	int val = get_value(dest, size);
	val &= get_value(source, size);

	// signed representation of the result; used for setting flags
	int val_s = sign_extend(val, size);

	// SET FLAGS REGISTER
	// Modifies: CF OF PF SF ZF (AF undefined)
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// ADJUST FLAG IS UNDEFINED
	clear_flags |= AF;

	// CARRY FLAG
	// clear the carry flag for logical operations
	clear_flags |= CF;

	// OVERFLOW FLAG
	// for info on OF see http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
	clear_flags |= OF;

	// PARITY FLAG
	if (checkparity(val)) set_flags |= PF;
	else clear_flags |= PF;

	// SIGN FLAG
	if (val_s < 0) set_flags |= SF;
	else clear_flags |= SF;

	// ZERO FLAG
	if (val == 0) set_flags |= ZF;
	else clear_flags |= ZF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void shl(struct operand *dest, struct operand *source, enum datasize size) {

	// shift left, (zero shifted in on right)
	// the Carry Flag contains the last bit shifted out

	int shiftCount = get_value(source, size) & 0x1f;
	int val_dest = get_value(dest, size);

	int val = val_dest << shiftCount;
	set_value(dest, val, size);

	// signed representation of the result; used for setting flags
	int val_s = sign_extend(val, size);

	// SET FLAGS REGISTER
	// Modifies: CF OF PF SF ZF (AF undefined)
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// no flags changed if shiftCount is zero
	if (shiftCount == 0) return;

	// ADJUST FLAG IS UNDEFINED
	set_flags |= AF;

	// CARRY FLAG
	// to get the last bit shifted out, right shift the initial value by (32/16/8 bits - shiftCount)
	// then set the carry flag to the rightmost bit
	int carry = val_dest >> (size - shiftCount);
	carry &= 0x01;
	if (carry != 0) set_flags |= CF;
	else clear_flags |= CF;

	// OVERFLOW FLAG
	// for info on OF see http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
	// The overflow flag will contain one if the two H.O. bits were different prior to a single bit shift. The overflow flag is undefined if the shift count is not one.
	// Actually OF is set if the high bit's value changes
	if ((sign_extend(val_dest, size) < 0) && (val_s < 0))
		clear_flags |= OF;
	else if ((sign_extend(val_dest, size) >= 0) && (val_s >= 0))
		clear_flags |= OF;
	else
		set_flags |= OF;

	// PARITY FLAG
	if (checkparity(val)) set_flags |= PF;
	else clear_flags |= PF;

	// SIGN FLAG
	if (val_s < 0) set_flags |= SF;
	else clear_flags |= SF;

	// ZERO FLAG
	if (val_s == 0) set_flags |= ZF;
	else clear_flags |= ZF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void shr(struct operand *dest, struct operand *source, enum datasize size) {

	// shift right, (zero shifted in on left)
	// the Carry Flag contains the last bit shifted out

	int shiftCount = get_value(source, size) & 0x1f;
	int val_dest = get_value(dest, size);

	int val = val_dest >> shiftCount;
	set_value(dest, val, size);

	// signed representation of the result; used for setting flags
	int val_s = sign_extend(val, size);

	// SET FLAGS REGISTER
	// Modifies: CF OF PF SF ZF (AF undefined)
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// no flags changed if shiftCount is zero
	if (shiftCount == 0) return;

	// ADJUST FLAG IS UNDEFINED
	set_flags |= AF;

	// CARRY FLAG
	// to get the last bit shifted out, right shift the initial value by (shiftCount - 1)
	// then set the carry flag to the rightmost bit
	int carry = val_dest >> (shiftCount - 1);
	carry &= 0x01;
	if (carry != 0) set_flags |= CF;
	else clear_flags |= CF;

	// OVERFLOW FLAG
	// for info on OF see http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
	// If the shift count is one, the overflow flag will contain the value of the H.O. bit of the operand prior to the shift (i.e., this instruction sets the overflow flag if the sign changes)
	int overflow = 0;
	if (shiftCount == 1) {
		overflow = val_dest >> (size - 1);
		overflow &= 0x01;
	}
	if (overflow != 0) set_flags |= OF;
	else clear_flags |= OF;

	// PARITY FLAG
	if (checkparity(val)) set_flags |= PF;
	else clear_flags |= PF;

	// SIGN FLAG (should always be clear)
	if (val_s < 0) set_flags |= SF;
	else clear_flags |= SF;

	// ZERO FLAG
	if (val_s == 0) set_flags |= ZF;
	else clear_flags |= ZF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void rol(struct operand *dest, struct operand *source, enum datasize size) {

	// bitwise rotation
	// for example, in the operation ROL AL,1, an 8-bit rotation is performed in which AL is shifted left by 1
	// and the original top bit of AL moves round into the low bit
	// The number of bits to rotate by is given by the second operand

	unsigned int val = get_value(dest, size);

	unsigned int shiftCount = get_value(source, size) & 0x1f;

	int carry = 0;

	int count = shiftCount;

	while (count > 0) {

		// leftmost bit goes into carry
		carry = (val >> (size - 1)) & 0x01;
		// left shift
		val = (val << 1)|(val >> (size - 1));

		count--;

	}

	set_value(dest, val, size);

	// SET FLAGS REGISTER
	// Modifies: CF OF
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// no flags changed if count is zero
	if (shiftCount == 0) return;

	// CARRY FLAG
	// the CF flag receives a copy of the bit that was shifted from one end to the other
	if (carry != 0) set_flags |= CF;
	else clear_flags |= CF;

	// OVERFLOW FLAG
	// For left rotates, the OF flag is set to the exclusive OR of the CF bit (after the rotate) and the most-significant bit of the result.
	// For right rotates, the OF flag is set to the exclusive OR of the two most-significant bits of the result.
	// carry XOR (val >> (size - 1))
	int tmp1 = (val >> (size - 1)) & 0x01;
	if (carry ^ tmp1) set_flags |= OF;
	else clear_flags |= OF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void ror(struct operand *dest, struct operand *source, enum datasize size) {

	// bitwise rotation
	// for example, in the operation ROL AL,1, an 8-bit rotation is performed in which AL is shifted left by 1
	// and the original top bit of AL moves round into the low bit
	// The number of bits to rotate by is given by the second operand

	unsigned int val = get_value(dest, size);

	unsigned int shiftCount = get_value(source, size) & 0x1f;

	int carry = 0;

	int count = shiftCount;

	while (count > 0) {

		// rightmost bit goes into carry
		carry = val & 0x01;
		// right shift
		val = (val >> 1)|(val << (size - 1));

		count--;

	}

	set_value(dest, val, size);

	// SET FLAGS REGISTER
	// Modifies: CF OF
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// no flags changed if count is zero
	if (shiftCount == 0) return;

	// CARRY FLAG
	// the CF flag receives a copy of the bit that was shifted from one end to the other
	if (carry != 0) set_flags |= CF;
	else clear_flags |= CF;

	// OVERFLOW FLAG
	// for left rotates, the OF flag is set to the exclusive OR of the CF bit (after the rotate) and the most-significant bit of the result.
	// for right rotates, the OF flag is set to the exclusive OR of the two most-significant bits of the result.
	// (val >> (size - 1)) XOR (val >> (size - 2))
	int tmp1 = (val >> (size - 1)) & 0x01;
	int tmp2 = (val >> (size - 2)) & 0x01;
	if (tmp1 ^ tmp2) set_flags |= OF;
	else clear_flags |= OF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void rcl(struct operand *dest, struct operand *source, enum datasize size) {

	// 9-bit, 17-bit or 33-bit bitwise rotation operation, involving the given first operand and the carry bit
	// for example, in the operation RCL AL,1, a 9-bit rotation is performed in which AL is shifted left by 1,
	// the top bit of AL moves into the carry flag, and the original value of the carry flag is placed in the low bit of AL
	// The number of bits to rotate by is given by the second operand

	unsigned int val = get_value(dest, size);

	unsigned int shiftCount = get_value(source, size) & 0x1f;

	if (size == WORD) shiftCount %= 17;
	if (size == BYTE) shiftCount %= 9;

	if (shiftCount == 0) return;

	int carry = 0;
	int prv_carry = 0;
	if ((flags & CF) == CF)
		prv_carry = 1;

	int count = shiftCount;

	while (count > 0) {

		// leftmost bit goes into carry
		carry = (val >> (size - 1)) & 0x01;
		// left shift; carry goes into rightmost bit
		val = (val << 1) | prv_carry;
		prv_carry = carry;

		count--;

	}

	set_value(dest, val, size);

	// SET FLAGS REGISTER
	// Modifies: CF OF
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// CARRY FLAG
	if (carry != 0) set_flags |= CF;
	else clear_flags |= CF;

	// OVERFLOW FLAG
	// for left rotates, the OF flag is set to the exclusive OR of the CF bit (after the rotate) and the most-significant bit of the result.
	// for right rotates, the OF flag is set to the exclusive OR of the two most-significant bits of the result.
	// carry XOR (val >> (size - 1))
	int tmp1 = (val >> (size - 1)) & 0x01;
	if (carry ^ tmp1) set_flags |= OF;
	else clear_flags |= OF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void rcr(struct operand *dest, struct operand *source, enum datasize size) {

	// 9-bit, 17-bit or 33-bit bitwise rotation operation, involving the given first operand and the carry bit
	// for example, in the operation RCL AL,1, a 9-bit rotation is performed in which AL is shifted left by 1,
	// the top bit of AL moves into the carry flag, and the original value of the carry flag is placed in the low bit of AL
	// The number of bits to rotate by is given by the second operand

	unsigned int val = get_value(dest, size);

	unsigned int shiftCount = get_value(source, size) & 0x1f;

	if (size == WORD) shiftCount %= 17;
	if (size == BYTE) shiftCount %= 9;

	if (shiftCount == 0) return;

	int carry = 0;
	int prv_carry = 0;
	if ((flags & CF) == CF)
		prv_carry = 1;

	int count = shiftCount;

	while (count > 0) {

		// rightmost bit goes into carry
		carry = val & 0x01;
		// right shift; carry goes into leftmost bit
		val = (val >> 1) | (prv_carry << (size - 1));
		prv_carry = carry;

		count--;

	}

	set_value(dest, val, size);

	// SET FLAGS REGISTER
	// Modifies: CF OF
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// CARRY FLAG
	if (carry != 0) set_flags |= CF;
	else clear_flags |= CF;

	// OVERFLOW FLAG
	// for left rotates, the OF flag is set to the exclusive OR of the CF bit (after the rotate) and the most-significant bit of the result.
	// for right rotates, the OF flag is set to the exclusive OR of the two most-significant bits of the result.
	// (val >> (size - 1)) XOR (val >> (size - 2))
	int tmp1 = (val >> (size - 1)) & 0x01;
	int tmp2 = (val >> (size - 2)) & 0x01;
	if (tmp1 ^ tmp2) set_flags |= OF;
	else clear_flags |= OF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

void jmp(struct operand *operand, enum datasize size) {

	// short jump
	// destination address is relative to the starting address of the following instruction
	// ie. current IP + len(current opcode) + offset

	// todo: determine the mechanics of jump instructions
	int val = sign_extend(get_value(operand, size), size);

	reg_ip = reg_ip + val;

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

void jmp_far(struct operand *offset, struct operand *segment, enum datasize size) {

	// far jump

	reg_cs = get_value(segment, size);
	reg_ip = get_value(offset, size);

}

// JE/JZ - Jump Equal / Jump Zero
void jz(struct operand *operand, enum datasize size) {

	if ((flags & ZF) == ZF) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JNE/JNZ - Jump Not Equal / Jump Not Zero
void jnz(struct operand *operand, enum datasize size) {

	if ((flags & ZF) == 0) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JC - Jump on Carry
// also refferred to as JB and JNAE
void jc(struct operand *operand, enum datasize size) {

	if ((flags & CF) == CF) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JNC - Jump Not Carry
// also refferred to as JAE or JNB
void jnc(struct operand *operand, enum datasize size) {

	if ((flags & CF) == 0) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JS - Jump Signed
void js(struct operand *operand, enum datasize size) {

	if ((flags & SF) == SF) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JNS - Jump Not Signed
void jns(struct operand *operand, enum datasize size) {

	if ((flags & SF) == 0) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JO - Jump on Overflow
void jo(struct operand *operand, enum datasize size) {

	if ((flags & OF) == OF) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JNO - Jump Not Overflow
void jno(struct operand *operand, enum datasize size) {

	if ((flags & OF) == 0) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JA/JNBE - Jump Above / Jump Not Below or Equal
void ja(struct operand *operand, enum datasize size) {

	// jump if the Carry Flag and Zero Flag are both clear
	if (((flags & CF) == 0) && ((flags & ZF) == 0)) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JBE/JNA - Jump Below or Equal / Jump Not Above
void jna(struct operand *operand, enum datasize size) {

	// jump if the Carry Flag or the Zero Flag is set
	if (((flags & CF) == CF) || ((flags & ZF) == ZF)) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JGE/JNL - Jump Greater or Equal / Jump Not Less
void jge(struct operand *operand, enum datasize size) {

	// jump if the Sign Flag equals the Overflow Flag
	int sign = 0;
	if ((flags & SF) == SF) sign = 1;
	int overflow = 0;
	if ((flags & OF) == OF) overflow = 1;

	if (sign == overflow) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JG/JNLE - Jump Greater / Jump Not Less or Equal
void jg(struct operand *operand, enum datasize size) {

	// jump if the Zero Flag is clear or the Sign Flag equals the Overflow Flag
	int sign = 0;
	if ((flags & SF) == SF) sign = 1;
	int overflow = 0;
	if ((flags & OF) == OF) overflow = 1;

	if ((sign == overflow) ||((flags & ZF) == 0)) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JL/JNGE - Jump Less / Jump Not Greater or Equal
void jl(struct operand *operand, enum datasize size) {

	// jump if the Sign Flag is not equal to Overflow Flag
	int sign = 0;
	if ((flags & SF) == SF) sign = 1;
	int overflow = 0;
	if ((flags & OF) == OF) overflow = 1;

	if (sign != overflow) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JLE/JNG - Jump Less or Equal / Jump Not Greater
void jle(struct operand *operand, enum datasize size) {

	// jump if the Zero Flag is set or the Sign Flag is not equal to the Overflow Flag
	int sign = 0;
	if ((flags & SF) == SF) sign = 1;
	int overflow = 0;
	if ((flags & OF) == OF) overflow = 1;

	if ((sign != overflow) || ((flags & ZF) == ZF)) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JP/JPE - Jump on Parity / Jump on Parity Even
void jp(struct operand *operand, enum datasize size) {

	if ((flags & PF) == PF) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JNP/JPO - Jump Not Parity / Jump Parity Odd
void jnp(struct operand *operand, enum datasize size) {

	if ((flags & PF) == 0) {
		int val = sign_extend(get_value(operand, size), size);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

// JCXZ - Jump if Register CX/ECX is Zero
void jcxz(struct operand *operand, enum datasize datasize, enum datasize address_size) {

	if (getregistervalue(cl_cx_ecx, address_size) == 0) {
		int val = sign_extend(get_value(operand, datasize), datasize);
		reg_ip = reg_ip + val;
	}

	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) reg_ip = reg_ip & 0xffff;

}

void cmp(struct operand *dest, struct operand *source, enum datasize size) {

	// subtract source from dest and sets the flags but discards the result

	int val_source = sign_extend(get_value(source, size), size);
	int val_dest = sign_extend(get_value(dest, size), size);

	int val = val_dest - val_source;

	// signed representation of the result; used for setting flags
	int val_s = sign_extend(val, size);

	// SET FLAGS REGISTER
	// Modifies: AF CF OF PF SF ZF
	// bitfield variable for setting the flags
	int set_flags = 0;
	int clear_flags = 0;

	// ADJUST (AKA AUXILLARY) FLAG
	// for info on AF see https://en.wikipedia.org/wiki/Half-carry_flag
	/* BOCHS */
	int valnibble = val_dest & 0x0F;
	valnibble -= (val_source & 0x0F);
	if (valnibble < 0)
		set_flags |= AF;
	else
		clear_flags |= AF;

	// CARRY FLAG
	if (size == DWORD) {
		if ((uint32_t)val_source > (uint32_t)val_dest)
			set_flags |= CF;
		else
			clear_flags |= CF;
	}
	if (size == WORD) {
		if ((uint16_t)val_source > (uint16_t)val_dest)
			set_flags |= CF;
		else
			clear_flags |= CF;
	}
	if (size == BYTE) {
		if ((uint8_t)val_source > (uint8_t)val_dest)
			set_flags |= CF;
		else
			clear_flags |= CF;
	}

	// OVERFLOW FLAG
	// for info on OF see http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
	int val1 = sign_extend(val_dest, size);
	int val2 = sign_extend(val_source, size);
	if ((val1 >= 0) && (val2 < 0)) {
		if (val_s < 0) set_flags |= OF;
		else clear_flags |= OF;
	}
	else if ((val1 < 0) && (val2 >= 0)) {
		if (val_s >= 0) set_flags |= OF;
		else clear_flags |= OF;
	}
	else clear_flags |= OF;

	// PARITY FLAG
	if (checkparity(val)) set_flags |= PF;
	else clear_flags |= PF;

	// SIGN FLAG
	if (val_s < 0) set_flags |= SF;
	else clear_flags |= SF;

	// ZERO FLAG
	if (val == 0) set_flags |= ZF;
	else clear_flags |= ZF;

	// apply the bitfield variable to the flags
	if (set_flags != 0) flags |= set_flags;
	if (clear_flags != 0) flags &= (~clear_flags);

}

// complement carry flag
void compl_c() {
	if ((flags & CF) == CF)
		flags &= (~CF);
	else
		flags |= CF;
}

// clear carry flag
void clear_c() {
	flags &= (~CF);
}

// set carry flag
void set_c() {
	flags |= CF;
}

// clear direction flag
void clear_d() {
	flags &= (~DF);
}

// set direction flag
void set_d() {
	flags |= DF;
}

// clear interrupt flag
void clear_i() {
	flags &= (~IF);
}

// set interrupt flag
void set_i() {
	flags |= IF;
}

// MOVSB, MOVSW, MOVSD: Move String
void movs(struct operand *dest, struct operand *source, enum datasize size) {

	// MOVSB copies the byte at [DS:SI] or [DS:ESI] to [ES:DI] or [ES:EDI].
	// It then increments or decrements SI and DI (or ESI and EDI)
	// (direction flag: increments if the flag is clear, decrements if it is set)

	int val = get_value(source, size);

	set_value(dest, val, size);

	int step = 0;
	if (size == BYTE) step = 1;
	if (size == WORD) step = 2;
	if (size == DWORD) step = 4;

	if ((flags & DF) == DF) {
		setregister(dest->register_data, getregistervalue(dest->register_data, size) - step, size);
		setregister(source->register_data, getregistervalue(source->register_data, size) - step, size);
	}
	else {
		setregister(dest->register_data, getregistervalue(dest->register_data, size) + step, size);
		setregister(source->register_data, getregistervalue(source->register_data, size) + step, size);
	}

}

// CMPSB, CMPSW, CMPSD: Compare Strings
void cmps(struct operand *dest, struct operand *source, enum datasize size) {

	// CMPSB compares the byte at [DS:SI] or [DS:ESI] with the byte at [ES:DI] or [ES:EDI], and sets the flags accordingly.
	// It then increments or decrements SI and DI (or ESI and EDI)
	// (direction flag: increments if the flag is clear, decrements if it is set)

	// call existing cmp function to do the work
	// for some reason, parameters are reversed for string operation
	cmp(source, dest, size);

	int step = 0;
	if (size == BYTE) step = 1;
	if (size == WORD) step = 2;
	if (size == DWORD) step = 4;

	if ((flags & DF) == DF) {
		setregister(dest->register_data, getregistervalue(dest->register_data, size) - step, size);
		setregister(source->register_data, getregistervalue(source->register_data, size) - step, size);
	}
	else {
		setregister(dest->register_data, getregistervalue(dest->register_data, size) + step, size);
		setregister(source->register_data, getregistervalue(source->register_data, size) + step, size);
	}

}

// STOSB, STOSW, STOSD: Store Byte to String
void stos(struct operand *dest, struct operand *source, enum datasize size) {

	// STOSB stores the byte in AL at [ES:DI] or [ES:EDI].
	// It then increments or decrements DI (or EDI)
	// (direction flag: increments if the flag is clear, decrements if it is set)

	int val = get_value(source, size);

	set_value(dest, val, size);

	int step = 0;
	if (size == BYTE) step = 1;
	if (size == WORD) step = 2;
	if (size == DWORD) step = 4;

	if ((flags & DF) == DF)
		setregister(dest->register_data, getregistervalue(dest->register_data, size) - step, size);
	else
		setregister(dest->register_data, getregistervalue(dest->register_data, size) + step, size);

}

// LODSB, LODSW, LODSD: Load from String
void lods(struct operand *dest, struct operand *source, enum datasize size) {

	// LODSB loads a byte from [DS:SI] or [DS:ESI] into AL.
	// It then increments or decrements SI or ESI.
	// (direction flag: increments if the flag is clear, decrements if it is set)

	int val = get_value(source, size);

	set_value(dest, val, size);

	int step = 0;
	if (size == BYTE) step = 1;
	if (size == WORD) step = 2;
	if (size == DWORD) step = 4;

	if ((flags & DF) == DF)
		setregister(source->register_data, getregistervalue(source->register_data, size) - step, size);
	else
		setregister(source->register_data, getregistervalue(source->register_data, size) + step, size);

}

// SCASB, SCASW, SCASD: Scan String
void scas(struct operand *dest, struct operand *source, enum datasize size) {

	// SCASB compares the byte in AL with the byte at [ES:DI] or [ES:EDI], and sets the flags accordingly.
	// It then increments or decrements DI (or EDI)
	// (depending on the direction flag: increments if the flag is clear, decrements if it is set)

	// call existing cmp function to do the work
	// for some reason, parameters are reversed for string operation
	cmp(source, dest, size);

	int step = 0;
	if (size == BYTE) step = 1;
	if (size == WORD) step = 2;
	if (size == DWORD) step = 4;

	if ((flags & DF) == DF)
		setregister(dest->register_data, getregistervalue(dest->register_data, size) - step, size);
	else
		setregister(dest->register_data, getregistervalue(dest->register_data, size) + step, size);

}

// REP MOVSB, REP MOVSW, REP MOVSD: Move String; repeat while (E)CX != 0
void rep_movs(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size) {

	int reps = getregistervalue(cl_cx_ecx, address_size);
	tick_count += reps - 1;

	for (int i=0; i < reps; i++) {
		movs(dest, source, datasize);
	}

	setregister(cl_cx_ecx, 0, address_size);

}

// REPNE CMPSB, REPNE CMPSW, REPNE CMPSD: Compare Strings while not equal
void repne_cmps(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size) {

	int reps = getregistervalue(cl_cx_ecx, address_size);
	tick_count += reps - 1;

	for (int i = reps; i > 0; i--) {
		cmps(dest, source, datasize);
		setregister(cl_cx_ecx, i, address_size);
		if ((flags & ZF) == ZF) break;
	}

}

// REPE CMPSB, REPE CMPSW, REPE CMPSD: Compare Strings while equal
void repe_cmps(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size) {

	int reps = getregistervalue(cl_cx_ecx, address_size);
	tick_count += reps - 1;

	for (int i = reps; i > 0; i--) {
		cmps(dest, source, datasize);
		setregister(cl_cx_ecx, i, address_size);
		if ((flags & ZF) == 0) break;
	}

}

// REP STOSB, REP STOSW, REP STOSD: Store Byte to String; repeat while (E)CX != 0
void rep_stos(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size) {

	int reps = getregistervalue(cl_cx_ecx, address_size);
	tick_count += reps - 1;

	for (int i=0; i < reps; i++) {
		stos(dest, source, datasize);
	}

	setregister(cl_cx_ecx, 0, address_size);

}

// REP LODSB, REP LODSW, REP LODSD: Load from String; repeat while (E)CX != 0
void rep_lods(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size) {

	int reps = getregistervalue(cl_cx_ecx, address_size);
	tick_count += reps - 1;

	for (int i=0; i < reps; i++) {
		lods(dest, source, datasize);
	}

	setregister(cl_cx_ecx, 0, address_size);

}

// REPNE SCASB, REPNE SCASW, REPNE SCASD: Scan String while not equal
void repne_scas(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size) {

	int reps = getregistervalue(cl_cx_ecx, address_size);
	tick_count += reps - 1;

	for (int i = reps; i > 0; i--) {
		scas(dest, source, datasize);
		setregister(cl_cx_ecx, i, address_size);
		if ((flags & ZF) == ZF) break;
	}

}

// REPE SCASB, REPE SCASW, REPE SCASD: Scan String while equal
void repe_scas(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size) {

	int reps = getregistervalue(cl_cx_ecx, address_size);
	tick_count += reps - 1;

	for (int i = reps; i > 0; i--) {
		scas(dest, source, datasize);
		setregister(cl_cx_ecx, i, address_size);
		if ((flags & ZF) == 0) break;
	}

}

void call(struct operand *operand, enum datasize size) {

	// near call, push IP and jump to destination address
	// destination address is relative to the starting address of the following instruction
	// ie. current IP + len(current opcode) + offset

	// push IP
	struct operand register_ip;
	memset(&register_ip, 0, sizeof(struct operand));
	register_ip.use_register = 1;
	register_ip.register_size = size;
	register_ip.register_data = ip;
	push(&register_ip, size);

	int val = get_value(operand, size);

	reg_ip = reg_ip + val;

	/* BOCHS */
	// make sure 16 bit mode gives a 16 bit address
	if (mode32 == 0) {
		reg_ip = reg_ip & 0xffff;
	}

}

void call_far(struct operand *offset, struct operand *segment, enum datasize size) {

	// far call, push CS, IP and jump to destination address

	// push CS
	struct operand register_cs;
	memset(&register_cs, 0, sizeof(struct operand));
	register_cs.use_register = 1;
	register_cs.register_size = WORD;
	register_cs.register_data = seg_cs;
	push(&register_cs, WORD);

	// push IP
	struct operand register_ip;
	memset(&register_ip, 0, sizeof(struct operand));
	register_ip.use_register = 1;
	register_ip.register_size = size;
	register_ip.register_data = ip;
	push(&register_ip, size);

	reg_cs = get_value(segment, size);
	reg_ip = get_value(offset, size);

}

void ret(struct operand *operand, enum datasize size) {

	// return, pop IP and transfer control to the new address
	// if imm_word_data is present, increment the stack pointer by this amount

	// pop IP
	struct operand register_ip;
	memset(&register_ip, 0, sizeof(struct operand));
	register_ip.use_register = 1;
	register_ip.register_size = size;
	register_ip.register_data = ip;
	pop(&register_ip, size);

	// increment the stack pointer
	int val = getregistervalue(ah_sp_esp, size);
	setregister(ah_sp_esp, val + operand->imm_word_data, size);

}

void retf(struct operand *operand, enum datasize size) {

	// far return, pop IP, CS and transfer control to the new address
	// if imm_word_data is present, increment the stack pointer by this amount

	// pop IP
	struct operand register_ip;
	memset(&register_ip, 0, sizeof(struct operand));
	register_ip.use_register = 1;
	register_ip.register_size = size;
	register_ip.register_data = ip;
	pop(&register_ip, size);

	// pop CS
	struct operand register_cs;
	memset(&register_cs, 0, sizeof(struct operand));
	register_cs.use_register = 1;
	register_cs.register_size = WORD;
	register_cs.register_data = seg_cs;
	pop(&register_cs, WORD);

	// increment the stack pointer
	int val = getregistervalue(ah_sp_esp, size);
	setregister(ah_sp_esp, val + operand->imm_word_data, size);

}

// software interrupt
void interrupt(struct operand *operand, enum datasize size) {

	// software interrupt:
	// push flags
	// clear the Trap and Interrupt Flags
	// push CS then push IP
	// load CS:IP with the value found in the interrupt vector table

	// push flags
	pushf(size);

	// clear trap & interrupt flags
	flags &= (~TF);
	flags &= (~IF);

	// push CS
	struct operand register_cs;
	memset(&register_cs, 0, sizeof(struct operand));
	register_cs.use_register = 1;
	register_cs.register_size = WORD;
	register_cs.register_data = seg_cs;
	push(&register_cs, WORD);

	// push IP
	struct operand register_ip;
	memset(&register_ip, 0, sizeof(struct operand));
	register_ip.use_register = 1;
	register_ip.register_size = size;
	register_ip.register_data = ip;
	push(&register_ip, size);

	// load CS:IP
	// each interrupt vector is 4 bytes, starting at segment 0000:0000
	int offset = operand->imm_word_data * 4;
	setregister(ip, get_mem(offset, WORD), size);
	setregister(seg_cs, get_mem(offset + 2, WORD), WORD);

}

// interrupt return
void iret(enum datasize size) {

	// return from interrupt by popping IP, CS and Flags from the stack

	// pop IP
	struct operand register_ip;
	memset(&register_ip, 0, sizeof(struct operand));
	register_ip.use_register = 1;
	register_ip.register_size = size;
	register_ip.register_data = ip;
	pop(&register_ip, size);

	// pop CS
	struct operand register_cs;
	memset(&register_cs, 0, sizeof(struct operand));
	register_cs.use_register = 1;
	register_cs.register_size = WORD;
	register_cs.register_data = seg_cs;
	pop(&register_cs, WORD);

	// pop flags
	popf(size);

}

void in(struct operand *dest, struct operand *source, enum datasize size) {

	// read a value from the port specified by source, into dest
	int port = get_value(source, WORD);

	if (read_from_port[port]) {
		int val = read_from_port[port](port, size);
		if (size == DWORD) zlogf("[t=%I64d] read DWORD from port: 0x%x value: 0x%08x\n", tick_count, port, (uint32_t)val);
		if (size == WORD) zlogf("[t=%I64d] read WORD from port: 0x%x value: 0x%04x\n", tick_count, port, (uint16_t)val);
		if (size == BYTE) zlogf("[t=%I64d] read BYTE from port: 0x%x value: 0x%02x\n", tick_count, port, (uint8_t)val);
		set_value(dest, val, size);
	}
	else {
		zlogf("[t=%I64d] attempt to read from unknown port: 0x%x\n", tick_count, port);
	}

}

void out(struct operand *dest, struct operand *source, enum datasize size) {

	// write the value of source into port specified by dest
	int port = get_value(dest, WORD);

	if (write_to_port[port]) {
		int val = get_value(source, size);
		if (size == DWORD) zlogf("[t=%I64d] write DWORD to port: 0x%x value: 0x%08x\n", tick_count, port, (uint32_t)val);
		if (size == WORD) zlogf("[t=%I64d] write WORD to port: 0x%x value: 0x%04x\n", tick_count, port, (uint16_t)val);
		if (size == BYTE) zlogf("[t=%I64d] write BYTE to port: 0x%x value: 0x%02x\n", tick_count, port, (uint8_t)val);
		write_to_port[port](port, val, size);
	}
	else {
		zlogf("[t=%I64d] attempt to write to unknown port: 0x%x\n", tick_count, port);
	}

}

void sahf() {

	// SAHF sets the low byte of the flags word according to the contents of the AH register
	// AH --> SF:ZF:0:AF:0:PF:1:CF

	int val = getregistervalue(ah_sp_esp, BYTE);

	// set low order byte of flags to ff
	// then clear bits based on AH register
	flags |= 0x00ff;

	val |= 0xff00;	// set high order byte of val to ff
	flags &= val;	// and flags with val to clear bits set to 0 in AH

	// set bit #1
	flags |= 0x02;

	// clear bits #3 and #5
	flags &= (~0x28);

}

void lahf() {

	// LAHF sets the AH register according to the contents of the low byte of the flags word
	// AH <-- SF:ZF:0:AF:0:PF:1:CF

	int val = flags & 0x00ff;

	// set bit #1
	val |= 0x02;

	// clear bits #3 and #5
	val &= (~0x28);

	setregister(ah_sp_esp, val, BYTE);

}

void cbw() {

	// CBW extends AL into AX by repeating the top bit of AL in every bit of AH

	int val = getregistervalue(al_ax_eax, BYTE);

	setregister(al_ax_eax, val, WORD);

}

void cwd() {

	// CWD extends AX into DX:AX by repeating the top bit of AX throughout DX

	int val = getregistervalue(al_ax_eax, WORD);

	if (val < 0)
		setregister(dl_dx_edx, -1, WORD);
	else
		setregister(dl_dx_edx, 0, WORD);

}

void cwde() {

	// CWDE extends AX into EAX by repeating the top bit of AX into EAX

	int val = getregistervalue(al_ax_eax, WORD);

	setregister(al_ax_eax, val, DWORD);

}

void cdq() {

	// CDQ extends EAX into EDX:EAX by repeating the top bit of EAX throughout EDX

	int val = getregistervalue(al_ax_eax, DWORD);

	if (val < 0)
		setregister(dl_dx_edx, -1, DWORD);
	else
		setregister(dl_dx_edx, 0, DWORD);

}

void movsx(struct operand *dest, struct operand *source, enum datasize size) {

	// copy source to dest
	// source is sign extended from size to match dest before being copied

	int val = get_value(source, size);

	set_value(dest, val, dest->register_size);

}

void movzx(struct operand *dest, struct operand *source, enum datasize size) {

	// copy source to dest
	// source is zero extended from size to match dest before being copied

	uint32_t val = get_value(source, size);

	set_value(dest, val, dest->register_size);

}

void loop(struct operand *operand, enum datasize datasize, enum datasize address_size) {

	// decrement either CX or ECX by one, and jump if the E/CX is not zero

	int val = getregistervalue(cl_cx_ecx, address_size) - 1;
	setregister(cl_cx_ecx, val, address_size);

	if (val != 0) {

		int offset = sign_extend(get_value(operand, datasize), datasize);
		reg_ip = reg_ip + offset;

		// make sure 16 bit mode gives a 16 bit address
		if (mode32 == 0) reg_ip = reg_ip & 0xffff;

	}

}

void loope(struct operand *operand, enum datasize datasize, enum datasize address_size) {

	// decrement either CX or ECX by one; jump if the E/CX is not zero and ZF is set

	int val = getregistervalue(cl_cx_ecx, address_size) - 1;
	setregister(cl_cx_ecx, val, address_size);

	if ((val != 0) && ( (flags & ZF) == ZF )) {

		int offset = sign_extend(get_value(operand, datasize), datasize);
		reg_ip = reg_ip + offset;

		// make sure 16 bit mode gives a 16 bit address
		if (mode32 == 0) reg_ip = reg_ip & 0xffff;

	}

}

void loopne(struct operand *operand, enum datasize datasize, enum datasize address_size) {

	// decrement either CX or ECX by one; jump if the E/CX is not zero and ZF is clear

	int val = getregistervalue(cl_cx_ecx, address_size) - 1;
	setregister(cl_cx_ecx, val, address_size);

	if ((val != 0) && ( (flags & ZF) == 0 )) {

		int offset = sign_extend(get_value(operand, datasize), datasize);
		reg_ip = reg_ip + offset;

		// make sure 16 bit mode gives a 16 bit address
		if (mode32 == 0) reg_ip = reg_ip & 0xffff;

	}

}

void ins(struct operand *dest, struct operand *source, enum datasize size) {

	// INSB inputs a byte from the I/O port specified in DX and stores it at [ES:DI] or [ES:EDI].
	// It then increments or decrements DI (or EDI)
	// (direction flag: increments if the flag is clear, decrements if it is set)

	in(dest, source, size);

	int step = 0;
	if (size == BYTE) step = 1;
	if (size == WORD) step = 2;
	if (size == DWORD) step = 4;

	if ((flags & DF) == DF)
		setregister(dest->register_data, getregistervalue(dest->register_data, size) - step, size);
	else
		setregister(dest->register_data, getregistervalue(dest->register_data, size) + step, size);

}

void outs(struct operand *dest, struct operand *source, enum datasize size) {

	// OUTSB loads a byte from [DS:SI] or [DS:ESI] and writes it to the I/O port specified in DX.
	// It then increments or decrements SI or ESI.
	// (direction flag: increments if the flag is clear, decrements if it is set)

	out(dest, source, size);

	int step = 0;
	if (size == BYTE) step = 1;
	if (size == WORD) step = 2;
	if (size == DWORD) step = 4;

	if ((flags & DF) == DF)
		setregister(source->register_data, getregistervalue(source->register_data, size) - step, size);
	else
		setregister(source->register_data, getregistervalue(source->register_data, size) + step, size);

}

// REP INSB, REP INSW, REP INSD: Input String from I/O Port; repeat while (E)CX != 0
void rep_ins(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size) {

	int reps = getregistervalue(cl_cx_ecx, address_size);
	tick_count += reps - 1;

	for (int i=0; i < reps; i++) {
		ins(dest, source, datasize);
	}

	setregister(cl_cx_ecx, 0, address_size);

}

// REP OUTSB, REP OUTSW, REP OUTSD: Output String to I/O Port; repeat while (E)CX != 0
void rep_outs(struct operand *dest, struct operand *source, enum datasize datasize, enum datasize address_size) {

	int reps = getregistervalue(cl_cx_ecx, address_size);
	tick_count += reps - 1;

	for (int i=0; i < reps; i++) {
		outs(dest, source, datasize);
	}

	setregister(cl_cx_ecx, 0, address_size);

}

//
void execute(struct instruction *instr) {

	reg_ip += instr->length;

	switch (instr->op) {
	case ADD:
		add(&instr->dest, &instr->source, instr->datasize);
		break;
	case SUB:
		sub(&instr->dest, &instr->source, instr->datasize);
		break;
	case AND:
		bitwise_and(&instr->dest, &instr->source, instr->datasize);
		break;
	case OR:
		bitwise_or(&instr->dest, &instr->source, instr->datasize);
		break;
	case XOR:
		bitwise_xor(&instr->dest, &instr->source, instr->datasize);
		break;
	case TEST:
		test(&instr->dest, &instr->source, instr->datasize);
		break;
	case CMP:
		cmp(&instr->dest, &instr->source, instr->datasize);
		break;
	case MOV:
		mov(&instr->dest, &instr->source, instr->datasize);
		break;
	case LEA:
		lea(&instr->dest, &instr->source, instr->datasize);
		break;
	case LDS:
	case LES:
	case LFS:
	case LGS:
	case LSS:
		ldfptr(instr->op, &instr->dest, &instr->source, instr->datasize);
		break;
	case INC:
		inc(&instr->dest, instr->datasize);
		break;
	case DEC:
		dec(&instr->dest, instr->datasize);
		break;
	case DIV:
		udiv(&instr->dest, instr->datasize);
		break;
	case IDIV:
		idiv(&instr->dest, instr->datasize);
		break;
	case MUL:
		mul(&instr->dest, instr->datasize);
		break;
	case IMUL:
		if (instr->source.use_imm_word == 1)
			imul(&instr->dest, &instr->source, instr->datasize);
		else
			imul(NULL, &instr->dest, instr->datasize);
		break;
	case NOT:
		bitwise_not(&instr->dest, instr->datasize);
		break;
	case NEG:
		neg(&instr->dest, instr->datasize);
		break;
	case PUSH:
		push(&instr->dest, instr->datasize);
		break;
	case POP:
		pop(&instr->dest, instr->datasize);
		break;
	case SHL:
		shl(&instr->dest, &instr->source, instr->datasize);
		break;
	case SHR:
		shr(&instr->dest, &instr->source, instr->datasize);
		break;
	case ROL:
		rol(&instr->dest, &instr->source, instr->datasize);
		break;
	case ROR:
		ror(&instr->dest, &instr->source, instr->datasize);
		break;
	case RCL:
		rcl(&instr->dest, &instr->source, instr->datasize);
		break;
	case RCR:
		rcr(&instr->dest, &instr->source, instr->datasize);
		break;
	case JMP:
		jmp(&instr->dest, instr->datasize);
		break;
	case CALL:
		call(&instr->dest, instr->datasize);
		break;
	case JMP_FAR:
		jmp_far(&instr->dest, &instr->source, instr->datasize);
		break;
	case CALL_FAR:
		call_far(&instr->dest, &instr->source, instr->datasize);
		break;
	case JZ:
		jz(&instr->dest, instr->datasize);
		break;
	case JNZ:
		jnz(&instr->dest, instr->datasize);
		break;
	case JC:
		jc(&instr->dest, instr->datasize);
		break;
	case JNC:
		jnc(&instr->dest, instr->datasize);
		break;
	case JS:
		js(&instr->dest, instr->datasize);
		break;
	case JNS:
		jns(&instr->dest, instr->datasize);
		break;
	case JO:
		jo(&instr->dest, instr->datasize);
		break;
	case JNO:
		jno(&instr->dest, instr->datasize);
		break;
	case JA:
		ja(&instr->dest, instr->datasize);
		break;
	case JNA:
		jna(&instr->dest, instr->datasize);
		break;
	case JGE:
		jge(&instr->dest, instr->datasize);
		break;
	case JG:
		jg(&instr->dest, instr->datasize);
		break;
	case JL:
		jl(&instr->dest, instr->datasize);
		break;
	case JLE:
		jle(&instr->dest, instr->datasize);
		break;
	case JP:
		jp(&instr->dest, instr->datasize);
		break;
	case JNP:
		jnp(&instr->dest, instr->datasize);
		break;
	case JCXZ:
		jcxz(&instr->dest, instr->datasize, WORD_SIZE(instr->prefix.address_size));
		break;
	case JECXZ:
		jcxz(&instr->dest, instr->datasize, WORD_SIZE(instr->prefix.address_size));
		break;
	case CMC:
		compl_c();
		break;
	case CLC:
		clear_c();
		break;
	case STC:
		set_c();
		break;
	case CLI:
		clear_i();
		break;
	case STI:
		set_i();
		break;
	case CLD:
		clear_d();
		break;
	case STD:
		set_d();
		break;
	case MOVS:
		movs(&instr->dest, &instr->source, instr->datasize);
		break;
	case CMPS:
		cmps(&instr->dest, &instr->source, instr->datasize);
		break;
	case STOS:
		stos(&instr->dest, &instr->source, instr->datasize);
		break;
	case LODS:
		lods(&instr->dest, &instr->source, instr->datasize);
		break;
	case SCAS:
		scas(&instr->dest, &instr->source, instr->datasize);
		break;
	case REP_MOVS:
		rep_movs(&instr->dest, &instr->source, instr->datasize, WORD_SIZE(instr->prefix.address_size));
		break;
	case REPNE_CMPS:
		repne_cmps(&instr->dest, &instr->source, instr->datasize, WORD_SIZE(instr->prefix.address_size));
		break;
	case REPE_CMPS:
		repe_cmps(&instr->dest, &instr->source, instr->datasize, WORD_SIZE(instr->prefix.address_size));
		break;
	case REP_STOS:
		rep_stos(&instr->dest, &instr->source, instr->datasize, WORD_SIZE(instr->prefix.address_size));
		break;
	case REP_LODS:
		rep_lods(&instr->dest, &instr->source, instr->datasize, WORD_SIZE(instr->prefix.address_size));
		break;
	case REPNE_SCAS:
		repne_scas(&instr->dest, &instr->source, instr->datasize, WORD_SIZE(instr->prefix.address_size));
		break;
	case REPE_SCAS:
		repe_scas(&instr->dest, &instr->source, instr->datasize, WORD_SIZE(instr->prefix.address_size));
		break;
	case PUSHA:
		pusha(instr->datasize);
		break;
	case PUSHF:
		pushf(instr->datasize);
		break;
	case POPA:
		popa(instr->datasize);
		break;
	case POPF:
		popf(instr->datasize);
		break;
	case NOP:
		nop();
		break;
	case XCHG:
		xchg(&instr->dest, &instr->source, instr->datasize);
		break;
	case IRET:
		iret(instr->datasize);
		break;
	case RET:
		ret(&instr->dest, instr->datasize);
		break;
	case RETF:
		retf(&instr->dest, instr->datasize);
		break;
	case INT:
		interrupt(&instr->dest, instr->datasize);
		break;
	case IN:
		in(&instr->dest, &instr->source, instr->datasize);
		break;
	case OUT:
		out(&instr->dest, &instr->source, instr->datasize);
		break;
	case SAHF:
		sahf();
		break;
	case LAHF:
		lahf();
		break;
	case CBW:
		cbw();
		break;
	case CWD:
		cwd();
		break;
	case CWDE:
		cwde();
		break;
	case CDQ:
		cdq();
		break;
	case MOVSX:
		movsx(&instr->dest, &instr->source, instr->datasize);
		break;
	case MOVZX:
		movzx(&instr->dest, &instr->source, instr->datasize);
		break;
	case LOOP:
		loop(&instr->dest, instr->datasize, WORD_SIZE(instr->prefix.address_size));
		break;
	case LOOPE:
		loope(&instr->dest, instr->datasize, WORD_SIZE(instr->prefix.address_size));
		break;
	case LOOPNE:
		loopne(&instr->dest, instr->datasize, WORD_SIZE(instr->prefix.address_size));
		break;
	case INS:
		ins(&instr->dest, &instr->source, instr->datasize);
		break;
	case OUTS:
		outs(&instr->dest, &instr->source, instr->datasize);
		break;
	case REP_INS:
		rep_ins(&instr->dest, &instr->source, instr->datasize, WORD_SIZE(instr->prefix.address_size));
		break;
	case REP_OUTS:
		rep_outs(&instr->dest, &instr->source, instr->datasize, WORD_SIZE(instr->prefix.address_size));
		break;
	case NOT_IMPL:
		zlogf("[t=%I64d] [%04x:%08x] opcode not implemented: 0x%x\n", tick_count, reg_cs, reg_ip, instr->bytes[0]);
		break;
	case INVALID_OPCODE:
		zlogf("[t=%I64d] [%04x:%08x] invalid opcode: 0x%x\n", tick_count, reg_cs, reg_ip - 1, instr->bytes[0]);
		break;
	}

	tick_count++;

}

// the parity flag is set when the least significant byte of the result has an even number of (1) bits set
int checkparity(int value) {
	value = value & 0xff;
	int bits = 0;
	for (int i=0; i<8; i++) {
		if ((value % 2) == 0)
			bits++;
		value = value >> 1;
	}
	// set the parity flag (PF) if the number of (1) bits is even
	if ((bits % 2) == 0) return 1;
	return 0;
}

// sets the flags according to the value provided in flag_bitfields
// each bit set in flag_bitfields will cause the relevent bit to be set in the flags register
void setflags(int flag_bitfields) {

	flags |= flag_bitfields;

}

// clears the flags according to the value provided in flag_bitfields
// each bit set in flag_bitfields will cause the relevent bit to be clear in the flags register
void clearflags(int flag_bitfields) {

	flags &= (~flag_bitfields);

}

// return the value of memory reference, register, or imm value
// Beware: there are two places register size can be populate, instruction.datasize and operand.register_size
// indeed, when a register is used to address memory, the register size is independent to the data size
int get_value(struct operand *operand, enum datasize size) {

	if (operand->use_register) {
		return getregistervalue(operand->register_data, operand->register_size);
	}
	if (operand->use_indregister && operand->use_offset) {
		if (size == DWORD) return (uint32_t)get_mem((getregistervalue(operand->register_segment, WORD) << 4) + getregistervalue(operand->register_data, operand->register_size) + operand->offset_data, DWORD);
		if (size == WORD) return (uint16_t)get_mem((getregistervalue(operand->register_segment, WORD) << 4) + getregistervalue(operand->register_data, operand->register_size) + operand->offset_data, WORD);
		if (size == BYTE) return (uint8_t)get_mem((getregistervalue(operand->register_segment, WORD) << 4) + getregistervalue(operand->register_data, operand->register_size) + operand->offset_data, BYTE);
	}
	if (operand->use_indregister) {
		if (size == DWORD) return (uint32_t)get_mem((getregistervalue(operand->register_segment, WORD) << 4) + getregistervalue(operand->register_data, operand->register_size), DWORD);
		if (size == WORD) return (uint16_t)get_mem((getregistervalue(operand->register_segment, WORD) << 4) + getregistervalue(operand->register_data, operand->register_size), WORD);
		if (size == BYTE) return (uint8_t)get_mem((getregistervalue(operand->register_segment, WORD) << 4) + getregistervalue(operand->register_data, operand->register_size), BYTE);
	}
	if (operand->use_imm_word) {
		if (size == DWORD) return (uint32_t)operand->imm_word_data;
		if (size == WORD) return (uint16_t)operand->imm_word_data;
		if (size == BYTE) return (uint8_t)operand->imm_word_data;
	}
	if (operand->use_disponly) {
		if (size == DWORD) return (uint32_t)get_mem((getregistervalue(operand->register_segment, WORD) << 4) + operand->disponly_data, DWORD);
		if (size == WORD) return (uint16_t)get_mem((getregistervalue(operand->register_segment, WORD) << 4) + operand->disponly_data, WORD);
		if (size == BYTE) return (uint8_t)get_mem((getregistervalue(operand->register_segment, WORD) << 4) + operand->disponly_data, BYTE);
	}

	//return NULL;
	return -1;	// maybe we should return the value in a reference, so we can return an error code here?

}

// set the value of memory reference or register
void set_value(struct operand *operand, int value, enum datasize size) {

	if (operand->use_register) {
		setregister(operand->register_data, value, size);
	}
	if (operand->use_indregister && operand->use_offset) {
		set_mem((getregistervalue(operand->register_segment, WORD) << 4) + getregistervalue(operand->register_data, operand->register_size) + operand->offset_data, value, size);
	}
	else if (operand->use_indregister) {
		set_mem((getregistervalue(operand->register_segment, WORD) << 4) + getregistervalue(operand->register_data, operand->register_size), value, size);
	}
	// cannot set an immediate value!
	if (operand->use_imm_word) {
	}
	if (operand->use_disponly) {
		set_mem((getregistervalue(operand->register_segment, WORD) << 4) + operand->disponly_data, value, size);
	}

}

void *getregister(enum rm_register reg, enum datasize size) {
	switch (reg) {
		case al_ax_eax:
			if (size == DWORD) return &reg_eax.value;
			if (size == WORD) return &reg_eax.r16.value;
			if (size == BYTE) return &reg_eax.r16.r8.rl;
		break;
		case cl_cx_ecx:
			if (size == DWORD) return &reg_ecx.value;
			if (size == WORD) return &reg_ecx.r16.value;
			if (size == BYTE) return &reg_ecx.r16.r8.rl;
		break;
		case dl_dx_edx:
			if (size == DWORD) return &reg_edx.value;
			if (size == WORD) return &reg_edx.r16.value;
			if (size == BYTE) return &reg_edx.r16.r8.rl;
		break;
		case bl_bx_ebx:
			if (size == DWORD) return &reg_ebx.value;
			if (size == WORD) return &reg_ebx.r16.value;
			if (size == BYTE) return &reg_ebx.r16.r8.rl;
		break;
		case ah_sp_esp:
			if (size == DWORD) return &reg_esp.value;
			if (size == WORD) return &reg_esp.r16.value;
			if (size == BYTE) return &reg_eax.r16.r8.rh;
		break;
		case ch_bp_ebp:
			if (size == DWORD) return &reg_ebp.value;
			if (size == WORD) return &reg_ebp.r16.value;
			if (size == BYTE) return &reg_ecx.r16.r8.rh;
		break;
		case dh_si_esi:
			if (size == DWORD) return &reg_esi.value;
			if (size == WORD) return &reg_esi.r16.value;
			if (size == BYTE) return &reg_edx.r16.r8.rh;
		break;
		case bh_di_edi:
			if (size == DWORD) return &reg_edi.value;
			if (size == WORD) return &reg_edi.r16.value;
			if (size == BYTE) return &reg_ebx.r16.r8.rh;
		break;
		case seg_es:
			return &reg_es;
		break;
		case seg_cs:
			return &reg_cs;
		break;
		case seg_ss:
			return &reg_ss;
		break;
		case seg_ds:
			return &reg_ds;
		break;
		case seg_fs:
			return &reg_fs;
		break;
		case seg_gs:
			return &reg_gs;
		break;
		case rm_flags:
			return &flags;
		break;
		case ip:
			return &reg_ip;
		break;
	}
	return NULL;
}

//we could either return a uint32_t here, or cast all memory addresses to an unsigned int
int getregistervalue(enum rm_register reg, enum datasize size) {
	switch (reg) {
		case al_ax_eax:
			if (size == DWORD) return reg_eax.value;
			if (size == WORD) return (uint16_t)reg_eax.r16.value;	// cast to unsigned prevents sign extension of negative values
			if (size == BYTE) return (uint8_t)reg_eax.r16.r8.rl;	// cast to unsigned prevents sign extension of negative values
		break;
		case cl_cx_ecx:
			if (size == DWORD) return reg_ecx.value;
			if (size == WORD) return (uint16_t)reg_ecx.r16.value;
			if (size == BYTE) return (uint8_t)reg_ecx.r16.r8.rl;
		break;
		case dl_dx_edx:
			if (size == DWORD) return reg_edx.value;
			if (size == WORD) return (uint16_t)reg_edx.r16.value;
			if (size == BYTE) return (uint8_t)reg_edx.r16.r8.rl;
		break;
		case bl_bx_ebx:
			if (size == DWORD) return reg_ebx.value;
			if (size == WORD) return (uint16_t)reg_ebx.r16.value;
			if (size == BYTE) return (uint8_t)reg_ebx.r16.r8.rl;
		break;
		case ah_sp_esp:
			if (size == DWORD) return reg_esp.value;
			if (size == WORD) return (uint16_t)reg_esp.r16.value;
			if (size == BYTE) return (uint8_t)reg_eax.r16.r8.rh;
		break;
		case ch_bp_ebp:
			if (size == DWORD) return reg_ebp.value;
			if (size == WORD) return (uint16_t)reg_ebp.r16.value;
			if (size == BYTE) return (uint8_t)reg_ecx.r16.r8.rh;
		break;
		case dh_si_esi:
			if (size == DWORD) return reg_esi.value;
			if (size == WORD) return (uint16_t)reg_esi.r16.value;
			if (size == BYTE) return (uint8_t)reg_edx.r16.r8.rh;
		break;
		case bh_di_edi:
			if (size == DWORD) return reg_edi.value;
			if (size == WORD) return (uint16_t)reg_edi.r16.value;
			if (size == BYTE) return (uint8_t)reg_ebx.r16.r8.rh;
		break;
		case ind_bx_si:
			return (uint16_t)reg_ebx.r16.value + (uint16_t)reg_esi.r16.value;
		break;
		case ind_bx_di:
			return (uint16_t)reg_ebx.r16.value + (uint16_t)reg_edi.r16.value;
		break;
		case ind_bp_si:
			return (uint16_t)reg_ebp.r16.value + (uint16_t)reg_esi.r16.value;
		break;
		case ind_bp_di:
			return (uint16_t)reg_ebp.r16.value + (uint16_t)reg_edi.r16.value;
		break;
		case ind_si:
			if (size == DWORD) return reg_esi.value;
			if (size == WORD) return (uint16_t)reg_esi.r16.value;
			if (size == BYTE) return (uint8_t)reg_edx.r16.r8.rh;
		break;
		case ind_di:
			if (size == DWORD) return reg_edi.value;
			if (size == WORD) return (uint16_t)reg_edi.r16.value;
			if (size == BYTE) return (uint8_t)reg_ebx.r16.r8.rh;
		break;
		case ind_bp:
			if (size == DWORD) return reg_ebp.value;
			if (size == WORD) return (uint16_t)reg_ebp.r16.value;
			if (size == BYTE) return (uint8_t)reg_ecx.r16.r8.rh;
		break;
		case ind_bx:
			if (size == DWORD) return reg_ebx.value;
			if (size == WORD) return (uint16_t)reg_ebx.r16.value;
			if (size == BYTE) return (uint8_t)reg_ebx.r16.r8.rl;
		break;
		case seg_es:
			return reg_es;
		break;
		case seg_cs:
			return reg_cs;
		break;
		case seg_ss:
			return reg_ss;
		break;
		case seg_ds:
			return reg_ds;
		break;
		case seg_fs:
			return reg_fs;
		break;
		case seg_gs:
			return reg_gs;
		break;
		case rm_flags:
			return flags;
		break;
		case ip:
			return reg_ip;
		break;
	}
	return -1;
}

void setregister(enum rm_register reg, int value, enum datasize size) {
	switch (reg) {
		case al_ax_eax:
			if (size == DWORD) reg_eax.value = value;
			if (size == WORD) reg_eax.r16.value = (uint16_t)value;
			if (size == BYTE) reg_eax.r16.r8.rl = (uint8_t)value;
		break;
		case cl_cx_ecx:
			if (size == DWORD) reg_ecx.value = value;
			if (size == WORD) reg_ecx.r16.value = (uint16_t)value;
			if (size == BYTE) reg_ecx.r16.r8.rl = (uint8_t)value;
		break;
		case dl_dx_edx:
			if (size == DWORD) reg_edx.value = value;
			if (size == WORD) reg_edx.r16.value = (uint16_t)value;
			if (size == BYTE) reg_edx.r16.r8.rl = (uint8_t)value;
		break;
		case bl_bx_ebx:
			if (size == DWORD) reg_ebx.value = value;
			if (size == WORD) reg_ebx.r16.value = (uint16_t)value;
			if (size == BYTE) reg_ebx.r16.r8.rl = (uint8_t)value;
		break;
		case ah_sp_esp:
			if (size == DWORD) reg_esp.value = value;
			if (size == WORD) reg_esp.r16.value = (uint16_t)value;
			if (size == BYTE) reg_eax.r16.r8.rh = (uint8_t)value;
		break;
		case ch_bp_ebp:
			if (size == DWORD) reg_ebp.value = value;
			if (size == WORD) reg_ebp.r16.value = (uint16_t)value;
			if (size == BYTE) reg_ecx.r16.r8.rh = (uint8_t)value;
		break;
		case dh_si_esi:
			if (size == DWORD) reg_esi.value = value;
			if (size == WORD) reg_esi.r16.value = (uint16_t)value;
			if (size == BYTE) reg_edx.r16.r8.rh = (uint8_t)value;
		break;
		case bh_di_edi:
			if (size == DWORD) reg_edi.value = value;
			if (size == WORD) reg_edi.r16.value = (uint16_t)value;
			if (size == BYTE) reg_ebx.r16.r8.rh = (uint8_t)value;
		break;
		case seg_es:
			reg_es = value;
		break;
		case seg_cs:
			reg_cs = value;
		break;
		case seg_ss:
			reg_ss = value;
		break;
		case seg_ds:
			reg_ds = value;
		break;
		case seg_fs:
			reg_fs = value;
		break;
		case seg_gs:
			reg_gs = value;
		break;
		case rm_flags:
			flags = value;
		break;
		case ip:
			reg_ip = value;
		break;
	}
}

// sign extend value into a full int
int sign_extend(int value, enum datasize size) {

	if (size == DWORD) {
		return (int32_t)value;
	}
	if (size == WORD) {
		return (int16_t)value;
	}
	if (size == BYTE) {
		return (int8_t)value;
	}

	return (int32_t)value;

}
