#include <stdio.h>
#include <stdlib.h>
#include <string.h>		// for memset()

#include "decode.h"

/* decode a sequence of opcodes */

int decode(uint8_t *opcodes, struct instruction *instr) {

	int bytes = 0;		// return the size of the instruction in bytes

	// initialise instruction
	memset(instr, 0, sizeof(struct instruction));
	instr->op = INVALID_OPCODE;
	instr->datasize = NIL;

	// process instruction prefix bytes
	uint8_t prefix_byte;

	// todo:
	// confirm the 66 prefix is ignored if either F2 or F3 are used
	// see http://stackoverflow.com/questions/7197282/order-for-encoding-x86-instruction-prefix-bytes

	while ((prefix_byte = check_prefix(&opcodes[bytes]))) {

		switch (prefix_byte) {
		case 0x0f:
			instr->prefix.extended_opcode = 1;
		break;
		case 0x26:
		case 0x2e:
		case 0x36:
		case 0x3e:
		case 0x64:
		case 0x65:
			instr->prefix.segment_override = prefix_byte;
		break;
		case 0x66:
			instr->prefix.operand_size = 1;
		break;
		case 0x67:
			instr->prefix.address_size = 1;
		break;
		case 0xf0:
		case 0xf2:
		case 0xf3:
			instr->prefix.lock_rep = prefix_byte;
		break;
		}

		bytes++;

	}

	// MOD-REG-R/M structure:
	//
	//  76 - 543 - 210
	// Mod   Reg   R/M
	// The R/M field, combined with MOD, specifies either
	// the second operand in a two-operand instruction, or
	// the only operand in a single-operand instruction like NOT or NEG
	//
	// The d bit in the opcode determines which operand is the source, and which is the destination:
	// d=0: MOD R/M <- REG, REG is the source
	// d=1: REG <- MOD R/M, REG is the destination
	//
	int destination_bit = (opcodes[bytes] >> 1) & 0b00000001;
	int operandsize_bit = opcodes[bytes] & 0b00000001;
	struct mod_reg_rm modregrm_data;
	memset(&modregrm_data, 0, sizeof(struct mod_reg_rm));

	switch (opcodes[bytes]) {
		case 0x00:		// ADD r/m8,reg8 => 00 [mod/reg/rm] [imm8/imm32]
			instr->op = ADD;
			instr->datasize = BYTE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x01:		// ADD r/m32,reg32 => 01 [mod/reg/rm] [imm8/imm32]
			instr->op = ADD;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x02:		// ADD reg8,r/m8 => 02 [mod/reg/rm] [imm8/imm32]
			instr->op = ADD;
			instr->datasize = BYTE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x03:		// ADD reg32,r/m32 => 03 [mod/reg/rm] [imm8/imm32]
			instr->op = ADD;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x04:		// ADD AL,imm8 => 04 imm8
			instr->op = ADD;
			instr->datasize = BYTE;
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = BYTE;
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x05:		// ADD EAX,imm32 => 05 imm32
			instr->op = ADD;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0x06:		// push es => 06
			instr->op = PUSH;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = seg_es;
			instr->dest.register_size = WORD;
		break;
		case 0x07:		// pop es => 07
			instr->op = POP;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = seg_es;
			instr->dest.register_size = WORD;
		break;
		case 0x08:		// OR r/m8,reg8 => 08 [mod/reg/rm] [imm8/imm32]
			instr->op = OR;
			instr->datasize = BYTE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x09:		// OR r/m32,reg32 => 09 [mod/reg/rm] [imm8/imm32]
			instr->op = OR;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x0a:		// OR reg8,r/m8 => 0A [mod/reg/rm] [imm8/imm32]
			instr->op = OR;
			instr->datasize = BYTE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x0b:		// OR reg32,r/m32 => 0B [mod/reg/rm] [imm8/imm32]
			instr->op = OR;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x0c:		// OR AL,imm8 => 0C imm8
			instr->op = OR;
			instr->datasize = BYTE;
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = BYTE;
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x0d:		// OR EAX,imm32 => 0D imm32
			instr->op = OR;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0x0e:		// push cs => 0E
			instr->op = PUSH;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = seg_cs;
			instr->dest.register_size = WORD;
		break;
		case 0x16:		// push ss => 16
			instr->op = PUSH;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = seg_ss;
			instr->dest.register_size = WORD;
		break;
		case 0x17:		// pop ss => 17
			instr->op = POP;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = seg_ss;
			instr->dest.register_size = WORD;
		break;
		case 0x1e:		// push ds => 1e
			instr->op = PUSH;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = seg_ds;
			instr->dest.register_size = WORD;
		break;
		case 0x1f:		// pop ds => 1f
			instr->op = POP;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = seg_ds;
			instr->dest.register_size = WORD;
		break;
		case 0x20:		// AND r/m8,reg8 => 20 [mod/reg/rm] [imm8/imm32]
			instr->op = AND;
			instr->datasize = BYTE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x21:		// AND r/m32,reg32 => 21 [mod/reg/rm] [imm8/imm32]
			instr->op = AND;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x22:		// AND reg8,r/m8 => 22 [mod/reg/rm] [imm8/imm32]
			instr->op = AND;
			instr->datasize = BYTE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x23:		// AND reg32,r/m32 => 23 [mod/reg/rm] [imm8/imm32]
			instr->op = AND;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x24:		// AND AL,imm8 => 24 imm8
			instr->op = AND;
			instr->datasize = BYTE;
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = BYTE;
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x25:		// AND EAX,imm32 => 25 imm32
			instr->op = AND;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0x28:		// SUB r/m8,reg8 => 28 [mod/reg/rm] [imm8/imm32]
			instr->op = SUB;
			instr->datasize = BYTE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x29:		// SUB r/m32,reg32 => 29 [mod/reg/rm] [imm8/imm32]
			instr->op = SUB;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x2a:		// SUB reg8,r/m8 => 2A [mod/reg/rm] [imm8/imm32]
			instr->op = SUB;
			instr->datasize = BYTE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x2b:		// SUB reg32,r/m32 => 2B [mod/reg/rm] [imm8/imm32]
			instr->op = SUB;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x2c:		// SUB AL,imm8 => 2C imm8
			instr->op = SUB;
			instr->datasize = BYTE;
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = BYTE;
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x2d:		// SUB EAX,imm32 => 2D imm32
			instr->op = SUB;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0x30:		// XOR r/m8,reg8 => 30 [mod/reg/rm] [imm8/imm32]
			instr->op = XOR;
			instr->datasize = BYTE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x31:		// XOR r/m32,reg32 => 31 [mod/reg/rm] [imm8/imm32]
			instr->op = XOR;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x32:		// XOR reg8,r/m8 => 32 [mod/reg/rm] [imm8/imm32]
			instr->op = XOR;
			instr->datasize = BYTE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x33:		// XOR reg32,r/m32 => 33 [mod/reg/rm] [imm8/imm32]
			instr->op = XOR;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x34:		// XOR AL,imm8 => 34 imm8
			instr->op = XOR;
			instr->datasize = BYTE;
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = BYTE;
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x35:		// XOR EAX,imm32 => 35 imm32
			instr->op = XOR;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0x38:		// CMP r/m8,reg8 => 38 [mod/reg/rm] [imm8/imm32]
			instr->op = CMP;
			instr->datasize = BYTE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x39:		// CMP r/m32,reg32 => 39 [mod/reg/rm] [imm8/imm32]
			instr->op = CMP;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x3a:		// CMP reg8,r/m8 => 3A [mod/reg/rm] [imm8/imm32]
			instr->op = CMP;
			instr->datasize = BYTE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x3b:		// CMP reg32,r/m32 => 3B [mod/reg/rm] [imm8/imm32]
			instr->op = CMP;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
		break;
		case 0x3c:		// CMP AL,imm8 => 3C imm8
			instr->op = CMP;
			instr->datasize = BYTE;
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = BYTE;
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x3d:		// CMP EAX,imm32 => 3D imm32
			instr->op = CMP;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0x40:		// INC reg32 => 40+r
		case 0x41:		// INC reg32 => 40+r
		case 0x42:		// INC reg32 => 40+r
		case 0x43:		// INC reg32 => 40+r
		case 0x44:		// INC reg32 => 40+r
		case 0x45:		// INC reg32 => 40+r
		case 0x46:		// INC reg32 => 40+r
		case 0x47:		// INC reg32 => 40+r
			instr->op = INC;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes], &modregrm_data);
			instr->dest.use_register = 1;
			instr->dest.register_data = modregrm_data.rm;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
		break;
		case 0x48:		// DEC reg32 => 48+r
		case 0x49:		// DEC reg32 => 48+r
		case 0x4a:		// DEC reg32 => 48+r
		case 0x4b:		// DEC reg32 => 48+r
		case 0x4c:		// DEC reg32 => 48+r
		case 0x4d:		// DEC reg32 => 48+r
		case 0x4e:		// DEC reg32 => 48+r
		case 0x4f:		// DEC reg32 => 48+r
			instr->op = DEC;	// note the registers are specified incorreclty at http://www.mathemainzel.info/files/x86asmref.html#dec
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes], &modregrm_data);
			instr->dest.use_register = 1;
			instr->dest.register_data = modregrm_data.rm;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
		break;
		case 0x50:		// push reg32 => 50+r
		case 0x51:		// push reg32 => 50+r
		case 0x52:		// push reg32 => 50+r
		case 0x53:		// push reg32 => 50+r
		case 0x54:		// push reg32 => 50+r
		case 0x55:		// push reg32 => 50+r
		case 0x56:		// push reg32 => 50+r
		case 0x57:		// push reg32 => 50+r
			instr->op = PUSH;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes], &modregrm_data);
			instr->dest.use_register = 1;
			instr->dest.register_data = modregrm_data.rm;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
		break;
		case 0x58:		// pop reg32 => 58+r
		case 0x59:		// pop reg32 => 58+r
		case 0x5a:		// pop reg32 => 58+r
		case 0x5b:		// pop reg32 => 58+r
		case 0x5c:		// pop reg32 => 58+r
		case 0x5d:		// pop reg32 => 58+r
		case 0x5e:		// pop reg32 => 58+r
		case 0x5f:		// pop reg32 => 58+r
			instr->op = POP;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes], &modregrm_data);
			instr->dest.use_register = 1;
			instr->dest.register_data = modregrm_data.rm;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
		break;
		case 0x60:		// PUSHA => 60
			instr->op = PUSHA;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
		break;
		case 0x61:		// POPA => 61
			instr->op = POPA;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
		break;
		case 0x68:		// ** [186] ** push imm32 => 68 imm32
			instr->op = PUSH;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0x69:		// IMUL reg32,r/m32,imm32 => 69 [mod/reg/rm] imm32
						// IMUL reg32,imm32 => 69 [mod/reg/rm] imm32
			instr->op = IMUL;	// todo: fix imul encoding
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], 1, operandsize_bit, instr);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0x6a:		// ** [186] ** push imm8 => 6A imm8
			instr->op = PUSH;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x6b:		// IMUL reg32,r/m32,imm8 => 6B [mod/reg/rm] imm8
						// IMUL reg32,imm8 => 6B [mod/reg/rm] imm8
			instr->op = IMUL;	// todo: fix imul encoding
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x6c:		// INS(B) => 6C
			instr->op = INS;
			if (instr->prefix.lock_rep == 0xf2 || instr->prefix.lock_rep == 0xf3) instr->op = REP_INS;
			instr->datasize = BYTE;
			instr->dest.use_indregister = 1;
			instr->dest.register_segment = seg_es;
			instr->dest.register_data = bh_di_edi;
			instr->dest.register_size = WORD_SIZE(instr->prefix.address_size);
			instr->source.use_register = 1;
			instr->source.register_data = dl_dx_edx;
			instr->source.register_size = WORD;
		break;
		case 0x6d:		// INS(W/D) => 6D
			instr->op = INS;
			if (instr->prefix.lock_rep == 0xf2 || instr->prefix.lock_rep == 0xf3) instr->op = REP_INS;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_indregister = 1;
			instr->dest.register_segment = seg_es;
			instr->dest.register_data = bh_di_edi;
			instr->dest.register_size = WORD_SIZE(instr->prefix.address_size);
			instr->source.use_register = 1;
			instr->source.register_data = dl_dx_edx;
			instr->source.register_size = WORD;
		break;
		case 0x6e:		// OUTS(B) => 6E
			instr->op = OUTS;
			if (instr->prefix.lock_rep == 0xf2 || instr->prefix.lock_rep == 0xf3) instr->op = REP_OUTS;
			instr->datasize = BYTE;
			instr->dest.use_register = 1;
			instr->dest.register_data = dl_dx_edx;
			instr->dest.register_size = WORD;
			instr->source.use_indregister = 1;
			instr->source.register_segment = get_segment_register(dh_si_esi, instr->prefix.segment_override);
			instr->source.register_data = dh_si_esi;
			instr->source.register_size = WORD_SIZE(instr->prefix.address_size);
		break;
		case 0x6f:		// OUTS(W/D) => 6F
			instr->op = OUTS;
			if (instr->prefix.lock_rep == 0xf2 || instr->prefix.lock_rep == 0xf3) instr->op = REP_OUTS;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = dl_dx_edx;
			instr->dest.register_size = WORD;
			instr->source.use_indregister = 1;
			instr->source.register_segment = get_segment_register(dh_si_esi, instr->prefix.segment_override);
			instr->source.register_data = dh_si_esi;
			instr->source.register_size = WORD_SIZE(instr->prefix.address_size);
		break;
		case 0x70:		// JO imm => 70 [imm8]
			instr->op = JO;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x71:		// JNO imm => 71 [imm8]
			instr->op = JNO;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x72:		// JC imm => 72 [imm8]
			instr->op = JC;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x73:		// JNC imm => 73 [imm8]
			instr->op = JNC;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x74:		// JZ imm => 74 [imm8]
			instr->op = JZ;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x75:		// JNZ imm => 75 [imm8]
			instr->op = JNZ;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x76:		// JNA imm => 76 [imm8]
			instr->op = JNA;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x77:		// JA imm => 77 [imm8]
			instr->op = JA;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x78:		// JS imm => 78 [imm8]
			instr->op = JS;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x79:		// JNS imm => 79 [imm8]
			instr->op = JNS;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x7A:		// JP imm => 7A [imm8]
			instr->op = JP;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x7B:		// JNP imm => 7B [imm8]
			instr->op = JNP;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x7C:		// JL imm => 7C [imm8]
			instr->op = JL;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x7D:		// JGE imm => 7D [imm8]
			instr->op = JGE;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x7E:		// JLE imm => 7E [imm8]
			instr->op = JLE;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x7F:		// JG imm => 7F [imm8]
			instr->op = JG;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0x80:		// ADD r/m8,imm8 => 80 /0 [] [imm8]
			if (instr->prefix.extended_opcode != 1) {
				instr->op = INVALID_OPCODE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				if (modregrm_data.reg == 0) instr->op = ADD;
				if (modregrm_data.reg == 1) instr->op = OR;
				if (modregrm_data.reg == 2) instr->op = NOT_IMPL;
				if (modregrm_data.reg == 3) instr->op = NOT_IMPL;
				if (modregrm_data.reg == 4) instr->op = AND;
				if (modregrm_data.reg == 5) instr->op = SUB;
				if (modregrm_data.reg == 6) instr->op = XOR;
				if (modregrm_data.reg == 7) instr->op = CMP;
				// we're making an assumption that every sub-instruction uses the same encoding
				instr->datasize = BYTE;
				bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
				instr->source.use_imm_word = 1;
				instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
				bytes += 1;
			}
			else {		// JO imm => 0F 80 [imm32]
				instr->op = JO;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x81:		// ADD r/m32,imm32 => 81 /0 [] [imm32]
			if (instr->prefix.extended_opcode != 1) {
				instr->op = INVALID_OPCODE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				if (modregrm_data.reg == 0) instr->op = ADD;
				if (modregrm_data.reg == 1) instr->op = OR;
				if (modregrm_data.reg == 2) instr->op = NOT_IMPL;
				if (modregrm_data.reg == 3) instr->op = NOT_IMPL;
				if (modregrm_data.reg == 4) instr->op = AND;
				if (modregrm_data.reg == 5) instr->op = SUB;
				if (modregrm_data.reg == 6) instr->op = XOR;
				if (modregrm_data.reg == 7) instr->op = CMP;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
				instr->source.use_imm_word = 1;
				instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
			else {		// JNO imm => 0F 81 [imm32]
				instr->op = JNO;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x82:		// this is the same as 0x80, according to http://ref.x86asm.net/coder32.html#x80
			if (instr->prefix.extended_opcode != 1) {
				instr->op = INVALID_OPCODE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				if (modregrm_data.reg == 0) instr->op = INVALID_OPCODE;	// todo: confirm whether this is ADD r/m8,imm8... invalid opcode for now...
				if (modregrm_data.reg == 1) instr->op = NOT_IMPL;
				if (modregrm_data.reg == 2) instr->op = NOT_IMPL;
				if (modregrm_data.reg == 3) instr->op = NOT_IMPL;
				if (modregrm_data.reg == 4) instr->op = NOT_IMPL;
				if (modregrm_data.reg == 5) instr->op = NOT_IMPL;
				if (modregrm_data.reg == 6) instr->op = NOT_IMPL;
				if (modregrm_data.reg == 7) instr->op = NOT_IMPL;
				instr->datasize = BYTE;
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			}
			else {		// JC imm => 0F 82 [imm32]
				instr->op = JC;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x83:		// ADD r/m32,imm8 => 80 /0 [] [imm8]
			if (instr->prefix.extended_opcode != 1) {
				instr->op = INVALID_OPCODE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				if (modregrm_data.reg == 0) instr->op = ADD;
				if (modregrm_data.reg == 1) instr->op = OR;
				if (modregrm_data.reg == 2) instr->op = NOT_IMPL;
				if (modregrm_data.reg == 3) instr->op = NOT_IMPL;
				if (modregrm_data.reg == 4) instr->op = AND;
				if (modregrm_data.reg == 5) instr->op = SUB;
				if (modregrm_data.reg == 6) instr->op = XOR;
				if (modregrm_data.reg == 7) instr->op = CMP;
				// here we run into trouble; this opcode behaves as 0x81 (32 bit register), except that the value being added is 8 bit sign-extended
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], 0, operandsize_bit, instr);
				instr->source.use_imm_word = 1;
				instr->source.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
				bytes += 1;
			}
			else {		// JNC imm => 0F 83 [imm32]
				instr->op = JNC;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x84:		// TEST r/m8,reg8 => 84 [mod/reg/rm] [imm8/imm32]
			if (instr->prefix.extended_opcode != 1) {
				instr->op = TEST;
				instr->datasize = BYTE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			}
			else {		// JZ imm => 0F 84 [imm32]
				instr->op = JZ;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x85:		// TEST r/m32,reg32 => 85 [mod/reg/rm] [imm8/imm32]
			if (instr->prefix.extended_opcode != 1) {
				instr->op = TEST;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			}
			else {		// JNZ imm => 0F 85 [imm32]
				instr->op = JNZ;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x86:		// XCHG reg8,r/m8 => 86 [mod/reg/rm] [imm8/imm32]
			if (instr->prefix.extended_opcode != 1) {
				instr->op = XCHG;
				instr->datasize = BYTE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			}
			else {		// JNA imm => 0F 86 [imm32]
				instr->op = JNA;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x87:		// XCHG reg32,r/m32 => 87 [mod/reg/rm] [imm8/imm32]
			if (instr->prefix.extended_opcode != 1) {
				instr->op = XCHG;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			}
			else {		// JA imm => 0F 87 [imm32]
				instr->op = JA;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x88:		// mov m/reg8, m/reg8 => 88 [mod/reg/rm] [imm8/imm32]
			if (instr->prefix.extended_opcode != 1) {
				instr->op = MOV;
				instr->datasize = BYTE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			}
			else {		// JS imm => 0F 88 [imm32]
				instr->op = JS;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x89:		// mov m/reg32, m/reg32 => 89 [mod/reg/rm] [imm8/imm32]
			if (instr->prefix.extended_opcode != 1) {
				instr->op = MOV;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			}
			else {		// JNS imm => 0F 89 [imm32]
				instr->op = JNS;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x8A:		// mov m/reg8, m/reg8 => 8A [mod/reg/rm] [imm8/imm32]
			if (instr->prefix.extended_opcode != 1) {
				instr->op = MOV;
				instr->datasize = BYTE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			}
			else {		// JP imm => 0F 8A [imm32]
				instr->op = JP;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x8b:		// mov m/reg32, m/reg32 => 8B [mod/reg/rm] [imm8/imm32]
			if (instr->prefix.extended_opcode != 1) {
				instr->op = MOV;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			}
			else {		// JNP imm => 0F 8B [imm32]
				instr->op = JNP;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x8c:		// mov m/reg32, m/reg32 => 8C [mod/reg/rm] []
			if (instr->prefix.extended_opcode != 1) {
				// these are MOV, according to http://www.mathemainzel.info/files/x86asmref.html#mov
				// MOV     rmw,sr	8C mr d0 d1
				// MOV     sr,rmw	8E mr d0 d1
				// sr = segment register, designated by the reg field in mod/reg/rm byte
				// 000 = es     100 = fs
				// 001 = cs		101 = gs
				// 010 = ss		110 = invalid
				// 011 = ds		111 = invalid
				instr->op = MOV;
				instr->datasize = WORD;		// segment registers are always 16 bits
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				modregrm_data.reg |= 0x08;	// set bit 4 of reg to indicate a segment register
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			}
			else {		// JL imm => 0F 8C [imm32]
				instr->op = JL;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x8d:		// LEA reg32,mem => 8D [mod/reg/rm] []
			if (instr->prefix.extended_opcode != 1) {
				instr->op = INVALID_OPCODE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);	// mod/rm must specify a memory location;
				if (modregrm_data.mod == 3) break;				// register addressing mode is not allowed
				instr->op = LEA;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], 1, operandsize_bit, instr);
			}
			else {		// JGE imm => 0F 8D [imm32]
				instr->op = JGE;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x8e:		// mov m/reg32, m/reg32 => 8E [mod/reg/rm] []
			if (instr->prefix.extended_opcode != 1) {
				// these are MOV, according to http://www.mathemainzel.info/files/x86asmref.html#mov
				// MOV     rmw,sr	8C mr d0 d1
				// MOV     sr,rmw	8E mr d0 d1
				// sr = segment register, designated by the reg field in mod/reg/rm byte
				// 000 = es     100 = fs
				// 001 = cs		101 = gs
				// 010 = ss		110 = invalid
				// 011 = ds		111 = invalid
				instr->op = MOV;
				instr->datasize = WORD;		// segment registers are always 16 bits
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				modregrm_data.reg |= 0x08;	// set bit 4 of reg to indicate a segment register
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			}
			else {		// JLE imm => 0F 8E [imm32]
				instr->op = JLE;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x8f:		// POP r/m32 => 8F /0 [] [imm32]
			if (instr->prefix.extended_opcode != 1) {
				instr->op = INVALID_OPCODE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				// fail if reg field != 0
				if (modregrm_data.reg != 0) break;
				instr->op = POP;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], 0, operandsize_bit, instr);
			}
			else {		// JG imm => 0F 8F [imm32]
				instr->op = JG;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_imm_word = 1;
				instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0x90:		// NOP (aka XCHG EAX,EAX) => 90
			instr->op = NOP;
			instr->datasize = NIL;
		break;
		case 0x91:		// XCHG EAX,reg32 => 90+r
		case 0x92:		// XCHG EAX,reg32 => 90+r
		case 0x93:		// XCHG EAX,reg32 => 90+r
		case 0x94:		// XCHG EAX,reg32 => 90+r
		case 0x95:		// XCHG EAX,reg32 => 90+r
		case 0x96:		// XCHG EAX,reg32 => 90+r
		case 0x97:		// XCHG EAX,reg32 => 90+r
			instr->op = XCHG;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			decode_modregrm(opcodes[bytes], &modregrm_data);
			instr->dest.use_register = 1;
			instr->dest.register_data = modregrm_data.rm;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_register = 1;
			instr->source.register_data = al_ax_eax;
			instr->source.register_size = WORD_SIZE(instr->prefix.operand_size);
		break;
		case 0x98:		// CBW/CWDE => 98
			if (WORD_SIZE(instr->prefix.operand_size) == WORD) {
				instr->op = CBW;
			}
			else {
				instr->op = CWDE;
			}
			instr->datasize = NIL;
		break;
		case 0x99:		// CWD/CDQ => 99
			if (WORD_SIZE(instr->prefix.operand_size) == WORD) {
				instr->op = CWD;
			}
			else {
				instr->op = CDQ;
			}
			instr->datasize = NIL;
		break;
		case 0x9a:		// CALL imm:imm32 => 9A [offset:imm32] [segment:imm16]
			instr->op = CALL_FAR;
			instr->datasize = WORD_SIZE(0);
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm(&opcodes[bytes+1], 0);
			bytes += imm_byte_count(0);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm16(&opcodes[bytes+1]);
			bytes += 2;
		break;
		case 0x9c:		// PUSHF => 9C
			instr->op = PUSHF;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
		break;
		case 0x9d:		// POPF => 9D
			instr->op = POPF;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
		break;
		case 0x9e:		// SAHF => 9E
			instr->op = SAHF;
			instr->datasize = NIL;
		break;
		case 0x9f:		// LAHF => 9F
			instr->op = LAHF;
			instr->datasize = NIL;
		break;
		case 0xa0:		// mov al, rmb => A0 [mem16/mem32]
			if (instr->prefix.extended_opcode != 1) {
				instr->op = MOV;
				instr->datasize = BYTE;
				instr->dest.use_register = 1;
				instr->dest.register_data = al_ax_eax;
				instr->dest.register_size = BYTE;
				instr->source.use_disponly = 1;
				instr->source.register_segment = get_segment_register(bh_di_edi, instr->prefix.segment_override);	// provide a bogus value to get ds segment!
				instr->source.disponly_data = decode_imm(&opcodes[bytes+1], instr->prefix.address_size);
				bytes += imm_byte_count(instr->prefix.address_size);
			}
			else {		// PUSH FS => 0F A0
				instr->op = PUSH;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_register = 1;
				instr->dest.register_data = seg_fs;
				instr->dest.register_size = WORD;
			}
		break;
		case 0xa1:		// mov eax, rmw => A1 [mem16/mem32]
			if (instr->prefix.extended_opcode != 1) {
				instr->op = MOV;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_register = 1;
				instr->dest.register_data = al_ax_eax;
				instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
				instr->source.use_disponly = 1;
				instr->source.register_segment = get_segment_register(bh_di_edi, instr->prefix.segment_override);	// provide a bogus value to get ds segment!
				instr->source.disponly_data = decode_imm(&opcodes[bytes+1], instr->prefix.address_size);
				bytes += imm_byte_count(instr->prefix.address_size);
			}
			else {		// POP FS => 0F A1
				instr->op = POP;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_register = 1;
				instr->dest.register_data = seg_fs;
				instr->dest.register_size = WORD;
			}
		break;
		case 0xa2:		// mov rmb, al => A2 [mem16/mem32]
			instr->op = MOV;
			instr->datasize = BYTE;
			instr->dest.use_disponly = 1;
			instr->dest.register_segment = get_segment_register(bh_di_edi, instr->prefix.segment_override);	// provide a bogus value to get ds segment!
			instr->dest.disponly_data = decode_imm(&opcodes[bytes+1], instr->prefix.address_size);
			instr->source.use_register = 1;
			instr->source.register_data = al_ax_eax;
			instr->source.register_size = BYTE;
			bytes += imm_byte_count(instr->prefix.address_size);
		break;
		case 0xa3:		// mov rmw, eax => A3 [mem16/mem32]
			instr->op = MOV;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_disponly = 1;
			instr->dest.register_segment = get_segment_register(bh_di_edi, instr->prefix.segment_override);	// provide a bogus value to get ds segment!
			instr->dest.disponly_data = decode_imm(&opcodes[bytes+1], instr->prefix.address_size);
			instr->source.use_register = 1;
			instr->source.register_data = al_ax_eax;
			instr->source.register_size = WORD_SIZE(instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.address_size);
		break;
		case 0xa4:		// MOVS(B) => A4
			instr->op = MOVS;
			if (instr->prefix.lock_rep == 0xf2 || instr->prefix.lock_rep == 0xf3) instr->op = REP_MOVS;
			instr->datasize = BYTE;
			instr->dest.use_indregister = 1;
			instr->dest.register_segment = seg_es;
			instr->dest.register_data = bh_di_edi;
			instr->dest.register_size = WORD_SIZE(instr->prefix.address_size);
			instr->source.use_indregister = 1;
			instr->source.register_segment = seg_ds;
			instr->source.register_data = dh_si_esi;
			instr->source.register_size = WORD_SIZE(instr->prefix.address_size);
		break;
		case 0xa5:		// MOVS(W/D) => A5
			instr->op = MOVS;
			if (instr->prefix.lock_rep == 0xf2 || instr->prefix.lock_rep == 0xf3) instr->op = REP_MOVS;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_indregister = 1;
			instr->dest.register_segment = seg_es;
			instr->dest.register_data = bh_di_edi;
			instr->dest.register_size = WORD_SIZE(instr->prefix.address_size);
			instr->source.use_indregister = 1;
			instr->source.register_segment = seg_ds;
			instr->source.register_data = dh_si_esi;
			instr->source.register_size = WORD_SIZE(instr->prefix.address_size);
		break;
		case 0xa6:		// CMPS(B) => A6
			instr->op = CMPS;
			if (instr->prefix.lock_rep == 0xf2) instr->op = REPNE_CMPS;
			if (instr->prefix.lock_rep == 0xf3) instr->op = REPE_CMPS;
			instr->datasize = BYTE;
			instr->dest.use_indregister = 1;
			instr->dest.register_segment = seg_es;
			instr->dest.register_data = bh_di_edi;
			instr->dest.register_size = WORD_SIZE(instr->prefix.address_size);
			instr->source.use_indregister = 1;
			instr->source.register_segment = seg_ds;
			instr->source.register_data = dh_si_esi;
			instr->source.register_size = WORD_SIZE(instr->prefix.address_size);
		break;
		case 0xa7:		// CMPS(W/D) => A7
			instr->op = CMPS;
			if (instr->prefix.lock_rep == 0xf2) instr->op = REPNE_CMPS;
			if (instr->prefix.lock_rep == 0xf3) instr->op = REPE_CMPS;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_indregister = 1;
			instr->dest.register_segment = seg_es;
			instr->dest.register_data = bh_di_edi;
			instr->dest.register_size = WORD_SIZE(instr->prefix.address_size);
			instr->source.use_indregister = 1;
			instr->source.register_segment = seg_ds;
			instr->source.register_data = dh_si_esi;
			instr->source.register_size = WORD_SIZE(instr->prefix.address_size);
		break;
		case 0xa8:		// TEST AL,imm8 => A8 imm8
			if (instr->prefix.extended_opcode != 1) {
				instr->op = TEST;
				instr->datasize = BYTE;
				instr->dest.use_register = 1;
				instr->dest.register_data = al_ax_eax;
				instr->dest.register_size = BYTE;
				instr->source.use_imm_word = 1;
				instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
				bytes += 1;
			}
			else {		// PUSH GS => 0F A8
				instr->op = PUSH;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_register = 1;
				instr->dest.register_data = seg_gs;
				instr->dest.register_size = WORD;
			}
		break;
		case 0xa9:		// TEST EAX,imm32 => A9 imm32
			if (instr->prefix.extended_opcode != 1) {
				instr->op = TEST;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_register = 1;
				instr->dest.register_data = al_ax_eax;
				instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
				instr->source.use_imm_word = 1;
				instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
			else {		// POP GS => 0F A9
				instr->op = POP;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_register = 1;
				instr->dest.register_data = seg_gs;
				instr->dest.register_size = WORD;
			}
		break;
		case 0xaa:		// STOS(B) => AA
			instr->op = STOS;
			if (instr->prefix.lock_rep == 0xf2 || instr->prefix.lock_rep == 0xf3) instr->op = REP_STOS;
			instr->datasize = BYTE;
			instr->dest.use_indregister = 1;
			instr->dest.register_segment = seg_es;
			instr->dest.register_data = bh_di_edi;
			instr->dest.register_size = WORD_SIZE(instr->prefix.address_size);
			instr->source.use_register = 1;
			instr->source.register_data = al_ax_eax;
			instr->source.register_size = BYTE;
		break;
		case 0xab:		// STOS(W/D) => AB
			instr->op = STOS;
			if (instr->prefix.lock_rep == 0xf2 || instr->prefix.lock_rep == 0xf3) instr->op = REP_STOS;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_indregister = 1;
			instr->dest.register_segment = seg_es;
			instr->dest.register_data = bh_di_edi;
			instr->dest.register_size = WORD_SIZE(instr->prefix.address_size);
			instr->source.use_register = 1;
			instr->source.register_data = al_ax_eax;
			instr->source.register_size = WORD_SIZE(instr->prefix.operand_size);
		break;
		case 0xac:		// LODS(B) => AC
			instr->op = LODS;
			if (instr->prefix.lock_rep == 0xf2 || instr->prefix.lock_rep == 0xf3) instr->op = REP_LODS;
			instr->datasize = BYTE;
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = BYTE;
			instr->source.use_indregister = 1;
			instr->source.register_segment = seg_ds;
			instr->source.register_data = dh_si_esi;
			instr->source.register_size = WORD_SIZE(instr->prefix.address_size);
		break;
		case 0xad:		// LODS(W/D) => AD
			instr->op = LODS;
			if (instr->prefix.lock_rep == 0xf2 || instr->prefix.lock_rep == 0xf3) instr->op = REP_LODS;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_indregister = 1;
			instr->source.register_segment = seg_ds;
			instr->source.register_data = dh_si_esi;
			instr->source.register_size = WORD_SIZE(instr->prefix.address_size);
		break;
		case 0xae:		// SCAS(B) => AE
			instr->op = SCAS;
			if (instr->prefix.lock_rep == 0xf2) instr->op = REPNE_SCAS;
			if (instr->prefix.lock_rep == 0xf3) instr->op = REPE_SCAS;
			instr->datasize = BYTE;
			instr->dest.use_indregister = 1;
			instr->dest.register_segment = seg_es;
			instr->dest.register_data = bh_di_edi;
			instr->dest.register_size = WORD_SIZE(instr->prefix.address_size);
			instr->source.use_register = 1;
			instr->source.register_data = al_ax_eax;
			instr->source.register_size = BYTE;
		break;
		case 0xaf:		// SCAS(W/D) => AF
			if (instr->prefix.extended_opcode != 1) {
				instr->op = SCAS;
				if (instr->prefix.lock_rep == 0xf2) instr->op = REPNE_SCAS;
				if (instr->prefix.lock_rep == 0xf3) instr->op = REPE_SCAS;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_indregister = 1;
				instr->dest.register_segment = seg_es;
				instr->dest.register_data = bh_di_edi;
				instr->dest.register_size = WORD_SIZE(instr->prefix.address_size);
				instr->source.use_register = 1;
				instr->source.register_data = al_ax_eax;
				instr->source.register_size = WORD_SIZE(instr->prefix.operand_size);
			}
			else {		// IMUL reg32,r/m32 => 0F AF [mod/reg/rm] [imm8/imm32]
				// todo: fix imul encoding
				// this instruction has an unusual encoding;
				// it may take one, two or three parameters
				// when I wrote it originally I did not consider this encoding and
				// consequently this is incompatible with the existing implementation.
				// I'll need to seperate this into 2 separate sub-ops ie IMUL and IMUL_1
				instr->op = IMUL;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			}
		break;
		case 0xb0:		// mov al, imm8 => B0 imm8
			instr->op = MOV;
			instr->datasize = BYTE;
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = BYTE;
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0xb1:		// mov cl, imm8 => B1 imm8
			instr->op = MOV;
			instr->datasize = BYTE;
			instr->dest.use_register = 1;
			instr->dest.register_data = cl_cx_ecx;
			instr->dest.register_size = BYTE;
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0xb2:		// mov dl, imm8 => B2 imm8
			if (instr->prefix.extended_opcode != 1) {
				instr->op = MOV;
				instr->datasize = BYTE;
				instr->dest.use_register = 1;
				instr->dest.register_data = dl_dx_edx;
				instr->dest.register_size = BYTE;
				instr->source.use_imm_word = 1;
				instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
				bytes += 1;
			}
			else {		// LSS reg32,mem => 0FB2 [mod/reg/rm] []
				instr->op = INVALID_OPCODE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);	// mod/rm must specify a memory location;
				if (modregrm_data.mod == 3) break;				// register addressing mode is not allowed
				instr->op = LSS;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], 1, 1, instr);
			}
		break;
		case 0xb3:		// mov bl, imm8 => B3 imm8
			instr->op = MOV;
			instr->datasize = BYTE;
			instr->dest.use_register = 1;
			instr->dest.register_data = bl_bx_ebx;
			instr->dest.register_size = BYTE;
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0xb4:		// mov ah, imm8 => B4 imm8
			if (instr->prefix.extended_opcode != 1) {
				instr->op = MOV;
				instr->datasize = BYTE;
				instr->dest.use_register = 1;
				instr->dest.register_data = ah_sp_esp;
				instr->dest.register_size = BYTE;
				instr->source.use_imm_word = 1;
				instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
				bytes += 1;
			}
			else {		// LFS reg32,mem => 0FB4 [mod/reg/rm] []
				instr->op = INVALID_OPCODE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);	// mod/rm must specify a memory location;
				if (modregrm_data.mod == 3) break;				// register addressing mode is not allowed
				instr->op = LFS;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], 1, 1, instr);
			}
		break;
		case 0xb5:		// mov ch, imm8 => B5 imm8
			if (instr->prefix.extended_opcode != 1) {
				instr->op = MOV;
				instr->datasize = BYTE;
				instr->dest.use_register = 1;
				instr->dest.register_data = ch_bp_ebp;
				instr->dest.register_size = BYTE;
				instr->source.use_imm_word = 1;
				instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
				bytes += 1;
			}
			else {		// LGS reg32,mem => 0FB5 [mod/reg/rm] []
				instr->op = INVALID_OPCODE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);	// mod/rm must specify a memory location;
				if (modregrm_data.mod == 3) break;				// register addressing mode is not allowed
				instr->op = LGS;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], 1, 1, instr);
			}
		break;
		case 0xb6:		// mov dh, imm8 => B6 imm8
			if (instr->prefix.extended_opcode != 1) {
				instr->op = MOV;
				instr->datasize = BYTE;
				instr->dest.use_register = 1;
				instr->dest.register_data = dh_si_esi;
				instr->dest.register_size = BYTE;
				instr->source.use_imm_word = 1;
				instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
				bytes += 1;
			}
			else {		// MOVZX reg32,r/m8 => 0F BE [mod/reg/rm] []
				instr->op = MOVZX;
				instr->datasize = BYTE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, 1, instr);
				// if source is a register, set size to byte
				if (instr->source.use_register == 1) {
					instr->source.register_size = instr->datasize;
				}
			}
		break;
		case 0xb7:		// mov bh, imm8 => B7 imm8
			if (instr->prefix.extended_opcode != 1) {
				instr->op = MOV;
				instr->datasize = BYTE;
				instr->dest.use_register = 1;
				instr->dest.register_data = bh_di_edi;
				instr->dest.register_size = BYTE;
				instr->source.use_imm_word = 1;
				instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
				bytes += 1;
			}
			else {		// MOVZX reg32,r/m16 => 0F BF [mod/reg/rm] []
				instr->op = MOVZX;
				instr->datasize = WORD;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, 1, instr);
				// if source is a register, set size to word
				if (instr->source.use_register == 1) {
					instr->source.register_size = instr->datasize;
				}
			}
		break;
		case 0xb8:		// mov eax, imm32 => B8 imm32
			instr->op = MOV;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0xb9:		// mov ecx, imm32 => B9 imm32
			instr->op = MOV;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = cl_cx_ecx;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0xba:		// mov edx, imm32 => BA imm32
			instr->op = MOV;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = dl_dx_edx;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0xbb:		// mov ebx, imm32 => BB imm32
			instr->op = MOV;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = bl_bx_ebx;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0xbc:		// mov esp, imm32 => BC imm32
			instr->op = MOV;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = ah_sp_esp;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0xbd:		// mov ebp, imm32 => BD imm32
			instr->op = MOV;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = ch_bp_ebp;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0xbe:		// mov esi, imm32 => BE imm32
			if (instr->prefix.extended_opcode != 1) {
				instr->op = MOV;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_register = 1;
				instr->dest.register_data = dh_si_esi;
				instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
				instr->source.use_imm_word = 1;
				instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
			else {		// MOVSX reg32,r/m8 => 0F BE [mod/reg/rm] []
				instr->op = MOVSX;
				instr->datasize = BYTE;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, 1, instr);
				// if source is a register, set size to byte
				if (instr->source.use_register == 1) {
					instr->source.register_size = instr->datasize;
				}
			}
		break;
		case 0xbf:		// mov edi, imm32 => BF imm32
			if (instr->prefix.extended_opcode != 1) {
				instr->op = MOV;
				instr->datasize = WORD_SIZE(instr->prefix.operand_size);
				instr->dest.use_register = 1;
				instr->dest.register_data = bh_di_edi;
				instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
				instr->source.use_imm_word = 1;
				instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
			else {		// MOVSX reg32,r/m16 => 0F BF [mod/reg/rm] []
				instr->op = MOVSX;
				instr->datasize = WORD;
				decode_modregrm(opcodes[bytes+1], &modregrm_data);
				bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], destination_bit, 1, instr);
				// if source is a register, set size to word
				if (instr->source.use_register == 1) {
					instr->source.register_size = instr->datasize;
				}
			}
		break;
		case 0xc0:		// SHL r/m8,imm8 => C0 /4 [] [imm8]
			instr->op = INVALID_OPCODE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			if (modregrm_data.reg == 0) instr->op = ROL;
			if (modregrm_data.reg == 1) instr->op = ROR;
			if (modregrm_data.reg == 2) instr->op = RCL;
			if (modregrm_data.reg == 3) instr->op = RCR;
			if (modregrm_data.reg == 4) instr->op = SHL;
			if (modregrm_data.reg == 5) instr->op = SHR;
			if (modregrm_data.reg == 6) instr->op = NOT_IMPL;
			if (modregrm_data.reg == 7) instr->op = NOT_IMPL;
			instr->datasize = BYTE;
			bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0xc1:		// SHL r/m32,imm8 => C1 /4 [] [imm8]
			instr->op = INVALID_OPCODE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			if (modregrm_data.reg == 0) instr->op = ROL;
			if (modregrm_data.reg == 1) instr->op = ROR;
			if (modregrm_data.reg == 2) instr->op = RCL;
			if (modregrm_data.reg == 3) instr->op = RCR;
			if (modregrm_data.reg == 4) instr->op = SHL;
			if (modregrm_data.reg == 5) instr->op = SHR;
			if (modregrm_data.reg == 6) instr->op = NOT_IMPL;
			if (modregrm_data.reg == 7) instr->op = NOT_IMPL;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0xc2:
			instr->op = RET;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm16(&opcodes[bytes+1]);
			bytes += 2;
		break;
		case 0xc3:
			instr->op = RET;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
		break;
		case 0xc4:		// LES reg32,mem => C4 [mod/reg/rm] []
			instr->op = INVALID_OPCODE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);	// mod/rm must specify a memory location;
			if (modregrm_data.mod == 3) break;				// register addressing mode is not allowed
			instr->op = LES;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], 1, 1, instr);
		break;
		case 0xc5:		// LDS reg32,mem => C5 [mod/reg/rm] []
			instr->op = INVALID_OPCODE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);	// mod/rm must specify a memory location;
			if (modregrm_data.mod == 3) break;				// register addressing mode is not allowed
			instr->op = LDS;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			bytes += assign_modregrm(&modregrm_data, &opcodes[bytes+2], 1, 1, instr);
		break;
		case 0xc6:		// mov m/reg8, imm8 => C6 /0 [off0/off8/off32] [imm8]
			// these are MOV, according to http://www.mathemainzel.info/files/x86asmref.html#mov
			// the online dissassembler refused to accept it: https://defuse.ca/online-x86-assembler.htm
			// MOV     rmb,ib	C6 /0 d0 d1 i0
			// MOV     rmw,iw	C7 mr d0 d1 i0 i1
			instr->op = INVALID_OPCODE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			// fail if reg field != 0
			if (modregrm_data.reg != 0) break;
			instr->op = MOV;
			instr->datasize = BYTE;
			bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], 0, operandsize_bit, instr);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0xc7:		// mov m/reg32, imm32 => C7 /0 [off0/off8/off32] [imm32]
			instr->op = INVALID_OPCODE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			// fail if reg field != 0
			if (modregrm_data.reg != 0) break;
			instr->op = MOV;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], 0, operandsize_bit, instr);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0xca:
			instr->op = RETF;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm16(&opcodes[bytes+1]);
			bytes += 2;
		break;
		case 0xcb:
			instr->op = RETF;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
		break;
		case 0xcc:		// INT3 => CC
			instr->op = INT;
			instr->datasize = NIL;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = 0x03;
		break;
		case 0xcd:		// INT => CD imm8
			instr->op = INT;
			instr->datasize = NIL;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0xcf:		// IRET => CF
			instr->op = IRET;
			instr->datasize = NIL;
		break;
		case 0xd0:		// SHL r/m8,1 => D0 /4
			instr->op = INVALID_OPCODE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			if (modregrm_data.reg == 0) instr->op = ROL;
			if (modregrm_data.reg == 1) instr->op = ROR;
			if (modregrm_data.reg == 2) instr->op = RCL;
			if (modregrm_data.reg == 3) instr->op = RCR;
			if (modregrm_data.reg == 4) instr->op = SHL;
			if (modregrm_data.reg == 5) instr->op = SHR;
			if (modregrm_data.reg == 6) instr->op = NOT_IMPL;
			if (modregrm_data.reg == 7) instr->op = NOT_IMPL;
			instr->datasize = BYTE;
			bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = 1;
		break;
		case 0xd1:		// SHL r/m32,1 => D1 /4
			instr->op = INVALID_OPCODE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			if (modregrm_data.reg == 0) instr->op = ROL;
			if (modregrm_data.reg == 1) instr->op = ROR;
			if (modregrm_data.reg == 2) instr->op = RCL;
			if (modregrm_data.reg == 3) instr->op = RCR;
			if (modregrm_data.reg == 4) instr->op = SHL;
			if (modregrm_data.reg == 5) instr->op = SHR;
			if (modregrm_data.reg == 6) instr->op = NOT_IMPL;
			if (modregrm_data.reg == 7) instr->op = NOT_IMPL;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], destination_bit, operandsize_bit, instr);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = 1;
		break;
		case 0xd2:		// SHL r/m8,CL => D2 /4
			instr->op = INVALID_OPCODE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			if (modregrm_data.reg == 0) instr->op = ROL;
			if (modregrm_data.reg == 1) instr->op = ROR;
			if (modregrm_data.reg == 2) instr->op = RCL;
			if (modregrm_data.reg == 3) instr->op = RCR;
			if (modregrm_data.reg == 4) instr->op = SHL;
			if (modregrm_data.reg == 5) instr->op = SHR;
			if (modregrm_data.reg == 6) instr->op = NOT_IMPL;
			if (modregrm_data.reg == 7) instr->op = NOT_IMPL;
			instr->datasize = BYTE;
			bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], 0, operandsize_bit, instr);
			instr->source.use_register = 1;
			instr->source.register_data = cl_cx_ecx;
			instr->source.register_size = BYTE;
		break;
		case 0xd3:		// SHL r/m32,CL => D3 /4
			instr->op = INVALID_OPCODE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			if (modregrm_data.reg == 0) instr->op = ROL;
			if (modregrm_data.reg == 1) instr->op = ROR;
			if (modregrm_data.reg == 2) instr->op = RCL;
			if (modregrm_data.reg == 3) instr->op = RCR;
			if (modregrm_data.reg == 4) instr->op = SHL;
			if (modregrm_data.reg == 5) instr->op = SHR;
			if (modregrm_data.reg == 6) instr->op = NOT_IMPL;
			if (modregrm_data.reg == 7) instr->op = NOT_IMPL;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], 0, operandsize_bit, instr);
			instr->source.use_register = 1;
			instr->source.register_data = cl_cx_ecx;
			instr->source.register_size = BYTE;
		break;
		case 0xe0:		// LOOPNE imm => E0 [imm8]
			instr->op = LOOPNE;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0xe1:		// LOOPE imm => E1 [imm8]
			instr->op = LOOPE;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0xe2:		// LOOP imm => E2 [imm8]
			instr->op = LOOP;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0xE3:		// JCXZ/JECXZ imm => E3 [imm8]
			instr->op = JCXZ;		// this instruction may mean CX or ECX, depending on the mode the CPU is in
			instr->datasize = BYTE;	// short jump = 128 bytes max
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0xe4:		// IN AL,imm8 => E4 [imm8]
			instr->op = IN;
			instr->datasize = BYTE;
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = BYTE;
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0xe5:		// IN EAX,imm8 => E5 [imm8]
			instr->op = IN;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0xe6:		// OUT imm8,AL => E6 [imm8]
			instr->op = OUT;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
			instr->source.use_register = 1;
			instr->source.register_data = al_ax_eax;
			instr->source.register_size = BYTE;
		break;
		case 0xe7:		// OUT imm8,EAX => E7 [imm8]
			instr->op = OUT;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8(&opcodes[bytes+1]);
			bytes += 1;
			instr->source.use_register = 1;
			instr->source.register_data = al_ax_eax;
			instr->source.register_size = WORD_SIZE(instr->prefix.operand_size);
		break;
		case 0xe8:		// CALL imm => E8 [imm32]
			instr->op = CALL;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0xe9:		// JMP imm => E9 [imm32]
			instr->op = JMP;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm_sign_extend(&opcodes[bytes+1], instr->prefix.operand_size);
			bytes += imm_byte_count(instr->prefix.operand_size);
		break;
		case 0xea:		// JMP imm:imm32 => EA [offset:imm32] [segment:imm16]
			instr->op = JMP_FAR;
			instr->datasize = WORD_SIZE(0);
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm(&opcodes[bytes+1], 0);
			bytes += imm_byte_count(0);
			instr->source.use_imm_word = 1;
			instr->source.imm_word_data = decode_imm16(&opcodes[bytes+1]);
			bytes += 2;
		break;
		case 0xeb:		// JMP SHORT imm => EB [imm8]
			instr->op = JMP;
			instr->datasize = BYTE;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = decode_imm8_sign_extend(&opcodes[bytes+1]);
			bytes += 1;
		break;
		case 0xec:		// IN AL,DX => EC
			instr->op = IN;
			instr->datasize = BYTE;
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = BYTE;
			instr->source.use_register = 1;
			instr->source.register_data = dl_dx_edx;
			instr->source.register_size = WORD;
		break;
		case 0xed:		// IN EAX,DX => ED
			instr->op = IN;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = al_ax_eax;
			instr->dest.register_size = WORD_SIZE(instr->prefix.operand_size);
			instr->source.use_register = 1;
			instr->source.register_data = dl_dx_edx;
			instr->source.register_size = WORD;
		break;
		case 0xee:		// OUT DX,AL => EE
			instr->op = OUT;
			instr->datasize = BYTE;
			instr->dest.use_register = 1;
			instr->dest.register_data = dl_dx_edx;
			instr->dest.register_size = WORD;
			instr->source.use_register = 1;
			instr->source.register_data = al_ax_eax;
			instr->source.register_size = BYTE;
		break;
		case 0xef:		// OUT DX,EAX => EF
			instr->op = OUT;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			instr->dest.use_register = 1;
			instr->dest.register_data = dl_dx_edx;
			instr->dest.register_size = WORD;
			instr->source.use_register = 1;
			instr->source.register_data = al_ax_eax;
			instr->source.register_size = WORD_SIZE(instr->prefix.operand_size);
		break;
		case 0xf1:		// INT1 => F1
			instr->op = INT;
			instr->datasize = NIL;
			instr->dest.use_imm_word = 1;
			instr->dest.imm_word_data = 0x01;
		break;
		case 0xf5:		// CMC => F5
			instr->op = CMC;
		break;
		case 0xf6:		// NOT r/m8 => F6 /2 []
			instr->op = INVALID_OPCODE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			if (modregrm_data.reg == 0) instr->op = TEST;	// TEST r/m8,imm8 => F6 /0 [] [imm8]
			if (modregrm_data.reg == 1) instr->op = NOT_IMPL;
			if (modregrm_data.reg == 2) instr->op = NOT;
			if (modregrm_data.reg == 3) instr->op = NEG;
			if (modregrm_data.reg == 4) instr->op = MUL;
			if (modregrm_data.reg == 5) instr->op = IMUL;	// todo: fix imul encoding
			if (modregrm_data.reg == 6) instr->op = DIV;
			if (modregrm_data.reg == 7) instr->op = IDIV;
			instr->datasize = BYTE;
			bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], 0, operandsize_bit, instr);
			if (instr->op == TEST) {
				instr->source.use_imm_word = 1;
				instr->source.imm_word_data = decode_imm8(&opcodes[bytes+1]);
				bytes += 1;
			}
		break;
		case 0xf7:		// NOT r/m32 => F7 /2 []
			instr->op = INVALID_OPCODE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			if (modregrm_data.reg == 0) instr->op = TEST;	// TEST r/m32,imm32 => F7 /0 [] [imm32]
			if (modregrm_data.reg == 1) instr->op = NOT_IMPL;
			if (modregrm_data.reg == 2) instr->op = NOT;
			if (modregrm_data.reg == 3) instr->op = NEG;
			if (modregrm_data.reg == 4) instr->op = MUL;
			if (modregrm_data.reg == 5) instr->op = IMUL;	// todo: fix imul encoding
			if (modregrm_data.reg == 6) instr->op = DIV;
			if (modregrm_data.reg == 7) instr->op = IDIV;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], 0, operandsize_bit, instr);
			if (instr->op == TEST) {
				instr->source.use_imm_word = 1;
				instr->source.imm_word_data = decode_imm(&opcodes[bytes+1], instr->prefix.operand_size);
				bytes += imm_byte_count(instr->prefix.operand_size);
			}
		break;
		case 0xf8:		// CLC => F8
			instr->op = CLC;
		break;
		case 0xf9:		// STC => F9
			instr->op = STC;
		break;
		case 0xfa:		// CLI => FA
			instr->op = CLI;
		break;
		case 0xfb:		// STI => FB
			instr->op = STI;
		break;
		case 0xfc:		// CLD => FC
			instr->op = CLD;
		break;
		case 0xfd:		// STD => FD
			instr->op = STD;
		break;
		case 0xfe:		// INC r/m8 => FE /0 []
			instr->op = INVALID_OPCODE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			if (modregrm_data.reg == 0) instr->op = INC;
			if (modregrm_data.reg == 1) instr->op = DEC;
			if (modregrm_data.reg == 2) instr->op = INVALID_OPCODE;
			if (modregrm_data.reg == 3) instr->op = INVALID_OPCODE;
			if (modregrm_data.reg == 4) instr->op = INVALID_OPCODE;
			if (modregrm_data.reg == 5) instr->op = INVALID_OPCODE;
			if (modregrm_data.reg == 6) instr->op = INVALID_OPCODE;
			if (modregrm_data.reg == 7) instr->op = INVALID_OPCODE;
			instr->datasize = BYTE;
			bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], 0, operandsize_bit, instr);
		break;
		case 0xff:		// PUSH r/m32 => FF /6 []
			instr->op = INVALID_OPCODE;
			decode_modregrm(opcodes[bytes+1], &modregrm_data);
			if (modregrm_data.reg == 0) instr->op = INC;
			if (modregrm_data.reg == 1) instr->op = DEC;
			if (modregrm_data.reg == 2) instr->op = CALL;
			// register addressing is invalid for CALL_FAR
			if ((modregrm_data.reg == 3) && (modregrm_data.mod != 3)) instr->op = CALL_FAR;
			if (modregrm_data.reg == 4) instr->op = JMP;
			// register addressing is invalid for JMP_FAR
			if ((modregrm_data.reg == 5) && (modregrm_data.mod != 3)) instr->op = JMP_FAR;
			if (modregrm_data.reg == 6) instr->op = PUSH;
			if (modregrm_data.reg == 7) instr->op = INVALID_OPCODE;
			instr->datasize = WORD_SIZE(instr->prefix.operand_size);
			bytes += assign_modrm(&modregrm_data, &opcodes[bytes+2], 0, operandsize_bit, instr);
		break;
		default:
			instr->op = INVALID_OPCODE;
			instr->datasize = NIL;
		break;
	}

	bytes++;	// account for the first opcode byte

	instr->length = bytes;

	for (int i=0; i<bytes; i++)
		instr->bytes[i] = opcodes[i];

	return bytes;

}

// if opcodes[0] is a valid prefix, return it
// otherwise return 0
uint8_t check_prefix(uint8_t *opcodes) {

	switch (opcodes[0]) {
		case 0x0f:		// Two byte instruction
		case 0x26:		// Instruction prefix: Segment override
		case 0x2e:		// Instruction prefix: Segment override
		case 0x36:		// Instruction prefix: Segment override
		case 0x3e:		// Instruction prefix: Segment override
		case 0x64:		// Instruction prefix: Segment override
		case 0x65:		// Instruction prefix: Segment override
		case 0x66:		// Instruction prefix: Operand size override
		case 0x67:		// Instruction prefix: Address size override
		case 0xf0:		// Instruction prefix: Lock prefix
		case 0xf2:		// Instruction prefix: REPNE
		case 0xf3:		// Instruction prefix: REP, REPE
			return opcodes[0];
		break;
	}

	return 0;

}

void decode_modregrm(uint8_t opcode, struct mod_reg_rm *modregrm_data) {

	uint8_t mod_rm_byte = opcode & 0b11000111;
	uint8_t reg_byte = opcode & 0b00111000;

	modregrm_data->mod = (mod_rm_byte >> 6) & 0b00000011;
	modregrm_data->rm = mod_rm_byte & 0b00000111;
	modregrm_data->reg = (reg_byte >> 3) & 0b00000111;

}

void decode_sib(uint8_t opcode, struct sib *sib_data) {

	uint8_t scale_byte = opcode & 0b11000000;
	uint8_t index_byte = opcode & 0b00111000;

	int scale = (scale_byte >> 6) & 0b00000011;
	sib_data->scale = 1;
	if (scale == 1) sib_data->scale = 2;
	if (scale == 2) sib_data->scale = 4;
	if (scale == 3) sib_data->scale = 8;
	sib_data->index = (index_byte >> 3) & 0b00000111;
	sib_data->base = opcode & 0b00000111;

	// if esp is used as an index, it is interpreted as base only; set scale to zero
	if (sib_data->index == 4) sib_data->scale = 0;

}

int assign_modregrm32(struct mod_reg_rm *modregrm_data, uint8_t *extra_opcodes, int destination_bit, int operandsize_bit, struct instruction *instr) {

	// todo:
	// complete implementation of 32-bit Displacement-Only Mode

	// the instruction datasize; this is usually determined by the size bit in the opcode
	enum datasize data_size;
	data_size = BYTE;
	if (operandsize_bit > 0) data_size = WORD_SIZE(instr->prefix.operand_size);
	// handle segment registers
	if (modregrm_data->reg & 0x08) data_size = WORD;

	// the addressing mode size; depending on whether the cpu is running in 16 bit or 32 bit mode, the
	// size of the segment register will vary
	enum datasize address_size;
	address_size = WORD_SIZE(instr->prefix.address_size);

	int bytes = 1;		// return the size of the instruction in bytes
	struct sib sib_data;
	memset(&sib_data, 0, sizeof(struct sib));

	switch (modregrm_data->mod) {
		case 0:
		case 1:
		case 2:
			// if rm = 100 (4 in decimal), use SIB addressing
			// 00 100 SIB  Mode
			// 01 100 SIB  +  disp8  Mode
			// 10 100 SIB  +  disp32  Mode
			if (modregrm_data->rm == 4) {
				decode_sib(extra_opcodes[0], &sib_data);
				bytes++;
			}
			if ((modregrm_data->mod == 0) && (modregrm_data->rm == 4)) {
				// SIB
				// note if sib_data.base = 5 we don't use EBP, rather 32 bit displacement + [index * scale]
				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->source.use_register = 1;
					instr->source.register_data = modregrm_data->reg;
					instr->source.register_size = data_size;
					if (sib_data.base == ch_bp_ebp) {
						instr->dest.use_disponly = 1;
						instr->dest.register_segment = get_segment_register32(bh_di_edi, instr->prefix.segment_override);		// provide a bogus value to get ds segment!
						instr->dest.disponly_data = decode_imm32(&extra_opcodes[1]);
						bytes += 4;
					}
					else {
						instr->dest.use_indregister = 1;
						instr->dest.register_segment = get_segment_register32(sib_data.base, instr->prefix.segment_override);
						instr->dest.register_data = sib_data.base;
					}
					instr->dest.register_size = address_size;
					instr->dest.use_sib = 1;
					instr->dest.sib_scale = sib_data.scale;
					instr->dest.sib_index = sib_data.index;
				}
				// If d=1, REG is the destination
				else {
					if (sib_data.base == ch_bp_ebp) {
						instr->source.use_disponly = 1;
						instr->source.register_segment = get_segment_register32(bh_di_edi, instr->prefix.segment_override);		// provide a bogus value to get ds segment!
						instr->source.disponly_data = decode_imm32(&extra_opcodes[1]);
						bytes += 4;
					}
					else {
						instr->source.use_indregister = 1;
						instr->source.register_segment = get_segment_register32(sib_data.base, instr->prefix.segment_override);
						instr->source.register_data = sib_data.base;
					}
					instr->source.register_size = address_size;
					instr->source.use_sib = 1;
					instr->source.sib_scale = sib_data.scale;
					instr->source.sib_index = sib_data.index;
					instr->dest.use_register = 1;
					instr->dest.register_data = modregrm_data->reg;
					instr->dest.register_size = data_size;
				}
				return bytes;
			}
			if ((modregrm_data->mod == 1) && (modregrm_data->rm == 4)) {
				// SIB + disp8
				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->source.use_register = 1;
					instr->source.register_data = modregrm_data->reg;
					instr->source.register_size = data_size;
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register32(sib_data.base, instr->prefix.segment_override);
					instr->dest.register_data = sib_data.base;
					instr->dest.register_size = address_size;
					instr->dest.use_sib = 1;
					instr->dest.sib_scale = sib_data.scale;
					instr->dest.sib_index = sib_data.index;
					instr->dest.use_offset = 1;
					instr->dest.offset_data = decode_imm8_sign_extend(&extra_opcodes[1]);
					bytes += 1;
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register32(sib_data.base, instr->prefix.segment_override);
					instr->source.register_data = sib_data.base;
					instr->source.register_size = address_size;
					instr->source.use_sib = 1;
					instr->source.sib_scale = sib_data.scale;
					instr->source.sib_index = sib_data.index;
					instr->source.use_offset = 1;
					instr->source.offset_data = decode_imm8_sign_extend(&extra_opcodes[1]);
					bytes += 1;
					instr->dest.use_register = 1;
					instr->dest.register_data = modregrm_data->reg;
					instr->dest.register_size = data_size;
				}
				return bytes;
			}
			if ((modregrm_data->mod == 2) && (modregrm_data->rm == 4)) {
				// SIB + disp32
				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->source.use_register = 1;
					instr->source.register_data = modregrm_data->reg;
					instr->source.register_size = data_size;
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register32(sib_data.base, instr->prefix.segment_override);
					instr->dest.register_data = sib_data.base;
					instr->dest.register_size = address_size;
					instr->dest.use_sib = 1;
					instr->dest.sib_scale = sib_data.scale;
					instr->dest.sib_index = sib_data.index;
					instr->dest.use_offset = 1;
					instr->dest.offset_data = decode_imm32_sign_extend(&extra_opcodes[1]);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register32(sib_data.base, instr->prefix.segment_override);
					instr->source.register_data = sib_data.base;
					instr->source.register_size = address_size;
					instr->source.use_sib = 1;
					instr->source.sib_scale = sib_data.scale;
					instr->source.sib_index = sib_data.index;
					instr->source.use_offset = 1;
					instr->source.offset_data = decode_imm32_sign_extend(&extra_opcodes[1]);
					bytes += imm_byte_count(instr->prefix.address_size);
					instr->dest.use_register = 1;
					instr->dest.register_data = modregrm_data->reg;
					instr->dest.register_size = data_size;
				}
				return bytes;
			}

			// if mod = 00 and rm = 101, use 32 bit displacement
			// 101 32-bit Displacement-Only Mode
			if ((modregrm_data->mod == 0) && (modregrm_data->rm == 5)) {

				// 88 05 01 00 00 00       mov    BYTE PTR ds:0x1,al

				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->source.use_register = 1;
					instr->source.register_data = modregrm_data->reg;
					instr->source.register_size = data_size;
					instr->dest.use_disponly = 1;
					instr->dest.register_segment = get_segment_register32(bh_di_edi, instr->prefix.segment_override);		// provide a bogus value to get ds segment!
					instr->dest.register_size = address_size;
					instr->dest.disponly_data = decode_imm(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_disponly = 1;
					instr->source.register_segment = get_segment_register32(bh_di_edi, instr->prefix.segment_override);		// provide a bogus value to get ds segment!
					instr->source.register_size = address_size;
					instr->source.disponly_data = decode_imm(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
					instr->dest.use_register = 1;
					instr->dest.register_data = modregrm_data->reg;
					instr->dest.register_size = data_size;
				}
				return bytes;
			}

			// all other conditions use indirect addressing mode
			if (modregrm_data->mod == 0) {
				// register indirect addressing mode
				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->source.use_register = 1;
					instr->source.register_data = modregrm_data->reg;
					instr->source.register_size = data_size;
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register32(modregrm_data->rm, instr->prefix.segment_override);
					instr->dest.register_data = modregrm_data->rm;
					instr->dest.register_size = address_size;
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register32(modregrm_data->rm, instr->prefix.segment_override);
					instr->source.register_data = modregrm_data->rm;
					instr->source.register_size = address_size;
					instr->dest.use_register = 1;
					instr->dest.register_data = modregrm_data->reg;
					instr->dest.register_size = data_size;
				}
				return 1;
			}
			// [ reg + disp8 ]
			if (modregrm_data->mod == 1) {

				// 88 45 01                mov    BYTE PTR [ebp+0x1],al

				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->source.use_register = 1;
					instr->source.register_data = modregrm_data->reg;
					instr->source.register_size = data_size;
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register32(modregrm_data->rm, instr->prefix.segment_override);
					instr->dest.register_data = modregrm_data->rm;
					instr->dest.register_size = address_size;
					instr->dest.use_offset = 1;
					instr->dest.offset_data = decode_imm8_sign_extend(extra_opcodes);
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register32(modregrm_data->rm, instr->prefix.segment_override);
					instr->source.register_data = modregrm_data->rm;
					instr->source.register_size = address_size;
					instr->source.use_offset = 1;
					instr->source.offset_data = decode_imm8_sign_extend(extra_opcodes);
					instr->dest.use_register = 1;
					instr->dest.register_data = modregrm_data->reg;
					instr->dest.register_size = data_size;
				}
				return 2;
			}
			// [ reg + disp32 ]
			if (modregrm_data->mod == 2) {
				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->source.use_register = 1;
					instr->source.register_data = modregrm_data->reg;
					instr->source.register_size = data_size;
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register32(modregrm_data->rm, instr->prefix.segment_override);
					instr->dest.register_data = modregrm_data->rm;
					instr->dest.register_size = address_size;
					instr->dest.use_offset = 1;
					instr->dest.offset_data = decode_imm_sign_extend(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register32(modregrm_data->rm, instr->prefix.segment_override);
					instr->source.register_data = modregrm_data->rm;
					instr->source.register_size = address_size;
					instr->source.use_offset = 1;
					instr->source.offset_data = decode_imm_sign_extend(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
					instr->dest.use_register = 1;
					instr->dest.register_data = modregrm_data->reg;
					instr->dest.register_size = data_size;
				}
				return bytes;
			}
			//return x;
		break;
		case 3:
			// if mod = 11, rm specifies a register
			// If d=0, REG is the source
			if (destination_bit == 0) {
				instr->source.use_register = 1;
				instr->source.register_data = modregrm_data->reg;
				instr->source.register_size = data_size;
				instr->dest.use_register = 1;
				instr->dest.register_data = modregrm_data->rm;
				instr->dest.register_size = data_size;
			}
			// If d=1, REG is the destination
			else {
				instr->source.use_register = 1;
				instr->source.register_data = modregrm_data->rm;
				instr->source.register_size = data_size;
				instr->dest.use_register = 1;
				instr->dest.register_data = modregrm_data->reg;
				instr->dest.register_size = data_size;
			}
			return 1;
		break;
	}
	// we should never reach this point!
	return -1;
}

// variation of assign_modregrm, used when the reg field is an extension of the instruction's opcode
int assign_modrm32(struct mod_reg_rm *modregrm_data, uint8_t *extra_opcodes, int destination_bit, int operandsize_bit, struct instruction *instr) {

	// the instruction datasize; this is usually determined by the size bit in the opcode
	enum datasize data_size;
	data_size = BYTE;
	if (operandsize_bit > 0) data_size = WORD_SIZE(instr->prefix.operand_size);

	// the addressing mode size
	enum datasize address_size;
	address_size = WORD_SIZE(instr->prefix.address_size);

	int bytes = 1;		// return the size of the instruction in bytes
	struct sib sib_data;
	memset(&sib_data, 0, sizeof(struct sib));

	switch (modregrm_data->mod) {
		case 0:
		case 1:
		case 2:
			// if rm = 100 (4 in decimal), use SIB addressing
			// 00 100 SIB  Mode
			// 01 100 SIB  +  disp8  Mode
			// 10 100 SIB  +  disp32  Mode
			if (modregrm_data->rm == 4) {
				decode_sib(extra_opcodes[0], &sib_data);
				bytes++;
			}
			if ((modregrm_data->mod == 0) && (modregrm_data->rm == 4)) {
				// SIB
				// note if sib_data.base = 5 we don't use EBP, rather 32 bit displacement + [index * scale]
				// If d=0, REG is the source
				if (destination_bit == 0) {
					if (sib_data.base == ch_bp_ebp) {
						instr->dest.use_disponly = 1;
						instr->dest.register_segment = get_segment_register32(bh_di_edi, instr->prefix.segment_override);		// provide a bogus value to get ds segment!
						instr->dest.disponly_data = decode_imm32(&extra_opcodes[1]);
						bytes += 4;
					}
					else {
						instr->dest.use_indregister = 1;
						instr->dest.register_segment = get_segment_register32(sib_data.base, instr->prefix.segment_override);
						instr->dest.register_data = sib_data.base;
					}
					instr->dest.register_size = address_size;
					instr->dest.use_sib = 1;
					instr->dest.sib_scale = sib_data.scale;
					instr->dest.sib_index = sib_data.index;
				}
				// If d=1, REG is the destination
				else {
					if (sib_data.base == ch_bp_ebp) {
						instr->source.use_disponly = 1;
						instr->source.register_segment = get_segment_register32(bh_di_edi, instr->prefix.segment_override);		// provide a bogus value to get ds segment!
						instr->source.disponly_data = decode_imm32(&extra_opcodes[1]);
						bytes += 4;
					}
					else {
						instr->source.use_indregister = 1;
						instr->source.register_segment = get_segment_register32(sib_data.base, instr->prefix.segment_override);
						instr->source.register_data = sib_data.base;
					}
					instr->source.register_size = address_size;
					instr->source.use_sib = 1;
					instr->source.sib_scale = sib_data.scale;
					instr->source.sib_index = sib_data.index;
				}
				return bytes;
			}
			if ((modregrm_data->mod == 1) && (modregrm_data->rm == 4)) {
				// SIB + disp8
				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register32(sib_data.base, instr->prefix.segment_override);
					instr->dest.register_data = sib_data.base;
					instr->dest.register_size = address_size;
					instr->dest.use_sib = 1;
					instr->dest.sib_scale = sib_data.scale;
					instr->dest.sib_index = sib_data.index;
					instr->dest.use_offset = 1;
					instr->dest.offset_data = decode_imm8_sign_extend(&extra_opcodes[1]);
					bytes += 1;
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register32(sib_data.base, instr->prefix.segment_override);
					instr->source.register_data = sib_data.base;
					instr->source.register_size = address_size;
					instr->source.use_sib = 1;
					instr->source.sib_scale = sib_data.scale;
					instr->source.sib_index = sib_data.index;
					instr->source.use_offset = 1;
					instr->source.offset_data = decode_imm8_sign_extend(&extra_opcodes[1]);
					bytes += 1;
				}
				return bytes;
			}
			if ((modregrm_data->mod == 2) && (modregrm_data->rm == 4)) {
				// SIB + disp32
				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register32(sib_data.base, instr->prefix.segment_override);
					instr->dest.register_data = sib_data.base;
					instr->dest.register_size = address_size;
					instr->dest.use_sib = 1;
					instr->dest.sib_scale = sib_data.scale;
					instr->dest.sib_index = sib_data.index;
					instr->dest.use_offset = 1;
					instr->dest.offset_data = decode_imm32_sign_extend(&extra_opcodes[1]);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register32(sib_data.base, instr->prefix.segment_override);
					instr->source.register_data = sib_data.base;
					instr->source.register_size = address_size;
					instr->source.use_sib = 1;
					instr->source.sib_scale = sib_data.scale;
					instr->source.sib_index = sib_data.index;
					instr->source.use_offset = 1;
					instr->source.offset_data = decode_imm32_sign_extend(&extra_opcodes[1]);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				return bytes;
			}

			// if mod = 00 and rm = 101, use 32 bit displacement
			// 101 32-bit Displacement-Only Mode
			if ((modregrm_data->mod == 0) && (modregrm_data->rm == 5)) {

				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->dest.use_disponly = 1;
					instr->dest.register_segment = get_segment_register32(bh_di_edi, instr->prefix.segment_override);		// provide a bogus value to get ds segment!
					instr->dest.register_size = address_size;
					instr->dest.disponly_data = decode_imm(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_disponly = 1;
					instr->source.register_segment = get_segment_register32(bh_di_edi, instr->prefix.segment_override);		// provide a bogus value to get ds segment!
					instr->source.register_size = address_size;
					instr->source.disponly_data = decode_imm(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				return bytes;
			}

			// all other conditions use indirect addressing mode
			if (modregrm_data->mod == 0) {
				// register indirect addressing mode
				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register32(modregrm_data->rm, instr->prefix.segment_override);
					instr->dest.register_data = modregrm_data->rm;
					instr->dest.register_size = address_size;
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register32(modregrm_data->rm, instr->prefix.segment_override);
					instr->source.register_data = modregrm_data->rm;
					instr->source.register_size = address_size;
				}
				return 1;
			}
			// [ reg + disp8 ]
			if (modregrm_data->mod == 1) {

				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register32(modregrm_data->rm, instr->prefix.segment_override);
					instr->dest.register_data = modregrm_data->rm;
					instr->dest.register_size = address_size;
					instr->dest.use_offset = 1;
					instr->dest.offset_data = decode_imm8_sign_extend(extra_opcodes);
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register32(modregrm_data->rm, instr->prefix.segment_override);
					instr->source.register_data = modregrm_data->rm;
					instr->source.register_size = address_size;
					instr->source.use_offset = 1;
					instr->source.offset_data = decode_imm8_sign_extend(extra_opcodes);
				}
				return 2;
			}
			// [ reg + disp32 ]
			if (modregrm_data->mod == 2) {
				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register32(modregrm_data->rm, instr->prefix.segment_override);
					instr->dest.register_data = modregrm_data->rm;
					instr->dest.register_size = address_size;
					instr->dest.use_offset = 1;
					instr->dest.offset_data = decode_imm_sign_extend(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register32(modregrm_data->rm, instr->prefix.segment_override);
					instr->source.register_data = modregrm_data->rm;
					instr->source.register_size = address_size;
					instr->source.use_offset = 1;
					instr->source.offset_data = decode_imm_sign_extend(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				return bytes;
			}
			//return x;
		break;
		case 3:
			// if mod = 11, rm specifies a register
			// If d=0, REG is the source
			if (destination_bit == 0) {
				instr->dest.use_register = 1;
				instr->dest.register_data = modregrm_data->rm;
				instr->dest.register_size = data_size;
			}
			// If d=1, REG is the destination
			else {
				instr->source.use_register = 1;
				instr->source.register_data = modregrm_data->rm;
				instr->source.register_size = data_size;
			}
			return 1;
		break;
	}
	// we should never reach this point!
	return -1;
}

int assign_modregrm16(struct mod_reg_rm *modregrm_data, uint8_t *extra_opcodes, int destination_bit, int operandsize_bit, struct instruction *instr) {

	// the instruction datasize; this is usually determined by the size bit in the opcode
	enum datasize data_size;
	data_size = BYTE;
	if (operandsize_bit > 0) data_size = WORD_SIZE(instr->prefix.operand_size);
	// handle segment registers
	if (modregrm_data->reg & 0x08) data_size = WORD;

	// the addressing mode size; depending on whether the cpu is running in 16 bit or 32 bit mode, the
	// size of the segment register will vary
	enum datasize address_size;
	address_size = WORD_SIZE(instr->prefix.address_size);

	int bytes = 1;		// return the size of the instruction in bytes

	switch (modregrm_data->mod) {
		case 0:
		case 1:
		case 2:
			// if mod = 00 and rm = 110, use 16 bit displacement
			// 110 16-bit Displacement-Only Mode
			if ((modregrm_data->mod == 0) && (modregrm_data->rm == 6)) {

				// 88 06 01 00       mov    BYTE PTR ds:0x1,al

				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->source.use_register = 1;
					instr->source.register_data = modregrm_data->reg;
					instr->source.register_size = data_size;
					instr->dest.use_disponly = 1;
					instr->dest.register_segment = get_segment_register16(bh_di_edi, instr->prefix.segment_override);		// provide a bogus value to get ds segment!
					instr->dest.register_size = address_size;
					instr->dest.disponly_data = decode_imm(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_disponly = 1;
					instr->source.register_segment = get_segment_register16(bh_di_edi, instr->prefix.segment_override);		// provide a bogus value to get ds segment!
					instr->source.register_size = address_size;
					instr->source.disponly_data = decode_imm(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
					instr->dest.use_register = 1;
					instr->dest.register_data = modregrm_data->reg;
					instr->dest.register_size = data_size;
				}
				return bytes;
			}

			// all other conditions use indirect addressing mode
			if (modregrm_data->mod == 0) {
				// register indirect addressing mode
				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->source.use_register = 1;
					instr->source.register_data = modregrm_data->reg;
					instr->source.register_size = data_size;
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register16(modregrm_data->rm + _16BITMODEOFFSET, instr->prefix.segment_override);
					instr->dest.register_data = modregrm_data->rm + _16BITMODEOFFSET;
					instr->dest.register_size = address_size;
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register16(modregrm_data->rm + _16BITMODEOFFSET, instr->prefix.segment_override);
					instr->source.register_data = modregrm_data->rm + _16BITMODEOFFSET;
					instr->source.register_size = address_size;
					instr->dest.use_register = 1;
					instr->dest.register_data = modregrm_data->reg;
					instr->dest.register_size = data_size;
				}
				return 1;
			}
			// [ reg + disp8 ]
			if (modregrm_data->mod == 1) {

				// 88 46 01                mov    BYTE PTR [bp+0x1],al

				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->source.use_register = 1;
					instr->source.register_data = modregrm_data->reg;
					instr->source.register_size = data_size;
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register16(modregrm_data->rm + _16BITMODEOFFSET, instr->prefix.segment_override);
					instr->dest.register_data = modregrm_data->rm + _16BITMODEOFFSET;
					instr->dest.register_size = address_size;
					instr->dest.use_offset = 1;
					instr->dest.offset_data = decode_imm8_sign_extend(extra_opcodes);
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register16(modregrm_data->rm + _16BITMODEOFFSET, instr->prefix.segment_override);
					instr->source.register_data = modregrm_data->rm + _16BITMODEOFFSET;
					instr->source.register_size = address_size;
					instr->source.use_offset = 1;
					instr->source.offset_data = decode_imm8_sign_extend(extra_opcodes);
					instr->dest.use_register = 1;
					instr->dest.register_data = modregrm_data->reg;
					instr->dest.register_size = data_size;
				}
				return 2;
			}
			// [ reg + disp16 ]
			if (modregrm_data->mod == 2) {
				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->source.use_register = 1;
					instr->source.register_data = modregrm_data->reg;
					instr->source.register_size = data_size;
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register16(modregrm_data->rm + _16BITMODEOFFSET, instr->prefix.segment_override);
					instr->dest.register_data = modregrm_data->rm + _16BITMODEOFFSET;
					instr->dest.register_size = address_size;
					instr->dest.use_offset = 1;
					instr->dest.offset_data = decode_imm_sign_extend(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register16(modregrm_data->rm + _16BITMODEOFFSET, instr->prefix.segment_override);
					instr->source.register_data = modregrm_data->rm + _16BITMODEOFFSET;
					instr->source.register_size = address_size;
					instr->source.use_offset = 1;
					instr->source.offset_data = decode_imm_sign_extend(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
					instr->dest.use_register = 1;
					instr->dest.register_data = modregrm_data->reg;
					instr->dest.register_size = data_size;
				}
				return bytes;
			}
			//return x;
		break;
		case 3:
			// if mod = 11, rm specifies a register
			// If d=0, REG is the source
			if (destination_bit == 0) {
				instr->source.use_register = 1;
				instr->source.register_data = modregrm_data->reg;
				instr->source.register_size = data_size;
				instr->dest.use_register = 1;
				instr->dest.register_data = modregrm_data->rm;
				instr->dest.register_size = data_size;
			}
			// If d=1, REG is the destination
			else {
				instr->source.use_register = 1;
				instr->source.register_data = modregrm_data->rm;
				instr->source.register_size = data_size;
				instr->dest.use_register = 1;
				instr->dest.register_data = modregrm_data->reg;
				instr->dest.register_size = data_size;
			}
			return 1;
		break;
	}
	// we should never reach this point!
	return -1;
}

// variation of assign_modregrm, used when the reg field is an extension of the instruction's opcode
int assign_modrm16(struct mod_reg_rm *modregrm_data, uint8_t *extra_opcodes, int destination_bit, int operandsize_bit, struct instruction *instr) {

	// the instruction datasize; this is usually determined by the size bit in the opcode
	enum datasize data_size;
	data_size = BYTE;
	if (operandsize_bit > 0) data_size = WORD_SIZE(instr->prefix.operand_size);

	// the addressing mode size
	enum datasize address_size;
	address_size = WORD_SIZE(instr->prefix.address_size);

	int bytes = 1;		// return the size of the instruction in bytes

	switch (modregrm_data->mod) {
		case 0:
		case 1:
		case 2:
			// if mod = 00 and rm = 110, use 16 bit displacement
			// 110 16-bit Displacement-Only Mode
			if ((modregrm_data->mod == 0) && (modregrm_data->rm == 6)) {

				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->dest.use_disponly = 1;
					instr->dest.register_segment = get_segment_register16(bh_di_edi, instr->prefix.segment_override);		// provide a bogus value to get ds segment!
					instr->dest.register_size = address_size;
					instr->dest.disponly_data = decode_imm(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_disponly = 1;
					instr->source.register_segment = get_segment_register16(bh_di_edi, instr->prefix.segment_override);		// provide a bogus value to get ds segment!
					instr->source.register_size = address_size;
					instr->source.disponly_data = decode_imm(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				return bytes;
			}

			// all other conditions use indirect addressing mode
			if (modregrm_data->mod == 0) {
				// register indirect addressing mode
				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register16(modregrm_data->rm + _16BITMODEOFFSET, instr->prefix.segment_override);
					instr->dest.register_data = modregrm_data->rm + _16BITMODEOFFSET;
					instr->dest.register_size = address_size;
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register16(modregrm_data->rm + _16BITMODEOFFSET, instr->prefix.segment_override);
					instr->source.register_data = modregrm_data->rm + _16BITMODEOFFSET;
					instr->source.register_size = address_size;
				}
				return 1;
			}
			// [ reg + disp8 ]
			if (modregrm_data->mod == 1) {

				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register16(modregrm_data->rm + _16BITMODEOFFSET, instr->prefix.segment_override);
					instr->dest.register_data = modregrm_data->rm + _16BITMODEOFFSET;
					instr->dest.register_size = address_size;
					instr->dest.use_offset = 1;
					instr->dest.offset_data = decode_imm8_sign_extend(extra_opcodes);
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register16(modregrm_data->rm + _16BITMODEOFFSET, instr->prefix.segment_override);
					instr->source.register_data = modregrm_data->rm + _16BITMODEOFFSET;
					instr->source.register_size = address_size;
					instr->source.use_offset = 1;
					instr->source.offset_data = decode_imm8_sign_extend(extra_opcodes);
				}
				return 2;
			}
			// [ reg + disp16 ]
			if (modregrm_data->mod == 2) {
				// If d=0, REG is the source
				if (destination_bit == 0) {
					instr->dest.use_indregister = 1;
					instr->dest.register_segment = get_segment_register16(modregrm_data->rm + _16BITMODEOFFSET, instr->prefix.segment_override);
					instr->dest.register_data = modregrm_data->rm + _16BITMODEOFFSET;
					instr->dest.register_size = address_size;
					instr->dest.use_offset = 1;
					instr->dest.offset_data = decode_imm_sign_extend(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				// If d=1, REG is the destination
				else {
					instr->source.use_indregister = 1;
					instr->source.register_segment = get_segment_register16(modregrm_data->rm + _16BITMODEOFFSET, instr->prefix.segment_override);
					instr->source.register_data = modregrm_data->rm + _16BITMODEOFFSET;
					instr->source.register_size = address_size;
					instr->source.use_offset = 1;
					instr->source.offset_data = decode_imm_sign_extend(extra_opcodes, instr->prefix.address_size);
					bytes += imm_byte_count(instr->prefix.address_size);
				}
				return bytes;
			}
			//return x;
		break;
		case 3:
			// if mod = 11, rm specifies a register
			// If d=0, REG is the source
			if (destination_bit == 0) {
				instr->dest.use_register = 1;
				instr->dest.register_data = modregrm_data->rm;
				instr->dest.register_size = data_size;
			}
			// If d=1, REG is the destination
			else {
				instr->source.use_register = 1;
				instr->source.register_data = modregrm_data->rm;
				instr->source.register_size = data_size;
			}
			return 1;
		break;
	}
	// we should never reach this point!
	return -1;
}

// get segment register (32 bit addressing mode)
// if seg_override is non-zero, return the corresponding segment
// else return the default segment for the given register, rm
enum rm_register get_segment_register32(enum rm_register rm, int seg_override) {

	if (seg_override != 0) {
		if (seg_override == 0x26) return seg_es;
		if (seg_override == 0x2e) return seg_cs;
		if (seg_override == 0x36) return seg_ss;
		if (seg_override == 0x3e) return seg_ds;
		if (seg_override == 0x64) return seg_fs;
		if (seg_override == 0x65) return seg_gs;
	}

	// https://courses.engr.illinois.edu/ece390/books/artofasm/CH04/CH04-2.html
	// https://courses.engr.illinois.edu/ece390/books/artofasm/CH04/CH04-3.html
	// On the 80386 you may specify any general purpose 32 bit register when using the register indirect addressing mode.
	// [eax], [ebx], [ecx], [edx], [esi], and [edi] all provide offsets, by default, into the data segment. The
	// [ebp] and [esp] addressing modes use the stack segment by default.
	switch (rm) {
	case al_ax_eax:
	case cl_cx_ecx:
	case dl_dx_edx:
	case bl_bx_ebx:
	case dh_si_esi:
	case bh_di_edi:
		return seg_ds;
		break;
	case ah_sp_esp:
	case ch_bp_ebp:
		return seg_ss;
		break;
	case ind_bx_si:
	case ind_bx_di:
	case ind_si:
	case ind_di:
	case ind_bx:
		return seg_ds;
		break;
	case ind_bp_si:
	case ind_bp_di:
	case ind_bp:
		return seg_ss;
		break;
	// segment registers cannot have segment registers defined!
	case seg_es:
	case seg_cs:
	case seg_ss:
	case seg_ds:
	case seg_fs:
	case seg_gs:
	case rm_flags:
	case ip:
		return 0;
		break;
	}
	return 0;
};

// get segment register (16 bit addressing mode)
// if seg_override is non-zero, return the corresponding segment
// else return the default segment for the given register, rm
enum rm_register get_segment_register16(enum rm_register rm, int seg_override) {

	if (seg_override != 0) {
		if (seg_override == 0x26) return seg_es;
		if (seg_override == 0x2e) return seg_cs;
		if (seg_override == 0x36) return seg_ss;
		if (seg_override == 0x3e) return seg_ds;
		if (seg_override == 0x64) return seg_fs;
		if (seg_override == 0x65) return seg_gs;
	}

	// https://courses.engr.illinois.edu/ece390/books/artofasm/CH04/CH04-2.html
	// https://courses.engr.illinois.edu/ece390/books/artofasm/CH04/CH04-3.html
	// 16 bit addressing modes:
	// The [bx], [si], and [di] modes use the ds segment by default. The [bp] addressing mode uses the stack segment (ss) by default.
	switch (rm) {
	case al_ax_eax:
	case cl_cx_ecx:
	case dl_dx_edx:
	case bl_bx_ebx:
	case dh_si_esi:
	case bh_di_edi:
		return seg_ds;
		break;
	case ah_sp_esp:
	case ch_bp_ebp:
		return seg_ss;
		break;
	case ind_bx_si:
	case ind_bx_di:
	case ind_si:
	case ind_di:
	case ind_bx:
		return seg_ds;
		break;
	case ind_bp_si:
	case ind_bp_di:
	case ind_bp:
		return seg_ss;
		break;
	// segment registers cannot have segment registers defined!
	case seg_es:
	case seg_cs:
	case seg_ss:
	case seg_ds:
	case seg_fs:
	case seg_gs:
	case rm_flags:
	case ip:
		return 0;
		break;
	}
	return 0;
};

uint8_t decode_imm8(uint8_t *opcode) {

	return *((uint8_t *)opcode);

}

uint16_t decode_imm16(uint8_t *opcode) {

	return *((uint16_t *)opcode);

}

uint32_t decode_imm32(uint8_t *opcode) {

	return *((uint32_t *)opcode);

}

int decode_imm8_sign_extend(uint8_t *opcode) {

	return *((int8_t *)opcode);

}

int decode_imm16_sign_extend(uint8_t *opcode) {

	return *((int16_t *)opcode);

}

int decode_imm32_sign_extend(uint8_t *opcode) {

	return *((int32_t *)opcode);

}
