#include <stdio.h>

#include "cpu.h"
#include "dev_8259.h"

#define PIC0_COMMAND 0x20	// Master PIC - Command
#define PIC0_DATA 0x21		// Master PIC - Data
#define PIC1_COMMAND 0xA0	// Slave PIC - Command
#define PIC1_DATA 0xA1		// Slave PIC - Data

int read_pic_port(int port, enum datasize);
void write_pic_port(int port, int data, enum datasize);
void process_eoi(void);

// master PIC IMR, IRR, ISR and base interrupt vector
int interrupt_mask0[8];		// IMR - when a bit is set, the PIC will ignore the corresponding interrupt request
int interrupt_request0[8];	// IRR - bits indicate pending interrupts not yet sent to the CPU
int in_service0[8];			// ISR - indicates which interrupt is currently being serviced by the CPU
int vector_offset0;			// Base interrupt vector
// Initialization Control Word (ICW)
int ICW0;
int ICW_status0;
// OCW3 command
int read_from_isr0;		// reading the command port returns the ISR
int read_from_irr0;		// reading the command port returns the IRR

// slave PIC IMR, IRR, ISR and base interrupt vector
int interrupt_mask1[8];		// IMR - when a bit is set, the PIC will ignore the corresponding interrupt request
int interrupt_request1[8];	// IRR - bits indicate pending interrupts not yet sent to the CPU
int in_service1[8];			// ISR - indicates which interrupt is currently being serviced by the CPU
int vector_offset1;			// Base interrupt vector
// Initialization Control Word (ICW)
int ICW1;
int ICW_status1;
// OCW3 command
int read_from_isr1;		// reading the command port returns the ISR
int read_from_irr1;		// reading the command port returns the IRR

// IRQ priority
int irq_priority[] = {0, 1, 2, 8, 9, 10, 11, 12, 13, 14, 15, 3, 4, 5, 6, 7};

int init_pic() {

	// initialise IMR, IRR and ISR
	for (int i=0; i<8; i++) {
		interrupt_mask0[i] = 0;
		interrupt_mask0[i] = 0;
		in_service0[i] = 0;
		interrupt_mask1[i] = 0;
		interrupt_mask1[i] = 0;
		in_service1[i] = 0;
	}

	// initialise the base interrupt vector offset
	vector_offset0 = 0x08;	// master
	vector_offset1 = 0x70;	// slave

	// initialise ICW
	ICW0 = 0x11;
	ICW_status0 = 0;
	ICW1 = 0x11;
	ICW_status1 = 0;

	// initialise OCW3 command
	read_from_isr0 = 0;
	read_from_irr0 = 1;
	read_from_isr1 = 0;
	read_from_irr1 = 1;

	// register io handler
	register_io_handler(PIC0_COMMAND, 1, &read_pic_port, &write_pic_port);
	register_io_handler(PIC0_DATA, 1, &read_pic_port, &write_pic_port);
	register_io_handler(PIC1_COMMAND, 1, &read_pic_port, &write_pic_port);
	register_io_handler(PIC1_DATA, 1, &read_pic_port, &write_pic_port);

	return 0;
}

void shutdown_pic() {

	// register io handler
	unregister_io_handler(PIC0_COMMAND, 1);
	unregister_io_handler(PIC0_DATA, 1);
	unregister_io_handler(PIC1_COMMAND, 1);
	unregister_io_handler(PIC1_DATA, 1);

}

// returns 1 if there is an unmasked interrupt pending, 0 otherwise
// if there is an interrupt pending the cpu should call acknowledge_irq
// to get the interrupt vector, handle the interrupt and issue an EOI
// to the appropriate PIC on completion
int check_irq_request() {

	// deny request if an interrupt is already in service

	if (any_bits_set(in_service0) || any_bits_set(in_service1)) {
		return 0;
	}

	// check irq input lines for new requests

	for (int i=0; i<16; i++) {

		if (irq_request[i]) {
			// make sure this request is not already registered...
			// set the IRR flag
			int irq = i;
			if (i < 8) {
				interrupt_request0[irq] = 1;
			}
			else {
				irq = i - 8;
				interrupt_request1[irq] = 1;
			}
		}

	}

	// check IMR, ISR to determine whether the interrupt needs to be serviced
	// note the higher priority (lower IRQ) requests are serviced before the lower priority requests

	for (int i=0; i<16; i++) {

		int ix = irq_priority[i];

		// ignore slave IRQs if IRQ 2 is masked
		if ((interrupt_mask0[2]) && (ix > 7)) {
			continue;
		}

		int irq = ix;
		if (ix < 8) {

			if ((interrupt_request0[irq]) && (!interrupt_mask0[irq])) {
				// return true if irq isn't masked and isn't in service
				if (!in_service0[irq]) return 1;
			}

		}
		else {

			irq = ix - 8;
			if ((interrupt_request1[irq]) && (!interrupt_mask1[irq])) {
				// return true if irq isn't masked and isn't in service
				if (!in_service1[irq]) return 1;
			}

		}

	}

	return 0;

}

// return the highest priority IRQ that needs servicing and set the ISR flag
int acknowledge_irq() {

	// reply with spurious IRQ if an interrupt is already in service

	if (any_bits_set(in_service0) || any_bits_set(in_service1)) {
		return 7 + vector_offset0;
	}

	for (int i=0; i<16; i++) {

		int ix = irq_priority[i];

		// ignore slave IRQs if IRQ 2 is masked
		if ((interrupt_mask0[2]) && (ix > 7)) {
			continue;
		}

		int irq = ix;
		if (ix < 8) {

			if ((interrupt_request0[irq]) && (!interrupt_mask0[irq])) {
				// return interrupt vector if irq isn't masked and isn't in service
				// note: all interrupts are masked out by the Interrupt Mask Register (IMR). In other words, this disables all hardware interrupts until a request has been made to end the interrupt. this requires an End of Interrupt (EOI) command to be sent to the PIC.
				if (!in_service0[irq]) {
					interrupt_request0[irq] = 0;	// clear IRR flag
					in_service0[irq] = 1;			// set ISR flag
					return irq + vector_offset0;
				}
			}

		}
		else {

			irq = ix - 8;
			if ((interrupt_request1[irq]) && (!interrupt_mask1[irq])) {
				// return interrupt vector if irq isn't masked and isn't in service
				// note: all interrupts are masked out by the Interrupt Mask Register (IMR). In other words, this disables all hardware interrupts until a request has been made to end the interrupt. this requires an End of Interrupt (EOI) command to be sent to the PIC.
				if (!in_service1[irq]) {
					interrupt_request1[irq] = 0;	// clear IRR flag
					in_service1[irq] = 1;			// set ISR flag
					return irq + vector_offset1;
				}
			}

		}

	}

	// in case no interrupt request exists, return a "spurious" IRQ
	// (IRQ 7 for the master PIC, or IRQ 15 for the slave PIC)
	return 7 + vector_offset0;

}

int read_pic_port(int port, enum datasize datasize) {

	int val = 0;

	switch (port) {
	case PIC0_COMMAND:	// master pic command port
		if (read_from_isr0) {
			// return the value of the In-Service Register (ISR)
			for (int i=0; i<8; i++) {
				int irq = i;
				val |= (in_service0[irq] << irq);
			}
			// set bit 2 if any of the slave ISR bits are set
			if (any_bits_set(in_service1)) val |= 0x04;
		}
		else {
			// return the value of the Interrupt Request Register (IRR)
			for (int i=0; i<8; i++) {
				int irq = i;
				val |= (interrupt_request0[irq] << irq);
			}
			// set bit 2 if any of the slave IRR bits are set
			if (any_bits_set(interrupt_request1)) val |= 0x04;
		}
		break;
	case PIC0_DATA:		// master pic data port
		// return the value of the Interrupt Mask register (IMR)
		for (int i=0; i<8; i++) {
			int irq = i;
			val |= (interrupt_mask0[irq] << irq);
		}
		break;
	case PIC1_COMMAND:	// slave pic command port
		if (read_from_isr1) {
			// return the value of the In-Service Register (ISR)
			for (int i=8; i<16; i++) {
				int irq = i - 8;
				val |= (in_service1[irq] << irq);
			}
		}
		else {
			// return the value of the Interrupt Request Register (IRR)
			for (int i=8; i<16; i++) {
				int irq = i - 8;
				val |= (interrupt_request1[irq] << irq);
			}
		}
		break;
	case PIC1_DATA:		// slave pic data port
		// return the value of the Interrupt Mask register (IMR)
		for (int i=8; i<16; i++) {
			int irq = i - 8;
			val |= (interrupt_mask1[irq] << irq);
		}
		break;
	}

	return val;

}

void write_pic_port(int port, int data, enum datasize datasize) {

	switch (port) {
	case PIC0_COMMAND:	// master pic command port
		if ((data & 0x20) == 0x20) {
			process_eoi();
		}
		else if ((data & 0x0b) == 0x0b) {
			// read the ISR
			read_from_isr0 = 1;
			read_from_irr0 = 0;
		}
		else if ((data & 0x0a) == 0x0a) {
			// read the IRR
			read_from_isr0 = 0;
			read_from_irr0 = 1;
		}
		else if ((data & 0x10) == 0x10) {
			// initialise master PIC
			ICW0 = data;
			ICW_status0 = 2;
		}
		break;
	case PIC0_DATA:		// master pic data port
		if (ICW_status0 == 2) {
			// set master pic vector offset
			vector_offset0 = data & 0xf8;
			ICW_status0++;
		}
		// ignore ICW3, it should never change from irq 2
		else if (ICW_status0 == 3) ICW_status0++;
		// ignore ICW4, changing these options is not supported
		else if (ICW_status0 == 4) ICW_status0++;
		else {
			// set the Interrupt Mask register (IMR)
			for (int i=0; i<8; i++) {
				if ((data & 0x01) == 0x01) interrupt_mask0[i] = 1;
				else interrupt_mask0[i] = 0;
				data = data >> 1;
			}
		}
		break;
	case PIC1_COMMAND:	// slave pic command port
		if ((data & 0x20) == 0x20) {
			process_eoi();
		}
		else if ((data & 0x0b) == 0x0b) {
			// read the ISR
			read_from_isr1 = 1;
			read_from_irr1 = 0;
		}
		else if ((data & 0x0a) == 0x0a) {
			// read the IRR
			read_from_isr1 = 0;
			read_from_irr1 = 1;
		}
		else if ((data & 0x10) == 0x10) {
			// initialise slave PIC
			ICW1 = data;
			ICW_status1 = 2;
		}
		break;
	case PIC1_DATA:		// slave pic data port
		if (ICW_status1 == 2) {
			// set slave pic vector offset
			vector_offset1 = data & 0xf8;
			ICW_status1++;
		}
		// ignore ICW3, it should never change from irq 2
		else if (ICW_status1 == 3) ICW_status1++;
		// ignore ICW4, changing these options is not supported
		else if (ICW_status1 == 4) ICW_status1++;
		else {
			// set the Interrupt Mask register (IMR)
			for (int i=0; i<8; i++) {
				if ((data & 0x01) == 0x01) interrupt_mask1[i] = 1;
				else interrupt_mask1[i] = 0;
				data = data >> 1;
			}
		}
		break;
	}

}

void process_eoi() {

	// get interrupt currently in service

	int irq = 0;

	for (int i=irq; i<16; i++) {

		irq = i;

		if (i < 8) {
			if (in_service0[i]) break;
		}
		else {
			if (in_service1[i - 8]) break;
		}

	}

	// do nothing if no interrupt was in service
	// note for slave interrupts 8-15, the eoi command is sent
	// to both PIC controllers. This should handle the case.
	if (irq == 16) return;

	// clear the irq input line
	// any subsequent interrupt requests that arrive while the
	// current one is being processed will be ignored
	irq_request[irq] = 0;

	// clear ISR flag
	if (irq < 8) {
		in_service0[irq] = 0;
	}
	else {
		irq -= 8;
		in_service1[irq] = 0;
	}

}

int any_bits_set(int *mask) {

	for (int i=0; i<8; i++)
		if (mask[i]) return 1;

	return 0;
}
