#include <stdio.h>

#include "zlog.h"
#include "cpu.h"
#include "dev_16550.h"

#define COM1	0x3F8
#define COM2	0x2F8
#define COM3	0x3E8
#define COM4	0x2E8

int read_uart_port(int port, enum datasize);
void write_uart_port(int port, int data, enum datasize);
struct COM_PORT *getComPort(int base_address);
void clear_transmit_fifo(struct COM_PORT *com);
void clear_receive_fifo(struct COM_PORT *com);
void write_transmit_fifo(uint8_t data, struct COM_PORT *com);
uint8_t read_receive_fifo(struct COM_PORT *com);

// uart registers
struct COM_PORT {
	int base_address;
	uint8_t transmit_fifo[16];
	uint8_t receive_fifo[16];
	uint16_t divisor_latch;
	uint8_t interrupt_enable;
	uint8_t interrupt_identification;
	uint8_t fifo_control;
	uint8_t line_control;
	uint8_t modem_control;
	uint8_t line_status;
	uint8_t modem_status;
	uint8_t scratch;
} com_port[4];

int init_uart(void) {

	// initialise internal uart registers
	for (int i=0; i<4; i++) {

		clear_transmit_fifo(&com_port[i]);
		clear_receive_fifo(&com_port[i]);

		com_port[i].divisor_latch = 0x00c;
		com_port[i].interrupt_enable = 0;
		com_port[i].interrupt_identification = 1;
		com_port[i].fifo_control = 0;
		com_port[i].line_control = 0;
		com_port[i].modem_control = 0;
		com_port[i].line_status = 0;
		com_port[i].modem_status = 0;
		com_port[i].scratch = 0;

	}

	// initialise com port address
	com_port[0].base_address = COM1;
	com_port[1].base_address = COM2;
	com_port[2].base_address = COM3;
	com_port[3].base_address = COM4;

	// register io handler
	register_io_handler(COM1, 8, &read_uart_port, &write_uart_port);
	register_io_handler(COM2, 8, &read_uart_port, &write_uart_port);
	register_io_handler(COM3, 8, &read_uart_port, &write_uart_port);
	register_io_handler(COM4, 8, &read_uart_port, &write_uart_port);

	return 0;
}

void shutdown_uart(void) {

	// register io handler
	unregister_io_handler(COM1, 8);
	unregister_io_handler(COM2, 8);
	unregister_io_handler(COM3, 8);
	unregister_io_handler(COM4, 8);

}

int read_uart_port(int port, enum datasize datasize) {

	int val = 0;
	int base_address = port & 0xfff8;
	int offset = port & 0x07;

	struct COM_PORT *com = getComPort(base_address);
	if (com == NULL) {
		zlogf("[t=%I64d] UART PORT [%04x] address not valid\n", tick_count, port);
		return 0;
	}

	switch (offset) {
	case 0:		// Receiver Buffer
		if ((com->line_control & 0x80) == 0x80) {
			// return divisor latch low byte
			val = com->divisor_latch & 0x0ff;
		}
		else {
			val = read_receive_fifo(com);
		}
		break;
	case 1:		// Interrupt Enable Register
		if ((com->line_control & 0x80) == 0x80) {
			// return divisor latch high byte
			val = (com->divisor_latch & 0xff00) >> 8;
		}
		else {
			val = com->interrupt_enable;
		}
		break;
	case 2:		// Interrupt Identification Register
		val = com->interrupt_identification;
		break;
	case 3:		// Line Control Register
		val = com->line_control;
		break;
	case 4:		// Modem Control Register
		val = com->modem_control;
		break;
	case 5:		// Line Status Register
		val = com->line_status;
		break;
	case 6:		// Modem Status Register
		val = com->modem_status;
		break;
	case 7:		// Scratch Register
		val = com->scratch;
		break;
	}

	return val;

}

void write_uart_port(int port, int data, enum datasize datasize) {

	int base_address = port & 0xfff8;
	int offset = port & 0x07;

	struct COM_PORT *com = getComPort(base_address);
	if (com == NULL) {
		zlogf("[t=%I64d] UART PORT [%04x] address not valid\n", tick_count, port);
		return;
	}

	switch (offset) {
	case 0:		// Transmitter Holding Buffer
		if ((com->line_control & 0x80) == 0x80) {
			// assign divisor latch low byte
			com->divisor_latch &= 0xff00;
			com->divisor_latch |= data;
		}
		else {
			write_transmit_fifo(data, com);
		}
		break;
	case 1:		// Interrupt Enable Register
		if ((com->line_control & 0x80) == 0x80) {
			// assign divisor latch high byte
			com->divisor_latch &= 0x00ff;
			com->divisor_latch |= (data << 8);
		}
		else {
			// zero out reserved bits
			com->interrupt_enable = data & 0x0f;
		}
		break;
	case 2:		// FIFO Control Register
		// zero out reserved bits
		com->fifo_control = data & 0xc9;
		break;
	case 3:		// Line Control Register
		com->line_control = data;
		if ((data & 0x04) == 0x04) {
			clear_transmit_fifo(com);
		}
		if ((data & 0x02) == 0x02) {
			clear_receive_fifo(com);
		}
		break;
	case 4:		// Modem Control Register
		// zero out reserved bits
		com->modem_control = data & 0x1f;
		break;
	case 7:		// Scratch Register
		com->scratch = data;
		break;
	}

}

struct COM_PORT *getComPort(int base_address) {

	for (int i=0; i<4; i++) {
		if (com_port[i].base_address == base_address)
			return &com_port[i];
	}

	return NULL;

}

void clear_transmit_fifo(struct COM_PORT *com) {

	for (int i=0; i<16; i++) {
		com->transmit_fifo[i] = 0;
	}

}

void clear_receive_fifo(struct COM_PORT *com) {

	for (int i=0; i<16; i++) {
		com->receive_fifo[i] = 0;
	}

}

void write_transmit_fifo(uint8_t data, struct COM_PORT *com) {

	// todo: fix this fifo
	com->transmit_fifo[0] = data;

}

uint8_t read_receive_fifo(struct COM_PORT *com) {

	// todo: fix this fifo
	return com->receive_fifo[0];

}
